<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Custom Parameters
    |--------------------------------------------------------------------------
    |
    | Accessible via config('param.key');
    | eg: config('params._SUPER_ADMIN_ID_')
    | any other location as required by the application or its packages.
    */

    '_SUPER_ADMIN_ID_' => 1,
    'employee_status' => [
        1 => 'Permanent',
        2 => 'Project',
        
    ],
    'nature_of_appointment' => [
        1 => 'Original',
        2 => 'Promotion',
        3 => 'Re-employment',
        4 => 'Transfer'
    ],
    'publication' => [
        'agency' => 'Agency Web Site',
        'csc_bulletin' => 'CSC Bulletin of Vacant Position',
        'newspaper' => 'Newspaper',
        'others' => 'Others',
    ],
    'step' => [
        1 => '1',
        2 => '2',
        3 => '3',
        4 => '4',
        5 => '5',
        6 => '6',
        7 => '7',
        8 => '8'
    ],
    'salary_grade' => [
        1 => 'SG 1',
        2 => 'SG 2',
        3 => 'SG 3',
        4 => 'SG 4',
        5 => 'SG 5',
        6 => 'SG 6',
        7 => 'SG 7',
        8 => 'SG 8',
        9 => 'SG 9',
        10 => 'SG 10',
        11 => 'SG 11',
        12 => 'SG 12',
        13 => 'SG 13',
        14 => 'SG 14',
        15 => 'SG 15',
        16 => 'SG 16',
        17 => 'SG 17',
        18 => 'SG 18',
        19 => 'SG 19',
        20 => 'SG 20',
        21 => 'SG 21',
        22 => 'SG 22',
        23 => 'SG 23',
        24 => 'SG 24',
        25 => 'SG 25',
        26 => 'SG 26',
        27 => 'SG 27',
        28 => 'SG 28',
        29 => 'SG 29',
        30 => 'SG 30',
        31 => 'SG 31',
        32 => 'SG 32',
        33 => 'SG 33'
    ],
    'examination_status' => [
        1 => 'Schedule for Exam',
        2 => 'Reschedule for Exam',
        3 => 'Failed Examination',
        4 => 'No Show',
        5 => 'Withdrawn Application',
        6 => 'For Interview',
    ],
    'interview_status' => [
        1 => 'Schedule For Interview',
        2 => 'Reschedule For Interview',
        3 => 'Failed Interview',
        4 => 'No Show',
        5 => 'Withdrawn Application',
        6 => 'Approved for Requirements',
    ],
    'option_1' => [
        1 => 'Occassional',
        2 => 'Frequent'
    ],
    'boarding_status' => [
        1 => 'Hired',
        2 => 'Pending for Requirements'
    ],
    'status' => [
        1 => 'Approved',
        2 => 'Disapproved',
    ],
    'eligibility_type' => [
        1 => 'Career Service Professional',
        2 => 'Career Service Sub-professional',
        3 => 'RA 1080 (Bar Exams)',
        4 => 'RA 1080 (Board Exams)',
        5 => 'Certified Public Accountant Exams',
        6 => 'Career Executive Service Eligible',
        7 => 'Career Service Executive Eligible',
    ],
    'months' => [
        '01' => 'January',
        '02' => 'February',
        '03' => 'March',
        '04' => 'April',
        '05' => 'May',
        '06' => 'June',
        '07' => 'July',
        '08' => 'August',
        '09' => 'September',
        '10' => 'October',
        '11' => 'November',
        '12' => 'December',
    ],
    'nonplantilla' => [
        1 => 'Contract of Service',
        2 => 'Contractual',
    ],

    'applicant_status' => [
        1 => 'Qualified',
        2 => 'Not Qualified',
        3 => 'Reference',
        4 => 'Closed Position',
    ]


];
