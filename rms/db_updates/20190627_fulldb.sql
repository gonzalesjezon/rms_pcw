-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.1.36-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win32
-- HeidiSQL Version:             9.5.0.5196
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
USE `pcw_db`;

-- Dumping structure for table pcw_db.appointments
DROP TABLE IF EXISTS `appointments`;
CREATE TABLE IF NOT EXISTS `appointments` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) DEFAULT NULL,
  `form33_hrmo` int(11) DEFAULT NULL,
  `form34b_hrmo` int(11) DEFAULT NULL,
  `form212_hrmo` int(11) DEFAULT NULL,
  `eligibility_hrmo` int(11) DEFAULT NULL,
  `form1_hrmo` int(11) DEFAULT NULL,
  `form32_hrmo` int(11) DEFAULT NULL,
  `form4_hrmo` int(11) DEFAULT NULL,
  `form33_cscfo` varchar(225) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `form34b_cscfo` varchar(225) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `form212_cscfo` varchar(225) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `eligibility_cscfo` varchar(225) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `form1_cscfo` varchar(225) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `form32_cscfo` varchar(225) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `form4_cscfo` varchar(225) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table pcw_db.appointments: ~1 rows (approximately)
/*!40000 ALTER TABLE `appointments` DISABLE KEYS */;
INSERT INTO `appointments` (`id`, `applicant_id`, `form33_hrmo`, `form34b_hrmo`, `form212_hrmo`, `eligibility_hrmo`, `form1_hrmo`, `form32_hrmo`, `form4_hrmo`, `form33_cscfo`, `form34b_cscfo`, `form212_cscfo`, `eligibility_cscfo`, `form1_cscfo`, `form32_cscfo`, `form4_cscfo`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, '2019-06-26 09:25:16', '2019-06-26 09:25:16', NULL);
/*!40000 ALTER TABLE `appointments` ENABLE KEYS */;

-- Dumping structure for table pcw_db.appointment_casual
DROP TABLE IF EXISTS `appointment_casual`;
CREATE TABLE IF NOT EXISTS `appointment_casual` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) NOT NULL DEFAULT '0',
  `employee_status` int(11) NOT NULL DEFAULT '0',
  `nature_of_appointment` int(11) NOT NULL DEFAULT '0',
  `appointing_officer` varchar(225) DEFAULT NULL,
  `hrmo` varchar(225) DEFAULT NULL,
  `hrmo_date_sign` date DEFAULT NULL,
  `date_sign` date DEFAULT NULL,
  `period_emp_from` date DEFAULT NULL,
  `period_emp_to` date DEFAULT NULL,
  `daily_wage` varchar(225) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table pcw_db.appointment_casual: ~0 rows (approximately)
/*!40000 ALTER TABLE `appointment_casual` DISABLE KEYS */;
/*!40000 ALTER TABLE `appointment_casual` ENABLE KEYS */;

-- Dumping structure for table pcw_db.appointment_forms
DROP TABLE IF EXISTS `appointment_forms`;
CREATE TABLE IF NOT EXISTS `appointment_forms` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) NOT NULL DEFAULT '0',
  `employee_status` int(11) NOT NULL DEFAULT '0',
  `nature_of_appointment` int(11) NOT NULL DEFAULT '0',
  `appointing_officer` varchar(225) DEFAULT NULL,
  `hrmo` varchar(225) DEFAULT NULL,
  `chairperson` varchar(225) DEFAULT NULL,
  `date_sign` date DEFAULT NULL,
  `publication_date_from` date DEFAULT NULL,
  `publication_date_to` date DEFAULT NULL,
  `hrmo_date_sign` date DEFAULT NULL,
  `chairperson_date_sign` date DEFAULT NULL,
  `period_emp_from` date DEFAULT NULL,
  `period_emp_to` date DEFAULT NULL,
  `date_issued` date DEFAULT NULL,
  `assessment_date` date DEFAULT NULL,
  `vice` varchar(225) DEFAULT NULL,
  `who` varchar(225) DEFAULT NULL,
  `hrmo_assessment_date` date DEFAULT NULL,
  `chairperson_deliberation_date` date DEFAULT NULL,
  `form_status` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Dumping data for table pcw_db.appointment_forms: ~0 rows (approximately)
/*!40000 ALTER TABLE `appointment_forms` DISABLE KEYS */;
INSERT INTO `appointment_forms` (`id`, `applicant_id`, `employee_status`, `nature_of_appointment`, `appointing_officer`, `hrmo`, `chairperson`, `date_sign`, `publication_date_from`, `publication_date_to`, `hrmo_date_sign`, `chairperson_date_sign`, `period_emp_from`, `period_emp_to`, `date_issued`, `assessment_date`, `vice`, `who`, `hrmo_assessment_date`, `chairperson_deliberation_date`, `form_status`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(2, 1, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, '2019-06-26 09:40:24', '2019-06-26 09:40:24', NULL);
/*!40000 ALTER TABLE `appointment_forms` ENABLE KEYS */;

-- Dumping structure for table pcw_db.appointment_issued
DROP TABLE IF EXISTS `appointment_issued`;
CREATE TABLE IF NOT EXISTS `appointment_issued` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) NOT NULL DEFAULT '0',
  `employee_status` int(11) NOT NULL DEFAULT '0',
  `date_issued` date DEFAULT NULL,
  `period_of_employment_from` date DEFAULT NULL,
  `period_of_employment_to` date DEFAULT NULL,
  `nature_of_appointment` int(11) NOT NULL DEFAULT '0',
  `appointing_officer` varchar(225) DEFAULT NULL,
  `publication_date_from` date DEFAULT NULL,
  `publication_date_to` date DEFAULT NULL,
  `hrmo` varchar(225) DEFAULT NULL,
  `date_sign` date DEFAULT NULL,
  `chairperson` varchar(225) DEFAULT NULL,
  `chairperson_date_sign` date DEFAULT NULL,
  `hrmo_date_sign` date DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Dumping data for table pcw_db.appointment_issued: ~0 rows (approximately)
/*!40000 ALTER TABLE `appointment_issued` DISABLE KEYS */;
INSERT INTO `appointment_issued` (`id`, `applicant_id`, `employee_status`, `date_issued`, `period_of_employment_from`, `period_of_employment_to`, `nature_of_appointment`, `appointing_officer`, `publication_date_from`, `publication_date_to`, `hrmo`, `date_sign`, `chairperson`, `chairperson_date_sign`, `hrmo_date_sign`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 1, 0, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, '2019-06-26 09:44:05', '2019-06-26 09:44:05', NULL);
/*!40000 ALTER TABLE `appointment_issued` ENABLE KEYS */;

-- Dumping structure for table pcw_db.appointment_processing
DROP TABLE IF EXISTS `appointment_processing`;
CREATE TABLE IF NOT EXISTS `appointment_processing` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) NOT NULL,
  `educ_qualification` varchar(225) DEFAULT NULL,
  `educ_remarks` varchar(225) DEFAULT NULL,
  `educ_check` int(11) DEFAULT '0',
  `exp_qualification` varchar(225) DEFAULT NULL,
  `exp_remarks` varchar(225) DEFAULT NULL,
  `exp_check` int(11) DEFAULT '0',
  `training_qualification` varchar(225) DEFAULT NULL,
  `training_remarks` varchar(225) DEFAULT NULL,
  `training_check` int(11) DEFAULT '0',
  `eligibility_qualification` varchar(225) DEFAULT NULL,
  `eligibility_remarks` varchar(225) DEFAULT NULL,
  `eligibility_check` int(11) DEFAULT '0',
  `other_qualification` varchar(225) DEFAULT NULL,
  `other_remarks` varchar(225) DEFAULT NULL,
  `other_check` int(11) DEFAULT '0',
  `ra_form_33` varchar(225) DEFAULT NULL,
  `ra_employee_status` varchar(225) DEFAULT NULL,
  `ra_nature_appointment` varchar(225) DEFAULT NULL,
  `ra_appointing_authority` varchar(225) DEFAULT NULL,
  `ra_date_sign` date DEFAULT NULL,
  `ra_date_publication` date DEFAULT NULL,
  `ra_certification` varchar(225) DEFAULT NULL,
  `ra_pds` varchar(225) DEFAULT NULL,
  `ra_eligibility` varchar(225) DEFAULT NULL,
  `ra_position_description` varchar(225) DEFAULT NULL,
  `ar_01` varchar(225) DEFAULT NULL,
  `ar_02` varchar(225) DEFAULT NULL,
  `ar_03` varchar(225) DEFAULT NULL,
  `ar_04` varchar(225) DEFAULT NULL,
  `ar_05` varchar(225) DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Dumping data for table pcw_db.appointment_processing: ~0 rows (approximately)
/*!40000 ALTER TABLE `appointment_processing` DISABLE KEYS */;
INSERT INTO `appointment_processing` (`id`, `applicant_id`, `educ_qualification`, `educ_remarks`, `educ_check`, `exp_qualification`, `exp_remarks`, `exp_check`, `training_qualification`, `training_remarks`, `training_check`, `eligibility_qualification`, `eligibility_remarks`, `eligibility_check`, `other_qualification`, `other_remarks`, `other_check`, `ra_form_33`, `ra_employee_status`, `ra_nature_appointment`, `ra_appointing_authority`, `ra_date_sign`, `ra_date_publication`, `ra_certification`, `ra_pds`, `ra_eligibility`, `ra_position_description`, `ar_01`, `ar_02`, `ar_03`, `ar_04`, `ar_05`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 1, NULL, NULL, 1, NULL, NULL, 0, NULL, NULL, 0, NULL, NULL, 0, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, '2019-06-26 09:48:25', '2019-06-26 09:48:25', NULL);
/*!40000 ALTER TABLE `appointment_processing` ENABLE KEYS */;

-- Dumping structure for table pcw_db.appointment_requirements
DROP TABLE IF EXISTS `appointment_requirements`;
CREATE TABLE IF NOT EXISTS `appointment_requirements` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) DEFAULT NULL,
  `pds_path` varchar(225) DEFAULT NULL,
  `saln_path` varchar(225) DEFAULT NULL,
  `eligibility_path` varchar(225) DEFAULT NULL,
  `training_path` varchar(225) DEFAULT NULL,
  `psa_path` varchar(225) DEFAULT NULL,
  `tor_path` varchar(225) DEFAULT NULL,
  `diploma_path` varchar(225) DEFAULT NULL,
  `philhealth_path` varchar(225) DEFAULT NULL,
  `nbi_path` varchar(225) DEFAULT NULL,
  `medical_path` varchar(225) DEFAULT NULL,
  `bir_path` varchar(225) DEFAULT NULL,
  `coe_path` varchar(225) DEFAULT NULL,
  `tin_number` varchar(225) DEFAULT NULL,
  `pagibig_number` varchar(225) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Dumping data for table pcw_db.appointment_requirements: ~0 rows (approximately)
/*!40000 ALTER TABLE `appointment_requirements` DISABLE KEYS */;
INSERT INTO `appointment_requirements` (`id`, `applicant_id`, `pds_path`, `saln_path`, `eligibility_path`, `training_path`, `psa_path`, `tor_path`, `diploma_path`, `philhealth_path`, `nbi_path`, `medical_path`, `bir_path`, `coe_path`, `tin_number`, `pagibig_number`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2019-06-26 09:34:48', '2019-06-26 09:40:24', NULL);
/*!40000 ALTER TABLE `appointment_requirements` ENABLE KEYS */;

-- Dumping structure for table pcw_db.assumptions
DROP TABLE IF EXISTS `assumptions`;
CREATE TABLE IF NOT EXISTS `assumptions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) NOT NULL,
  `head_of_office` varchar(225) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `attested_by` varchar(225) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `assumption_date` date DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table pcw_db.assumptions: ~0 rows (approximately)
/*!40000 ALTER TABLE `assumptions` DISABLE KEYS */;
INSERT INTO `assumptions` (`id`, `applicant_id`, `head_of_office`, `attested_by`, `assumption_date`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 1, NULL, NULL, NULL, 1, 1, '2019-06-26 09:34:48', '2019-06-26 09:40:24', NULL);
/*!40000 ALTER TABLE `assumptions` ENABLE KEYS */;

-- Dumping structure for table pcw_db.attestations
DROP TABLE IF EXISTS `attestations`;
CREATE TABLE IF NOT EXISTS `attestations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) NOT NULL,
  `agency_receiving_offer` varchar(225) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `action_employee_status` int(11) DEFAULT NULL,
  `period_from` date DEFAULT NULL,
  `period_to` date DEFAULT NULL,
  `date_action` date DEFAULT NULL,
  `date_release` date DEFAULT NULL,
  `date_issuance` date DEFAULT NULL,
  `publication_from` date DEFAULT NULL,
  `publication_to` date DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table pcw_db.attestations: ~0 rows (approximately)
/*!40000 ALTER TABLE `attestations` DISABLE KEYS */;
/*!40000 ALTER TABLE `attestations` ENABLE KEYS */;

-- Dumping structure for table pcw_db.boarding_applicants
DROP TABLE IF EXISTS `boarding_applicants`;
CREATE TABLE IF NOT EXISTS `boarding_applicants` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `start_time` varchar(50) DEFAULT NULL,
  `board_status` int(11) DEFAULT '0',
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table pcw_db.boarding_applicants: ~0 rows (approximately)
/*!40000 ALTER TABLE `boarding_applicants` DISABLE KEYS */;
/*!40000 ALTER TABLE `boarding_applicants` ENABLE KEYS */;

-- Dumping structure for table pcw_db.countries
DROP TABLE IF EXISTS `countries`;
CREATE TABLE IF NOT EXISTS `countries` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=243 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table pcw_db.countries: ~242 rows (approximately)
/*!40000 ALTER TABLE `countries` DISABLE KEYS */;
INSERT INTO `countries` (`id`, `code`, `name`) VALUES
	(1, 'PH', 'Philippines'),
	(2, 'AF', 'Afghanistan'),
	(3, 'AL', 'Albania'),
	(4, 'DZ', 'Algeria'),
	(5, 'AS', 'American Samoa'),
	(6, 'AD', 'Andorra'),
	(7, 'AO', 'Angola'),
	(8, 'AI', 'Anguilla'),
	(9, 'AQ', 'Antarctica'),
	(10, 'AG', 'Antigua and/or Barbuda'),
	(11, 'AR', 'Argentina'),
	(12, 'AM', 'Armenia'),
	(13, 'AW', 'Aruba'),
	(14, 'AU', 'Australia'),
	(15, 'AT', 'Austria'),
	(16, 'AZ', 'Azerbaijan'),
	(17, 'BS', 'Bahamas'),
	(18, 'BH', 'Bahrain'),
	(19, 'BD', 'Bangladesh'),
	(20, 'BB', 'Barbados'),
	(21, 'BY', 'Belarus'),
	(22, 'BE', 'Belgium'),
	(23, 'BZ', 'Belize'),
	(24, 'BJ', 'Benin'),
	(25, 'BM', 'Bermuda'),
	(26, 'BT', 'Bhutan'),
	(27, 'BO', 'Bolivia'),
	(28, 'BA', 'Bosnia and Herzegovina'),
	(29, 'BW', 'Botswana'),
	(30, 'BV', 'Bouvet Island'),
	(31, 'BR', 'Brazil'),
	(32, 'IO', 'British lndian Ocean Territory'),
	(33, 'BN', 'Brunei Darussalam'),
	(34, 'BG', 'Bulgaria'),
	(35, 'BF', 'Burkina Faso'),
	(36, 'BI', 'Burundi'),
	(37, 'KH', 'Cambodia'),
	(38, 'CM', 'Cameroon'),
	(39, 'CA', 'Canada'),
	(40, 'CV', 'Cape Verde'),
	(41, 'KY', 'Cayman Islands'),
	(42, 'CF', 'Central African Republic'),
	(43, 'TD', 'Chad'),
	(44, 'CL', 'Chile'),
	(45, 'CN', 'China'),
	(46, 'CX', 'Christmas Island'),
	(47, 'CC', 'Cocos (Keeling) Islands'),
	(48, 'CO', 'Colombia'),
	(49, 'KM', 'Comoros'),
	(50, 'CG', 'Congo'),
	(51, 'CK', 'Cook Islands'),
	(52, 'CR', 'Costa Rica'),
	(53, 'HR', 'Croatia (Hrvatska)'),
	(54, 'CU', 'Cuba'),
	(55, 'CY', 'Cyprus'),
	(56, 'CZ', 'Czech Republic'),
	(57, 'CD', 'Democratic Republic of Congo'),
	(58, 'DK', 'Denmark'),
	(59, 'DJ', 'Djibouti'),
	(60, 'DM', 'Dominica'),
	(61, 'DO', 'Dominican Republic'),
	(62, 'TP', 'East Timor'),
	(63, 'EC', 'Ecudaor'),
	(64, 'EG', 'Egypt'),
	(65, 'SV', 'El Salvador'),
	(66, 'GQ', 'Equatorial Guinea'),
	(67, 'ER', 'Eritrea'),
	(68, 'EE', 'Estonia'),
	(69, 'ET', 'Ethiopia'),
	(70, 'FK', 'Falkland Islands (Malvinas)'),
	(71, 'FO', 'Faroe Islands'),
	(72, 'FJ', 'Fiji'),
	(73, 'FI', 'Finland'),
	(74, 'FR', 'France'),
	(75, 'FX', 'France, Metropolitan'),
	(76, 'GF', 'French Guiana'),
	(77, 'PF', 'French Polynesia'),
	(78, 'TF', 'French Southern Territories'),
	(79, 'GA', 'Gabon'),
	(80, 'GM', 'Gambia'),
	(81, 'GE', 'Georgia'),
	(82, 'DE', 'Germany'),
	(83, 'GH', 'Ghana'),
	(84, 'GI', 'Gibraltar'),
	(85, 'GR', 'Greece'),
	(86, 'GL', 'Greenland'),
	(87, 'GD', 'Grenada'),
	(88, 'GP', 'Guadeloupe'),
	(89, 'GU', 'Guam'),
	(90, 'GT', 'Guatemala'),
	(91, 'GN', 'Guinea'),
	(92, 'GW', 'Guinea-Bissau'),
	(93, 'GY', 'Guyana'),
	(94, 'HT', 'Haiti'),
	(95, 'HM', 'Heard and Mc Donald Islands'),
	(96, 'HN', 'Honduras'),
	(97, 'HK', 'Hong Kong'),
	(98, 'HU', 'Hungary'),
	(99, 'IS', 'Iceland'),
	(100, 'IN', 'India'),
	(101, 'ID', 'Indonesia'),
	(102, 'IR', 'Iran (Islamic Republic of)'),
	(103, 'IQ', 'Iraq'),
	(104, 'IE', 'Ireland'),
	(105, 'IL', 'Israel'),
	(106, 'IT', 'Italy'),
	(107, 'CI', 'Ivory Coast'),
	(108, 'JM', 'Jamaica'),
	(109, 'JP', 'Japan'),
	(110, 'JO', 'Jordan'),
	(111, 'KZ', 'Kazakhstan'),
	(112, 'KE', 'Kenya'),
	(113, 'KI', 'Kiribati'),
	(114, 'KP', 'Korea, Democratic People\'s Republic of'),
	(115, 'KR', 'Korea, Republic of'),
	(116, 'KW', 'Kuwait'),
	(117, 'KG', 'Kyrgyzstan'),
	(118, 'LA', 'Lao People\'s Democratic Republic'),
	(119, 'LV', 'Latvia'),
	(120, 'LB', 'Lebanon'),
	(121, 'LS', 'Lesotho'),
	(122, 'LR', 'Liberia'),
	(123, 'LY', 'Libyan Arab Jamahiriya'),
	(124, 'LI', 'Liechtenstein'),
	(125, 'LT', 'Lithuania'),
	(126, 'LU', 'Luxembourg'),
	(127, 'MO', 'Macau'),
	(128, 'MK', 'Macedonia'),
	(129, 'MG', 'Madagascar'),
	(130, 'MW', 'Malawi'),
	(131, 'MY', 'Malaysia'),
	(132, 'MV', 'Maldives'),
	(133, 'ML', 'Mali'),
	(134, 'MT', 'Malta'),
	(135, 'MH', 'Marshall Islands'),
	(136, 'MQ', 'Martinique'),
	(137, 'MR', 'Mauritania'),
	(138, 'MU', 'Mauritius'),
	(139, 'TY', 'Mayotte'),
	(140, 'MX', 'Mexico'),
	(141, 'FM', 'Micronesia, Federated States of'),
	(142, 'MD', 'Moldova, Republic of'),
	(143, 'MC', 'Monaco'),
	(144, 'MN', 'Mongolia'),
	(145, 'MS', 'Montserrat'),
	(146, 'MA', 'Morocco'),
	(147, 'MZ', 'Mozambique'),
	(148, 'MM', 'Myanmar'),
	(149, 'NA', 'Namibia'),
	(150, 'NR', 'Nauru'),
	(151, 'NP', 'Nepal'),
	(152, 'NL', 'Netherlands'),
	(153, 'AN', 'Netherlands Antilles'),
	(154, 'NC', 'New Caledonia'),
	(155, 'NZ', 'New Zealand'),
	(156, 'NI', 'Nicaragua'),
	(157, 'NE', 'Niger'),
	(158, 'NG', 'Nigeria'),
	(159, 'NU', 'Niue'),
	(160, 'NF', 'Norfork Island'),
	(161, 'MP', 'Northern Mariana Islands'),
	(162, 'NO', 'Norway'),
	(163, 'OM', 'Oman'),
	(164, 'PK', 'Pakistan'),
	(165, 'PW', 'Palau'),
	(166, 'PA', 'Panama'),
	(167, 'PG', 'Papua New Guinea'),
	(168, 'PY', 'Paraguay'),
	(169, 'PE', 'Peru'),
	(170, 'PN', 'Pitcairn'),
	(171, 'PL', 'Poland'),
	(172, 'PT', 'Portugal'),
	(173, 'PR', 'Puerto Rico'),
	(174, 'QA', 'Qatar'),
	(175, 'SS', 'Republic of South Sudan'),
	(176, 'RE', 'Reunion'),
	(177, 'RO', 'Romania'),
	(178, 'RU', 'Russian Federation'),
	(179, 'RW', 'Rwanda'),
	(180, 'KN', 'Saint Kitts and Nevis'),
	(181, 'LC', 'Saint Lucia'),
	(182, 'VC', 'Saint Vincent and the Grenadines'),
	(183, 'WS', 'Samoa'),
	(184, 'SM', 'San Marino'),
	(185, 'ST', 'Sao Tome and Principe'),
	(186, 'SA', 'Saudi Arabia'),
	(187, 'SN', 'Senegal'),
	(188, 'RS', 'Serbia'),
	(189, 'SC', 'Seychelles'),
	(190, 'SL', 'Sierra Leone'),
	(191, 'SG', 'Singapore'),
	(192, 'SK', 'Slovakia'),
	(193, 'SI', 'Slovenia'),
	(194, 'SB', 'Solomon Islands'),
	(195, 'SO', 'Somalia'),
	(196, 'ZA', 'South Africa'),
	(197, 'GS', 'South Georgia South Sandwich Islands'),
	(198, 'ES', 'Spain'),
	(199, 'LK', 'Sri Lanka'),
	(200, 'SH', 'St. Helena'),
	(201, 'PM', 'St. Pierre and Miquelon'),
	(202, 'SD', 'Sudan'),
	(203, 'SR', 'Suriname'),
	(204, 'SJ', 'Svalbarn and Jan Mayen Islands'),
	(205, 'SZ', 'Swaziland'),
	(206, 'SE', 'Sweden'),
	(207, 'CH', 'Switzerland'),
	(208, 'SY', 'Syrian Arab Republic'),
	(209, 'TW', 'Taiwan'),
	(210, 'TJ', 'Tajikistan'),
	(211, 'TZ', 'Tanzania, United Republic of'),
	(212, 'TH', 'Thailand'),
	(213, 'TG', 'Togo'),
	(214, 'TK', 'Tokelau'),
	(215, 'TO', 'Tonga'),
	(216, 'TT', 'Trinidad and Tobago'),
	(217, 'TN', 'Tunisia'),
	(218, 'TR', 'Turkey'),
	(219, 'TM', 'Turkmenistan'),
	(220, 'TC', 'Turks and Caicos Islands'),
	(221, 'TV', 'Tuvalu'),
	(222, 'US', 'United States'),
	(223, 'UG', 'Uganda'),
	(224, 'UA', 'Ukraine'),
	(225, 'AE', 'United Arab Emirates'),
	(226, 'GB', 'United Kingdom'),
	(227, 'UM', 'United States minor outlying islands'),
	(228, 'UY', 'Uruguay'),
	(229, 'UZ', 'Uzbekistan'),
	(230, 'VU', 'Vanuatu'),
	(231, 'VA', 'Vatican City State'),
	(232, 'VE', 'Venezuela'),
	(233, 'VN', 'Vietnam'),
	(234, 'VG', 'Virgin Islands (British)'),
	(235, 'VI', 'Virgin Islands (U.S.)'),
	(236, 'WF', 'Wallis and Futuna Islands'),
	(237, 'EH', 'Western Sahara'),
	(238, 'YE', 'Yemen'),
	(239, 'YU', 'Yugoslavia'),
	(240, 'ZR', 'Zaire'),
	(241, 'ZM', 'Zambia'),
	(242, 'ZW', 'Zimbabwe');
/*!40000 ALTER TABLE `countries` ENABLE KEYS */;

-- Dumping structure for table pcw_db.educations
DROP TABLE IF EXISTS `educations`;
CREATE TABLE IF NOT EXISTS `educations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) DEFAULT NULL,
  `school_name` varchar(225) DEFAULT NULL,
  `course` varchar(225) DEFAULT NULL,
  `attendance_from` varchar(225) DEFAULT NULL,
  `attendance_to` varchar(225) DEFAULT NULL,
  `level` varchar(225) DEFAULT NULL,
  `graduated` varchar(225) DEFAULT NULL,
  `awards` varchar(225) DEFAULT NULL,
  `educ_level` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Dumping data for table pcw_db.educations: ~3 rows (approximately)
/*!40000 ALTER TABLE `educations` DISABLE KEYS */;
INSERT INTO `educations` (`id`, `applicant_id`, `school_name`, `course`, `attendance_from`, `attendance_to`, `level`, `graduated`, `awards`, `educ_level`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 1, 'Primary 1', NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2019-06-26 09:23:58', '2019-06-26 09:23:58', NULL),
	(2, 1, 'Secondary 1', NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL, '2019-06-26 09:23:58', '2019-06-26 09:23:58', NULL),
	(3, 1, 'College 1', NULL, NULL, NULL, NULL, NULL, NULL, 4, NULL, NULL, '2019-06-26 09:23:58', '2019-06-26 09:23:58', NULL);
/*!40000 ALTER TABLE `educations` ENABLE KEYS */;

-- Dumping structure for table pcw_db.eligibilities
DROP TABLE IF EXISTS `eligibilities`;
CREATE TABLE IF NOT EXISTS `eligibilities` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) DEFAULT NULL,
  `eligibility_ref` varchar(225) DEFAULT NULL,
  `rating` varchar(225) DEFAULT NULL,
  `exam_place` varchar(225) DEFAULT NULL,
  `license_number` varchar(225) DEFAULT NULL,
  `license_validity` varchar(225) DEFAULT NULL,
  `exam_date` varchar(225) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Dumping data for table pcw_db.eligibilities: ~1 rows (approximately)
/*!40000 ALTER TABLE `eligibilities` DISABLE KEYS */;
INSERT INTO `eligibilities` (`id`, `applicant_id`, `eligibility_ref`, `rating`, `exam_place`, `license_number`, `license_validity`, `exam_date`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
	(1, 1, '2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-06-26 09:23:58', '2019-06-26 09:23:58');
/*!40000 ALTER TABLE `eligibilities` ENABLE KEYS */;

-- Dumping structure for table pcw_db.erasure_alterations
DROP TABLE IF EXISTS `erasure_alterations`;
CREATE TABLE IF NOT EXISTS `erasure_alterations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) DEFAULT NULL,
  `from_date` date DEFAULT NULL,
  `to_date` date DEFAULT NULL,
  `particulars` text,
  `appointing_officer` text,
  `sign_date` date DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table pcw_db.erasure_alterations: ~0 rows (approximately)
/*!40000 ALTER TABLE `erasure_alterations` DISABLE KEYS */;
/*!40000 ALTER TABLE `erasure_alterations` ENABLE KEYS */;

-- Dumping structure for table pcw_db.evaluations
DROP TABLE IF EXISTS `evaluations`;
CREATE TABLE IF NOT EXISTS `evaluations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) NOT NULL,
  `job_id` int(10) unsigned NOT NULL,
  `performance` int(10) unsigned DEFAULT NULL,
  `performance_divide` int(10) unsigned DEFAULT NULL,
  `performance_average` int(10) unsigned DEFAULT NULL,
  `performance_percent` int(10) unsigned DEFAULT NULL,
  `performance_score` int(10) unsigned DEFAULT NULL,
  `eligibility` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `training` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seminar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `minimum_education_points` int(10) unsigned DEFAULT NULL,
  `minimum_training_points` int(10) unsigned DEFAULT NULL,
  `education_points` int(10) unsigned DEFAULT NULL,
  `training_points` int(10) unsigned DEFAULT NULL,
  `education_training_total_points` int(10) unsigned DEFAULT NULL,
  `education_training_percent` int(10) unsigned DEFAULT NULL,
  `education_training_score` int(10) unsigned DEFAULT NULL,
  `relevant_positions_held` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `minimum_experience_requirement` int(10) unsigned DEFAULT NULL,
  `additional_points` int(10) unsigned DEFAULT NULL,
  `experience_accomplishments_total_points` int(10) unsigned DEFAULT NULL,
  `experience_accomplishments_percent` int(10) unsigned DEFAULT NULL,
  `experience_accomplishments_score` int(10) unsigned DEFAULT NULL,
  `potential` int(10) unsigned DEFAULT NULL,
  `potential_average_rating` decimal(5,2) DEFAULT NULL,
  `potential_percentage_rating` int(10) unsigned DEFAULT NULL,
  `potential_percent` int(10) unsigned DEFAULT NULL,
  `potential_score` int(10) unsigned DEFAULT NULL,
  `psychosocial` int(10) unsigned DEFAULT NULL,
  `psychosocial_average_rating` int(10) unsigned DEFAULT NULL,
  `psychosocial_percentage_rating` int(10) unsigned DEFAULT NULL,
  `psychosocial_percent` int(10) unsigned DEFAULT NULL,
  `psychosocial_score` int(10) unsigned DEFAULT NULL,
  `total_percent` int(10) unsigned DEFAULT NULL,
  `total_score` decimal(5,2) DEFAULT NULL,
  `evaluated_by` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reviewed_by` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `noted_by` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `recommended` int(10) unsigned DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table pcw_db.evaluations: ~0 rows (approximately)
/*!40000 ALTER TABLE `evaluations` DISABLE KEYS */;
/*!40000 ALTER TABLE `evaluations` ENABLE KEYS */;

-- Dumping structure for table pcw_db.examinations
DROP TABLE IF EXISTS `examinations`;
CREATE TABLE IF NOT EXISTS `examinations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) DEFAULT NULL,
  `exam_date` date DEFAULT NULL,
  `exam_time` varchar(50) DEFAULT NULL,
  `exam_location` varchar(50) DEFAULT NULL,
  `resched_exam_date` date DEFAULT NULL,
  `resched_exam_time` varchar(50) DEFAULT NULL,
  `exam_status` int(11) DEFAULT NULL,
  `notify` int(11) DEFAULT NULL,
  `notify_resched_exam` int(11) DEFAULT '0',
  `notify_exam_status` int(11) DEFAULT '0',
  `confirmed` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Dumping data for table pcw_db.examinations: ~1 rows (approximately)
/*!40000 ALTER TABLE `examinations` DISABLE KEYS */;
INSERT INTO `examinations` (`id`, `applicant_id`, `exam_date`, `exam_time`, `exam_location`, `resched_exam_date`, `resched_exam_time`, `exam_status`, `notify`, `notify_resched_exam`, `notify_exam_status`, `confirmed`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 1, NULL, '1018pm', 'Manila', NULL, NULL, 7, 0, 0, 0, 0, 1, NULL, '2019-06-26 09:24:49', '2019-06-26 09:24:49', NULL);
/*!40000 ALTER TABLE `examinations` ENABLE KEYS */;

-- Dumping structure for table pcw_db.interviews
DROP TABLE IF EXISTS `interviews`;
CREATE TABLE IF NOT EXISTS `interviews` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) DEFAULT NULL,
  `interview_date` date DEFAULT NULL,
  `interview_time` varchar(50) DEFAULT NULL,
  `interview_location` varchar(50) DEFAULT NULL,
  `resched_interview_date` date DEFAULT NULL,
  `resched_interview_time` varchar(50) DEFAULT NULL,
  `interview_status` int(11) DEFAULT NULL,
  `notify` int(11) DEFAULT NULL,
  `noftiy_resched_interview` int(11) DEFAULT NULL,
  `confirmed` int(11) DEFAULT NULL,
  `psb_chairperson` varchar(225) DEFAULT NULL,
  `psb_secretariat` varchar(225) DEFAULT NULL,
  `psb_member` varchar(225) DEFAULT NULL,
  `psm_sweap_rep` varchar(225) DEFAULT NULL,
  `psb_end_user` varchar(225) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Dumping data for table pcw_db.interviews: ~1 rows (approximately)
/*!40000 ALTER TABLE `interviews` DISABLE KEYS */;
INSERT INTO `interviews` (`id`, `applicant_id`, `interview_date`, `interview_time`, `interview_location`, `resched_interview_date`, `resched_interview_time`, `interview_status`, `notify`, `noftiy_resched_interview`, `confirmed`, `psb_chairperson`, `psb_secretariat`, `psb_member`, `psm_sweap_rep`, `psb_end_user`, `created_by`, `updated_by`, `created_at`, `deleted_at`, `updated_at`) VALUES
	(1, 1, NULL, '10:80 AM', 'Manila', NULL, NULL, 3, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, '2019-06-26 09:25:16', NULL, '2019-06-26 09:25:16');
/*!40000 ALTER TABLE `interviews` ENABLE KEYS */;

-- Dumping structure for table pcw_db.jobs
DROP TABLE IF EXISTS `jobs`;
CREATE TABLE IF NOT EXISTS `jobs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `plantilla_item_id` int(10) unsigned DEFAULT NULL,
  `employee_status_id` int(10) unsigned DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `education` text COLLATE utf8mb4_unicode_ci,
  `experience` text COLLATE utf8mb4_unicode_ci,
  `training` text COLLATE utf8mb4_unicode_ci,
  `eligibility` text COLLATE utf8mb4_unicode_ci,
  `duties_responsibilities` text COLLATE utf8mb4_unicode_ci,
  `key_competencies` text COLLATE utf8mb4_unicode_ci,
  `monthly_basic_salary` decimal(10,3) DEFAULT '0.000',
  `daily_salary` decimal(12,3) DEFAULT '0.000',
  `pera_amount` decimal(12,3) DEFAULT '0.000',
  `clothing_amount` decimal(12,3) DEFAULT '0.000',
  `midyear_amount` decimal(12,3) DEFAULT '0.000',
  `yearend_amount` decimal(12,3) DEFAULT '0.000',
  `cashgift_amount` decimal(12,3) DEFAULT '0.000',
  `status` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `station` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reporting_line` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `requirements` text COLLATE utf8mb4_unicode_ci,
  `compentency_1` text COLLATE utf8mb4_unicode_ci,
  `compentency_2` text COLLATE utf8mb4_unicode_ci,
  `compentency_3` text COLLATE utf8mb4_unicode_ci,
  `compentency_4` text COLLATE utf8mb4_unicode_ci,
  `compentency_5` text COLLATE utf8mb4_unicode_ci,
  `expires` timestamp NULL DEFAULT NULL,
  `deadline_date` date DEFAULT NULL,
  `publish` tinyint(1) NOT NULL DEFAULT '0',
  `publication_1` int(11) NOT NULL DEFAULT '0',
  `publication_2` int(11) NOT NULL DEFAULT '0',
  `publication_3` int(11) NOT NULL DEFAULT '0',
  `publication_4` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table pcw_db.jobs: ~1 rows (approximately)
/*!40000 ALTER TABLE `jobs` DISABLE KEYS */;
INSERT INTO `jobs` (`id`, `plantilla_item_id`, `employee_status_id`, `description`, `education`, `experience`, `training`, `eligibility`, `duties_responsibilities`, `key_competencies`, `monthly_basic_salary`, `daily_salary`, `pera_amount`, `clothing_amount`, `midyear_amount`, `yearend_amount`, `cashgift_amount`, `status`, `station`, `reporting_line`, `requirements`, `compentency_1`, `compentency_2`, `compentency_3`, `compentency_4`, `compentency_5`, `expires`, `deadline_date`, `publish`, `publication_1`, `publication_2`, `publication_3`, `publication_4`, `created_at`, `updated_at`, `created_by`, `updated_by`, `deleted_at`) VALUES
	(1, 76, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 28759.000, 0.000, 2000.000, 6000.000, 345108.000, 345108.000, 5000.000, 'plantilla', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-07-05', 1, 1, 0, 1, 0, '2019-06-26 09:22:09', '2019-06-26 09:22:09', 1, NULL, NULL);
/*!40000 ALTER TABLE `jobs` ENABLE KEYS */;

-- Dumping structure for table pcw_db.job_offers
DROP TABLE IF EXISTS `job_offers`;
CREATE TABLE IF NOT EXISTS `job_offers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) NOT NULL,
  `pera_amount` decimal(13,2) DEFAULT NULL,
  `clothing_allowance` decimal(13,2) DEFAULT NULL,
  `year_end_bonus` decimal(13,2) DEFAULT NULL,
  `cash_gift` decimal(13,2) DEFAULT NULL,
  `executive_director` varchar(225) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `job_offer_date` date DEFAULT NULL,
  `joboffer_status` int(11) NOT NULL DEFAULT '0',
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table pcw_db.job_offers: ~0 rows (approximately)
/*!40000 ALTER TABLE `job_offers` DISABLE KEYS */;
/*!40000 ALTER TABLE `job_offers` ENABLE KEYS */;

-- Dumping structure for table pcw_db.matrix_qualifications
DROP TABLE IF EXISTS `matrix_qualifications`;
CREATE TABLE IF NOT EXISTS `matrix_qualifications` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) DEFAULT NULL,
  `remarks` varchar(50) DEFAULT NULL,
  `status` int(11) DEFAULT '0',
  `isc_chairperson` varchar(50) DEFAULT NULL,
  `isc_member_one` varchar(50) DEFAULT NULL,
  `isc_member_two` varchar(50) DEFAULT NULL,
  `ea_representative` varchar(50) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table pcw_db.matrix_qualifications: ~0 rows (approximately)
/*!40000 ALTER TABLE `matrix_qualifications` DISABLE KEYS */;
/*!40000 ALTER TABLE `matrix_qualifications` ENABLE KEYS */;

-- Dumping structure for table pcw_db.oath_offices
DROP TABLE IF EXISTS `oath_offices`;
CREATE TABLE IF NOT EXISTS `oath_offices` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) NOT NULL,
  `person_administering` varchar(225) DEFAULT NULL,
  `oath_date` date DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Dumping data for table pcw_db.oath_offices: ~0 rows (approximately)
/*!40000 ALTER TABLE `oath_offices` DISABLE KEYS */;
INSERT INTO `oath_offices` (`id`, `applicant_id`, `person_administering`, `oath_date`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 1, NULL, NULL, 1, 1, '2019-06-26 09:34:48', '2019-06-26 09:40:24', NULL);
/*!40000 ALTER TABLE `oath_offices` ENABLE KEYS */;

-- Dumping structure for table pcw_db.password_resets
DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table pcw_db.password_resets: ~0 rows (approximately)
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;

-- Dumping structure for table pcw_db.position_descriptions
DROP TABLE IF EXISTS `position_descriptions`;
CREATE TABLE IF NOT EXISTS `position_descriptions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) DEFAULT NULL,
  `immediate_supervisor` varchar(225) DEFAULT NULL,
  `higher_supervisor` varchar(225) DEFAULT NULL,
  `item_number` varchar(225) DEFAULT NULL,
  `position_title` varchar(225) DEFAULT NULL,
  `used_tools` text,
  `managerial` int(11) DEFAULT NULL,
  `supervisor` int(11) DEFAULT NULL,
  `non_supervisor` int(11) DEFAULT NULL,
  `staff` int(11) DEFAULT NULL,
  `general_public` int(11) DEFAULT NULL,
  `other_agency` varchar(50) DEFAULT NULL,
  `other_contacts` varchar(50) DEFAULT NULL,
  `office_work` int(11) DEFAULT NULL,
  `field_work` int(11) DEFAULT NULL,
  `other_condition` varchar(50) DEFAULT NULL,
  `description_function_unit` text,
  `description_function_position` text,
  `compentency_1` text,
  `compentency_2` text,
  `percentage_work_time` varchar(50) DEFAULT NULL,
  `responsibilities` varchar(50) DEFAULT NULL,
  `supervisor_name` varchar(50) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table pcw_db.position_descriptions: ~0 rows (approximately)
/*!40000 ALTER TABLE `position_descriptions` DISABLE KEYS */;
/*!40000 ALTER TABLE `position_descriptions` ENABLE KEYS */;

-- Dumping structure for table pcw_db.preliminary_evaluation
DROP TABLE IF EXISTS `preliminary_evaluation`;
CREATE TABLE IF NOT EXISTS `preliminary_evaluation` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) NOT NULL,
  `age` tinyint(3) unsigned NOT NULL,
  `education` text COLLATE utf8mb4_unicode_ci,
  `experience` text COLLATE utf8mb4_unicode_ci,
  `eligibility` text COLLATE utf8mb4_unicode_ci,
  `training` text COLLATE utf8mb4_unicode_ci,
  `remarks` text COLLATE utf8mb4_unicode_ci,
  `isc_chairperson` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `isc_member_one` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `isc_member_two` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ea_representative` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` int(10) unsigned DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table pcw_db.preliminary_evaluation: ~0 rows (approximately)
/*!40000 ALTER TABLE `preliminary_evaluation` DISABLE KEYS */;
/*!40000 ALTER TABLE `preliminary_evaluation` ENABLE KEYS */;

-- Dumping structure for table pcw_db.recommendations
DROP TABLE IF EXISTS `recommendations`;
CREATE TABLE IF NOT EXISTS `recommendations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) NOT NULL,
  `sign_one` varchar(225) DEFAULT NULL,
  `sign_two` varchar(225) DEFAULT NULL,
  `sign_three` varchar(225) DEFAULT NULL,
  `sign_four` varchar(225) DEFAULT NULL,
  `sign_five` varchar(225) DEFAULT NULL,
  `prepared_by` varchar(225) DEFAULT NULL,
  `recommend_status` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table pcw_db.recommendations: ~0 rows (approximately)
/*!40000 ALTER TABLE `recommendations` DISABLE KEYS */;
/*!40000 ALTER TABLE `recommendations` ENABLE KEYS */;

-- Dumping structure for table pcw_db.resignation_acceptance
DROP TABLE IF EXISTS `resignation_acceptance`;
CREATE TABLE IF NOT EXISTS `resignation_acceptance` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) DEFAULT NULL,
  `letter_date` date DEFAULT NULL,
  `resignation_date` date DEFAULT NULL,
  `appointing_officer` text,
  `sign_date` date DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table pcw_db.resignation_acceptance: ~0 rows (approximately)
/*!40000 ALTER TABLE `resignation_acceptance` DISABLE KEYS */;
/*!40000 ALTER TABLE `resignation_acceptance` ENABLE KEYS */;

-- Dumping structure for table pcw_db.rms_modules
DROP TABLE IF EXISTS `rms_modules`;
CREATE TABLE IF NOT EXISTS `rms_modules` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(225) DEFAULT NULL,
  `description` varchar(225) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;

-- Dumping data for table pcw_db.rms_modules: ~0 rows (approximately)
/*!40000 ALTER TABLE `rms_modules` DISABLE KEYS */;
INSERT INTO `rms_modules` (`id`, `name`, `description`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
  (1, 'psipop', 'Itemize & Plantilla', NULL, NULL, NULL, NULL, NULL),
  (2, 'jobs', 'Jobs', NULL, NULL, NULL, NULL, NULL),
  (3, 'applicant', 'Applicants', NULL, NULL, NULL, NULL, NULL),
  (4, 'examinations', 'Examinations', NULL, NULL, NULL, NULL, NULL),
  (5, 'interviews', 'Interviews', NULL, NULL, NULL, NULL, NULL),
  (6, 'appointment-form', 'Appointmnet Form', NULL, NULL, NULL, NULL, NULL),
  (7, 'appointment-issued', 'Appointment Issued', NULL, NULL, NULL, NULL, NULL),
  (8, 'appointment-processing', 'Appointment Processing', NULL, NULL, NULL, NULL, NULL),
  (9, 'appointment-requirements', 'Pre Emp. Requirements', NULL, NULL, NULL, NULL, NULL),
  (10, 'appointment-casual', 'Plantilla of Casual Appointment', NULL, NULL, NULL, NULL, NULL),
  (11, 'position-descriptions', 'Position Description', NULL, NULL, NULL, NULL, NULL),
  (12, 'oath-office', 'Oath of Office', NULL, NULL, NULL, NULL, NULL),
  (13, 'assumption', 'Assumption', NULL, NULL, NULL, NULL, NULL),
  (14, 'erasure_alterations', 'Erasure Alterations', NULL, NULL, NULL, NULL, NULL),
  (15, 'acceptance_resignation', 'Acceptance Resignation', NULL, NULL, NULL, NULL, NULL),
  (16, 'boarding_applicant', 'Applicant Onboarding', NULL, NULL, NULL, NULL, NULL),
  (17, 'report', 'Report', NULL, NULL, NULL, NULL, NULL),
  (18, 'access_modules', 'Access Module', NULL, NULL, NULL, NULL, NULL),
  (19, 'users', 'User', NULL, NULL, NULL, NULL, NULL);
/*!40000 ALTER TABLE `rms_modules` ENABLE KEYS */;

-- Dumping structure for table pcw_db.selection_lineup
DROP TABLE IF EXISTS `selection_lineup`;
CREATE TABLE IF NOT EXISTS `selection_lineup` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table pcw_db.selection_lineup: ~0 rows (approximately)
/*!40000 ALTER TABLE `selection_lineup` DISABLE KEYS */;
/*!40000 ALTER TABLE `selection_lineup` ENABLE KEYS */;

-- Dumping structure for table pcw_db.trainings
DROP TABLE IF EXISTS `trainings`;
CREATE TABLE IF NOT EXISTS `trainings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) DEFAULT NULL,
  `title_learning_programs` varchar(225) DEFAULT NULL,
  `inclusive_date_from` varchar(225) DEFAULT NULL,
  `inclusive_date_to` varchar(225) DEFAULT NULL,
  `number_hours` varchar(225) DEFAULT NULL,
  `ld_type` varchar(225) DEFAULT NULL,
  `sponsored_by` varchar(225) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Dumping data for table pcw_db.trainings: ~1 rows (approximately)
/*!40000 ALTER TABLE `trainings` DISABLE KEYS */;
INSERT INTO `trainings` (`id`, `applicant_id`, `title_learning_programs`, `inclusive_date_from`, `inclusive_date_to`, `number_hours`, `ld_type`, `sponsored_by`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 1, 'Position 1', NULL, NULL, '48', NULL, NULL, NULL, NULL, '2019-06-26 09:23:59', '2019-06-26 09:23:59', NULL);
/*!40000 ALTER TABLE `trainings` ENABLE KEYS */;

-- Dumping structure for table pcw_db.workexperiences
DROP TABLE IF EXISTS `workexperiences`;
CREATE TABLE IF NOT EXISTS `workexperiences` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) DEFAULT NULL,
  `inclusive_date_from` varchar(50) DEFAULT NULL,
  `inclusive_date_to` varchar(50) DEFAULT NULL,
  `position_title` varchar(225) DEFAULT NULL,
  `department` varchar(225) DEFAULT NULL,
  `monthly_salary` varchar(225) DEFAULT NULL,
  `salary_grade` varchar(225) DEFAULT NULL,
  `status_of_appointment` varchar(225) DEFAULT NULL,
  `govt_service` varchar(225) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


-- Dumping structure for table pcw_db.positionitem
DROP TABLE IF EXISTS `positionitem`;
CREATE TABLE IF NOT EXISTS `positionitem` (
  `RefId` bigint(50) NOT NULL AUTO_INCREMENT,
  `Code` varchar(50) DEFAULT NULL,
  `Name` varchar(300) DEFAULT NULL,
  `PositionRefId` bigint(50) DEFAULT NULL,
  `PositionLevelRefId` bigint(50) DEFAULT NULL,
  `PositionClassificationRefId` bigint(50) DEFAULT NULL,
  `OfficeRefId` bigint(50) DEFAULT NULL,
  `SalaryGradeRefId` int(10) DEFAULT NULL,
  `JobGradeRefId` int(10) DEFAULT NULL,
  `DivisionRefId` int(10) DEFAULT NULL,
  `has_occupied` int(10) DEFAULT '0',
  `applicant_id` int(10) DEFAULT NULL,
  `StepIncrementRefId` int(10) DEFAULT NULL,
  `SalaryAmount` decimal(15,2) DEFAULT NULL,
  `Remarks` varchar(300) DEFAULT NULL,
  `EducRequirements` text,
  `WorkExpRequirements` text,
  `TrainingRequirements` text,
  `EligibilityRequirements` text,
  `LastUpdateDate` date DEFAULT NULL,
  `LastUpdateTime` time DEFAULT NULL,
  `LastUpdateBy` varchar(50) DEFAULT NULL,
  `Data` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`RefId`),
  UNIQUE KEY `RefId` (`RefId`)
) ENGINE=InnoDB AUTO_INCREMENT=80 DEFAULT CHARSET=latin1;

-- Dumping data for table pcw_db.positionitem: ~77 rows (approximately)
/*!40000 ALTER TABLE `positionitem` DISABLE KEYS */;
INSERT INTO `positionitem` (`RefId`, `Code`, `Name`, `PositionRefId`, `PositionLevelRefId`, `PositionClassificationRefId`, `OfficeRefId`, `SalaryGradeRefId`, `JobGradeRefId`, `DivisionRefId`, `has_occupied`, `applicant_id`, `StepIncrementRefId`, `SalaryAmount`, `Remarks`, `EducRequirements`, `WorkExpRequirements`, `TrainingRequirements`, `EligibilityRequirements`, `LastUpdateDate`, `LastUpdateTime`, `LastUpdateBy`, `Data`) VALUES
  (1, NULL, 'NCRFWB-EXED3-1-1998', 1, NULL, NULL, NULL, 27, NULL, 1, 1, 1, 5, 122031.00, NULL, NULL, NULL, NULL, NULL, '2018-11-16', '03:29:39', 'Admin', 'E'),
  (2, NULL, 'NCRFWB-DED3-1-1998', 2, NULL, NULL, NULL, 26, NULL, 1, 1, 2, 3, 106091.00, NULL, NULL, NULL, NULL, NULL, '2018-11-16', '03:29:39', 'Admin', 'E'),
  (3, NULL, 'NCRFWB-DED3-1-2004', 2, NULL, NULL, NULL, 26, NULL, 1, 1, 3, 1, 102910.00, NULL, NULL, NULL, NULL, NULL, '2018-11-16', '03:29:39', 'Admin', 'E'),
  (4, NULL, 'NCRFWB-CADOF-9-2004', 3, NULL, NULL, NULL, 23, NULL, 2, 1, 4, 1, 73299.00, NULL, NULL, NULL, NULL, NULL, '2018-11-16', '03:29:40', 'Admin', 'E'),
  (5, NULL, 'NCRFWB-ATY3-1-2016', 4, NULL, NULL, NULL, 20, NULL, 1, 1, 5, 1, 52554.00, NULL, NULL, NULL, NULL, NULL, '2018-11-16', '03:29:40', 'Admin', 'E'),
  (6, NULL, 'NCRFWB-ATY3-2-2016', 4, NULL, NULL, NULL, 20, NULL, 1, 1, 6, 1, 52554.00, NULL, NULL, NULL, NULL, NULL, '2018-11-16', '03:29:41', 'Admin', 'E'),
  (7, NULL, 'NCRFWB-A3-1-1998', 5, NULL, NULL, NULL, 18, NULL, 2, 1, 7, 1, 42099.00, NULL, NULL, NULL, NULL, NULL, '2018-11-16', '03:29:41', 'Admin', 'E'),
  (8, NULL, 'NCRFWB-ADOF5-10-2004', 6, NULL, NULL, NULL, 17, NULL, 2, 1, 8, 8, 41413.00, NULL, NULL, NULL, NULL, NULL, '2018-11-16', '03:29:42', 'Admin', 'E'),
  (9, NULL, 'NCRFWB-ADOF5-11-2004', 6, NULL, NULL, NULL, 17, NULL, 2, 1, 9, 2, 38543.00, NULL, NULL, NULL, NULL, NULL, '2018-11-16', '03:29:42', 'Admin', 'E'),
  (10, NULL, 'NCRFWB-ADOF5-12-2004', 6, NULL, NULL, NULL, 17, NULL, 2, 1, 10, 2, 38543.00, NULL, NULL, NULL, NULL, NULL, '2018-11-16', '03:29:43', 'Admin', 'E'),
  (11, NULL, 'NCRFWB-A2-1-1998', 7, NULL, NULL, NULL, 15, NULL, 2, 1, 11, 1, 31765.00, NULL, NULL, NULL, NULL, NULL, '2018-11-16', '03:29:43', 'Admin', 'E'),
  (12, NULL, 'NCRFWB-ADOF3-14-2004', 8, NULL, NULL, NULL, 13, NULL, 2, 1, 12, 2, 26806.00, NULL, NULL, NULL, NULL, NULL, '2018-11-16', '03:29:44', 'Admin', 'E'),
  (13, NULL, 'NCRFWB-ADOF3-15-2004', 8, NULL, NULL, NULL, 13, NULL, 2, 1, 13, 2, 26806.00, NULL, NULL, NULL, NULL, NULL, '2018-11-16', '03:29:44', 'Admin', 'E'),
  (14, NULL, 'NCRFWB-SADAS2-1-2004', 9, NULL, NULL, NULL, 13, NULL, 2, 1, 14, 2, 26806.00, NULL, NULL, NULL, NULL, NULL, '2018-11-16', '03:29:45', 'Admin', 'E'),
  (15, NULL, 'NCRFWB-ADAS2-5-2004', 10, NULL, NULL, NULL, 33, NULL, 1, 1, 15, 1, 16282.00, NULL, NULL, NULL, NULL, NULL, '2018-11-16', '03:29:45', 'Admin', 'E'),
  (16, NULL, 'NCRFWB-ADAS2-6-2004', 10, NULL, NULL, NULL, 33, NULL, 2, 1, 16, 8, 17369.00, NULL, NULL, NULL, NULL, NULL, '2018-11-16', '03:29:45', 'Admin', 'E'),
  (17, NULL, 'NCRFWB-ADAS2-7-2004', 10, NULL, NULL, NULL, 33, NULL, 2, 1, 17, 8, 17369.00, NULL, NULL, NULL, NULL, NULL, '2018-11-16', '03:29:46', 'Admin', 'E'),
  (18, NULL, 'NCRFWB-ADAS1-6-2004', 11, NULL, NULL, NULL, 7, NULL, 2, 1, 18, 2, 15380.00, NULL, NULL, NULL, NULL, NULL, '2018-11-16', '03:29:46', 'Admin', 'E'),
  (19, NULL, 'NCRFWB-ADA6-3-2004', 12, NULL, NULL, NULL, 6, NULL, 2, 1, 19, 2, 14459.00, NULL, NULL, NULL, NULL, NULL, '2018-11-16', '03:29:47', 'Admin', 'E'),
  (20, NULL, 'NCRFWB-ADA4-8-2004', 13, NULL, NULL, NULL, 4, NULL, 2, 0, NULL, 8, 13424.00, NULL, NULL, NULL, NULL, NULL, '2018-11-16', '03:29:47', 'Admin', 'E'),
  (21, NULL, 'NCRFWB-ADA4-13-2004', 13, NULL, NULL, NULL, 4, NULL, 2, 1, 21, 5, 13097.00, NULL, NULL, NULL, NULL, NULL, '2018-11-16', '03:29:47', 'Admin', 'E'),
  (22, NULL, 'NCRFWB-ADA4-14-2004', 13, NULL, NULL, NULL, 4, NULL, 2, 0, NULL, 1, 12674.00, NULL, NULL, NULL, NULL, NULL, '2018-11-16', '03:29:48', 'Admin', 'E'),
  (23, NULL, 'NCRFWB-ADA4-12-2004', 13, NULL, NULL, NULL, 4, NULL, 2, 1, 23, 1, 12674.00, NULL, NULL, NULL, NULL, NULL, '2018-11-16', '03:29:48', 'Admin', 'E'),
  (24, NULL, 'NCRFWB-ADA4-11-2004', 13, NULL, NULL, NULL, 4, NULL, 2, 0, NULL, 1, 12674.00, NULL, NULL, NULL, NULL, NULL, '2018-11-16', '03:29:49', 'Admin', 'E'),
  (25, NULL, 'NCRFWB-ADA3-15-2004', 14, NULL, NULL, NULL, 3, NULL, 2, 1, 25, 8, 12620.00, NULL, NULL, NULL, NULL, NULL, '2018-11-16', '03:29:49', 'Admin', 'E'),
  (26, NULL, 'NCRFWB-ADA3-16-2004', 14, NULL, NULL, NULL, 3, NULL, 2, 1, 26, 8, 12620.00, NULL, NULL, NULL, NULL, NULL, '2018-11-16', '03:29:50', 'Admin', 'E'),
  (27, NULL, 'NCRFWB-ADA2-8-2004', 15, NULL, NULL, NULL, 2, NULL, 2, 1, 27, 6, 11671.00, NULL, NULL, NULL, NULL, NULL, '2018-11-16', '03:29:50', 'Admin', 'E'),
  (28, NULL, 'NCRFWB-INFO5-2-2007', 16, NULL, NULL, NULL, 23, NULL, 3, 1, 28, 1, 73299.00, NULL, NULL, NULL, NULL, NULL, '2018-11-16', '03:29:51', 'Admin', 'E'),
  (29, NULL, 'NCRFWB-INFO4-3-2007', 17, NULL, NULL, NULL, 21, NULL, 3, 0, NULL, 2, 59597.00, NULL, NULL, NULL, NULL, NULL, '2018-11-16', '03:29:51', 'Admin', 'E'),
  (30, NULL, 'NCRFWB-ITO1-6-2007', 18, NULL, NULL, NULL, 18, NULL, 3, 1, 30, 2, 42730.00, NULL, NULL, NULL, NULL, NULL, '2018-11-16', '03:29:51', 'Admin', 'E'),
  (31, NULL, 'NCRFWB-INFO3-1-2008', 19, NULL, NULL, NULL, 17, NULL, 3, 1, 31, 2, 38543.00, NULL, NULL, NULL, NULL, NULL, '2018-11-16', '03:29:52', 'Admin', 'E'),
  (32, NULL, 'NCRFWB-SADAS3-2-2004', 20, NULL, NULL, NULL, 14, NULL, 3, 1, 32, 2, 29359.00, NULL, NULL, NULL, NULL, NULL, '2018-11-16', '03:29:52', 'Admin', 'E'),
  (33, NULL, 'NCRFWB-INFOSA2-7-2007', 21, NULL, NULL, NULL, 15, NULL, 3, 1, 33, 2, 32147.00, NULL, NULL, NULL, NULL, NULL, '2018-11-16', '03:29:52', 'Admin', 'E'),
  (34, NULL, 'NCRFWB-PLO2-4-2016', 22, NULL, NULL, NULL, 14, NULL, 3, 1, 34, 1, 29010.00, NULL, NULL, NULL, NULL, NULL, '2018-11-16', '03:29:53', 'Admin', 'E'),
  (35, NULL, 'NCRFWB-INFOSA1-6-2016', 23, NULL, NULL, NULL, 11, NULL, 3, 1, 35, 1, 22149.00, NULL, NULL, NULL, NULL, NULL, '2018-11-16', '03:29:53', 'Admin', 'E'),
  (36, NULL, 'NCRFWB-LIB1-1-2007', 24, NULL, NULL, NULL, 10, NULL, 3, 1, 36, 4, 20963.00, NULL, NULL, NULL, NULL, NULL, '2018-11-16', '03:29:54', 'Admin', 'E'),
  (37, NULL, 'NCRFWB-PLO1-5-2016', 25, NULL, NULL, NULL, 10, NULL, 3, 1, 37, 1, 20179.00, NULL, NULL, NULL, NULL, NULL, '2018-11-16', '03:29:54', 'Admin', 'E'),
  (38, NULL, 'NCRFWB-ADAS3-5-2004', 26, NULL, NULL, NULL, 8, NULL, 3, 0, NULL, 1, 17627.00, NULL, NULL, NULL, NULL, NULL, '2018-11-16', '03:29:55', 'Admin', 'E'),
  (39, NULL, 'NCRFWB-ADA6-5-2004', 12, NULL, NULL, NULL, 6, NULL, 3, 1, 39, 4, 14699.00, NULL, NULL, NULL, NULL, NULL, '2018-11-16', '03:29:55', 'Admin', 'E'),
  (40, NULL, 'NCRFWB-CGAD-7-2016', 27, NULL, NULL, NULL, 23, NULL, 4, 1, 40, 3, 75512.00, NULL, NULL, NULL, NULL, NULL, '2018-11-16', '03:29:55', 'Admin', 'E'),
  (41, NULL, 'NCRFWB-SVGAD-6-2016', 28, NULL, NULL, NULL, 21, NULL, 4, 1, 41, 2, 59597.00, NULL, NULL, NULL, NULL, NULL, '2018-11-16', '03:29:56', 'Admin', 'E'),
  (42, NULL, 'NCRFWB-SVGAD-7-2016', 28, NULL, NULL, NULL, 21, NULL, 4, 1, 42, 1, 58717.00, NULL, NULL, NULL, NULL, NULL, '2018-11-16', '03:29:56', 'Admin', 'E'),
  (43, NULL, 'NCRFWB-SRGAD-6-2016', 29, NULL, NULL, NULL, 17, NULL, 4, 1, 43, 1, 38085.00, NULL, NULL, NULL, NULL, NULL, '2018-11-16', '03:29:57', 'Admin', 'E'),
  (44, NULL, 'NCRFWB-SRGAD-8-2016', 29, NULL, NULL, NULL, 17, NULL, 4, 1, 44, 1, 38085.00, NULL, NULL, NULL, NULL, NULL, '2018-11-16', '03:29:57', 'Admin', 'E'),
  (45, NULL, 'NCRFWB-SRGAD-7-2016', 29, NULL, NULL, NULL, 17, NULL, 4, 1, 45, 1, 38085.00, NULL, NULL, NULL, NULL, NULL, '2018-11-16', '03:29:57', 'Admin', 'E'),
  (46, NULL, 'NCRFWB-GAD2-4-2016', 30, NULL, NULL, NULL, 14, NULL, 4, 1, 46, 6, 30799.00, NULL, NULL, NULL, NULL, NULL, '2018-11-16', '03:29:58', 'Admin', 'E'),
  (47, NULL, 'NCRFWB-GAD2-8-2016', 30, NULL, NULL, NULL, 14, NULL, 4, 1, 47, 1, 29010.00, NULL, NULL, NULL, NULL, NULL, '2018-11-16', '03:29:58', 'Admin', 'E'),
  (48, NULL, 'NCRFWB-GAD2-3-2016', 30, NULL, NULL, NULL, 14, NULL, 4, 1, 48, 1, 29010.00, NULL, NULL, NULL, NULL, NULL, '2018-11-16', '03:29:58', 'Admin', 'E'),
  (49, NULL, 'NCRFWB-GAD2-6-2016', 30, NULL, NULL, NULL, 14, NULL, 4, 1, 49, 1, 29010.00, NULL, NULL, NULL, NULL, NULL, '2018-11-16', '03:29:59', 'Admin', 'E'),
  (50, NULL, 'NCRFWB-GAD2-5-2016', 30, NULL, NULL, NULL, 14, NULL, 4, 1, 50, 1, 29010.00, NULL, NULL, NULL, NULL, NULL, '2018-11-16', '03:29:59', 'Admin', 'E'),
  (51, NULL, 'NCRFWB-GAD2-7-2016', 30, NULL, NULL, NULL, 14, NULL, 4, 1, 51, 1, 29010.00, NULL, NULL, NULL, NULL, NULL, '2018-11-16', '03:30:00', 'Admin', 'E'),
  (52, NULL, 'NCRFWB-PLA-1-1998', 31, NULL, NULL, NULL, 33, NULL, 4, 1, 52, 1, 16282.00, NULL, NULL, NULL, NULL, NULL, '2018-11-16', '03:30:01', 'Admin', 'E'),
  (53, NULL, 'NCRFWB-ADA4-17-2004', 32, NULL, NULL, NULL, 4, NULL, 4, 1, 53, 1, 12674.00, NULL, NULL, NULL, NULL, NULL, '2018-11-16', '03:30:01', 'Admin', 'E'),
  (54, NULL, 'NCRFWB-CGAD-6-2016', 27, NULL, NULL, NULL, 23, NULL, 5, 1, 54, 2, 74397.00, NULL, NULL, NULL, NULL, NULL, '2018-11-16', '03:30:02', 'Admin', 'E'),
  (55, NULL, 'NCRFWB-SVGAD-5-2016', 28, NULL, NULL, NULL, 21, NULL, 5, 1, 55, 3, 60491.00, NULL, NULL, NULL, NULL, NULL, '2018-11-16', '03:30:02', 'Admin', 'E'),
  (56, NULL, 'NCRFWB-SVGAD-8-2016', 28, NULL, NULL, NULL, 21, NULL, 5, 1, 56, 1, 58717.00, NULL, NULL, NULL, NULL, NULL, '2018-11-16', '03:30:03', 'Admin', 'E'),
  (57, NULL, 'NCRFWB-SRGAD-5-2016', 29, NULL, NULL, NULL, 17, NULL, 4, 1, 57, 3, 39007.00, NULL, NULL, NULL, NULL, NULL, '2018-11-16', '03:30:03', 'Admin', 'E'),
  (58, NULL, 'NCRFWB-SRGAD-4-2016', 29, NULL, NULL, NULL, 18, NULL, 5, 1, 58, 1, 38085.00, NULL, NULL, NULL, NULL, NULL, '2018-11-16', '03:30:04', 'Admin', 'E'),
  (59, NULL, 'NCRFWB-SRGAD-10-2016', 29, NULL, NULL, NULL, 19, NULL, 5, 1, 59, 1, 38085.00, NULL, NULL, NULL, NULL, NULL, '2018-11-16', '03:30:04', 'Admin', 'E'),
  (60, NULL, 'NCRFWB-SRGAD-9-2016', 29, NULL, NULL, NULL, 20, NULL, 5, 1, 60, 1, 38085.00, NULL, NULL, NULL, NULL, NULL, '2018-11-16', '03:30:05', 'Admin', 'E'),
  (61, NULL, 'NCRFWB-SRGAD-3-2016', 29, NULL, NULL, NULL, 21, NULL, 5, 1, 61, 1, 38085.00, NULL, NULL, NULL, NULL, NULL, '2018-11-16', '03:30:05', 'Admin', 'E'),
  (62, NULL, 'NCRFWB-GAD2-2-2016', 30, NULL, NULL, NULL, 14, NULL, 5, 1, 62, 1, 29010.00, NULL, NULL, NULL, NULL, NULL, '2018-11-16', '03:30:06', 'Admin', 'E'),
  (63, NULL, 'NCRFWB-GAD2-9-2016', 30, NULL, NULL, NULL, 15, NULL, 5, 1, 105, 1, 29010.00, NULL, NULL, NULL, NULL, NULL, '2018-11-16', '03:30:06', 'Admin', 'E'),
  (64, NULL, 'NCRFWB-GAD2-1-2016', 30, NULL, NULL, NULL, 16, NULL, 5, 1, 64, 1, 29010.00, NULL, NULL, NULL, NULL, NULL, '2018-11-16', '03:30:06', 'Admin', 'E'),
  (65, NULL, 'NCRFWB-ADA4-16-2004', 13, NULL, NULL, NULL, 4, NULL, 5, 0, NULL, 6, 13206.00, NULL, NULL, NULL, NULL, NULL, '2018-11-16', '03:30:06', 'Admin', 'E'),
  (66, NULL, 'NCRFWB-CGAD-8-2016', 27, NULL, NULL, NULL, 23, NULL, 6, 1, 66, 2, 74397.00, NULL, NULL, NULL, NULL, NULL, '2018-11-16', '03:30:07', 'Admin', 'E'),
  (67, NULL, 'NCRFWB-SVGAD-9-2016', 28, NULL, NULL, NULL, 21, NULL, 6, 1, 67, 1, 58717.00, NULL, NULL, NULL, NULL, NULL, '2018-11-16', '03:30:07', 'Admin', 'E'),
  (68, NULL, 'NCRFWB-SVGAD-10-2016', 28, NULL, NULL, NULL, 22, NULL, 6, 1, 68, 1, 58717.00, NULL, NULL, NULL, NULL, NULL, '2018-11-16', '03:30:08', 'Admin', 'E'),
  (69, NULL, 'NCRFWB-SRGAD-12-2016', 29, NULL, NULL, NULL, 17, NULL, 6, 1, 69, 1, 38085.00, NULL, NULL, NULL, NULL, NULL, '2018-11-16', '03:30:08', 'Admin', 'E'),
  (70, NULL, 'NCRFWB-SRGAD-11-2016', 29, NULL, NULL, NULL, 18, NULL, 6, 1, 70, 1, 38085.00, NULL, NULL, NULL, NULL, NULL, '2018-11-16', '03:30:09', 'Admin', 'E'),
  (71, NULL, 'NCRFWB-GAD2-12-2016', 30, NULL, NULL, NULL, 14, NULL, 6, 1, 71, 1, 29010.00, NULL, NULL, NULL, NULL, NULL, '2018-11-16', '03:30:09', 'Admin', 'E'),
  (72, NULL, 'NCRFWB-GAD2-11-2016', 30, NULL, NULL, NULL, 14, NULL, 6, 1, 72, 1, 29010.00, NULL, NULL, NULL, NULL, NULL, '2018-11-16', '03:30:10', 'Admin', 'E'),
  (73, NULL, 'NCRFWB-GAD2-13-2016', 30, NULL, NULL, NULL, 14, NULL, 6, 1, 73, 1, 29010.00, NULL, NULL, NULL, NULL, NULL, '2018-11-16', '03:30:10', 'Admin', 'E'),
  (74, NULL, 'NCRFWB-LIB2-1-1998', 33, NULL, NULL, NULL, 14, NULL, 3, 0, NULL, 4, 30071.00, NULL, NULL, NULL, NULL, NULL, '2018-11-16', '03:30:11', 'Admin', 'E'),
  (75, NULL, 'NCRFWB-GAD2-14-2016', 30, NULL, NULL, NULL, 14, NULL, 6, 0, NULL, 1, 29010.00, NULL, NULL, NULL, NULL, NULL, '2018-11-16', '03:30:11', 'Admin', 'E'),
  (76, NULL, 'NCRFWB-ADOF3-13-2004', 34, NULL, NULL, NULL, 13, NULL, 2, 0, NULL, 8, 28759.00, NULL, NULL, NULL, NULL, NULL, '2018-11-16', '03:30:11', 'Admin', 'E'),
  (79, NULL, 'NCRFWB-ADOF5-3-2016', 53, 0, 0, 7, 17, NULL, NULL, 1, 117, 1, 38085.00, NULL, NULL, NULL, NULL, NULL, '2019-03-25', '10:36:59', 'admin', 'E');
/*!40000 ALTER TABLE `positionitem` ENABLE KEYS */;


-- Dumping data for table pcw_db.workexperiences: ~0 rows (approximately)
/*!40000 ALTER TABLE `workexperiences` DISABLE KEYS */;
/*!40000 ALTER TABLE `workexperiences` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
