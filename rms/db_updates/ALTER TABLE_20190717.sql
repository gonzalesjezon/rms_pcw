ALTER TABLE `educations`
	CHANGE COLUMN `attendance_from` `attendance_from` DATE NULL DEFAULT NULL AFTER `course`,
	CHANGE COLUMN `attendance_to` `attendance_to` DATE NULL DEFAULT NULL AFTER `attendance_from`,
	ADD COLUMN `ongoing` INT(11) NULL DEFAULT '0' AFTER `educ_level`;


ALTER TABLE `workexperiences`
	CHANGE COLUMN `inclusive_date_from` `inclusive_date_from` DATE NULL DEFAULT NULL AFTER `applicant_id`,
	CHANGE COLUMN `inclusive_date_to` `inclusive_date_to` DATE NULL DEFAULT NULL AFTER `inclusive_date_from`,
	ADD COLUMN `present_work` INT(11) NULL DEFAULT 0 AFTER `govt_service`;


ALTER TABLE `applicants`
	ADD COLUMN `appointment_status` VARCHAR(255) NULL DEFAULT NULL AFTER `gwa`;