ALTER TABLE `applicant_ratings`
	ADD COLUMN `training_remarks` TEXT NULL DEFAULT NULL AFTER `total_points`,
	ADD COLUMN `experience_remarks` TEXT NULL DEFAULT NULL AFTER `training_remarks`;