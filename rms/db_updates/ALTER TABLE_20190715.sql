ALTER TABLE `appointment_requirements`
	ADD COLUMN `complied` INT(11) NULL DEFAULT '0' AFTER `pagibig_number`;

ALTER TABLE `jobs`
	ADD COLUMN `professional_fees` DECIMAL(12,3) NULL DEFAULT '0.000' AFTER `cashgift_amount`;

ALTER TABLE `jobs`
	ADD COLUMN `duration_from` DATE NULL DEFAULT NULL AFTER `deadline_date`,
	ADD COLUMN `duration_to` DATE NULL DEFAULT NULL AFTER `duration_from`;

ALTER TABLE `jobs`
	CHANGE COLUMN `requirements` `requirements` TEXT NULL DEFAULT NULL COLLATE 'utf8mb4_unicode_ci' AFTER `reporting_line`,
	CHANGE COLUMN `compentency_1` `compentency_1` TEXT NULL DEFAULT NULL COLLATE 'utf8mb4_unicode_ci' AFTER `requirements`,
	CHANGE COLUMN `compentency_2` `compentency_2` TEXT NULL DEFAULT NULL COLLATE 'utf8mb4_unicode_ci' AFTER `compentency_1`,
	CHANGE COLUMN `compentency_3` `compentency_3` TEXT NULL DEFAULT NULL COLLATE 'utf8mb4_unicode_ci' AFTER `compentency_2`,
	CHANGE COLUMN `compentency_4` `compentency_4` TEXT NULL DEFAULT NULL COLLATE 'utf8mb4_unicode_ci' AFTER `compentency_3`,
	CHANGE COLUMN `compentency_5` `compentency_5` TEXT NULL DEFAULT NULL COLLATE 'utf8mb4_unicode_ci' AFTER `compentency_4`,
	ADD COLUMN `other_qualification` TEXT NULL DEFAULT NULL AFTER `compentency_5`;

ALTER TABLE `assumptions`
	CHANGE COLUMN `created_by` `created_by` INT(11) NULL DEFAULT NULL AFTER `assumption_date`;

ALTER TABLE `oath_offices`
	CHANGE COLUMN `created_by` `created_by` INT(11) NULL DEFAULT NULL AFTER `oath_date`;