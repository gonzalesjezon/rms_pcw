ALTER TABLE `examinations`
	ADD COLUMN `average_raw_score` VARCHAR(50) NULL DEFAULT NULL AFTER `exam_location`,
	ADD COLUMN `sub_total_rating` VARCHAR(50) NULL DEFAULT NULL AFTER `average_raw_score`;

ALTER TABLE `interviews`
	ADD COLUMN `average_raw_score` VARCHAR(225) NULL DEFAULT NULL AFTER `cost_for_hire`,
	ADD COLUMN `sub_total_rating` VARCHAR(225) NULL DEFAULT NULL AFTER `average_raw_score`;