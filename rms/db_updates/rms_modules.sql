-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.1.36-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win32
-- HeidiSQL Version:             9.5.0.5196
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table pcw_db.rms_modules
DROP TABLE IF EXISTS `rms_modules`;
CREATE TABLE IF NOT EXISTS `rms_modules` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(225) DEFAULT NULL,
  `description` varchar(225) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;

-- Dumping data for table pcw_db.rms_modules: ~0 rows (approximately)
/*!40000 ALTER TABLE `rms_modules` DISABLE KEYS */;
INSERT INTO `rms_modules` (`id`, `name`, `description`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 'psipop', 'Itemize & Plantilla', NULL, NULL, NULL, NULL, NULL),
	(2, 'jobs', 'Jobs', NULL, NULL, NULL, NULL, NULL),
	(3, 'applicant', 'Applicants', NULL, NULL, NULL, NULL, NULL),
	(4, 'examinations', 'Examinations', NULL, NULL, NULL, NULL, NULL),
	(5, 'interviews', 'Interviews', NULL, NULL, NULL, NULL, NULL),
	(6, 'appointment-form', 'Appointmnet Form', NULL, NULL, NULL, NULL, NULL),
	(7, 'appointment-issued', 'Appointment Issued', NULL, NULL, NULL, NULL, NULL),
	(8, 'appointment-processing', 'Appointment Processing', NULL, NULL, NULL, NULL, NULL),
	(9, 'appointment-requirements', 'Pre Emp. Requirements', NULL, NULL, NULL, NULL, NULL),
	(10, 'appointment-casual', 'Plantilla of Casual Appointment', NULL, NULL, NULL, NULL, NULL),
	(11, 'position-descriptions', 'Position Description', NULL, NULL, NULL, NULL, NULL),
	(12, 'oath-office', 'Oath of Office', NULL, NULL, NULL, NULL, NULL),
	(13, 'assumption', 'Assumption', NULL, NULL, NULL, NULL, NULL),
	(14, 'erasure_alterations', 'Erasure Alterations', NULL, NULL, NULL, NULL, NULL),
	(15, 'acceptance_resignation', 'Acceptance Resignation', NULL, NULL, NULL, NULL, NULL),
	(16, 'boarding_applicant', 'Applicant Onboarding', NULL, NULL, NULL, NULL, NULL),
	(17, 'report', 'Report', NULL, NULL, NULL, NULL, NULL),
	(18, 'access_modules', 'Access Module', NULL, NULL, NULL, NULL, NULL),
	(19, 'users', 'User', NULL, NULL, NULL, NULL, NULL);
/*!40000 ALTER TABLE `rms_modules` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
