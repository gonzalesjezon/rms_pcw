ALTER TABLE `background_checkings`
	ADD COLUMN `character_reference` VARCHAR(225) NULL DEFAULT NULL AFTER `interview_contact`;

ALTER TABLE `background_checkings`
	ADD COLUMN `hr_staff_name` VARCHAR(225) NULL DEFAULT NULL AFTER `character_reference`;