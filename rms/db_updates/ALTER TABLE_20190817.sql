ALTER TABLE `jobs`
	ADD COLUMN `csc_education` TEXT NULL AFTER `other_qualification`,
	ADD COLUMN `csc_work_experience` TEXT NULL AFTER `csc_education`,
	ADD COLUMN `csc_eligibility` TEXT NULL AFTER `csc_work_experience`,
	ADD COLUMN `csc_training` TEXT NULL AFTER `csc_eligibility`;

ALTER TABLE `interviews`
	ADD COLUMN `cost_for_hire` DECIMAL(9,2) NULL DEFAULT NULL AFTER `confirmed`;


DROP TABLE IF EXISTS `background_checkings`;
CREATE TABLE IF NOT EXISTS `background_checkings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) DEFAULT NULL,
  `provided_reference` int(11) DEFAULT '0',
  `reference_check` int(11) DEFAULT '0',
  `permission` int(11) DEFAULT '0',
  `applicant_relationship` varchar(225) DEFAULT NULL,
  `applicant_capacity` varchar(225) DEFAULT NULL,
  `date_from` date DEFAULT NULL,
  `date_to` date DEFAULT NULL,
  `duties_and_responsibilitties` varchar(225) DEFAULT NULL,
  `have_pending_case` varchar(225) DEFAULT NULL,
  `applicant_satisfactory` varchar(225) DEFAULT NULL,
  `reason_for_leaving` varchar(225) DEFAULT NULL,
  `applicant_adaptability` varchar(225) DEFAULT NULL,
  `applicant_work_performance` varchar(225) DEFAULT NULL,
  `applicant_contribution` varchar(225) DEFAULT NULL,
  `performance_concerns` varchar(225) DEFAULT NULL,
  `comments` varchar(225) DEFAULT NULL,
  `job_requirements` varchar(225) DEFAULT NULL,
  `reemploy_the_applicant` varchar(225) DEFAULT NULL,
  `final_comments` varchar(225) DEFAULT NULL,
  `interview_contact` varchar(225) DEFAULT NULL,
  `use_of_profance_language` int(11) DEFAULT '0',
  `drug_user` int(11) DEFAULT '0',
  `risque_photos` int(11) DEFAULT '0',
  `discriminating_comments` int(11) DEFAULT '0',
  `bullying_statement` int(11) DEFAULT '0',
  `observe_grammar` int(11) DEFAULT '0',
  `sharing_confidential_information` int(11) DEFAULT '0',
  `condacted_by` varchar(225) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

INSERT INTO `rms_modules` (`name`, `description`) VALUES ('background_checking', 'Background Checking');

DROP TABLE IF EXISTS `interview_guides`;
CREATE TABLE IF NOT EXISTS `interview_guides` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) DEFAULT NULL,
  `interviewer` varchar(225) DEFAULT NULL,
  `question_point_1` decimal(9,2) DEFAULT NULL,
  `question_point_2` decimal(9,2) DEFAULT NULL,
  `question_point_3` decimal(9,2) DEFAULT NULL,
  `question_point_4` decimal(9,2) DEFAULT NULL,
  `question_point_5` decimal(9,2) DEFAULT NULL,
  `question_point_6` decimal(9,2) DEFAULT NULL,
  `comments` text,
  `total_score` decimal(9,2) DEFAULT NULL,
  `date_created` date DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
