ALTER TABLE `applicants`
	ADD COLUMN `exam_status` INT(11) NULL DEFAULT '0' AFTER `qualified`,
	ADD COLUMN `interview_status` INT(11) NULL DEFAULT '0' AFTER `exam_status`;

ALTER TABLE `jobs`
	ADD COLUMN `cos_position_title` VARCHAR(255) NULL DEFAULT NULL AFTER `reporting_line`;