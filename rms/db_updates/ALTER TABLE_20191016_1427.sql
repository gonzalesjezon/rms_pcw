-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.1.36-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win32
-- HeidiSQL Version:             9.5.0.5196
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table pcw_db.access_modules
DROP TABLE IF EXISTS `access_modules`;
CREATE TABLE IF NOT EXISTS `access_modules` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `access_name` varchar(225) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Dumping data for table pcw_db.access_modules: ~2 rows (approximately)
/*!40000 ALTER TABLE `access_modules` DISABLE KEYS */;
INSERT INTO `access_modules` (`id`, `access_name`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(2, 'Admin', 1, 1, '2019-04-04 15:45:17', '2019-04-04 15:46:45', NULL),
	(3, 'HR 1', 1, NULL, '2019-06-27 20:57:48', '2019-06-27 20:57:48', NULL);
/*!40000 ALTER TABLE `access_modules` ENABLE KEYS */;

-- Dumping structure for table pcw_db.access_rights
DROP TABLE IF EXISTS `access_rights`;
CREATE TABLE IF NOT EXISTS `access_rights` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `access_module_id` int(11) DEFAULT NULL,
  `module_id` int(11) DEFAULT NULL,
  `to_view` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

-- Dumping data for table pcw_db.access_rights: ~4 rows (approximately)
/*!40000 ALTER TABLE `access_rights` DISABLE KEYS */;
INSERT INTO `access_rights` (`id`, `access_module_id`, `module_id`, `to_view`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(6, 2, 19, 1, 1, NULL, '2019-06-27 20:49:17', '2019-06-27 20:49:17', NULL),
	(7, 2, 19, 1, NULL, 1, '2019-06-27 20:57:14', '2019-06-27 20:57:14', NULL),
	(8, 3, 3, 1, NULL, 1, '2019-06-27 20:57:48', '2019-06-27 20:57:48', NULL),
	(9, 3, 2, 1, NULL, 1, '2019-06-27 20:57:48', '2019-06-27 20:57:48', NULL);
/*!40000 ALTER TABLE `access_rights` ENABLE KEYS */;

-- Dumping structure for table pcw_db.applicants
DROP TABLE IF EXISTS `applicants`;
CREATE TABLE IF NOT EXISTS `applicants` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `job_id` int(11) NOT NULL,
  `reference_no` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `first_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `middle_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `extension_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nickname` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mobile_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contact_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `telephone_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `publication` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `birthday` varchar(225) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `birth_place` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gender` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `civil_status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `citizenship` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `filipino` tinyint(1) DEFAULT NULL,
  `naturalized` tinyint(1) DEFAULT NULL,
  `height` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `weight` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `blood_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pagibig` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gsis` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `philhealth` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tin` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sss` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `govt_issued_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `govt_id_issued_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `govt_id_issued_place` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `govt_id_date_issued` timestamp NULL DEFAULT NULL,
  `govt_id_valid_until` timestamp NULL DEFAULT NULL,
  `house_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `street` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `subdivision` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `barangay` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `province` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `permanent_house_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `permanent_street` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `permanent_subdivision` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `permanent_barangay` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `permanent_city` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `permanent_province` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `permanent_country` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `permanent_telephone_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `zip_code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `permanent_zip_code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image_path` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `application_letter_path` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pds_path` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `employment_certificate_path` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tor_path` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `coe_path` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `training_certificate_path` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `info_sheet_path` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `curriculum_vitae_path` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `active` int(11) DEFAULT '0',
  `qualified` int(11) DEFAULT '0',
  `exam_status` int(11) DEFAULT '0',
  `interview_status` int(11) DEFAULT '0',
  `remarks` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gwa` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `appointment_status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `indigenous_member` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `person_id_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `solo_id_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `date_qualified` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table pcw_db.applicants: ~1 rows (approximately)
/*!40000 ALTER TABLE `applicants` DISABLE KEYS */;
INSERT INTO `applicants` (`id`, `job_id`, `reference_no`, `first_name`, `middle_name`, `last_name`, `extension_name`, `nickname`, `email_address`, `mobile_number`, `contact_number`, `telephone_number`, `publication`, `birthday`, `birth_place`, `gender`, `civil_status`, `citizenship`, `filipino`, `naturalized`, `height`, `weight`, `blood_type`, `pagibig`, `gsis`, `philhealth`, `tin`, `sss`, `govt_issued_id`, `govt_id_issued_number`, `govt_id_issued_place`, `govt_id_date_issued`, `govt_id_valid_until`, `house_number`, `street`, `subdivision`, `barangay`, `city`, `province`, `country`, `permanent_house_number`, `permanent_street`, `permanent_subdivision`, `permanent_barangay`, `permanent_city`, `permanent_province`, `permanent_country`, `permanent_telephone_number`, `zip_code`, `permanent_zip_code`, `image_path`, `application_letter_path`, `pds_path`, `employment_certificate_path`, `tor_path`, `coe_path`, `training_certificate_path`, `info_sheet_path`, `curriculum_vitae_path`, `active`, `qualified`, `exam_status`, `interview_status`, `remarks`, `gwa`, `appointment_status`, `indigenous_member`, `person_id_no`, `solo_id_no`, `created_by`, `updated_by`, `date_qualified`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(3, 6, '5da4785c7e566', 'eyJpdiI6IjU3ekxGU1p6UFdOYTNzd25KWWNMZGc9PSIsInZhbHVlIjoiZHFmS1dyb1FRRm5zb3pJVjNlcnVQdz09IiwibWFjIjoiNzRkMmE2ZWM5YTY2N2M3NTE3M2RjODc2NGJmODEzYmQxOTFmMjZhNTVmYzk5NTYwZmE3NzI3OTA2OGEzMjM4NyJ9', 'eyJpdiI6IkNcL3ZvaTdnS040bit1dFpBOGNnSkFRPT0iLCJ2YWx1ZSI6IjdqZ1hXbEVkcjdlOTJDNys3emIyQnc9PSIsIm1hYyI6ImM5YTc4ZjdjMzZiYTcwMzMxNTE3YzY5YWMxMzFmNWY4ZDM3ODgzNDdhMWI3ZmU5NzBhNDFkYTc4ZDk1YzQ4NWMifQ==', 'eyJpdiI6Im9MWHdDRmlvU1hiVTRVOGNEbkhFeHc9PSIsInZhbHVlIjoic2lKVEpjRVh2aGRiaXZ5a1FCdVV4UT09IiwibWFjIjoiZjhiY2ZjYzg0Nzg2YzQxZmFiMGIyM2EwMzI0ZWE1YjA2MTJjM2NkZGZkYWEyZmU2ZDE2MDJjOWQzY2JiMDZkMiJ9', 'eyJpdiI6IjFjTWJ2WWtyQmRsVTFNUktqTWhvbHc9PSIsInZhbHVlIjoicWEya1BhaFNkbWNsRkpIa0dmQ211UT09IiwibWFjIjoiMTdlMTJhOGRkM2Y0OTAxYzFhYWJmOTdkOWY4NTU0YjYzYWUwMjM1ZWUxYjUxZWUyZGRlMWIxZmUzYmQxODFkNiJ9', NULL, 'eyJpdiI6ImpUVDVOYWhrQkdJNGU5Y1E0MlwvdkZ3PT0iLCJ2YWx1ZSI6Ilwvd0pySkZqXC9ZUXBrdnZZait5MHRNdUJhdEN5V2x1OG1HcFNXWk1vTzJsQT0iLCJtYWMiOiI1YzY1MDMyZmVjMTY2ZTdjYWVjYTMxOTlmYzUxOWUwNGE0M2ZjYzFkMWE4NDAwOWYwNmYwZjZkZjBmODY3YzVkIn0=', '3456789', NULL, NULL, 'agency', 'eyJpdiI6InV2d3l1QVRUdEZ5dWlCNkhIdFNJbFE9PSIsInZhbHVlIjoiMGNKMDhmVTg1aTNvYUtTeTVhbStHOU1acHZxOExxR1c5Nk5mYXY1UzF6UT0iLCJtYWMiOiIwZGI3NzQ1YzIyYmE3Y2U0ODliMTU5OTU0M2I0YmI5OTk5NzY1M2Q1Yjg0ZjUyNTM1NTJmMDliYTJmNWY5NWM0In0=', 'Manila', 'male', 'single', NULL, 0, 0, '232', '444', 'A', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'eyJpdiI6IlwvZW9WNUNCbzR4ekZkNE5cL2FiM3BOUT09IiwidmFsdWUiOiJvd2lsNEZiQk5JbDVEazVTalZkdkpnPT0iLCJtYWMiOiI3MDg0Njk4ZjM1Njg1ZThmZjMzM2I1MjJkOTJjM2FmY2ExM2NlNGQ5MWViODIxYzIzZWM4ZDU2OTFmMTRjMmZmIn0=', 'eyJpdiI6IlwvSVJRa3BOZ08wdDFlWEVlZmZ0WUFRPT0iLCJ2YWx1ZSI6IjVoSG40VWFreHE1U2praldNUUowUEE9PSIsIm1hYyI6ImNkNTg2YWM0ZDU5ZTkwMzFlYjc1YTljYTIwOGRhMzJlYTVhNjJjMGYyNTQ4OWUxYzA1NzZhZTdlMjVjYWIxMmQifQ==', 'eyJpdiI6Im5KMlltUzVYV0JtaGhydDVBU0Z0MlE9PSIsInZhbHVlIjoiNE5vYkRjbFhaUUNWXC80bFdZQ1lhMHc9PSIsIm1hYyI6ImQ5NzE5ZDE3ZDVlMDIxM2Q1NjBiYzY1MDE5MWQ1ZDhjZWZkY2ZiYjg2YTIwNWFlMWY4NjgzYTA4MGEwMDA0MTkifQ==', 'eyJpdiI6Iis2bGtvOUVhbmU0MVNZbnFxb0pyYUE9PSIsInZhbHVlIjoicXB6M2Zvcmp1UE5QdUkwb0VnVkppdz09IiwibWFjIjoiOWMyYTZhMDJlNTk1NjFhMWVjMmY2MGZkMTRhMmI5NjM0NGQ5YjE3ZjJlMDliY2EyZTQ5Njg5YTYzNmY1YTNiYiJ9', 'eyJpdiI6IllIem4wMVZDWTNSeG9LN2htMTBKYmc9PSIsInZhbHVlIjoiZjArMjdKXC96ZkhaWTJMTWpWXC9vY0pBPT0iLCJtYWMiOiIwZmUwNTViYTZiZGZkZDJiMWJlYTk4NzI4YTdkM2M4OWZjNTg1Njg2OGI5ZmE5ZDMxMGY3YzFhNGVkOTFiYmQ2In0=', 'eyJpdiI6ImsxbG9sU01NUHZqMVBFdFFaSXhHM1E9PSIsInZhbHVlIjoianVwb2lzd3ZWcGZQbzJkMFFvVHpKUT09IiwibWFjIjoiMDNjOTMwNmI0MmUxOWQyOWY4Y2M5Yzc3NTA1MTZhOTdlNzMzZjZmNzRmZjM0YjAwYTcwNDg1NGVjNGI2OTQ2MyJ9', NULL, 'eyJpdiI6IkJsTVJpcFBkMm1jNVhKYXlVSXdVMnc9PSIsInZhbHVlIjoiSDNPYklFbHNXU0JSdjBYV2FTV1JQdz09IiwibWFjIjoiMzhmY2UxMDhhNGQ4YzEzNjdkOWRmMDU5MjE2MjA0MWRjMjljNmQzOTJjZTc3M2VlYzVjZjNjNWEyNzY1YjBiZiJ9', 'eyJpdiI6ImxpZFV0TTluY1RaUlNPeDZjMkFZSEE9PSIsInZhbHVlIjoiWnFZMWx2TjZRTFNpK3pWRFNNc0xDZz09IiwibWFjIjoiYzg3ZTIwZDE2MjJlMmYwZWRkMDM0OGM2NGYzNDEwNjFlOWI1OTNiMTgzZjQxOTUzYzg1MDBiODhkZWI2NTEyZiJ9', 'eyJpdiI6IkN0SWVOcERUUnBiWGdkSHpWQ0QzSlE9PSIsInZhbHVlIjoiWEd2cTlxbVRmOW5jZ3hGVWF1WjVtUT09IiwibWFjIjoiMjgxMWE3NTc0YWI3M2ZhNTg1MTBhNzdjYjI2M2EzMzkzNTJlZjhjZmYwNTE0MDIwMWY1M2VkZGYxM2QwZDc0NSJ9', 'eyJpdiI6Ikp3OE1OUFRpN0Y2dzdPY1IwK1dTWkE9PSIsInZhbHVlIjoiVmh5YjNzcklyckkwRVo3TUhEYzNOZz09IiwibWFjIjoiMGYwNGFkN2M1YjllOTEwZTViNmE1YjhmMTY0MjIwMTA2YTFmNWZiOTAxNTE5NjYwZjRiMDMxMTc3MTQyOTk4ZiJ9', 'eyJpdiI6IjVlQUxQMTRyTWp2bFJNYkxmZGgyckE9PSIsInZhbHVlIjoiQ0dDRlhPUGdwVm1FVVNNMDV4T0JoUT09IiwibWFjIjoiZGU4NGMwZjM2MmNhMmFjZGIzMzUzMjcxYjdjMTZlYWM3ZGVjODJhNjU4N2IxYjQyMDk1MDE2MjRlMDU0MDYzNSJ9', 'eyJpdiI6IkNheXc1MTRlR3hvUytIWXRUNmo5aVE9PSIsInZhbHVlIjoiRnBQN3J3RDNmRUxwekQ0RFZrZkJBQT09IiwibWFjIjoiODhkMTA0MWVmMmRmNzhkMDM5ZmYwMGE5OWU1ZjAzNTI1OGY2MWU4OWVlMGUzODMxMGUyYWVjYzNjMGYwYjNhZSJ9', NULL, NULL, 'eyJpdiI6ImJVTlJ6dGJEMzVNc0tvdVdURTVtSmc9PSIsInZhbHVlIjoiQlp5bFV5NHNYb3BIRDhzaHd4VWlcL0E9PSIsIm1hYyI6ImQ1NDZiNGQ2MzE0M2RiZDVmMGQxM2VmYWM3Njc3ZmYyMTk4ZjcxNGNkZjYxZjY2NWUzNDcyNTc3NzNmNjUzZDQifQ==', 'eyJpdiI6Im9DXC9zZ3c4cFdaQ0RzMDB2bjh1d2ZRPT0iLCJ2YWx1ZSI6InpLZ1ZzMnNkQisweVpiK0djeGVWNlE9PSIsIm1hYyI6IjMwNzFhNjAwOTdlYzVjMjc4Y2I2ZjczMzE2MzBhMmY5Y2I0OTA0ZTZjZWU5ZGRlZDIxZWZkYWM1OWExYjdlODYifQ==', NULL, 'cf882b3923ae40211765dbe0183d3142.csv', 'cf882b3923ae40211765dbe0183d3142.csv', 'cf882b3923ae40211765dbe0183d3142.csv', 'ccebfdd669ec4c537d23ce2c5256a7a6.csv', 'cf882b3923ae40211765dbe0183d3142.csv', 'ccebfdd669ec4c537d23ce2c5256a7a6.csv', NULL, 'C:\\xampp\\tmp\\phpA30F.tmp', 0, 1, 0, 0, NULL, NULL, 'nonplantilla', NULL, NULL, NULL, 88888888, 1, '2019-10-16 03:09:38', '2019-10-14 13:30:04', '2019-10-16 03:09:38', NULL);
/*!40000 ALTER TABLE `applicants` ENABLE KEYS */;

-- Dumping structure for table pcw_db.applicant_ratings
DROP TABLE IF EXISTS `applicant_ratings`;
CREATE TABLE IF NOT EXISTS `applicant_ratings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) DEFAULT NULL,
  `position_level` varchar(225) DEFAULT NULL,
  `education_points` decimal(9,2) DEFAULT NULL,
  `experience_points` decimal(9,2) DEFAULT NULL,
  `training_points` decimal(9,2) DEFAULT NULL,
  `total_points` decimal(9,2) DEFAULT NULL,
  `training_remarks` text,
  `experience_remarks` text,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Dumping data for table pcw_db.applicant_ratings: ~1 rows (approximately)
/*!40000 ALTER TABLE `applicant_ratings` DISABLE KEYS */;
INSERT INTO `applicant_ratings` (`id`, `applicant_id`, `position_level`, `education_points`, `experience_points`, `training_points`, `total_points`, `training_remarks`, `experience_remarks`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(2, 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, '2019-10-16 03:49:54', '2019-10-16 03:49:54', NULL);
/*!40000 ALTER TABLE `applicant_ratings` ENABLE KEYS */;

-- Dumping structure for table pcw_db.appointments
DROP TABLE IF EXISTS `appointments`;
CREATE TABLE IF NOT EXISTS `appointments` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) DEFAULT NULL,
  `form33_hrmo` int(11) DEFAULT NULL,
  `form34b_hrmo` int(11) DEFAULT NULL,
  `form212_hrmo` int(11) DEFAULT NULL,
  `eligibility_hrmo` int(11) DEFAULT NULL,
  `form1_hrmo` int(11) DEFAULT NULL,
  `form32_hrmo` int(11) DEFAULT NULL,
  `form4_hrmo` int(11) DEFAULT NULL,
  `form33_cscfo` varchar(225) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `form34b_cscfo` varchar(225) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `form212_cscfo` varchar(225) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `eligibility_cscfo` varchar(225) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `form1_cscfo` varchar(225) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `form32_cscfo` varchar(225) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `form4_cscfo` varchar(225) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table pcw_db.appointments: ~0 rows (approximately)
/*!40000 ALTER TABLE `appointments` DISABLE KEYS */;
/*!40000 ALTER TABLE `appointments` ENABLE KEYS */;

-- Dumping structure for table pcw_db.appointment_casual
DROP TABLE IF EXISTS `appointment_casual`;
CREATE TABLE IF NOT EXISTS `appointment_casual` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) NOT NULL DEFAULT '0',
  `employee_status` int(11) NOT NULL DEFAULT '0',
  `nature_of_appointment` int(11) NOT NULL DEFAULT '0',
  `appointing_officer` varchar(225) DEFAULT NULL,
  `hrmo` varchar(225) DEFAULT NULL,
  `hrmo_date_sign` date DEFAULT NULL,
  `date_sign` date DEFAULT NULL,
  `period_emp_from` date DEFAULT NULL,
  `period_emp_to` date DEFAULT NULL,
  `daily_wage` varchar(225) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Dumping data for table pcw_db.appointment_casual: ~1 rows (approximately)
/*!40000 ALTER TABLE `appointment_casual` DISABLE KEYS */;
INSERT INTO `appointment_casual` (`id`, `applicant_id`, `employee_status`, `nature_of_appointment`, `appointing_officer`, `hrmo`, `hrmo_date_sign`, `date_sign`, `period_emp_from`, `period_emp_to`, `daily_wage`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(2, 3, 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, '818.18', 1, NULL, '2019-10-16 04:08:18', '2019-10-16 04:08:18', NULL);
/*!40000 ALTER TABLE `appointment_casual` ENABLE KEYS */;

-- Dumping structure for table pcw_db.appointment_forms
DROP TABLE IF EXISTS `appointment_forms`;
CREATE TABLE IF NOT EXISTS `appointment_forms` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) NOT NULL DEFAULT '0',
  `employee_status` int(11) NOT NULL DEFAULT '0',
  `nature_of_appointment` int(11) NOT NULL DEFAULT '0',
  `appointing_officer` varchar(225) DEFAULT NULL,
  `hrmo` varchar(225) DEFAULT NULL,
  `chairperson` varchar(225) DEFAULT NULL,
  `posted_in` varchar(225) DEFAULT NULL,
  `date_sign` date DEFAULT NULL,
  `publication_date_from` date DEFAULT NULL,
  `publication_date_to` date DEFAULT NULL,
  `hrmo_date_sign` date DEFAULT NULL,
  `chairperson_date_sign` date DEFAULT NULL,
  `period_emp_from` date DEFAULT NULL,
  `period_emp_to` date DEFAULT NULL,
  `date_issued` date DEFAULT NULL,
  `assessment_date` date DEFAULT NULL,
  `vice` varchar(225) DEFAULT NULL,
  `who` varchar(225) DEFAULT NULL,
  `hrmo_assessment_date` date DEFAULT NULL,
  `chairperson_deliberation_date` date DEFAULT NULL,
  `form_status` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Dumping data for table pcw_db.appointment_forms: ~1 rows (approximately)
/*!40000 ALTER TABLE `appointment_forms` DISABLE KEYS */;
INSERT INTO `appointment_forms` (`id`, `applicant_id`, `employee_status`, `nature_of_appointment`, `appointing_officer`, `hrmo`, `chairperson`, `posted_in`, `date_sign`, `publication_date_from`, `publication_date_to`, `hrmo_date_sign`, `chairperson_date_sign`, `period_emp_from`, `period_emp_to`, `date_issued`, `assessment_date`, `vice`, `who`, `hrmo_assessment_date`, `chairperson_deliberation_date`, `form_status`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(2, 3, 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, '2019-10-16 03:59:21', '2019-10-16 03:59:21', NULL);
/*!40000 ALTER TABLE `appointment_forms` ENABLE KEYS */;

-- Dumping structure for table pcw_db.appointment_issued
DROP TABLE IF EXISTS `appointment_issued`;
CREATE TABLE IF NOT EXISTS `appointment_issued` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) NOT NULL DEFAULT '0',
  `employee_status` int(11) NOT NULL DEFAULT '0',
  `date_issued` date DEFAULT NULL,
  `period_of_employment_from` date DEFAULT NULL,
  `period_of_employment_to` date DEFAULT NULL,
  `nature_of_appointment` int(11) NOT NULL DEFAULT '0',
  `appointing_officer` varchar(225) DEFAULT NULL,
  `publication_date_from` date DEFAULT NULL,
  `publication_date_to` date DEFAULT NULL,
  `hrmo` varchar(225) DEFAULT NULL,
  `date_sign` date DEFAULT NULL,
  `chairperson` varchar(225) DEFAULT NULL,
  `chairperson_date_sign` date DEFAULT NULL,
  `hrmo_date_sign` date DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Dumping data for table pcw_db.appointment_issued: ~1 rows (approximately)
/*!40000 ALTER TABLE `appointment_issued` DISABLE KEYS */;
INSERT INTO `appointment_issued` (`id`, `applicant_id`, `employee_status`, `date_issued`, `period_of_employment_from`, `period_of_employment_to`, `nature_of_appointment`, `appointing_officer`, `publication_date_from`, `publication_date_to`, `hrmo`, `date_sign`, `chairperson`, `chairperson_date_sign`, `hrmo_date_sign`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(3, 3, 0, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, '2019-10-16 04:02:31', '2019-10-16 04:02:31', NULL);
/*!40000 ALTER TABLE `appointment_issued` ENABLE KEYS */;

-- Dumping structure for table pcw_db.appointment_processing
DROP TABLE IF EXISTS `appointment_processing`;
CREATE TABLE IF NOT EXISTS `appointment_processing` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) NOT NULL,
  `educ_qualification` varchar(225) DEFAULT NULL,
  `educ_remarks` varchar(225) DEFAULT NULL,
  `educ_check` int(11) DEFAULT '0',
  `exp_qualification` varchar(225) DEFAULT NULL,
  `exp_remarks` varchar(225) DEFAULT NULL,
  `exp_check` int(11) DEFAULT '0',
  `training_qualification` varchar(225) DEFAULT NULL,
  `training_remarks` varchar(225) DEFAULT NULL,
  `training_check` int(11) DEFAULT '0',
  `eligibility_qualification` varchar(225) DEFAULT NULL,
  `eligibility_remarks` varchar(225) DEFAULT NULL,
  `eligibility_check` int(11) DEFAULT '0',
  `other_qualification` varchar(225) DEFAULT NULL,
  `other_remarks` varchar(225) DEFAULT NULL,
  `other_check` int(11) DEFAULT '0',
  `ra_form_33` varchar(225) DEFAULT NULL,
  `ra_employee_status` varchar(225) DEFAULT NULL,
  `ra_nature_appointment` varchar(225) DEFAULT NULL,
  `ra_appointing_authority` varchar(225) DEFAULT NULL,
  `ra_date_sign` date DEFAULT NULL,
  `ra_date_publication` date DEFAULT NULL,
  `ra_certification` varchar(225) DEFAULT NULL,
  `ra_pds` varchar(225) DEFAULT NULL,
  `ra_eligibility` varchar(225) DEFAULT NULL,
  `ra_position_description` varchar(225) DEFAULT NULL,
  `ar_01` varchar(225) DEFAULT NULL,
  `ar_02` varchar(225) DEFAULT NULL,
  `ar_03` varchar(225) DEFAULT NULL,
  `ar_04` varchar(225) DEFAULT NULL,
  `ar_05` varchar(225) DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Dumping data for table pcw_db.appointment_processing: ~0 rows (approximately)
/*!40000 ALTER TABLE `appointment_processing` DISABLE KEYS */;
/*!40000 ALTER TABLE `appointment_processing` ENABLE KEYS */;

-- Dumping structure for table pcw_db.appointment_requirements
DROP TABLE IF EXISTS `appointment_requirements`;
CREATE TABLE IF NOT EXISTS `appointment_requirements` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) DEFAULT NULL,
  `pds_path` varchar(225) DEFAULT NULL,
  `saln_path` varchar(225) DEFAULT NULL,
  `eligibility_path` varchar(225) DEFAULT NULL,
  `training_path` varchar(225) DEFAULT NULL,
  `psa_path` varchar(225) DEFAULT NULL,
  `tor_path` varchar(225) DEFAULT NULL,
  `diploma_path` varchar(225) DEFAULT NULL,
  `philhealth_path` varchar(225) DEFAULT NULL,
  `nbi_path` varchar(225) DEFAULT NULL,
  `medical_path` varchar(225) DEFAULT NULL,
  `bir_path` varchar(225) DEFAULT NULL,
  `coe_path` varchar(225) DEFAULT NULL,
  `tin_number` varchar(225) DEFAULT NULL,
  `pagibig_number` varchar(225) DEFAULT NULL,
  `complied` int(11) DEFAULT '0',
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Dumping data for table pcw_db.appointment_requirements: ~1 rows (approximately)
/*!40000 ALTER TABLE `appointment_requirements` DISABLE KEYS */;
INSERT INTO `appointment_requirements` (`id`, `applicant_id`, `pds_path`, `saln_path`, `eligibility_path`, `training_path`, `psa_path`, `tor_path`, `diploma_path`, `philhealth_path`, `nbi_path`, `medical_path`, `bir_path`, `coe_path`, `tin_number`, `pagibig_number`, `complied`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(2, 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, '2019-10-16 03:58:47', '2019-10-16 03:59:24', NULL);
/*!40000 ALTER TABLE `appointment_requirements` ENABLE KEYS */;

-- Dumping structure for table pcw_db.assumptions
DROP TABLE IF EXISTS `assumptions`;
CREATE TABLE IF NOT EXISTS `assumptions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) NOT NULL,
  `head_of_office` varchar(225) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `attested_by` varchar(225) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `assumption_date` date DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table pcw_db.assumptions: ~1 rows (approximately)
/*!40000 ALTER TABLE `assumptions` DISABLE KEYS */;
INSERT INTO `assumptions` (`id`, `applicant_id`, `head_of_office`, `attested_by`, `assumption_date`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(2, 3, NULL, NULL, NULL, NULL, 1, '2019-10-16 03:59:23', '2019-10-16 03:59:23', NULL);
/*!40000 ALTER TABLE `assumptions` ENABLE KEYS */;

-- Dumping structure for table pcw_db.attestations
DROP TABLE IF EXISTS `attestations`;
CREATE TABLE IF NOT EXISTS `attestations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) NOT NULL,
  `agency_receiving_offer` varchar(225) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `action_employee_status` int(11) DEFAULT NULL,
  `period_from` date DEFAULT NULL,
  `period_to` date DEFAULT NULL,
  `date_action` date DEFAULT NULL,
  `date_release` date DEFAULT NULL,
  `date_issuance` date DEFAULT NULL,
  `publication_from` date DEFAULT NULL,
  `publication_to` date DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table pcw_db.attestations: ~0 rows (approximately)
/*!40000 ALTER TABLE `attestations` DISABLE KEYS */;
/*!40000 ALTER TABLE `attestations` ENABLE KEYS */;

-- Dumping structure for table pcw_db.background_checkings
DROP TABLE IF EXISTS `background_checkings`;
CREATE TABLE IF NOT EXISTS `background_checkings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) DEFAULT NULL,
  `provided_reference` int(11) DEFAULT '0',
  `reference_check` int(11) DEFAULT '0',
  `permission` int(11) DEFAULT '0',
  `applicant_relationship` varchar(225) DEFAULT NULL,
  `applicant_capacity` varchar(225) DEFAULT NULL,
  `date_from` date DEFAULT NULL,
  `date_to` date DEFAULT NULL,
  `duties_and_responsibilitties` varchar(225) DEFAULT NULL,
  `have_pending_case` varchar(225) DEFAULT NULL,
  `applicant_satisfactory` varchar(225) DEFAULT NULL,
  `reason_for_leaving` varchar(225) DEFAULT NULL,
  `applicant_adaptability` varchar(225) DEFAULT NULL,
  `applicant_work_performance` varchar(225) DEFAULT NULL,
  `applicant_contribution` varchar(225) DEFAULT NULL,
  `performance_concerns` varchar(225) DEFAULT NULL,
  `comments` varchar(225) DEFAULT NULL,
  `job_requirements` varchar(225) DEFAULT NULL,
  `reemploy_the_applicant` varchar(225) DEFAULT NULL,
  `final_comments` varchar(225) DEFAULT NULL,
  `interview_contact` varchar(225) DEFAULT NULL,
  `character_reference` varchar(225) DEFAULT NULL,
  `hr_staff_name` varchar(225) DEFAULT NULL,
  `use_of_profance_language` int(11) DEFAULT '0',
  `drug_user` int(11) DEFAULT '0',
  `risque_photos` int(11) DEFAULT '0',
  `discriminating_comments` int(11) DEFAULT '0',
  `bullying_statement` int(11) DEFAULT '0',
  `observe_grammar` int(11) DEFAULT '0',
  `sharing_confidential_information` int(11) DEFAULT '0',
  `condacted_by` varchar(225) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Dumping data for table pcw_db.background_checkings: ~1 rows (approximately)
/*!40000 ALTER TABLE `background_checkings` DISABLE KEYS */;
INSERT INTO `background_checkings` (`id`, `applicant_id`, `provided_reference`, `reference_check`, `permission`, `applicant_relationship`, `applicant_capacity`, `date_from`, `date_to`, `duties_and_responsibilitties`, `have_pending_case`, `applicant_satisfactory`, `reason_for_leaving`, `applicant_adaptability`, `applicant_work_performance`, `applicant_contribution`, `performance_concerns`, `comments`, `job_requirements`, `reemploy_the_applicant`, `final_comments`, `interview_contact`, `character_reference`, `hr_staff_name`, `use_of_profance_language`, `drug_user`, `risque_photos`, `discriminating_comments`, `bullying_statement`, `observe_grammar`, `sharing_confidential_information`, `condacted_by`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(2, 3, 0, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, NULL, 1, NULL, '2019-10-16 03:46:36', '2019-10-16 03:46:36', NULL);
/*!40000 ALTER TABLE `background_checkings` ENABLE KEYS */;

-- Dumping structure for table pcw_db.boarding_applicants
DROP TABLE IF EXISTS `boarding_applicants`;
CREATE TABLE IF NOT EXISTS `boarding_applicants` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `start_time` varchar(50) DEFAULT NULL,
  `board_status` int(11) DEFAULT '0',
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Dumping data for table pcw_db.boarding_applicants: ~0 rows (approximately)
/*!40000 ALTER TABLE `boarding_applicants` DISABLE KEYS */;
/*!40000 ALTER TABLE `boarding_applicants` ENABLE KEYS */;

-- Dumping structure for table pcw_db.educations
DROP TABLE IF EXISTS `educations`;
CREATE TABLE IF NOT EXISTS `educations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) DEFAULT NULL,
  `school_name` varchar(225) DEFAULT NULL,
  `course` varchar(225) DEFAULT NULL,
  `attendance_from` date DEFAULT NULL,
  `attendance_to` date DEFAULT NULL,
  `level` varchar(225) DEFAULT NULL,
  `graduated` varchar(225) DEFAULT NULL,
  `awards` varchar(225) DEFAULT NULL,
  `educ_level` int(11) DEFAULT NULL,
  `ongoing` int(11) DEFAULT '0',
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

-- Dumping data for table pcw_db.educations: ~3 rows (approximately)
/*!40000 ALTER TABLE `educations` DISABLE KEYS */;
INSERT INTO `educations` (`id`, `applicant_id`, `school_name`, `course`, `attendance_from`, `attendance_to`, `level`, `graduated`, `awards`, `educ_level`, `ongoing`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(7, 3, 'Moriones Elem School', NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, NULL, '2019-10-14 13:30:05', '2019-10-14 13:30:05', NULL),
	(8, 3, 'Manila High School', NULL, NULL, NULL, NULL, NULL, NULL, 2, 0, NULL, NULL, '2019-10-14 13:30:05', '2019-10-14 13:30:05', NULL),
	(9, 3, 'STI College', 'BS IT', NULL, NULL, NULL, NULL, NULL, 4, 0, NULL, NULL, '2019-10-14 13:30:05', '2019-10-14 13:30:05', NULL);
/*!40000 ALTER TABLE `educations` ENABLE KEYS */;

-- Dumping structure for table pcw_db.eligibilities
DROP TABLE IF EXISTS `eligibilities`;
CREATE TABLE IF NOT EXISTS `eligibilities` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) DEFAULT NULL,
  `eligibility_ref` varchar(225) DEFAULT NULL,
  `other_specify` varchar(225) DEFAULT NULL,
  `rating` varchar(225) DEFAULT NULL,
  `exam_place` varchar(225) DEFAULT NULL,
  `license_number` varchar(225) DEFAULT NULL,
  `license_validity` varchar(225) DEFAULT NULL,
  `exam_date` varchar(225) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

-- Dumping data for table pcw_db.eligibilities: ~1 rows (approximately)
/*!40000 ALTER TABLE `eligibilities` DISABLE KEYS */;
INSERT INTO `eligibilities` (`id`, `applicant_id`, `eligibility_ref`, `other_specify`, `rating`, `exam_place`, `license_number`, `license_validity`, `exam_date`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(8, 3, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-10-14 13:30:05', '2019-10-14 13:30:05', NULL);
/*!40000 ALTER TABLE `eligibilities` ENABLE KEYS */;

-- Dumping structure for table pcw_db.erasure_alterations
DROP TABLE IF EXISTS `erasure_alterations`;
CREATE TABLE IF NOT EXISTS `erasure_alterations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) DEFAULT NULL,
  `from_date` date DEFAULT NULL,
  `to_date` date DEFAULT NULL,
  `particulars` text,
  `appointing_officer` text,
  `sign_date` date DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Dumping data for table pcw_db.erasure_alterations: ~0 rows (approximately)
/*!40000 ALTER TABLE `erasure_alterations` DISABLE KEYS */;
/*!40000 ALTER TABLE `erasure_alterations` ENABLE KEYS */;

-- Dumping structure for table pcw_db.evaluations
DROP TABLE IF EXISTS `evaluations`;
CREATE TABLE IF NOT EXISTS `evaluations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) NOT NULL,
  `job_id` int(10) unsigned NOT NULL,
  `performance` int(10) unsigned DEFAULT NULL,
  `performance_divide` int(10) unsigned DEFAULT NULL,
  `performance_average` int(10) unsigned DEFAULT NULL,
  `performance_percent` int(10) unsigned DEFAULT NULL,
  `performance_score` int(10) unsigned DEFAULT NULL,
  `eligibility` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `training` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seminar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `minimum_education_points` int(10) unsigned DEFAULT NULL,
  `minimum_training_points` int(10) unsigned DEFAULT NULL,
  `education_points` int(10) unsigned DEFAULT NULL,
  `training_points` int(10) unsigned DEFAULT NULL,
  `education_training_total_points` int(10) unsigned DEFAULT NULL,
  `education_training_percent` int(10) unsigned DEFAULT NULL,
  `education_training_score` int(10) unsigned DEFAULT NULL,
  `relevant_positions_held` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `minimum_experience_requirement` int(10) unsigned DEFAULT NULL,
  `additional_points` int(10) unsigned DEFAULT NULL,
  `experience_accomplishments_total_points` int(10) unsigned DEFAULT NULL,
  `experience_accomplishments_percent` int(10) unsigned DEFAULT NULL,
  `experience_accomplishments_score` int(10) unsigned DEFAULT NULL,
  `potential` int(10) unsigned DEFAULT NULL,
  `potential_average_rating` decimal(5,2) DEFAULT NULL,
  `potential_percentage_rating` int(10) unsigned DEFAULT NULL,
  `potential_percent` int(10) unsigned DEFAULT NULL,
  `potential_score` int(10) unsigned DEFAULT NULL,
  `psychosocial` int(10) unsigned DEFAULT NULL,
  `psychosocial_average_rating` int(10) unsigned DEFAULT NULL,
  `psychosocial_percentage_rating` int(10) unsigned DEFAULT NULL,
  `psychosocial_percent` int(10) unsigned DEFAULT NULL,
  `psychosocial_score` int(10) unsigned DEFAULT NULL,
  `total_percent` int(10) unsigned DEFAULT NULL,
  `total_score` decimal(5,2) DEFAULT NULL,
  `evaluated_by` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reviewed_by` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `noted_by` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `recommended` int(10) unsigned DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table pcw_db.evaluations: ~0 rows (approximately)
/*!40000 ALTER TABLE `evaluations` DISABLE KEYS */;
/*!40000 ALTER TABLE `evaluations` ENABLE KEYS */;

-- Dumping structure for table pcw_db.examinations
DROP TABLE IF EXISTS `examinations`;
CREATE TABLE IF NOT EXISTS `examinations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) DEFAULT NULL,
  `exam_date` date DEFAULT NULL,
  `exam_time` varchar(50) DEFAULT NULL,
  `exam_location` varchar(50) DEFAULT NULL,
  `average_raw_score` varchar(50) DEFAULT NULL,
  `sub_total_rating` varchar(50) DEFAULT NULL,
  `resched_exam_date` date DEFAULT NULL,
  `resched_exam_time` varchar(50) DEFAULT NULL,
  `exam_status` int(11) DEFAULT NULL,
  `notify` int(11) DEFAULT NULL,
  `notify_resched_exam` int(11) DEFAULT '0',
  `notify_exam_status` int(11) DEFAULT '0',
  `confirmed` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Dumping data for table pcw_db.examinations: ~1 rows (approximately)
/*!40000 ALTER TABLE `examinations` DISABLE KEYS */;
INSERT INTO `examinations` (`id`, `applicant_id`, `exam_date`, `exam_time`, `exam_location`, `average_raw_score`, `sub_total_rating`, `resched_exam_date`, `resched_exam_time`, `exam_status`, `notify`, `notify_resched_exam`, `notify_exam_status`, `confirmed`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(2, 3, '2019-10-18', '10:20AM', 'Manila', NULL, NULL, NULL, NULL, 6, 0, 0, 0, 0, 1, 1, '2019-10-16 03:12:25', '2019-10-16 03:53:34', NULL);
/*!40000 ALTER TABLE `examinations` ENABLE KEYS */;

-- Dumping structure for table pcw_db.interviews
DROP TABLE IF EXISTS `interviews`;
CREATE TABLE IF NOT EXISTS `interviews` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) DEFAULT NULL,
  `interview_date` date DEFAULT NULL,
  `interview_time` varchar(50) DEFAULT NULL,
  `interview_location` varchar(50) DEFAULT NULL,
  `resched_interview_date` date DEFAULT NULL,
  `resched_interview_time` varchar(50) DEFAULT NULL,
  `interview_status` int(11) DEFAULT NULL,
  `notify` int(11) DEFAULT NULL,
  `noftiy_resched_interview` int(11) DEFAULT NULL,
  `confirmed` int(11) DEFAULT NULL,
  `cost_for_hire` decimal(9,2) DEFAULT NULL,
  `average_raw_score` varchar(225) DEFAULT NULL,
  `sub_total_rating` varchar(225) DEFAULT NULL,
  `psb_chairperson` varchar(225) DEFAULT NULL,
  `psb_secretariat` varchar(225) DEFAULT NULL,
  `psb_member` varchar(225) DEFAULT NULL,
  `psm_sweap_rep` varchar(225) DEFAULT NULL,
  `psb_end_user` varchar(225) DEFAULT NULL,
  `date_approved` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Dumping data for table pcw_db.interviews: ~1 rows (approximately)
/*!40000 ALTER TABLE `interviews` DISABLE KEYS */;
INSERT INTO `interviews` (`id`, `applicant_id`, `interview_date`, `interview_time`, `interview_location`, `resched_interview_date`, `resched_interview_time`, `interview_status`, `notify`, `noftiy_resched_interview`, `confirmed`, `cost_for_hire`, `average_raw_score`, `sub_total_rating`, `psb_chairperson`, `psb_secretariat`, `psb_member`, `psm_sweap_rep`, `psb_end_user`, `date_approved`, `created_by`, `updated_by`, `created_at`, `deleted_at`, `updated_at`) VALUES
	(2, 3, '2019-10-18', '10:00AM', 'Manila', NULL, NULL, 6, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-10-16 03:54:25', 1, NULL, '2019-10-16 03:54:25', NULL, '2019-10-16 03:54:25');
/*!40000 ALTER TABLE `interviews` ENABLE KEYS */;

-- Dumping structure for table pcw_db.interview_guides
DROP TABLE IF EXISTS `interview_guides`;
CREATE TABLE IF NOT EXISTS `interview_guides` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) DEFAULT NULL,
  `interviewer` varchar(225) DEFAULT NULL,
  `question_point_1` decimal(9,2) DEFAULT NULL,
  `question_point_2` decimal(9,2) DEFAULT NULL,
  `question_point_3` decimal(9,2) DEFAULT NULL,
  `question_point_4` decimal(9,2) DEFAULT NULL,
  `question_point_5` decimal(9,2) DEFAULT NULL,
  `question_point_6` decimal(9,2) DEFAULT NULL,
  `comments` text,
  `total_score` decimal(9,2) DEFAULT NULL,
  `date_created` date DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Dumping data for table pcw_db.interview_guides: ~1 rows (approximately)
/*!40000 ALTER TABLE `interview_guides` DISABLE KEYS */;
INSERT INTO `interview_guides` (`id`, `applicant_id`, `interviewer`, `question_point_1`, `question_point_2`, `question_point_3`, `question_point_4`, `question_point_5`, `question_point_6`, `comments`, `total_score`, `date_created`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(2, 3, 'Juan Dela Cruz', 20.00, 10.00, 10.00, 10.00, 10.00, 10.00, 'Test Comment', 70.00, '2019-10-16', 1, NULL, '2019-10-16 03:55:59', '2019-10-16 03:55:59', NULL);
/*!40000 ALTER TABLE `interview_guides` ENABLE KEYS */;

-- Dumping structure for table pcw_db.jobs
DROP TABLE IF EXISTS `jobs`;
CREATE TABLE IF NOT EXISTS `jobs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `plantilla_item_id` int(10) unsigned DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `education` text COLLATE utf8mb4_unicode_ci,
  `experience` text COLLATE utf8mb4_unicode_ci,
  `training` text COLLATE utf8mb4_unicode_ci,
  `eligibility` text COLLATE utf8mb4_unicode_ci,
  `duties_responsibilities` text COLLATE utf8mb4_unicode_ci,
  `key_competencies` text COLLATE utf8mb4_unicode_ci,
  `monthly_basic_salary` decimal(10,3) DEFAULT '0.000',
  `daily_salary` decimal(12,3) DEFAULT '0.000',
  `pera_amount` decimal(12,3) DEFAULT '0.000',
  `clothing_amount` decimal(12,3) DEFAULT '0.000',
  `midyear_amount` decimal(12,3) DEFAULT '0.000',
  `yearend_amount` decimal(12,3) DEFAULT '0.000',
  `cashgift_amount` decimal(12,3) DEFAULT '0.000',
  `professional_fees` decimal(12,3) DEFAULT '0.000',
  `status` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `station` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reporting_line` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cos_position_title` varchar(225) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `other_specify` varchar(225) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `requirements` text COLLATE utf8mb4_unicode_ci,
  `compentency_1` text COLLATE utf8mb4_unicode_ci,
  `compentency_2` text COLLATE utf8mb4_unicode_ci,
  `compentency_3` text COLLATE utf8mb4_unicode_ci,
  `compentency_4` text COLLATE utf8mb4_unicode_ci,
  `compentency_5` text COLLATE utf8mb4_unicode_ci,
  `other_qualification` text COLLATE utf8mb4_unicode_ci,
  `csc_education` text COLLATE utf8mb4_unicode_ci,
  `csc_work_experience` text COLLATE utf8mb4_unicode_ci,
  `csc_eligibility` text COLLATE utf8mb4_unicode_ci,
  `csc_training` text COLLATE utf8mb4_unicode_ci,
  `expires` timestamp NULL DEFAULT NULL,
  `publish_date` date DEFAULT NULL,
  `deadline_date` date DEFAULT NULL,
  `duration_from` date DEFAULT NULL,
  `duration_to` date DEFAULT NULL,
  `approved_date` date DEFAULT NULL,
  `publish` tinyint(1) NOT NULL DEFAULT '0',
  `publication_1` int(11) NOT NULL DEFAULT '0',
  `publication_2` int(11) NOT NULL DEFAULT '0',
  `publication_3` int(11) NOT NULL DEFAULT '0',
  `publication_4` int(11) NOT NULL DEFAULT '0',
  `appointer_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table pcw_db.jobs: ~3 rows (approximately)
/*!40000 ALTER TABLE `jobs` DISABLE KEYS */;
INSERT INTO `jobs` (`id`, `plantilla_item_id`, `description`, `education`, `experience`, `training`, `eligibility`, `duties_responsibilities`, `key_competencies`, `monthly_basic_salary`, `daily_salary`, `pera_amount`, `clothing_amount`, `midyear_amount`, `yearend_amount`, `cashgift_amount`, `professional_fees`, `status`, `station`, `reporting_line`, `cos_position_title`, `other_specify`, `requirements`, `compentency_1`, `compentency_2`, `compentency_3`, `compentency_4`, `compentency_5`, `other_qualification`, `csc_education`, `csc_work_experience`, `csc_eligibility`, `csc_training`, `expires`, `publish_date`, `deadline_date`, `duration_from`, `duration_to`, `approved_date`, `publish`, `publication_1`, `publication_2`, `publication_3`, `publication_4`, `appointer_id`, `created_at`, `updated_at`, `created_by`, `updated_by`, `deleted_at`) VALUES
	(4, 25, NULL, NULL, '<div>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod</div><div>tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,</div><div>quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo</div><div>consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse</div><div>cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non</div><div>proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</div>', NULL, NULL, NULL, NULL, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'nonplantilla', NULL, NULL, NULL, NULL, NULL, '<div>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod</div><div>tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,</div><div>quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo</div><div>consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse</div><div>cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non</div><div>proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</div>', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-09-14', NULL, NULL, NULL, 1, 1, 0, 0, 0, 117, '2019-08-31 08:06:49', '2019-08-31 08:20:31', 1, NULL, NULL),
	(5, 23, NULL, '<div>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod</div><div>tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,</div><div>quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo</div><div>consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse</div><div>cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non</div><div>proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</div>', NULL, NULL, NULL, NULL, NULL, 0.000, 0.000, 2000.000, 6000.000, 12674.000, 12674.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, 'FB', NULL, '<div>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod</div><div>tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,</div><div>quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo</div><div>consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse</div><div>cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non</div><div>proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</div>', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-10-07', '2019-09-07', NULL, NULL, '2019-09-02', 1, 1, 0, 0, 1, 117, '2019-08-31 08:07:12', '2019-10-07 04:35:58', 1, NULL, NULL),
	(6, 79, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'nonplantilla', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-10-08', '2019-11-23', NULL, NULL, '2019-10-08', 1, 1, 0, 1, 0, 91, '2019-10-08 09:44:29', '2019-10-08 09:44:29', 1, NULL, NULL);
/*!40000 ALTER TABLE `jobs` ENABLE KEYS */;

-- Dumping structure for table pcw_db.job_offers
DROP TABLE IF EXISTS `job_offers`;
CREATE TABLE IF NOT EXISTS `job_offers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) NOT NULL,
  `pera_amount` decimal(13,2) DEFAULT NULL,
  `clothing_allowance` decimal(13,2) DEFAULT NULL,
  `year_end_bonus` decimal(13,2) DEFAULT NULL,
  `cash_gift` decimal(13,2) DEFAULT NULL,
  `executive_director` varchar(225) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `job_offer_date` date DEFAULT NULL,
  `joboffer_status` int(11) NOT NULL DEFAULT '0',
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table pcw_db.job_offers: ~0 rows (approximately)
/*!40000 ALTER TABLE `job_offers` DISABLE KEYS */;
/*!40000 ALTER TABLE `job_offers` ENABLE KEYS */;

-- Dumping structure for table pcw_db.matrix_qualifications
DROP TABLE IF EXISTS `matrix_qualifications`;
CREATE TABLE IF NOT EXISTS `matrix_qualifications` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) DEFAULT NULL,
  `remarks` varchar(50) DEFAULT NULL,
  `status` int(11) DEFAULT '0',
  `isc_chairperson` varchar(50) DEFAULT NULL,
  `isc_member_one` varchar(50) DEFAULT NULL,
  `isc_member_two` varchar(50) DEFAULT NULL,
  `ea_representative` varchar(50) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table pcw_db.matrix_qualifications: ~0 rows (approximately)
/*!40000 ALTER TABLE `matrix_qualifications` DISABLE KEYS */;
/*!40000 ALTER TABLE `matrix_qualifications` ENABLE KEYS */;

-- Dumping structure for table pcw_db.oath_offices
DROP TABLE IF EXISTS `oath_offices`;
CREATE TABLE IF NOT EXISTS `oath_offices` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) NOT NULL,
  `person_administering` varchar(225) DEFAULT NULL,
  `oath_date` date DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Dumping data for table pcw_db.oath_offices: ~1 rows (approximately)
/*!40000 ALTER TABLE `oath_offices` DISABLE KEYS */;
INSERT INTO `oath_offices` (`id`, `applicant_id`, `person_administering`, `oath_date`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(2, 3, NULL, NULL, NULL, 1, '2019-10-16 03:59:24', '2019-10-16 03:59:24', NULL);
/*!40000 ALTER TABLE `oath_offices` ENABLE KEYS */;

-- Dumping structure for table pcw_db.psipop
DROP TABLE IF EXISTS `psipop`;
CREATE TABLE IF NOT EXISTS `psipop` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `item_number` varchar(225) DEFAULT NULL,
  `incumbent_name` varchar(225) DEFAULT NULL,
  `mode_of_separation` varchar(225) DEFAULT NULL,
  `date_vacated` date DEFAULT NULL,
  `position_id` int(11) DEFAULT NULL,
  `office_id` int(11) DEFAULT NULL,
  `division_id` int(11) DEFAULT NULL,
  `department_id` int(11) DEFAULT NULL,
  `employee_status_id` int(11) DEFAULT NULL,
  `salary_grade_id` int(11) DEFAULT NULL,
  `step_id` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT '0',
  `basic_salary` decimal(13,2) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=80 DEFAULT CHARSET=latin1;

-- Dumping data for table pcw_db.psipop: ~78 rows (approximately)
/*!40000 ALTER TABLE `psipop` DISABLE KEYS */;
INSERT INTO `psipop` (`id`, `item_number`, `incumbent_name`, `mode_of_separation`, `date_vacated`, `position_id`, `office_id`, `division_id`, `department_id`, `employee_status_id`, `salary_grade_id`, `step_id`, `status`, `basic_salary`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
	(2, 'NCRFWB-EXED3-1-1998', 'VERZOSA, EMMELINE LAHOZ', NULL, NULL, 1, NULL, 1, NULL, 1, 27, 5, 1, 122031.00, NULL, NULL, NULL, NULL),
	(3, 'NCRFWB-DED3-1-1998', 'GUTIERREZ, CECILE BALTAZAR', NULL, NULL, 2, NULL, 1, NULL, 1, 26, 3, 1, 106091.00, NULL, NULL, NULL, NULL),
	(4, 'NCRFWB-DED3-1-2004', 'BALMES, MARIA KRISTINE JOSEFINA GONDA', NULL, NULL, 2, NULL, 1, NULL, 1, 26, 1, 1, 102910.00, NULL, NULL, NULL, NULL),
	(5, 'NCRFWB-CADOF-9-2004', 'ETRATA, LOLITA ESPINA', NULL, NULL, 3, NULL, 2, NULL, 1, 23, 1, 1, 73299.00, NULL, NULL, NULL, NULL),
	(6, 'NCRFWB-ATY3-1-2016', 'CUEVAS, MAY ZYRA VILLAS', NULL, NULL, 4, NULL, 1, NULL, 1, 20, 1, 1, 52554.00, NULL, NULL, NULL, NULL),
	(7, 'NCRFWB-ATY3-2-2016', 'SALDANA, BIANCA BALAJORO', NULL, NULL, 4, NULL, 1, NULL, 1, 20, 1, 1, 52554.00, NULL, NULL, NULL, NULL),
	(8, 'NCRFWB-A3-1-1998', 'CATUBAG, CHARY GRACE CAISIDO', NULL, NULL, 5, NULL, 2, NULL, 1, 18, 1, 1, 42099.00, NULL, NULL, NULL, NULL),
	(9, 'NCRFWB-ADOF5-10-2004', 'ALVIS, RICA BRAÃ‘A', NULL, NULL, 6, NULL, 2, NULL, 1, 17, 8, 1, 41413.00, NULL, NULL, NULL, NULL),
	(10, 'NCRFWB-ADOF5-11-2004', 'TASONG, MARIA CALAMBA', NULL, NULL, 6, NULL, 2, NULL, 1, 17, 2, 1, 38543.00, NULL, NULL, NULL, NULL),
	(11, 'NCRFWB-ADOF5-12-2004', 'CAASI, MARIA THERESA ESTELLA', NULL, NULL, 6, NULL, 2, NULL, 1, 17, 2, 1, 38543.00, NULL, NULL, NULL, NULL),
	(12, 'NCRFWB-A2-1-1998', 'MANUEL, NATHANAEL ANTONIO', NULL, NULL, 7, 0, 0, 0, 1, 15, 1, 1, 31765.00, NULL, 1, NULL, '2019-08-31 07:23:16'),
	(13, 'NCRFWB-ADOF3-14-2004', 'MAANO, LEANDRA ARLENE ALBA', NULL, NULL, 8, NULL, 2, NULL, 1, 13, 2, 1, 26806.00, NULL, NULL, NULL, NULL),
	(14, 'NCRFWB-ADOF3-15-2004', 'PADAYAO, CLARITA CARAYUGAN', NULL, NULL, 8, NULL, 2, NULL, 1, 13, 2, 1, 26806.00, NULL, NULL, NULL, NULL),
	(15, 'NCRFWB-SADAS2-1-2004', 'RAMIREZ, LISA ROBEL', NULL, NULL, 9, NULL, 2, NULL, 1, 13, 2, 1, 26806.00, NULL, NULL, NULL, NULL),
	(16, 'NCRFWB-ADAS2-5-2004', 'OBUSAN, JOANN PAJARES', NULL, NULL, 10, NULL, 1, NULL, 1, 33, 1, 1, 16282.00, NULL, NULL, NULL, NULL),
	(17, 'NCRFWB-ADAS2-6-2004', 'PUNZALAN, MICAELA MORAL', NULL, NULL, 10, NULL, 2, NULL, 1, 33, 8, 1, 17369.00, NULL, NULL, NULL, NULL),
	(18, 'NCRFWB-ADAS2-7-2004', 'PAPASIN, EMORIE MUYOT', NULL, NULL, 10, NULL, 2, NULL, 1, 33, 8, 1, 17369.00, NULL, NULL, NULL, NULL),
	(19, 'NCRFWB-ADAS1-6-2004', 'SILVERIO, REY BALMACEDA', NULL, NULL, 11, NULL, 2, NULL, 1, 7, 2, 1, 15380.00, NULL, NULL, NULL, NULL),
	(20, 'NCRFWB-ADA6-3-2004', 'OCONNOR, BERNADETTE ROLDAN', NULL, NULL, 12, NULL, 2, NULL, 1, 6, 2, 1, 14459.00, NULL, NULL, NULL, NULL),
	(21, 'NCRFWB-ADA4-8-2004', NULL, NULL, NULL, 13, NULL, 2, NULL, 1, 4, 8, 0, 13424.00, NULL, NULL, NULL, NULL),
	(22, 'NCRFWB-ADA4-13-2004', 'GALLANO, ALBERT DINO', NULL, NULL, 13, NULL, 2, NULL, 1, 4, 5, 1, 13097.00, NULL, NULL, NULL, NULL),
	(23, 'NCRFWB-ADA4-14-2004', NULL, NULL, NULL, 72, 13, 1, 0, 1, 4, 1, 0, 12674.00, NULL, 1, NULL, '2019-08-31 08:03:51'),
	(24, 'NCRFWB-ADA4-12-2004', 'MADOLI, MA. RAESSA MASAGANDA', NULL, NULL, 13, NULL, 2, NULL, 1, 4, 1, 1, 12674.00, NULL, NULL, NULL, NULL),
	(25, 'NCRFWB-ADA4-11-2004', 'Jhon Doe', 'Resign', '2019-09-05', 238, 8, 14, 0, 2, 4, 1, 0, 12674.00, NULL, 1, NULL, '2019-09-05 04:38:43'),
	(26, 'NCRFWB-ADA3-15-2004', 'ABILAY, VIRGILIO GARIN', NULL, NULL, 14, NULL, 2, NULL, 1, 3, 8, 1, 12620.00, NULL, NULL, NULL, NULL),
	(27, 'NCRFWB-ADA3-16-2004', 'CAPIRAL, ENRIQUE LOPENA', NULL, NULL, 14, NULL, 2, NULL, 1, 3, 8, 1, 12620.00, NULL, NULL, NULL, NULL),
	(28, 'NCRFWB-ADA2-8-2004', 'QUIJANA, ARLYNE INACAY', NULL, NULL, 15, NULL, 2, NULL, 1, 2, 6, 1, 11671.00, NULL, NULL, NULL, NULL),
	(29, 'NCRFWB-INFO5-2-2007', 'CASTRO, HONEY MACAPAGAL', NULL, NULL, 16, NULL, 3, NULL, 1, 23, 1, 1, 73299.00, NULL, NULL, NULL, NULL),
	(30, 'NCRFWB-INFO4-3-2007', NULL, NULL, NULL, 17, NULL, 3, NULL, 1, 21, 2, 0, 59597.00, NULL, NULL, NULL, NULL),
	(31, 'NCRFWB-ITO1-6-2007', 'ATANACIO, VICKY TORRES', NULL, NULL, 18, NULL, 3, NULL, 1, 18, 2, 1, 42730.00, NULL, NULL, NULL, NULL),
	(32, 'NCRFWB-INFO3-1-2008', 'FRANCISCO, ANNE DOMINIQUE DELOS SANTOS', NULL, NULL, 19, NULL, 3, NULL, 1, 17, 2, 1, 38543.00, NULL, NULL, NULL, NULL),
	(33, 'NCRFWB-SADAS3-2-2004', 'SOTTO, ANNA MARIA BAUTISTA', NULL, NULL, 20, NULL, 3, NULL, 1, 14, 2, 1, 29359.00, NULL, NULL, NULL, NULL),
	(34, 'NCRFWB-INFOSA2-7-2007', 'NATIVIDAD, NICO TAGLE', NULL, NULL, 21, NULL, 3, NULL, 1, 15, 2, 1, 32147.00, NULL, NULL, NULL, NULL),
	(35, 'NCRFWB-PLO2-4-2016', 'SANTOS, RAMIL PANGILINAN', NULL, NULL, 22, NULL, 3, NULL, 1, 14, 1, 1, 29010.00, NULL, NULL, NULL, NULL),
	(36, 'NCRFWB-INFOSA1-6-2016', 'ESQUIBAL, MARY GLADYS ANTON', NULL, NULL, 23, NULL, 3, NULL, 1, 11, 1, 1, 22149.00, NULL, NULL, NULL, NULL),
	(37, 'NCRFWB-LIB1-1-2007', 'PASCUAL, JONATHAN ABELLO', NULL, NULL, 24, NULL, 3, NULL, 1, 10, 4, 1, 20963.00, NULL, NULL, NULL, NULL),
	(38, 'NCRFWB-PLO1-5-2016', 'VILLALUNA, MARICAR DULFO', NULL, NULL, 25, NULL, 3, NULL, 1, 10, 1, 1, 20179.00, NULL, NULL, NULL, NULL),
	(39, 'NCRFWB-ADAS3-5-2004', NULL, NULL, NULL, 26, NULL, 3, NULL, 1, 8, 1, 0, 17627.00, NULL, NULL, NULL, NULL),
	(40, 'NCRFWB-ADA6-5-2004', 'GARMINO, RICARDO JAVIER', NULL, NULL, 12, NULL, 3, NULL, 1, 6, 4, 1, 14699.00, NULL, NULL, NULL, NULL),
	(41, 'NCRFWB-CGAD-7-2016', 'MILLAR, NHARLEEN SANTOS', NULL, NULL, 27, NULL, 4, NULL, 1, 23, 3, 1, 75512.00, NULL, NULL, NULL, NULL),
	(42, 'NCRFWB-SVGAD-6-2016', 'SASUMAN, JOSEPHINE KHALEEN MOISES', NULL, NULL, 28, NULL, 4, NULL, 1, 21, 2, 1, 59597.00, NULL, NULL, NULL, NULL),
	(43, 'NCRFWB-SVGAD-7-2016', 'BAYLOSIS, MA. REBECCA RAFAELA REYES', NULL, NULL, 28, NULL, 4, NULL, 1, 21, 1, 1, 58717.00, NULL, NULL, NULL, NULL),
	(44, 'NCRFWB-SRGAD-6-2016', 'BRIGOLA, MARIA JANICA VILLAS', NULL, NULL, 29, NULL, 4, NULL, 1, 17, 1, 1, 38085.00, NULL, NULL, NULL, NULL),
	(45, 'NCRFWB-SRGAD-8-2016', 'SAN JUAN, CLEHENIA AURORA BETCO', NULL, NULL, 29, NULL, 4, NULL, 1, 17, 1, 1, 38085.00, NULL, NULL, NULL, NULL),
	(46, 'NCRFWB-SRGAD-7-2016', 'AREVALO, AVERY SILK SORIANO', NULL, NULL, 29, NULL, 4, NULL, 1, 17, 1, 1, 38085.00, NULL, NULL, NULL, NULL),
	(47, 'NCRFWB-GAD2-4-2016', 'LAGUMBAY, ANASTACIO MALAKI', NULL, NULL, 30, NULL, 4, NULL, 1, 14, 6, 1, 30799.00, NULL, NULL, NULL, NULL),
	(48, 'NCRFWB-GAD2-8-2016', 'LEE, KRISTINE ANNE VIRGO', NULL, NULL, 30, NULL, 4, NULL, 1, 14, 1, 1, 29010.00, NULL, NULL, NULL, NULL),
	(49, 'NCRFWB-GAD2-3-2016', 'ORCILLA, ARMANDO GOMEZ', NULL, NULL, 30, NULL, 4, NULL, 1, 14, 1, 1, 29010.00, NULL, NULL, NULL, NULL),
	(50, 'NCRFWB-GAD2-6-2016', 'ABIOG, ANIKA HANNA BELUANG', NULL, NULL, 30, NULL, 4, NULL, 1, 14, 1, 1, 29010.00, NULL, NULL, NULL, NULL),
	(51, 'NCRFWB-GAD2-5-2016', 'RAYA, VINNA ABEGAIL DARIO', NULL, NULL, 30, NULL, 4, NULL, 1, 14, 1, 1, 29010.00, NULL, NULL, NULL, NULL),
	(52, 'NCRFWB-GAD2-7-2016', 'MALUYA, JENNYLIN NUNEZ', NULL, NULL, 30, NULL, 4, NULL, 1, 14, 1, 1, 29010.00, NULL, NULL, NULL, NULL),
	(53, 'NCRFWB-PLA-1-1998', 'BUAN, RICHELLE CANETE', NULL, NULL, 31, NULL, 4, NULL, 1, 33, 1, 1, 16282.00, NULL, NULL, NULL, NULL),
	(54, 'NCRFWB-ADA4-17-2004', 'TIAUZON, JOMAY FERRER', NULL, NULL, 32, NULL, 4, NULL, 1, 4, 1, 1, 12674.00, NULL, NULL, NULL, NULL),
	(55, 'NCRFWB-CGAD-6-2016', 'BALEDA, ANITA ESTRERA', NULL, NULL, 27, NULL, 5, NULL, 1, 23, 2, 1, 74397.00, NULL, NULL, NULL, NULL),
	(56, 'NCRFWB-SVGAD-5-2016', 'DULAWAN, JEANETTE KINDIPAN', NULL, NULL, 28, NULL, 5, NULL, 1, 21, 3, 1, 60491.00, NULL, NULL, NULL, NULL),
	(57, 'NCRFWB-SVGAD-8-2016', 'MAZO, RAYMOND JAY LACSA', NULL, NULL, 28, NULL, 5, NULL, 1, 21, 1, 1, 58717.00, NULL, NULL, NULL, NULL),
	(58, 'NCRFWB-SRGAD-5-2016', 'DAGNALAN, KAREN GASPI', NULL, NULL, 29, NULL, 4, NULL, 1, 17, 3, 1, 39007.00, NULL, NULL, NULL, NULL),
	(59, 'NCRFWB-SRGAD-4-2016', 'TEODORO, KIMBERLY ANNE EFONDO', NULL, NULL, 29, NULL, 5, NULL, 1, 18, 1, 1, 38085.00, NULL, NULL, NULL, NULL),
	(60, 'NCRFWB-SRGAD-10-2016', 'PEJI, KIM HAROLD TAMAYO', NULL, NULL, 29, NULL, 5, NULL, 1, 19, 1, 1, 38085.00, NULL, NULL, NULL, NULL),
	(61, 'NCRFWB-SRGAD-9-2016', 'HUFANCIA, RONALYN VALENZUELA', NULL, NULL, 29, NULL, 5, NULL, 1, 20, 1, 1, 38085.00, NULL, NULL, NULL, NULL),
	(62, 'NCRFWB-SRGAD-3-2016', 'BELLIN, JOCELYN TOLENTINO', NULL, NULL, 29, NULL, 5, NULL, 1, 21, 1, 1, 38085.00, NULL, NULL, NULL, NULL),
	(63, 'NCRFWB-GAD2-2-2016', 'OMAS-AS, ELIZABETH BALAGOT', NULL, NULL, 30, NULL, 5, NULL, 1, 14, 1, 1, 29010.00, NULL, NULL, NULL, NULL),
	(64, 'NCRFWB-GAD2-9-2016', 'PATIO, WRAKLE SANCHEZ', NULL, NULL, 30, NULL, 5, NULL, 1, 15, 1, 1, 29010.00, NULL, NULL, NULL, NULL),
	(65, 'NCRFWB-GAD2-1-2016', 'MANGA, RHODORA ANGELINE VALDEZ', NULL, NULL, 30, NULL, 5, NULL, 1, 16, 1, 1, 29010.00, NULL, NULL, NULL, NULL),
	(66, 'NCRFWB-ADA4-16-2004', NULL, NULL, NULL, 13, NULL, 5, NULL, 1, 4, 6, 0, 13206.00, NULL, NULL, NULL, NULL),
	(67, 'NCRFWB-CGAD-8-2016', 'JUSAYAN, MACARIO TORRES', NULL, NULL, 27, NULL, 6, NULL, 1, 23, 2, 1, 74397.00, NULL, NULL, NULL, NULL),
	(68, 'NCRFWB-SVGAD-9-2016', 'DELGADO, MARIANNE KRISTINE VITO', NULL, NULL, 28, NULL, 6, NULL, 1, 21, 1, 1, 58717.00, NULL, NULL, NULL, NULL),
	(69, 'NCRFWB-SVGAD-10-2016', 'SUSARA, PAMELA CAPERIÃ‘A', NULL, NULL, 28, NULL, 6, NULL, 1, 22, 1, 1, 58717.00, NULL, NULL, NULL, NULL),
	(70, 'NCRFWB-SRGAD-12-2016', 'CHUCK, SILAYAN TERESITA KINTANAR', NULL, NULL, 29, NULL, 6, NULL, 1, 17, 1, 1, 38085.00, NULL, NULL, NULL, NULL),
	(71, 'NCRFWB-SRGAD-11-2016', 'CORRAL, MILDRED LATOGA', NULL, NULL, 29, NULL, 6, NULL, 1, 18, 1, 1, 38085.00, NULL, NULL, NULL, NULL),
	(72, 'NCRFWB-GAD2-12-2016', 'ESTURAS, CLAIRE RUZZEL ALIBUDBUD', NULL, NULL, 30, NULL, 6, NULL, 1, 14, 1, 1, 29010.00, NULL, NULL, NULL, NULL),
	(73, 'NCRFWB-GAD2-11-2016', 'GANDEZA JR., RENE ANDRADA', NULL, NULL, 30, NULL, 6, NULL, 1, 14, 1, 1, 29010.00, NULL, NULL, NULL, NULL),
	(74, 'NCRFWB-GAD2-13-2016', 'RODRIGUEZ, BERNADETTE DIMACULANGAN', NULL, NULL, 30, NULL, 6, NULL, 1, 14, 1, 1, 29010.00, NULL, NULL, NULL, NULL),
	(75, 'NCRFWB-LIB2-1-1998', NULL, NULL, NULL, 33, NULL, 3, NULL, 1, 14, 4, 0, 30071.00, NULL, NULL, NULL, NULL),
	(76, 'NCRFWB-GAD2-14-2016', NULL, NULL, NULL, 30, NULL, 6, NULL, 1, 14, 1, 0, 29010.00, NULL, NULL, NULL, NULL),
	(77, 'NCRFWB-ADOF3-13-2004', NULL, NULL, NULL, 34, NULL, 2, NULL, 1, 13, 8, 0, 28759.00, NULL, NULL, NULL, NULL),
	(78, 'NCRFWB-ADOF5-3-2016', 'MAYRENA, ROSE JEAN M', NULL, NULL, 53, 7, NULL, NULL, 1, 17, 1, 1, 38085.00, NULL, NULL, NULL, NULL),
	(79, 'COS0001', NULL, NULL, NULL, 42, 12, 7, 1, 2, 14, 1, 0, 18000.00, 1, NULL, '2019-10-08 09:42:02', '2019-10-08 09:42:02');
/*!40000 ALTER TABLE `psipop` ENABLE KEYS */;

-- Dumping structure for table pcw_db.resignation_acceptance
DROP TABLE IF EXISTS `resignation_acceptance`;
CREATE TABLE IF NOT EXISTS `resignation_acceptance` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) DEFAULT NULL,
  `letter_date` date DEFAULT NULL,
  `resignation_date` date DEFAULT NULL,
  `appointing_officer` text,
  `sign_date` date DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table pcw_db.resignation_acceptance: ~0 rows (approximately)
/*!40000 ALTER TABLE `resignation_acceptance` DISABLE KEYS */;
/*!40000 ALTER TABLE `resignation_acceptance` ENABLE KEYS */;

-- Dumping structure for table pcw_db.rms_modules
DROP TABLE IF EXISTS `rms_modules`;
CREATE TABLE IF NOT EXISTS `rms_modules` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(225) DEFAULT NULL,
  `description` varchar(225) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;

-- Dumping data for table pcw_db.rms_modules: ~17 rows (approximately)
/*!40000 ALTER TABLE `rms_modules` DISABLE KEYS */;
INSERT INTO `rms_modules` (`id`, `name`, `description`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 'psipop', 'Itemize & Plantilla', NULL, NULL, NULL, NULL, NULL),
	(2, 'jobs', 'Jobs', NULL, NULL, NULL, NULL, NULL),
	(3, 'applicant', 'Applicants', NULL, NULL, NULL, NULL, NULL),
	(4, 'examinations', 'Examinations', NULL, NULL, NULL, NULL, NULL),
	(5, 'interviews', 'Interviews', NULL, NULL, NULL, NULL, NULL),
	(6, 'appointment-form', 'Appointmnet Form', NULL, NULL, NULL, NULL, NULL),
	(7, 'appointment-issued', 'Appointment Issued', NULL, NULL, NULL, NULL, NULL),
	(8, 'appointment-processing', 'Appointment Processing', NULL, NULL, NULL, NULL, NULL),
	(9, 'appointment-requirements', 'Pre Emp. Requirements', NULL, NULL, NULL, NULL, NULL),
	(10, 'appointment-casual', 'Plantilla of Casual Appointment', NULL, NULL, NULL, NULL, NULL),
	(11, 'position-descriptions', 'Position Description', NULL, NULL, NULL, NULL, NULL),
	(12, 'oath-office', 'Oath of Office', NULL, NULL, NULL, NULL, NULL),
	(13, 'assumption', 'Assumption', NULL, NULL, NULL, NULL, NULL),
	(14, 'erasure_alterations', 'Erasure Alterations', NULL, NULL, NULL, NULL, NULL),
	(15, 'acceptance_resignation', 'Acceptance Resignation', NULL, NULL, NULL, NULL, NULL),
	(16, 'boarding_applicant', 'Applicant Onboarding', NULL, NULL, NULL, NULL, NULL),
	(17, 'report', 'Report', NULL, NULL, NULL, NULL, NULL),
	(18, 'access_modules', 'Access Module', NULL, NULL, NULL, NULL, NULL),
	(19, 'users', 'User', NULL, NULL, NULL, NULL, NULL),
	(20, 'background_checking', 'Background Checking', NULL, NULL, NULL, NULL, NULL);
/*!40000 ALTER TABLE `rms_modules` ENABLE KEYS */;

-- Dumping structure for table pcw_db.selection_lineup
DROP TABLE IF EXISTS `selection_lineup`;
CREATE TABLE IF NOT EXISTS `selection_lineup` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table pcw_db.selection_lineup: ~0 rows (approximately)
/*!40000 ALTER TABLE `selection_lineup` DISABLE KEYS */;
/*!40000 ALTER TABLE `selection_lineup` ENABLE KEYS */;

-- Dumping structure for table pcw_db.trainings
DROP TABLE IF EXISTS `trainings`;
CREATE TABLE IF NOT EXISTS `trainings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) DEFAULT NULL,
  `title_learning_programs` varchar(225) DEFAULT NULL,
  `inclusive_date_from` varchar(225) DEFAULT NULL,
  `inclusive_date_to` varchar(225) DEFAULT NULL,
  `number_hours` varchar(225) DEFAULT NULL,
  `ld_type` varchar(225) DEFAULT NULL,
  `sponsored_by` varchar(225) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- Dumping data for table pcw_db.trainings: ~1 rows (approximately)
/*!40000 ALTER TABLE `trainings` DISABLE KEYS */;
INSERT INTO `trainings` (`id`, `applicant_id`, `title_learning_programs`, `inclusive_date_from`, `inclusive_date_to`, `number_hours`, `ld_type`, `sponsored_by`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(5, 3, 'Training I', NULL, NULL, '48', NULL, NULL, NULL, NULL, '2019-10-14 13:30:05', '2019-10-14 13:30:05', NULL);
/*!40000 ALTER TABLE `trainings` ENABLE KEYS */;

-- Dumping structure for table pcw_db.workexperiences
DROP TABLE IF EXISTS `workexperiences`;
CREATE TABLE IF NOT EXISTS `workexperiences` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) DEFAULT NULL,
  `inclusive_date_from` date DEFAULT NULL,
  `inclusive_date_to` date DEFAULT NULL,
  `position_title` varchar(225) DEFAULT NULL,
  `department` varchar(225) DEFAULT NULL,
  `monthly_salary` varchar(225) DEFAULT NULL,
  `salary_grade` varchar(225) DEFAULT NULL,
  `status_of_appointment` varchar(225) DEFAULT NULL,
  `govt_service` varchar(225) DEFAULT NULL,
  `present_work` int(11) DEFAULT '0',
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Dumping data for table pcw_db.workexperiences: ~1 rows (approximately)
/*!40000 ALTER TABLE `workexperiences` DISABLE KEYS */;
INSERT INTO `workexperiences` (`id`, `applicant_id`, `inclusive_date_from`, `inclusive_date_to`, `position_title`, `department`, `monthly_salary`, `salary_grade`, `status_of_appointment`, `govt_service`, `present_work`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(3, 3, '2019-10-14', '2019-10-15', 'Admin I', NULL, NULL, NULL, NULL, '1', 0, NULL, NULL, '2019-10-14 13:30:05', '2019-10-14 13:30:05', NULL);
/*!40000 ALTER TABLE `workexperiences` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
