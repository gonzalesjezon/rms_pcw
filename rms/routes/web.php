<?php

/**
 * Public Routes
 *
 */
Route::get('/frontend', 'FronEndController@index');
Route::post('frontend/postJob', 'FronEndController@postJob')->name('frontend.post_job');
Route::resources(['frontend' => 'FronEndController']);
Route::get('/', 'Auth\LoginController@showLoginForm');
Route::get('/menu', 'HomeController@index')->name('menu');
Route::get('/careers', 'HomeController@careers')->name('careers');
Auth::routes();

/**
 * Routes that require user to be logged in
 * filtered by middleware auth in controller
 */
Route::get('/', 'DashboardController@index');
Route::get('/dashboard', 'DashboardController@index')->name('dashboard');
Route::get('/non-plantilla', 'JobsController@nonPlantilla')->name('jobs.nonplantilla');
Route::get('evaluation/rating', 'EvaluationController@rating')->name('evaluation.rating');
Route::get('jobs/publish', 'JobsController@publish')->name('jobs.publish');
Route::get('jobs/getPsipop', 'JobsController@getPsipop')->name('jobs.get-psipop');
Route::get('jobs/typePersonnel', 'JobsController@typePersonnel')->name('jobs.type-personnel');
Route::get('evaluation/matrix-qualification', 'EvaluationController@matrixQualification')->name('evaluation.matrix');
Route::get('evaluation/comparative-ranking', 'EvaluationController@comparativeRanking')->name('evaluation.comparative');
Route::get('evaluation/report', 'EvaluationController@evaluationReport')->name('evaluation.report');
Route::get('evaluation/matrix-report', 'EvaluationController@matrixQualificationReport')->name('evaluation.matrix-report');
Route::get('evaluation/comparative-report', 'EvaluationController@comparativeReport')->name('evaluation.comparative-report');
Route::get('report/appointments_issued', 'ReportController@appointmentIssued')->name('report.appointments_issued');
Route::get('report/absence_qualified_eligible', 'ReportController@absenceQualifiedEligible')->name('report.absence_qualified_eligible');
Route::get('report/dibar', 'ReportController@dibarReport')->name('report.dibar');
Route::get('report/publication_vacant_position', 'ReportController@vacantPosition')->name('report.publication_vacant_position');
Route::get('report/appointment_form_regulated', 'ReportController@appointmentFormRegulated')->name('report.appointment_form_regulated');
Route::get('report/medical_certificate', 'ReportController@medicalCertificate')->name('report.medical_certificate');
Route::get('report/preliminary_evaluation', 'ReportController@preliminaryEvaluation')->name('report.preliminary_evaluation');
Route::get('report/selection_lineup', 'ReportController@selectionLineup')->name('report.selection_lineup');
Route::get('report/checklist', 'ReportController@checklistReport')->name('report.checklist');
Route::get('report/appointments_casual', 'ReportController@appointmentCasual')->name('report.appointments_casual');
Route::get('report/accession_form', 'ReportController@accessionForm')->name('report.accession_form');
Route::get('report/separation_form', 'ReportController@sepearationForm')->name('report.separation_form');
Route::get('report/pipeline-report', 'ReportController@pipelineReport')->name('report.pipeline-report');
Route::get('report/status_of_vacancy', 'ReportController@statusVacancyReport')->name('report.status_of_vacancy');
Route::get('report/appointment-transmital', 'ReportController@appointmentTransmital')->name('report.appointment-transmital');
Route::get('report/work_force_complement', 'ReportController@workForceComplement')->name('report.work_force_complement');
Route::get('report/cos_agreement', 'ReportController@cosAgreement')->name('report.cos_agreement');
Route::get('report/comparative_matrix', 'ReportController@comparativeMatrix')->name('report.comparative_matrix');
Route::get('report/shortlisted', 'ReportController@shortListed')->name('report.shortlisted');
Route::get('appointment/report', 'AppointmentController@appointmentReport')->name('appointment.report');
Route::get('appointment/create-appointment', 'AppointmentController@createAppointmentForm')->name('appointment.create-appointment');
Route::get('appointment/report-form', 'AppointmentController@reportForm')->name('appointment.report-form');
Route::get('assumption/report', 'AssumptionController@assumptionReport')->name('assumption.report');
Route::get('preliminary_evaluation/getApplicant', 'PreliminaryEvaluationController@getApplicant')->name('evaluation.getapplicant');
Route::get('selected_applicant/selectedApplicant', 'SelectedApplicantController@selectedApplicant')->name('selected_applicant.selected-applicant');
Route::get('examinations/getApplicant', 'ExaminationController@getApplicant')->name('examinations.get-applicant');
Route::get('interviews/getApplicant', 'InterviewController@getApplicant')->name('interviews.get-applicant');
Route::get('oath-office/report', 'OathOfficeController@oathOfficeReport')->name('oath-office.report');
Route::get('position-descriptions/report', 'PositionDescriptionController@report')->name('position-descriptions.report');
Route::get('erasure_alterations/report', 'ErasureAlterationController@report')->name('erasure_alterations.report');
Route::get('acceptance_resignation/report', 'AcceptanceResignationController@report')->name('acceptance_resignation.report');
Route::get('appointment-form/report', 'AppointmentFormController@report')->name('appointment-form.report');
Route::get('appointment-processing/report', 'AppointmentProcessingController@report')->name('appointment-processing.report');
Route::get('appointment-form/report', 'AppointmentFormController@report')->name('appointment-form.report');
Route::get('background_checking/report', 'BackgroundCheckingController@report')->name('background_checking.report');
Route::get('jobs/report', 'JobsController@report')->name('jobs.report');
Route::get('interview_guide/report', 'InterviewGuideController@report')->name('interview_guide.report');
Route::get('applicant_ratings/report', 'ApplicantRatingController@report')->name('applicant_ratings.report');

Route::post('applicant.store-qualified',
    'ApplicantController@storeQualified')->name('applicant.store-qualified');

Route::post('evaluation/store-matrix-qualification',
    'EvaluationController@storeMatrixQualification')->name('evaluation.storeMatrix');

Route::post('evaluation/store-comparative-ranking',
    'EvaluationController@storeComparativeRanking')->name('evaluation.storeComparative');

Route::post('appointment/store-appointment-form',
    'AppointmentController@storeAppointmentForm')->name('appointment.storeAppointmentForm');

Route::post('selected_applicant/store-appointee',
    'SelectedApplicantController@storeExam')->name('selected_applicant.store-exam');

Route::post('applicant/delete',
    'ApplicantController@delete')->name('applicant.delete');


Route::post('examinations/sendEmail',
    'ExaminationController@sendEmail')->name('examinations.sendEmail');

Route::post('interviews/sendMail',
    'InterviewController@sendMail')->name('interviews.sendEmail');

Route::post('applicant/sendMail',
    'ApplicantController@sendMail')->name('applicant.sendEmail');

Route::post('boarding_applicant/sendMail',
    'BoardApplicantController@sendMail')->name('boarding_applicant.sendEmail');

Route::resources([
    'applicant_ratings' => 'ApplicantRatingController',
    'interview_guide' => 'InterviewGuideController',
    'background_checking' => 'BackgroundCheckingController',
    'users' => 'UsersController',
    'access_modules' => 'AccessModuleController',
    'appointment-form' => 'AppointmentFormController',
    'appointment-issued' => 'AppointmentIssuedController',
    'appointment-processing' => 'AppointmentProcessingController',
    'boarding_applicant' => 'BoardApplicantController',
    'acceptance_resignation' => 'AcceptanceResignationController',
    'erasure_alterations' => 'ErasureAlterationController',
    'position-descriptions' => 'PositionDescriptionController',
    'oath-office' => 'OathOfficeController',
    'appointment-casual' => 'AppointmentCasualController',
    'appointment-requirements' => 'AppointmentRequirementController',
    'psipop' => 'PSIPOPController',
    'examinations' => 'ExaminationController',
    'interviews' => 'InterviewController',
    'applicant' => 'ApplicantController',
    'config' => 'ConfigController',
    'preliminary_evaluation' => 'PreliminaryEvaluationController',
    'selected_applicant' => 'SelectedApplicantController',
    'evaluation' => 'EvaluationController',
    'jobs' => 'JobsController',
    'recommendation' => 'RecommendationController',
    'appointment' => 'AppointmentController',
    'joboffer' => 'JobOfferController',
    'assumption' => 'AssumptionController',
    'attestation' => 'AttestationController',
    'report' => 'ReportController',
]);

