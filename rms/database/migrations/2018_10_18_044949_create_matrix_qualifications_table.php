<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMatrixQualificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('matrix_qualifications')) {
            Schema::create('matrix_qualifications', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('applicant_id');
                $table->tinyInteger('age', false, true);
                $table->text('education')->nullable();
                $table->text('experience')->nullable();
                $table->text('eligibility')->nullable();
                $table->text('training')->nullable();
                $table->text('remarks')->nullable();
                $table->string('isc_chairperson', 100)->nullable();
                $table->string('isc_member_one', 100)->nullable();
                $table->string('isc_member_two', 100)->nullable();
                $table->string('ea_representative', 100)->nullable();
                $table->integer('created_by', false, true);
                $table->integer('updated_by')->nullable();
                $table->timestamps();
                $table->softDeletes();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('matrix_qualifications');
    }
}
