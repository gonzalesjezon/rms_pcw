@extends('layouts.app')

@section('content')
    <div class="page-head">
        <h2 class="page-head-title">Schedule of Interviews</h2>
    </div>

    <!-- Job Form -->
    <div class="row">
        <div class="col-md-12">
            <div class="card card-border-color card-border-color-primary">
                <div class="card-header card-header-divider">
                    <span class="card-subtitle">You can create a new interview schedule in the form below.</span>
                </div>
                <div class="card-body">
                    @include('interviews._form', [
                        'action' => 'InterviewController@store',
                        'actionPosition' => 'InterviewController@getApplicant',
                        'method' => 'POST',
                        'job' => @$job,
                        'selected' => @$selected,
                        'currentJob' => @$currentJob,
                    ])
                </div>
            </div>
        </div>
    </div>
@endsection
