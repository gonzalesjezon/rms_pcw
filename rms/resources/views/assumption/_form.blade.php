@section('css')
    <link rel="stylesheet" type="text/css"
        href="{{ URL::asset('beagle-assets/lib/datetimepicker/css/bootstrap-datetimepicker.min.css') }}">
  <link rel="stylesheet" type="text/css" href="{{ URL::asset('beagle-assets/lib/select2/css/select2.min.css') }}">
  <link rel="stylesheet" type="text/css"
        href="{{ URL::asset('beagle-assets/lib/bootstrap-slider/css/bootstrap-slider.min.css') }}">
@endsection

{!! Form::open(['action' => $action, 'method' => $method, 'id' => 'evaluation-form']) !!}

<div class="form-group row">
   {{ Form::label('', 'Applicant Name', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
    <div class="col-5">
         @if(@$assumption)
            {!! Form::label('', @$assumption->applicant->getFullName(), ['class'=>'col-12 col-sm-3 col-form-label text-sm-right font-weight-bold']) !!}
            <input type="hidden" name="applicant_id" value="{{@$assumption->applicant_id}}">
        @else
            <select name="applicant_id" class="form-control form-control-xs" id="applicant_id">
                <option value="0">Select applicant</option>
                  @foreach($appointees as $appointee)
                  <option value="{{$appointee->applicant_id}}">{{ $appointee->applicant->getFullName()}}</option>
                  @endforeach
              </select>
        @endif
    </div>
</div>

<div class="form-group row">
    {{ Form::label('head', 'Head of Office/Department/Unit', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
    <div class="col-5">
        {{ Form::text('head_of_office', @$assumption->head_of_office, [
                'class' => 'form-control form-control-sm',
                'placeholder' => 'Head of Office/Department/Unit',
                'required' => 'true'
            ])
        }}
    </div>
</div>

<div class="form-group row">
    <label class="col-12 col-sm-3 col-form-label text-sm-right"> Date </label>
    <div class="col-12 col-sm-7 col-md-5 col-lg-4 col-xl-3">
        <div data-min-view="2" data-date-format="yyyy-mm-dd" class="input-group date datetimepicker">
            <input size="16" type="text" value="{{ @$assumption->assumption_date }}" name="assumption_date"
                   class="form-control form-control-sm"
                   placeholder="Date Now"
                   required="true"
            >
            <div class="input-group-append">
                <button class="btn btn-primary"><i class="icon-th mdi mdi-calendar"></i></button>
            </div>
        </div>
    </div>
</div>

<div class="form-group row">
    {{ Form::label('attested_by', 'Attested By:', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
    <div class="col-5">
        {{ Form::text('attested_by', @$assumption->attested_by, [
                'class' => 'form-control form-control-sm',
                'placeholder' => 'Attested By',
                'required' => 'true'
            ])
        }}
    </div>
</div>

<div class="form-group row">
    <div class="col-5 offset-3">
        Highest Ranking HRMO
    </div>
</div>

<div class="form-group row text-right">
    <div class="col col-sm-10 col-lg-9 offset-sm-1 offset-lg-0">
        {{ Form::submit('Submit', ['id' => 'job-submit', 'class'=>'btn btn-primary btn-space']) }}
        {{ Form::reset('Clear Form', ['id'=>'clear-form', 'class'=>'btn btn-space btn-danger']) }}
    </div>
</div>

{!! Form::close() !!}

@section('scripts')
    <!-- JS Libraries -->
    <script src="{{ URL::asset('beagle-assets/lib/jquery-ui/jquery-ui.min.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('beagle-assets/lib/moment.js/min/moment.min.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('beagle-assets/js/app-form-elements.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('beagle-assets/lib/select2/js/select2.min.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('beagle-assets/lib/select2/js/select2.full.min.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('beagle-assets/lib/bootstrap-slider/bootstrap-slider.min.js') }}"
            type="text/javascript"></script>
    <script src="{{ URL::asset('beagle-assets/lib/datetimepicker/js/bootstrap-datetimepicker.min.js') }}"
            type="text/javascript"></script>
    <script src="{{ URL::asset('beagle-assets/js/app-form-elements.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('beagle-assets/lib/parsley/parsley.min.js') }}" type="text/javascript"></script>
    <script>
      $(document).ready(function() {
        //initialize the javascript
        App.init();
        App.formElements();
        $('#evaluation-form').parsley(); // frontend validation
      });
    </script>
@endsection
