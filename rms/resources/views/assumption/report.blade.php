@extends('layouts.print')

@section('css')
@endsection

@section('content')
<div class="form-group row text-right mb-4">
  <div class="col col-10 col-lg-9 offset-1 offset-lg-3 d-print-none">
    {{ Form::button('Print', ['id' => 'evaluation-report', 'class'=>'btn btn-primary btn-space', 'type'=>'submit']) }}
    {{ Form::reset('Cancel', ['class'=>'btn btn-space btn-danger']) }}
  </div>
</div>

<div id="reports" style="width:960px;margin: auto; font-size: 16px;font-family: Times New Roman, serif;">
  <div class="row mb-4">
    <div class="col-3"><i>CS Form No. 4 <br> Revised 2018</i></div>
  </div>

  <div class="row mb-6">
    <div class="col-12 text-center">
      <h3 class="mb-0 pb-0" style="font-size: 14pt;">Republic of the Philippines</h3>
      <div class="font-weight-bold" style="font-size: 16pt;">Philippine Commission on Women</div>
      <div>1145 J.P Laurel St. San Miguel Manila</div>
    </div>
  </div>

  <div class="row mb-4">
    <div class="col-12 text-center">
      <h3><b>CERTIFICATION OF ASSUMPTION TO DUTY</b></h3>
    </div>
  </div>

  <div class="row mb-2">
    <div class="col-12 text-justify">
      <p style="text-indent: 0.5in;line-height:0.7cm;">This is to certify that Mr./Ms <span class="font-weight-bold"><u>{!! $assumption->applicant->getFullName() !!}</u></span> has assumed the duties and responsibilities as <u><span class="font-weight-bold">{!! @$assumption->applicant->job->plantilla_item->position->Name !!}</u> of the <u>{!! @$assumption->applicant->job->plantilla_item->division->Name !!}</span></u>  effective <u><span class="font-weight-bold">{!! date('F d, Y', strtotime($assumption->assumption_date)) !!}</span></u>.</p>
    </div>
  </div>

  <div class="row mb-2">
    <div class="col-12 text-justify">
      <p style="text-indent: 0.5in;line-height:0.7cm;">This certification is issued in connection with the issuance of the appointment of Mr. <b>{!! @$assumption->applicant->getFullName() !!}</b> as <b>{!! @$assumption->applicant->job->plantilla_item->position->Name !!}</b>.</p>
    </div>
  </div>

  <div class="row mb-2">
    <div class="col-12 text-justify">
      <p style="text-indent: 0.5in;line-height:0.7cm;">Done this {{ date('d', time()) }}  day of {{ date('F Y', time()) }} in Quezon City.</p>
    </div>
  </div>

  <div style="height: 5em;"></div>

  <div class="row mb-1">
    <div class="col-9"></div>
    <div class="col-3 text-center">{!! $assumption->head_of_office !!}</div>
  </div>

  <div class="row mb-4">
    <div class="col-9"></div>
    <div class="col-3 text-center border-top border-dark">Head Office/Department/Unit</div>
  </div>

  <div class="row form-group mb-8">
    <div class="col-1">Date: </div>
    <div class="border-bottom border-dark col-2 text-center">{!! $assumption->assumption_date !!}</div>
  </div>

  <div class="row mb-8">
    <div class="col-2 text-left">Attested By </div>
  </div>


  <div class="row mb-1">
    <div class="col-3 text-center">{!! $assumption->attested_by !!}</div>
  </div>

  <div class="row">
    <div class="col-3 text-center border-top border-dark font-weight-bold">HRMO</div>
  </div>

  <div style="height: 10em;"></div>

  <div class="row mb-1" style="font-size: 10pt;">
    <div class="col-6" >
      201 File <br>
      Admin <br>
      COA <br>
      CSC
    </div>
  </div>

  <div class="row mb-4" style="font-size: 10pt;">
    <div class="col-10"></div>
    <div class="col-2 border border-dark font-weight-bold">
      <i>
        For submission to CSCFO
        within 30 days from the
        date of assumption of the appointee
      </i>
    </div>
  </div>


</div>
@endsection

@section('scripts')
<script type="text/javascript">
  $(document).ready(function() {
    $('#evaluation-report').click(function() {
      window.print();
    });
  });
</script>
@endsection