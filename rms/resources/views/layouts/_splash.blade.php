<body class="be-splash-screen">
@yield('content')
<script src="{{ URL::asset('beagle-assets/lib/jquery/jquery.min.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('beagle-assets/lib/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js') }}"
        type="text/javascript"></script>
<script src="{{ URL::asset('beagle-assets/lib/bootstrap/dist/js/bootstrap.bundle.min.js') }}"
        type="text/javascript"></script>
<script src="{{ URL::asset('beagle-assets/js/app.js') }}" type="text/javascript"></script>
@yield('scripts')
<footer>
    <div class="container-fluid">
        <div class="row">
            <div class="col bg-primary text-center text-light h-25">
                <h4>Powered By: e2e Solutions Management Phils. Inc. &copy;</h4></div>
        </div>
    </div>
</footer>
</body>