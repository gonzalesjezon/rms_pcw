@extends('layouts.app')

@section('content')
    <div class="page-head">
        <h2 class="page-head-title">Pre Employment Requirements</h2>
    </div>

    <!-- Applicant Form -->
    <div class="row">
        <div class="col-md-12">
            <div class="card card-border-color card-border-color-primary">
                <div class="card-header card-header-divider">
                    <span class="card-subtitle">You can create an pre employment requirement in the form below.</span></div>
                <div class="card-body">
                    @include('appointment-requirements._form', [
                        'action' => 'AppointmentRequirementController@store',
                        'method' => 'POST',
                        'interviewees' => $interviewees,
                    ])
                </div>
            </div>
        </div>
    </div>
@endsection
