@extends('layouts.app')

@section('content')
    <div class="page-head">
        <h2 class="page-head-title">Edit Configuration</h2>
        <nav aria-label="breadcrumb" role="navigation">
            <ol class="breadcrumb page-head-nav">
                <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Dashboard</a></li>
                <li class="breadcrumb-item active"><a href="{{ route('config.index' ) }}">Configuration</a></li>
            </ol>
        </nav>
    </div>

    <!-- Applicant Form -->
    <div class="row">
        <div class="col-md-12">
            <div class="card card-border-color card-border-color-primary">
                <div class="card-header card-header-divider">
                    <div class="btn-group mb-4" role="group" aria-label="Basic example">
                        <a href="{{ route('dashboard') }}" class="btn btn-success"><i class="icon icon-left mdi mdi-home"></i> Dashboard</a>
                        <a href="{{ route('config.index') }}" class="btn btn-success"><i class="icon icon-left mdi mdi-account-add"></i> Configuration</a>
                    </div>
                    <span class="card-subtitle">You can update configuration in the form below.</span></div>
                <div class="card-body">
                    @include('config._form', [
                        'action' => [$action, $config->id],
                        'method' => $method,
                        'config' => $config
                    ])
                </div>
            </div>
        </div>
    </div>
@endsection
