<link rel="stylesheet" href="{{ URL::asset('beagle-assets/css/app.css') }}" type="text/css"/>
<div style="width: 760px; margin: auto;">

	<div class="row form-group">
		<div class="col-12">
			<p>{{ date('F d, Y',time())}}</p>
			@if($type == 'qualification' || $type == 'applicant')
			<p class="mt-4">{{ Crypy::decrypt($data->first_name) }}</p>
			<p class="mt-4">Dear Mr/Ms {{ $data->getFullName() }}</p>
			@else
			<p class="mt-4">{{ Crypt::decrypt($data->applicant->first_name) }}</p>
			<p class="mt-4">Dear Mr/Ms {{ Crypt::decrypt($data->applicant->last_name) }}</p>
			@endif
			<p class="mt-4">Good day!</p>

			<!-- EXAMINATION EMAIL -->
			@if($type == 'exam')

				<!-- FOR EXAMINATION -->
				@if($status == 1)
				<p class="mt-4">
					Thank you for your interest in exploring career opportunities in PCW. Upon initial assessment, you qualified to our vacant {{ ($data->applicant->job->status == 'plantilla') ? $data->applicant->job->plantilla_item->position->Name : $data->applicant->job->cos_position_title }} position. We are inviting you to take the technical and psychological exam on {{ date('F d, Y',strtotime($data->exam_date)) }} at {{ $data->exam_time }}, 1145 J.P. Laurel St., San Miguel Manila 1005 Philippines. Please come on time, late comers will not be entertained.
				</p>
				<p class="mt-2">Please reply with your name to confirm attendance.</p>
				@endif

				<!-- EXAM RESCHEDULE -->
				@if($status == 2)
				<p class="mt-4">
					As per request, your appointment has been moved to {{ date('F d, Y',strtotime($data->resched_exam_date)) }} at {{ $data->resched_exam_time }}.Please confirm.
				</p>
				@endif

				<!-- FAILED EXAM -->
				@if($status == 3)
				<p class="mt-4">
					This is regarding your application to the {{ ($data->applicant->job->status == 'plantilla') ? $data->applicant->job->plantilla_item->position->Name : $data->applicant->job->cos_position_title }} position at PCW HR.
				</p>
				<p class="mt-2">
					We regret to inform you that you did not pass the qualifying examination for the said position. Although we were impressed with your competencies and experience, we are looking for a personnel who’d closely match the need of the position.
				</p>
				<p class="mt-2">Thank you for your time and interest and we wish you good luck in your future applications.</p>
				@endif

				<!-- EXAM NO SHOW -->
				@if($status == 4)
				<p class="mt-4">
					Due to your non-appearance to the scheduled appointment, we will be removing you from our list of shortlisted applicants.
				</p>
				<p class="mt-2">Feel free to apply after 6 months. </p>
				@endif

				<!-- EXAM WITHDRAWN -->
				@if($status == 5)
				<p class="mt-4">
					Thank you for informing us on the withdrawal of your application. Please feel free to notify us if you wish to cancel this action.
				</p>
				<p class="mt-2">
					You may contact us at telephone number 735 1654 loc 127 or 103 / 733 6611.
				</p>
				@endif

				<!-- FOR REFERENCE -->
				<!-- @if($status == 6)
				<p class="mt-4">
					This is regarding your application to the {{ ($data->applicant->job->status == 'plantilla') ? $data->applicant->job->plantilla_item->position->Name : $data->applicant->job->cos_position_title }} position at PCW.
				</p>
				<p class="mt-2">
					Unfortunately we did not find an applicable opening for you at the moment. However, due to your impressive background, we would keep your resume in our records for future reference in case we have a vacancy which suits your qualifications.
				</p>
				<p class="mt-2">
					Thank you for your time and interest and we wish you good luck in your future applications.
				</p>
				@endif -->

			@endif

			<!-- FOR INTERVIEWL MAIL -->
			@if($type == 'interview')
				<!-- FOR INTERVIEW -->

				@if($status == 1)
				<p class="mt-4">
					Congratulations on passing the exam for {{ ($data->applicant->job->status == 'plantilla') ? $data->applicant->job->plantilla_item->position->Name : $data->applicant->job->cos_position_title }} position. We are inviting you for an interview on {{ date('F d, Y',strtotime($data->interview_date)) }} at {{ $data->interview_time }}, 1145 J.P. Laurel St., San Miguel Manila 1005 Philippines.
				</p>

				<p class="mt-2">Bring 5 copies of your updated resume and PDS and please reply with your name to confirm attendance.</p>
				@endif

				<!-- RESCHED INTERVIEW -->
				@if($status == 2)
				<p class="mt-4">
					As per request, your appointment has been moved to {{ date('F d, Y',strtotime($data->resched_interview_date)) }} at {{ $data->resched_interview_time }}. Please confirm.
				</p>
				@endif

				<!-- FAILED INTERVIEW -->
				@if($status == 3)
				<p class="mt-4">
					Thank you for taking the time to meet with our team about the {{ ($data->applicant->job->status == 'plantilla') ? $data->applicant->job->plantilla_item->position->Name : $data->applicant->job->cos_position_title }} position at PCW. It was a pleasure to learn more about your skills and accomplishments.
				</p>
				<p class="mt-2">Unfortunately, our team did not select you for further consideration.</p>
				<p class="mt-2">I would like to note that competition for jobs in our Department is always strong and that we often have to make difficult choices between many high-caliber candidates. Now that we’ve had the chance to know more about you, we will be keeping your resume on file for future openings that better fit your profile.
				</p>
				<p class="mt-2">Thanks again for your interest and best of luck with your job search.</p>
				@endif

				<!-- INTERVIEW NO SHOW -->
				@if($status == 4)
				<p class="mt-4">
					Due to your non-appearance to the scheduled appointment, we will be removing you from our list of shortlisted applicants.
				</p>
				<p class="mt-2">Feel free to apply after 6 months. </p>
				@endif

				<!-- INTERVIEW WITHDRAWN -->
				@if($status == 5)
				<p class="mt-4">
					Thank you for informing us on the withdrawal of your application. Please feel free to notify us if you wish to cancel this action.
				</p>
				<p class="mt-2">
					You may contact us at telephone number 735 1654 loc 127 or 103 / 733 6611.
				</p>
				@endif

				<!-- INTERVIEW APPROVED FOR REQUIREMENTS -->
				@if($status == 6)
				<p class="mt-4">Congratulations!</p>
				<p class="mt-2">In connection to your application to the {{ ($data->applicant->job->status == 'plantilla') ? $data->applicant->job->plantilla_item->position->Name : $data->applicant->job->cos_position_title }}  position, we are pleased to inform you that you have passed the qualifications required of the job.</p>
				<p class="mt-2">To facilitate your employment, please accomplish the pre-employment requirements sent via email and submit to PCW HR ten (10) days upon receipt hereof.</p>
				<p class="mt-2">For further inquiries, you may contact us at telephone number 735 1654 loc 127 or 736 4955</p>
				@endif

				<!-- FOR REFERENCE -->
				<!-- @if($status == 7)
				<p class="mt-4">
					This is regarding your application to the {{ ($data->applicant->job->status == 'plantilla') ? $data->applicant->job->plantilla_item->position->Name : $data->applicant->job->cos_position_title }} position at PCW.
				</p>
				<p class="mt-2">
					Unfortunately we did not find an applicable opening for you at the moment. However, due to your impressive background, we would keep your resume in our records for future reference in case we have a vacancy which suits your qualifications.
				</p>
				<p class="mt-2">
					Thank you for your time and interest and we wish you good luck in your future applications.
				</p>
				@endif -->

			@endif

			@if($type == 'qualification')
				@if($status == 0)
				<p class="mt-4">
					Thank you for your interest in exploring career opportunities in PCW.
					However, we regret to inform you that we do not have an existing position that matches your qualifications as of this moment
				</p>
				<p class="mt-2">
					We will be placing your application in our active file and contact you should a more suitable role becomes available.
				</p>
				<p class="mt-2">Thank you and have a nice day</p>
				@endif
			@endif

			@if($type == 'onboard')
				@if($status == 1)
				<p class="mt-4">
					Welcome to PCW!.
				</p>
				<p class="mt-2">
					We are all really excited to welcome you to our team! As agreed, your start date {{ date('F d, Y',strtotime($data->start_date)) }}. We expect you to be in our office at {!! $data->start_time !!} and our dress code is business casual.
				</p>
				<p class="mt-2">For your first week, we have also planned a few trainging sessions to give you a better understanding of the Commission and its program and services.</p>
				<p class="mt-2">Our team is excited to meet you and look forward to introducing themselves to you during flag ceremony.</p>
				<p class="mt-2">If you have any questions prior to your arrival, please feel free to email or call me and I'll be more than happy to help you.</p>
				<p class="mt-2">We are looking forward to working with you and seeing you achieve great things!</p>
				@endif
			@endif

			@if($type == 'applicant')
				<!-- Not Qualified -->
				@if($status == 2)
				<p class="mt-4">
					Thank you for your interest in exploring career opportunities with the Philippine Commission on Women. 
				</p>

				<p class="mt-2">
					Your credentials are impressive and was forwarded to the Screening Committee along with a considerable amount of other applications. We regret that after careful evaluation of your resume against the qualification standards and job requirements, you have not been selected to undergo the exam and interview process at this time.
				</p>

				<p class="mt-2">
					We appreciate the time that you invested in applying for this position. We encourage you to check our job postings from time to time and apply for future vacancies that better match your qualifications.
				</p>

				<p class="mt-4">
					We wish you the best in your current job search. We look forward to having a chance to consider you for another role in the future.
				</p>
				@endif

				<!-- For Reference -->
				@if($status == 3)
				<p class="mt-4">
					This is regarding your application to the {{ ($data->applicant->job->status == 'plantilla') ? $data->applicant->job->plantilla_item->position->Name : $data->applicant->job->cos_position_title }} position at PCW.
				</p>

				<p class="mt-2">
					Unfortunately we did not find an applicable opening for you at the moment. However, due to your impressive background, we would keep your resume in our records for future reference in case we have a vacancy which suits your qualifications. 
				</p>

				<p class="mt-4">
					Thank you for your time and interest and we wish you good luck in your future applications.
				</p>
				@endif

				<!-- For Close Position -->
				@if($status == 4)
					<p class="mt-4">Thank you for applying to this position. However, we are no longer sourcing for applications on the position applied for as of this moment. Please feel free to check our other vacancies and take note on the deadline of sending applications. 
					</p>
				@endif
			@endif

			@if($type == 'applicant')
			<p class="mt-7">Very truly yours,</p>
			@else
			<p class="mt-7">Best regards,</p>
			@endif
			<p class="mt-5">Human Resource Management and Development Section <br>
			   Philippine Commission on Women </p>
		</div>
	</div>
</div>