@section('css')
      <link rel="stylesheet" type="text/css"
        href="{{ URL::asset('beagle-assets/lib/datetimepicker/css/bootstrap-datetimepicker.min.css') }}">
  <link rel="stylesheet" type="text/css" href="{{ URL::asset('beagle-assets/lib/select2/css/select2.min.css') }}">
  <link rel="stylesheet" type="text/css"
        href="{{ URL::asset('beagle-assets/lib/bootstrap-slider/css/bootstrap-slider.min.css') }}">
@endsection

{!! Form::open(['action' => $action, 'method' => $method, 'id' => 'evaluation-form']) !!}

@if(@$form)

  <div class="row">
    <div class="col-6">
      <div class="form-group row">
        {{ Form::label('', 'Applicant', ['class' => 'col-12 col-sm-2 col-form-label text-sm-right font-weight-bold']) }}
        <div class="col-12 col-sm-8 col-lg-6">
          <input type="text" value="{!! @$form->applicant->getFullName() !!}" class="form-control-sm form-control" readonly>
        </div>
      </div>
    </div>
  </div>

  <input type="hidden" name="applicant_id" value="{{@$form->applicant_id}}">

@else

  <div class="row">
    <div class="col-6">
      <div class="form-group row">
        {{ Form::label('', 'Select Applicant', ['class' => 'col-12 col-sm-3 col-form-label text-sm-right font-weight-bold']) }}
        <div class="col-12 col-sm-8 col-lg-6">
          <select name="applicant_id" class="form-control form-control-xs" id="applicant_id">
            <option value="0">Select applicant</option>
              @foreach($appointees as $appointee)
              <option value="{{$appointee->applicant_id}}" data-training="{{ $appointee->applicant->latestTraining->title_learning_programs }}"  data-experience="{{ @$appointee->applicant->latestExperience->position_title }}"  data-education="{{ $appointee->applicant->highestEducation->course }}" data-eligibility="{{ config('params.eligibility_type.'.@$appointee->applicant->latest_eligibility->eligibility_ref).'<br>'.$appointee->applicant->latest_eligibility->other_specify }}" >{{ $appointee->applicant->getFullName()}}</option>
              @endforeach
          </select>
        </div>
      </div>
    </div>
  </div>

@endif

<div class="form-group row">
    <div class="col-12">
        <table class="table table-bordered">
            <tbody>
                <tr>
                    <td class="p-1 font-weight-bold">Qualification Standards</td>
                    <td style="background-color: #d0caca;" colspan="4" class="p-0">&nbsp;</td>
                </tr>
                <tr class="text-center">
                    <td>Criteria</td>
                    <td>Requirements</td>
                    <td>Appointee's Qualification <br> (Provide Specific Details)</td>
                    <td>QS MET</td>
                    <td>Remarks</td>
                </tr>
                <tr>
                    <td>Education</td>
                    <td>
                        <p id="education">
                            {!! @$form->applicant->highestEducation->course !!}
                        </p>
                    </td>
                    <td>
                        <textarea name="educ_qualification" class="form-control form-control-xs">{!! @$form->educ_qualification !!}</textarea>
                    </td>
                    <td >
                        <div class="col-12 col-sm-8 col-lg-6 offset-2">
                              <div class="switch-button switch-button-success switch-button-yesno">
                                    @if(@$form->educ_check == 1)
                                    <input type="checkbox" name="educ_check" id="swt0" checked="true"><span>
                                    @else
                                    <input type="checkbox" name="educ_check" id="swt0"><span>
                                    @endif
                                  <label for="swt0"></label></span>
                              </div>
                          </div>
                    </td>
                    <td>
                        <textarea name="educ_remarks" class="form-control form-control-xs">{{@$form->educ_remarks}}</textarea>
                    </td>
                </tr>
                <tr>
                    <td>Experience</td>
                    <td>
                        <p id="experience">
                            {!! @$form->applicant->latestExperience->position_title !!}
                        </p>
                    </td>
                    <td>
                        <textarea name="exp_qualification" class="form-control form-control-xs">{!! @$form->exp_qualification !!}</textarea>
                    </td>
                    <td >
                        <div class="col-12 col-sm-8 col-lg-6 offset-2">
                              <div class="switch-button switch-button-success switch-button-yesno">
                                @if(@$form->exp_check == 1)
                                <input type="checkbox" name="exp_check" id="swt1" checked="true"><span>
                                @else
                                <input type="checkbox" name="exp_check" id="swt1"><span>
                                @endif
                                  <label for="swt1"></label></span>
                              </div>
                          </div>
                    </td>
                    <td>
                        <textarea name="exp_remarks" class="form-control form-control-xs">{!! @$form->exp_remarks !!}</textarea>
                    </td>
                </tr>
                <tr>
                    <td>Training</td>
                    <td>
                        <p id="training">
                            {!! @$form->applicant->latestTraining->title_learning_programs !!}
                        </p>
                    </td>
                    <td>
                        <textarea name="training_qualification" class="form-control form-control-xs">{{@$form->training_qualification}}</textarea>
                    </td>
                    <td >
                        <div class="col-12 col-sm-8 col-lg-6 offset-2">
                              <div class="switch-button switch-button-success switch-button-yesno">
                                    @if(@$form->training_check == 1)
                                    <input type="checkbox" name="training_check" id="swt2" checked="true"><span>
                                    @else
                                    <input type="checkbox" name="training_check" id="swt2"><span>
                                    @endif
                                  <label for="swt2"></label></span>
                              </div>
                          </div>
                    </td>
                    <td>
                        <textarea name="training_remarks" class="form-control form-control-xs">{{@$form->training_remarks}}</textarea>
                    </td>
                </tr>
                <tr>
                    <td>Eligibility</td>
                    <td>
                        <p id="eligibility">
                            {!! config('params.eligibility_type.'.@$form->applicant->latest_eligibility->eligibility_ref).'<br>'.@$form->applicant->latest_eligibility->other_specify !!}
                        </p>
                    </td>
                    <td>
                        <textarea name="eligibility_qualification" class="form-control form-control-xs">{{@$form->eligibility_qualification}}</textarea>
                    </td>
                    <td >
                        <div class="col-12 col-sm-8 col-lg-6 offset-2">
                              <div class="switch-button switch-button-success switch-button-yesno">
                                @if(@$form->eligibility_check == 1)
                                  <input type="checkbox" name="eligibility_check" id="swt3" checked="true"><span>
                                @else
                                    <input type="checkbox" name="eligibility_check" id="swt3"><span>
                                @endif
                                  <label for="swt3"></label></span>
                              </div>
                          </div>
                    </td>
                    <td>
                        <textarea name="eligibility_remarks" class="form-control form-control-xs">{!! @$form->eligibility_remarks !!}</textarea>
                    </td>
                </tr>
                <tr>
                    <td>Others if applicable <br> (e.g., Age, Term of Office)</td>
                    <td></td>
                    <td>
                        <textarea name="other_qualification" class="form-control form-control-xs">{!! @$form->other_qualification !!}</textarea>
                    </td>
                    <td >
                        <div class="col-12 col-sm-8 col-lg-6 offset-2">
                              <div class="switch-button switch-button-success switch-button-yesno">
                                @if(@$form->other_check)
                                <input type="checkbox" name="other_check" id="swt4" checked="true"><span>
                                @else
                                <input type="checkbox" name="other_check" id="swt4"><span>
                                @endif
                                  <label for="swt4"></label></span>
                              </div>
                          </div>
                    </td>
                    <td>
                        <textarea name="other_remarks" class="form-control form-control-xs">{!! @$form->other_remarks !!}</textarea>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</div>

<div class="form-group row">
    <div class="col-12">
        <table class="table table-bordered">
            <tbody>
                <tr>
                    <td class="p-1 font-weight-bold" style="width: 35em;">Common Requirements for Regular Appointments</td>
                    <td style="background-color: #d0caca;" class="p-0">&nbsp;</td>
                </tr>
                <tr class="text-center">
                    <td>Requirements</td>
                    <td>Detail/Compliance</td>
                </tr>
                <tr>
                    <td>CS Form 33-A (revised 2017) in triplicate copies</td>
                    <td>
                        <textarea name="ra_form_33" class="form-control form-control-xs">{{@$form->ra_form_33}}</textarea>
                    </td>
                </tr>
                <tr>
                    <td>Employement Status</td>
                    <td>
                        <textarea name="ra_employee_status" class="form-control form-control-xs">{{@$form->ra_employee_status}}</textarea>
                    </td>
                </tr>
                <tr>
                    <td>Nature of Appointment</td>
                    <td>
                        <textarea name="ra_nature_appointment" class="form-control form-control-xs">{{@$form->ra_nature_appointment}}</textarea>
                    </td>
                </tr>
                <tr>
                    <td>Appointing Authority</td>
                    <td>
                        <textarea name="ra_appointing_authority" class="form-control form-control-xs">{{@$form->ra_appointing_authority}}</textarea>
                    </td>
                </tr>
                <tr>
                    <td>Date of Signing</td>
                    <td>
                        <div class="form-group row">
                            <div class="col-12 col-sm-7 col-md-5 col-lg-4 col-xl-8">
                                <div data-min-view="2" data-date-format="yyyy-mm-dd" class="input-group date datetimepicker">
                                    <input size="16" type="text" name="ra_date_sign"
                                           class="form-control form-control-sm" value="{{@$form->ra_date_sign}}">
                                    <div class="input-group-append">
                                        <button class="btn btn-primary"><i class="icon-th mdi mdi-calendar"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>Date of Pulbication/Posting of Vacant Position</td>
                    <td>
                        <div class="form-group row">
                            <div class="col-12 col-sm-7 col-md-5 col-lg-4 col-xl-8">
                                <div data-min-view="2" data-date-format="yyyy-mm-dd" class="input-group date datetimepicker">
                                    <input size="16" type="text" name="ra_date_publication"
                                           class="form-control form-control-sm" value="{{@$form->ra_date_publication}}">
                                    <div class="input-group-append">
                                        <button class="btn btn-primary"><i class="icon-th mdi mdi-calendar"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>Certification by PSB Chairman (at the back of appoitnment) or a copy of the proceedings of PSB's Deliberation</td>
                    <td>
                        <textarea name="ra_certification" class="form-control form-control-xs">{{@$form->ra_certification}}</textarea>
                    </td>
                </tr>
                <tr>
                    <td>Personal Data Sheet (ra Form 212, Revised 2017) Completely Filled with Picture Attached</td>
                    <td>
                        <textarea name="ra_pds" class="form-control form-control-xs">{{@$form->ra_pds}}</textarea>
                    </td>
                </tr>
                <tr>
                    <td>Certificate of Eligiblity/License (Authenticated Copy)</td>
                    <td>
                        <textarea name="ra_eligibility" class="form-control form-control-xs">{{@$appointment->ra_eligibility}}</textarea>
                    </td>
                </tr>
                <tr>
                    <td>Position Description Form (PDF)</td>
                    <td>
                        <textarea name="ra_position_description" class="form-control form-control-xs">{{@$form->ra_position_description}}</textarea>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</div>

<div class="form-group row">
    <div class="col-12">
        <table class="table table-bordered">
            <tbody>
                <tr>
                    <td class="p-1 font-weight-bold" style="width: 35em;">Additional and Specific Cases</td>
                    <td style="background-color: #d0caca;" class="p-0">&nbsp;</td>
                </tr>
                <tr class="text-center">
                    <td>Requirements</td>
                    <td>Detail/Compliance</td>
                </tr>
                <tr>
                    <td>
                        Erasures/alterations on the appointment and other supporting documents (Changes duly initialed by authorized officials and accompanies by a communication authenticating changes made)
                    </td>
                    <td>
                        <textarea name="ar_01" class="form-control form-control-xs">{{@$form->ar_01}}</textarea>
                    </td>
                </tr>
                <tr>
                    <td>Appointee with decided administrative/criminal case (certified true copy of decision rendered submitted)</td>
                    <td>
                        <textarea name="ar_02" class="form-control form-control-xs">{{@$form->ar_02}}</textarea>
                    </td>
                </tr>
                <tr>
                    <td>Discrepancy in name/place of birth (Requirements and procedures as amended by CSC Resolution No,991907 dated August 27, 1999)</td>
                    <td>
                        <textarea name="ar_03" class="form-control form-control-xs">{{@$form->ar_03}}</textarea>
                    </td>
                </tr>
                <tr>
                    <td>COMELEC Ban (Exemption from COMELEC)</td>
                    <td>
                        <textarea name="ar_04" class="form-control form-control-xs">{{@$form->ar_04}}</textarea>
                    </td>
                </tr>
                <tr>
                    <td>
                        Non-Disciplinary Demotion
                        <ul>
                            <li>Certification of the Agency Head that demotion is not a result of an administrative case</li>
                            <li>Written consent by the employee interposing no object to the demotion</li>
                        </ul>
                    </td>
                    <td>
                        <textarea name="ar_05" class="form-control form-control-xs">{{@$form->ar_05}}</textarea>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</div>

<div class="form-group row text-right">
  <div class="col col-sm-12 ">
    <input type="hidden" name="id" value="{{@$form->id}}">
    
    {{ Form::submit('Save', ['id' => 'attestation-submit', 'class'=>'btn btn-success btn-space']) }}
    {{ Form::reset('Cancel', ['id'=>'clear-form', 'class'=>'btn btn-space btn-danger']) }}
  </div>
</div>
{!! Form::close() !!}

@section('scripts')
    <!-- JS Libraries -->
    <script src="{{ URL::asset('beagle-assets/lib/jquery-ui/jquery-ui.min.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('beagle-assets/lib/moment.js/min/moment.min.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('beagle-assets/js/app-form-elements.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('beagle-assets/lib/select2/js/select2.min.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('beagle-assets/lib/bootstrap-slider/bootstrap-slider.min.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('beagle-assets/lib/datetimepicker/js/bootstrap-datetimepicker.min.js') }}" type="text/javascript"></script>

    <script>
      $(document).ready(function() {
        //initialize the javascript
        App.init();
        App.formElements();

        $('#applicant_id').change(function(){
            education = $(this).find(':selected').data('education');
            experience = $(this).find(':selected').data('experience');
            training = $(this).find(':selected').data('training');
            eligibility = $(this).find(':selected').data('eligibility');

            $('#education').text(education);
            $('#experience').text(experience);
            $('#training').text(training);
            $('#eligibility').text(eligibility);
        });
      });
    </script>
@endsection
