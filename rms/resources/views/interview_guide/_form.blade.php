@section('css')
<link rel="stylesheet" type="text/css"
        href="{{ URL::asset('beagle-assets/lib/datetimepicker/css/bootstrap-datetimepicker.min.css') }}">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.min.css"
          rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('beagle-assets/lib/summernote/summernote-bs4.css') }}" />
@endsection


{!! Form::open(['action' => $action, 'method' => $method, 'id'=>'interview-form']) !!}
<input type="hidden" name="id" value="{{ @$guide->id }}">
<div class="row">
    <div class="col-6">

    	@if(@$guide)
    		<div class="form-group row {{ $errors->has('applicant_id') ? 'has-error' : ''}}">
            {{ Form::label('applicant_id', 'Applicant Name', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
            <div class="col-12 col-sm-8 col-lg-6">
               {{ Form::text('', @$guide->applicant->getFullName(), [
            		'class' => 'form-control form-control-sm',
            		'readonly' => 'true'
            	]) }}
            </div>
        </div>
    	@else
	    	<div class="form-group row {{ $errors->has('applicant_id') ? 'has-error' : ''}}">
		        {{ Form::label('applicant_id', 'Select Applicant', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
		        <div class="col-12 col-sm-8 col-lg-6">
		            <select name="applicant_id" id="applicant_id" class="form-control form-control-xs" required>
							      <option value="0">Select applcaint</option>
							      @foreach($interviewees as $interviewee)
							      <option value="{{$interviewee->applicant_id }}">{{$interviewee->applicant->getFullName() }}</option>
							      @endforeach
						    </select>
		        {!! $errors->first('applicant_id', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
		        </div>
		    </div>	
    	@endif

    </div>

    <div class="col-6">
    	<div class="form-group row">
		    <label class="col-12 col-sm-1 col-form-label text-sm-right">Date </label>
		    <div class="col-12 col-sm-7 col-md-5 col-lg-6">
		        <div data-min-view="2" data-date-format="yyyy-mm-dd" class="input-group date datetimepicker">
		            <input size="16" type="text" value="{{ @$guide->date_created }}" name="date_created"
		                   class="form-control form-control-sm">
		            <div class="input-group-append">
		                <button class="btn btn-primary"><i class="icon-th mdi mdi-calendar"></i></button>
		            </div>
		        </div>
		    </div>
		</div>
    </div>
</div>

<div class="row">
	<div class="col-6">
		<div class="form-group row {{ $errors->has('interviewer') ? 'has-error' : ''}}">
            {{ Form::label('interviewer', 'Interviewer', [
            'class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
            <div class="col-12 col-sm-8 col-lg-6">
            	{{ Form::text('interviewer', @$guide->interviewer, [
            		'class' => 'form-control form-control-sm'
            	]) }}
            {!! $errors->first('interviewer', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
            </div>
        </div>
	</div>
</div>

<table class="table table-bordered mt-6">
	<thead class="font-weight-bold text-center">
		<tr>
			<th>Interview Questions</th>
			<th>Max Points</th>
			<th>Score</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td>
				<p class="font-weight-bold">To assess confidence, intension and career plan</p>
				<ul>
					<li>Describe yourself, educational background, knowledge, comptencies, skills, strengths and experieces as a worker.</li>
					<li>Why do we need to hire you? What are you going to contribute and bring to PCW?</li>
				</ul>
			</td>
			<td class="text-center">20</td>
			<td>
				<input type="text" name="question_point_1" class="form-control form-control-sm input" value="{{ @$guide->question_point_2}}">
			</td>
		</tr>

		<tr>
			<td>
				<p class="font-weight-bold">
					To assess level of understanding of technical and professional roles
				</p>
				<ul>
					<li>Describe the tax functions of an {!! (@$guide->applicant->job->plantilla_item) ? @$guide->applicant->job->plantilla_item->position->Name : @$guide->applicant->job->cos_position_title !!}</li>
					<li>What skills and traits do you possess which will help you in performing the tasks</li>
				</ul>
				<p class="mb-0 pb-0"><u>Skills/Behavior to look for:</u></p>
				<p>Technical/professional knowledge, tact, work standards, maturity, commitment to task</p>
			</td>
			<td class="text-center">25</td>
			<td>
				<input type="text" name="question_point_2" class="form-control form-control-sm input" value="{{ @$guide->question_point_2}}">
			</td>
		</tr>

		<tr>
			<td>
				<p class="font-weight-bold">
					To assess practical experience and problem solving skills
				</p>
				<ul>
					<li>
						What can you consider your major accomplishment
						/ contribution to your previous agency/office that let to a successful implementation of a program or an activity?
					</li>
					<li>
						Cite the most challenging experience you have had in your job? How did you overcome this?
					</li>
				</ul>

				<p class="mb-0 pb-0"><u>Skills/Behavior to look for:</u></p>
				<p>Maturity, firm but non-threatening resolve, startegic thinking, ability to seize opportunities, ability to think outside the box, ability to handle stress</p>
			</td>
			<td class="text-center">20</td>
			<td>
				<input type="text" name="question_point_3" class="form-control form-control-sm input" value="{{ @$guide->question_point_3}}">
			</td>
		</tr>

		<tr>
			<td>
				<p class="font-weight-bold">
					To assess coordination and leadership skills
				</p>
				<ul>
					<li>
						What are your work ethics?
					</li>
					<li>
						What specific experiences you have manifested your coordination, facilitation, and leadership skills
					</li>
				</ul>

				<p class="mb-0 pb-0"><u>Skills/Behavior to look for:</u></p>
				<p>Energy, motivivational skills, leadership, commitment to task</p>
			</td>
			<td class="text-center">15</td>
			<td>
				<input type="text" name="question_point_4" class="form-control form-control-sm input" value="{{ @$guide->question_point_4}}">
			</td>
		</tr>

		<tr>
			<td>
				<p class="font-weight-bold">
					To assess teamwork and interpersonal skills
				</p>
				<ul>
					<li>
						In work situation, what energizes you? What inspires you to work hard and achieve goals?
					</li>
					<li>
						What turns you off? (work, people, situation) and how do you normally handle the situation?
					</li>
				</ul>

				<p class="mb-0 pb-0"><u>Skills/Behavior to look for:</u></p>
				<p>
					Rapport, work effectively with others to meet project goals, ability to handle stress, flexibility, resilence
				</p>
			</td>
			<td class="text-center">10</td>
			<td>
				<input type="text" name="question_point_5" class="form-control form-control-sm input" value="{{ @$guide->question_point_5}}">
			</td>
		</tr>

		<tr>
			<td>
				<p class="font-weight-bold">
					To assess flexibility
				</p>
				<ul>
					<li>
						How do you normally react to pressure or changing requirements of the job?
					</li>
					<li>
						If you are considered for the position, what additional training will we need to provide you in order to perform the duties of this position?
					</li>
				</ul>

				<p class="mb-0 pb-0"><u>Skills/Behavior to look for:</u></p>
				<p>
					Flexibility, adaptability to change
				</p>
			</td>
			<td class="text-center">10</td>
			<td>
				<input type="text" name="question_point_6" class="form-control form-control-sm input" value="{{ @$guide->question_point_6}}">
			</td>
		</tr>
		<tr >
			<td class="text-right font-weight-bold">Subtotal</td>
			<td class="text-center font-weight-bold">100</td>
			<td class="text-center font-weight-bold">
				<input type="text" name="total_score" id="total_score" class="border-0 form-control form-control-sm">
			</td>
		</tr>
	</tbody>
</table>

<div class="row">
	 <div class="col-12">
        {{ Form::label('', 'Notes/Comment', ['class'=>'col-12 col-sm-2 col-form-label text-sm-left font-weight-bold']) }}
        <div class="form-group {{ $errors->has('comments') ? 'has-error' : ''}}">
            <div class="col-12">
                <div id="comments" name="comments"></div>
                {{ Form::textarea('comments', @$guide->comments,['id'=>'comments-text', 'class'=>'d-none']) }}
                {!! $errors->first('comments', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
            </div>
        </div>
    </div>
</div>


<div class="form-group row text-right">
    <div class="col col-sm-12 ">
        {{ Form::submit('Submit', ['id' => 'job-submit', 'class'=>'btn btn-primary btn-space']) }}
        {{ Form::reset('Clear Form', ['id'=>'clear-form', 'class'=>'btn btn-space btn-danger']) }}
    </div>
</div>
{!! Form::close() !!}

@section('scripts')
    @include('interview_guide._form-script')
@endsection
