@extends('layouts.app')

@section('content')
    <div class="page-head">
        <h2 class="page-head-title">Interview Guide</h2>
    </div>

    <!-- Job Form -->
    <div class="row">
        <div class="col-md-12">
            <div class="card card-border-color card-border-color-primary">
                <div class="card-header card-header-divider">
                    <span class="card-subtitle">You can update the schedule of interview guide in the form below.</span>
                </div>
                <div class="card-body">
                    @include('interview_guide._form', [
                        'action' => ['InterviewGuideController@update', $guide->id],
                        'method' => 'PATCH',
                        'interviewees' => $interviewees,
                        'guide' => $guide,
                    ])
                </div>
            </div>
        </div>
    </div>
@endsection
