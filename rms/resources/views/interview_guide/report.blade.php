@extends('layouts.print')

@section('css')
<style type="text/css">
	.v-top{
		vertical-align: top !important;
	}

	.v-middle{
		vertical-align: middle !important;
	}

	.table>thead>tr>th,
	.table>tbody>tr>td{
		border:1px solid #333 !important;
		vertical-align: middle !important;
		padding: 2px;
	}

</style>
@endsection

@section('content')
 <div class="form-group row text-right d-print-none">
    <div class="col col-sm-10 col-lg-9 offset-sm-1 offset-lg-3">
      {{ Form::button('Print', ['id' => 'evaluation-report', 'class'=>'btn btn-primary btn-space', 'type'=>'submit']) }}
      {{ Form::reset('Cancel', ['class'=>'btn btn-space btn-danger']) }}
    </div>
  </div>

<div class="reports" style="width: 760px;margin: auto;font-size: 12px;font-family: Arial, Helvetica, sans-serif;">

	<div class="row">
		<div class="col-12">
			<h3 style="font-size: 14px;" class="font-weight-bold mb-1">Philippine Commission on Women</h3>
			<h3 class="font-weight-bold mt-1 mb-4" style="font-size: 14px;">INTERVIEW GUIDE</h3>
			<p class="mb-0 font-weight-bold">Name of Applicant: {!! @$guide->applicant->getFullName() !!}</p>
			<p class="mb-0 font-weight-bold">Position: {!! (@$guide->applicant->job->status == 'plantilla') ? @$guide->applicant->job->plantilla_item->position->Name : @$guide->applicant->job->plantilla_item->position->Name !!}</p>
			<p class="mb-0 font-weight-bold">SG: {!! (@$guide->applicant->job->plantilla_item->salary_grade) ? @$guide->applicant->job->plantilla_item->salary_grade->Name : 'NA' !!}</p>
		</div>
	</div>

	<table class="table table-bordered">
		<thead class="font-weight-bold text-center">
			<tr>
				<th>Interview Questions</th>
				<th>Max Points</th>
				<th>Score</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td>
					<p class="font-weight-bold mb-1">To assess confidence, intension and career plan</p>
					<ul>
						<li>Describe yourself, educational background, knowledge, comptencies, skills, strengths and experieces as a worker.</li>
						<li>Why do we need to hire you? What are you going to contribute and bring to PCW?</li>
					</ul>
				</td>
				<td class="text-center">20</td>
				<td class="text-center">
					{!! @$guide->question_point_1 !!}
				</td>
			</tr>

			<tr>
				<td>
					<p class="font-weight-bold mb-1">
						To assess level of understanding of technical and professional roles
					</p>
					<ul>
						<li>Describe the tax functions of an {!! (@$guide->applicant->job->plantilla_item) ? @$guide->applicant->job->plantilla_item->position->Name : @$guide->applicant->job->cos_position_title !!}</li>
						<li>What skills and traits do you possess which will help you in performing the tasks</li>
					</ul>
					<p class="mb-0 pb-0"><u>Skills/Behavior to look for:</u></p>
					<p>Technical/professional knowledge, tact, work standards, maturity, commitment to task</p>
				</td>
				<td class="text-center">25</td>
				<td class="text-center">
					{!! @$guide->question_point_2 !!}
				</td>
			</tr>

			<tr>
				<td>
					<p class="font-weight-bold mb-1">
						To assess practical experience and problem solving skills
					</p>
					<ul>
						<li>
							What can you consider your major accomplishment
							/ contribution to your previous agency/office that let to a successful implementation of a program or an activity?
						</li>
						<li>
							Cite the most challenging experience you have had in your job? How did you overcome this?
						</li>
					</ul>

					<p class="mb-0 pb-0"><u>Skills/Behavior to look for:</u></p>
					<p>Maturity, firm but non-threatening resolve, startegic thinking, ability to seize opportunities, ability to think outside the box, ability to handle stress</p>
				</td>
				<td class="text-center">20</td>
				<td class="text-center">
					{!! @$guide->question_point_3 !!}
				</td>
			</tr>

			<tr>
				<td>
					<p class="font-weight-bold mb-1">
						To assess coordination and leadership skills
					</p>
					<ul>
						<li>
							What are your work ethics?
						</li>
						<li>
							What specific experiences you have manifested your coordination, facilitation, and leadership skills
						</li>
					</ul>

					<p class="mb-0 pb-0"><u>Skills/Behavior to look for:</u></p>
					<p>Energy, motivivational skills, leadership, commitment to task</p>
				</td>
				<td class="text-center">15</td>
				<td class="text-center">
					{!! @$guide->question_point_4 !!}
				</td>
			</tr>

			<tr>
				<td>
					<p class="font-weight-bold mb-1">
						To assess teamwork and interpersonal skills
					</p>
					<ul>
						<li>
							In work situation, what energizes you? What inspires you to work hard and achieve goals?
						</li>
						<li>
							What turns you off? (work, people, situation) and how do you normally handle the situation?
						</li>
					</ul>

					<p class="mb-0 pb-0"><u>Skills/Behavior to look for:</u></p>
					<p>
						Rapport, work effectively with others to meet project goals, ability to handle stress, flexibility, resilence
					</p>
				</td>
				<td class="text-center">10</td>
				<td class="text-center">
					{!! @$guide->question_point_5 !!}
				</td>
			</tr>

			<tr>
				<td>
					<p class="font-weight-bold mb-1">
						To assess flexibility
					</p>
					<ul>
						<li>
							How do you normally react to pressure or changing requirements of the job?
						</li>
						<li>
							If you are considered for the position, what additional training will we need to provide you in order to perform the duties of this position?
						</li>
					</ul>

					<p class="mb-0 pb-0"><u>Skills/Behavior to look for:</u></p>
					<p>
						Flexibility, adaptability to change
					</p>
				</td>
				<td class="text-center">10</td>
				<td class="text-center">
					{!! @$guide->question_point_6 !!}
				</td>
			</tr>
			<tr >
				<td class="text-right font-weight-bold">Subtotal</td>
				<td class="text-center font-weight-bold">100</td>
				<td class="text-center font-weight-bold">
					{!! @$guide->total_score !!}
				</td>
			</tr>
		</tbody>
	</table>

	<div class="row">
		<div class="col-12">
			<p class="font-weight-bold">NOTES/COMMENTS:</p>
			{!! @$guide->comments !!}
		</div>
	</div>

	<div class="row mb-6">
		<div class="col-8"></div>
		<div class="col-4 text-center font-weight-bold">
			INTERVIEWER/DATE:
		</div>
	</div>

	<div class="row">
		<div class="col-8"></div>
		<div class="col-4 text-center font-weight-bold border-bottom border-dark">
			{!! @$guide->interviewer !!} {!! '/ '.@$guide->date_created !!}
		</div>
	</div>

</div>


@endsection

@section('scripts')
<script type="text/javascript">
  $(document).ready(function() {
    $('#evaluation-report').click(function() {
      window.print();
    });
  });
</script>
@endsection