@extends('layouts.app')

@section('content')
    <div class="page-head">
        <h2 class="page-head-title">Interview Guide</h2>
    </div>

    <!-- Job Form -->
    <div class="row">
        <div class="col-md-12">
            <div class="card card-border-color card-border-color-primary">
                <div class="card-header card-header-divider">
                    <span class="card-subtitle">You can create a new interview guide in the form below.</span>
                </div>
                <div class="card-body">
                    @include('interview_guide._form', [
                        'action' => 'InterviewGuideController@store',
                        'method' => 'POST',
                        'interviewees' => @$interviewees,
                    ])
                </div>
            </div>
        </div>
    </div>
@endsection
