<script src="{{ URL::asset('beagle-assets/lib/moment.js/min/moment.min.js')}}"></script>
<script src="{{ URL::asset('beagle-assets/lib/datetimepicker/js/bootstrap-datetimepicker.min.js') }}"
        type="text/javascript"></script>
<script src="{{ URL::asset('beagle-assets/lib/summernote/summernote-bs4.min.js') }}"
        type="text/javascript"></script>
<script src="{{ URL::asset('beagle-assets/lib/summernote/summernote-ext-beagle.js') }}"
        type="text/javascript"></script>
<script src="{{ URL::asset('beagle-assets/js/app-form-wysiwyg.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('beagle-assets/lib/select2/js/select2.min.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('beagle-assets/lib/select2/js/select2.full.min.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('beagle-assets/lib/bootstrap-slider/bootstrap-slider.min.js') }}"
        type="text/javascript"></script>
<script src="{{ URL::asset('beagle-assets/js/app-form-elements.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('beagle-assets/lib/parsley/parsley.min.js') }}" type="text/javascript"></script>
<script type="text/javascript">
  $(document).ready(function() {
    //initialize the javascript
    App.init();
    App.formElements();
    $('#interview-form').parsley(); //frontend validation


    // MULTIPLE INPUT FIELDS SUMMATION
    $(document).on('keyup','.input',function(){
        var sum = 0;
        $('.input').each(function(){
            sum += +$(this).val();
        });
        $('#total_score').val(sum);
    });

    let data = {
      '#comments': `{!! @$guide->comments !!}`,
    };

    // initialize editors for each data element
    App.textEditors(Object.keys(data));

    // when validation fails, get data from hidden input texts
    // and set as value for wysiwyg editor
    for (var key in data) {
      if (data.hasOwnProperty(key)) {
        if (data[key] === '') {
          data[key] = $(`${key}-text`).html();
        }
        setData(key, data[key]);
      }
    }

    // set data for wysiwyg editors
    function setData(selector = '', data = '') {
      $(selector).next('.note-editor').find('.note-editing-area > .note-editable').html(data);
    }

    $('.note-toolbar').remove();
    // on form submit, get data from wysiwyg editors
    // pass to hidden input elements
    $('#job-submit').click(function() {
      for (let key in data) {
        if (data.hasOwnProperty(key)) {
          let element = getData(key);
          $(`${key}-text`).val(element);
        }
      }
    });

    function getData(selector = '') {
      let data = $(selector).next('.note-editor').find('.note-editing-area > .note-editable');
      return data.html();
    }

  });
</script>