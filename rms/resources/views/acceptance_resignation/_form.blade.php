@section('css')
    <link rel="stylesheet" type="text/css"
        href="{{ URL::asset('beagle-assets/lib/datetimepicker/css/bootstrap-datetimepicker.min.css') }}">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.min.css"
          rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('beagle-assets/lib/summernote/summernote-bs4.css') }}" />
@endsection


{!! Form::open(['action' => $action, 'method' => $method, 'id' => 'evaluation-form']) !!}


<div class="form-group row">
        {{ Form::label('', 'Applicant Name', [
          'class'=>'col-12 col-sm-3 col-form-label text-sm-right'
       ])}}
      <div class="col-12 col-sm-8 col-lg-3">
        <select name="applicant_id" class="form-control form-control-xs" id="applicant_id">
            <option value="0">Select applicant</option>
            @foreach($applicants as $applicant)
            <option value="{{$applicant->id}}" {{ ($applicant->id == @$resignation->applicant_id) ? 'selected' : '' }}>{!! $applicant->last_name !!}, {!! $applicant->first_name !!} {!! $applicant->middle_name !!} </option>
            @endforeach
        </select>
    </div>
</div>

<div class="form-group row">
    {{ Form::label('', 'Date Letter of Resignation:', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
    <div class="col-3">
        <div data-min-view="2" data-date-format="yyyy-mm-dd" class="input-group date datetimepicker">
            <input size="16" type="text" value="{{@$resignation->letter_date}}" name="letter_date"
                   class="form-control form-control-sm"
                   required="true"
            >
            <div class="input-group-append">
                <button class="btn btn-primary"><i class="icon-th mdi mdi-calendar"></i></button>
            </div>
        </div>
    </div>
</div>


<div class="form-group row">
    {{ Form::label('', 'Date of Effectivity of Resignation', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
    <div class="col-3">
        <div data-min-view="2" data-date-format="yyyy-mm-dd" class="input-group date datetimepicker">
            <input size="16" type="text" value="{{@$resignation->resignation_date}}" name="resignation_date"
                   class="form-control form-control-sm"
                   required="true"
            >
            <div class="input-group-append">
                <button class="btn btn-primary"><i class="icon-th mdi mdi-calendar"></i></button>
            </div>
        </div>
    </div>
</div>

<div class="form-group row">
    {{ Form::label('', 'Appointing Officer:', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
    <div class="col-3">
        {{ Form::text('appointing_officer', @$resignation->appointing_officer, [
                'class' => 'form-control form-control-sm',
                'required' => 'true'
            ])
        }}
    </div>
</div>

<div class="form-group row">
    {{ Form::label('', 'Date Sign:', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
    <div class="col-3">
        <div data-min-view="2" data-date-format="yyyy-mm-dd" class="input-group date datetimepicker">
            <input size="16" type="text" value="{{@$resignation->sign_date}}" name="sign_date"
                   class="form-control form-control-sm"
                   required="true"
            >
            <div class="input-group-append">
                <button class="btn btn-primary"><i class="icon-th mdi mdi-calendar"></i></button>
            </div>
        </div>
    </div>
</div>

<input type="hidden" name="id" value="{{@$resignation->id}}">


<div class="form-group row text-right">
    <div class="col col-sm-10 col-lg-9 offset-sm-1 offset-lg-0">
        {{ Form::submit('Submit', ['id' => 'job-submit', 'class'=>'btn btn-primary btn-space']) }}
        {{ Form::reset('Clear Form', ['id'=>'clear-form', 'class'=>'btn btn-space btn-danger']) }}
    </div>
</div>

{!! Form::close() !!}

@section('scripts')
    <!-- JS Libraries -->
    <script src="{{ URL::asset('beagle-assets/lib/moment.js/min/moment.min.js')}}"></script>
    <script src="{{ URL::asset('beagle-assets/lib/datetimepicker/js/bootstrap-datetimepicker.min.js') }}"
            type="text/javascript"></script>
    <script src="{{ URL::asset('beagle-assets/lib/summernote/summernote-bs4.min.js') }}"
            type="text/javascript"></script>
    <script src="{{ URL::asset('beagle-assets/lib/summernote/summernote-ext-beagle.js') }}"
            type="text/javascript"></script>
    <script src="{{ URL::asset('beagle-assets/js/app-form-wysiwyg.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('beagle-assets/lib/select2/js/select2.min.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('beagle-assets/lib/select2/js/select2.full.min.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('beagle-assets/lib/bootstrap-slider/bootstrap-slider.min.js') }}"
            type="text/javascript"></script>
    <script src="{{ URL::asset('beagle-assets/js/app-form-elements.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('beagle-assets/lib/parsley/parsley.min.js') }}" type="text/javascript"></script>
    <script>
      $(document).ready(function() {
        //initialize the javascript
        App.init();
        App.formElements();
        $('#evaluation-form').parsley(); // frontend validation

      });
    </script>
@endsection
