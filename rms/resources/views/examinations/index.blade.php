@extends('layouts.app')

@section('css')
  <link rel="stylesheet" type="text/css"
        href="{{ URL::asset('beagle-assets/lib/datatables/datatables.net-bs4/css/dataTables.bootstrap4.css') }}" />
@endsection

@section('content')
  <div class="page-head">
    <h2 class="page-head-title">Schedule of Examination</h2>
  </div>
  <div class="main-content container-fluid">
    <div class="row">
      <div class="col-sm-12">
        <div class="card card-table">
          <div class="card-header">
            <!-- <a href="{{ route('examinations.create') }}" class="btn btn-space btn-primary"
               title="Add a vacant position">
                <i class="icon icon-left mdi mdi-account-add"></i> Add
            </a> -->

            <div class="btn-group btn-space">
              <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false">
                <i class="icon icon-left mdi mdi-account-add"></i> Add <span class="icon-dropdown mdi mdi-chevron-down"></span></button>
              <div class="dropdown-menu" role="menu" x-placement="bottom-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, 30px, 0px);">
                <a class="dropdown-item" href="{{ route('examinations.create',['status' => 'plantilla']) }}">Plantilla</a>
                <a class="dropdown-item" href="{{ route('examinations.create',['status' => 'nonplantilla']) }}">Non Plantilla</a>
              </div>
            </div>
           <!--  <a href="{{ route('examinations.index') }}" class="btn btn-space btn-warning" title="Print">
                <i class="icon icon-left mdi mdi-account-add"></i> Print
            </a> -->
          </div>
          <div class="card-body">
            <table id="table1" class="table table-striped table-hover table-fw-widget">
              <thead>
              <tr>
                <th>Actions</th>
                <th>Applicant Name</th>
                <th>Position Applied</th>
                <th>Date & Time</th>
                <th>Location</th>
                <th>Confirmation</th>
                <th>Status</th>
              </tr>
              </thead>
              <tbody>
                 @foreach($examinations as $exam)
                  <tr>
                    <td class="actions text-left">
                      <div class="tools">
                        <button type="button" data-toggle="dropdown"
                                class="btn btn-secondary dropdown-toggle" aria-expanded="false">
                          <i class="icon icon-left mdi mdi-settings-square"> </i> Options
                          <span class="icon-dropdown mdi mdi-chevron-down"> </span>
                        </button>

                        <div role="menu" class="dropdown-menu" x-placement="bottom-start">
                        	<a href="{{ route('examinations.edit', $exam->id) }}"
                               class="dropdown-item"><i class="icon icon-left mdi mdi-edit"></i>Edit</a>
                            <div class="dropdown-divider"></div>

<!--                             <a href="#"
                             class="dropdown-item md-trigger"
                             data-modal="md-flipH"
                             target="_blank"
                          	>
                            <i class="icon icon-left mdi mdi-print"> </i>Print
                          	</a>

                           <div class="dropdown-divider"></div> -->

                          {!! Form::open([
                              'method'=>'DELETE',
                              'url' => ['examinations', $exam->id],
                              'style' => 'display:inline'
                          ]) !!}
                          {!! Form::button('<i class="icon icon-left mdi mdi-delete mr-1"></i> Delete', [
                              'type' => 'submit',
                              'style' => 'color: #504e4e',
                              'class' => 'dropdown-item',
                              'title' => 'Delete Record',
                              'onclick'=>'return confirm("Confirm delete?")'
                          ])!!}
                          <input type="hidden" name="applicant_id" value="{{$exam->applicant_id}}">
                          {!! Form::close() !!}

                        </div>
                      </div>
                    </td>
                    <td>{{ $exam->applicant->getFullName()}}</td>
                    <td>{!! @$exam->applicant->job->plantilla_item->position->Name !!}</td>
                    <td>{{ $exam->exam_date }} {{$exam->exam_time}}</td>
                    <td>{{ $exam->exam_location }}</td>
                    <td>{{ ($exam->confirmed == 1) ? 'Yes' : 'No' }}</td>
                    <td>{{ config('params.examination_status.'.$exam->exam_status)}}</td>
                  </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection

@section('scripts')
  <script src="{{ URL::asset('beagle-assets/lib/datatables/datatables.net/js/jquery.dataTables.js') }}"
          type="text/javascript"></script>
  <script src="{{ URL::asset('beagle-assets/lib/datatables/datatables.net-bs4/js/dataTables.bootstrap4.js') }}"
          type="text/javascript"></script>
  <script src="{{ URL::asset('beagle-assets/lib/datatables/datatables.net-buttons/js/dataTables.buttons.min.js') }}"
          type="text/javascript"></script>
  <script src="{{ URL::asset('beagle-assets/lib/datatables/datatables.net-buttons/js/buttons.html5.min.js') }}"
          type="text/javascript"></script>
  <script src="{{ URL::asset('beagle-assets/lib/datatables/datatables.net-buttons/js/buttons.flash.min.js') }}"
          type="text/javascript"></script>
  <script src="{{ URL::asset('beagle-assets/lib/datatables/datatables.net-buttons/js/buttons.print.min.js') }}"
          type="text/javascript"></script>
  <script src="{{ URL::asset('beagle-assets/lib/datatables/datatables.net-buttons/js/buttons.colVis.min.js') }}"
          type="text/javascript"></script>
  <script src="{{ URL::asset('beagle-assets/lib/datatables/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js') }}"
          type="text/javascript"></script>
  <script src="{{ URL::asset('beagle-assets/js/app-tables-datatables.js') }}" type="text/javascript"></script>
  <script type="text/javascript">
    $(document).ready(function() {
      //initialize the javascript
      App.init();
      App.dataTables();
    });
  </script>
@endsection
