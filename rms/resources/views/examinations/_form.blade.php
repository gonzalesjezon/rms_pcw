@section('css')
<link rel="stylesheet" type="text/css"
        href="{{ URL::asset('beagle-assets/lib/datetimepicker/css/bootstrap-datetimepicker.min.css') }}">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.min.css"
          rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('beagle-assets/lib/summernote/summernote-bs4.css') }}" />
@endsection

{!! Form::open(['action' => $actionPosition, 'method' => 'GET', 'id' => 'get-form']) !!}
<input type="hidden" name="status" value="{{$status}}">
@if($status == 'plantilla')

<div class="row">
    <div class="col-6">
        <div class="form-group row {{ $errors->has('job_id') ? 'has-error' : ''}}">
            {{ Form::label('job_id', 'Position Title', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
            <div class="col-12 col-sm-8 col-lg-6">
                <select name="job_id" id="job_id" class="form-control form-control-xs" {{ (@$examination) ? 'disabled' : '' }}>
				      <option value="0">Select position</option>
				      @foreach($jobs as $job)
				      <option value="{{$job->id}}" {{ ($job->id == @$currentJob->id) ? 'selected' : '' }}>{{$job->plantilla_item->position->Name}}</option>
				      @endforeach
			    </select>
                {!! $errors->first('job_id', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
            </div>
        </div>
    </div>
</div>
@else

<div class="row">
    <div class="col-6">
        <div class="form-group row {{ $errors->has('job_id') ? 'has-error' : ''}}">
            {{ Form::label('job_id', 'Position Title', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
            <div class="col-12 col-sm-8 col-lg-6">
                <select name="job_id" id="job_id" class="form-control form-control-xs" {{ (@$examination) ? 'disabled' : '' }}>
				      <option value="0">Select position</option>
				      @foreach($jobs as $job)
				      <option value="{{$job->id}}" {{ ($job->id == @$currentJob->id) ? 'selected' : '' }}>{{$job->plantilla_item->position->Name }}</option>
				      @endforeach
			    </select>
                {!! $errors->first('job_id', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
            </div>
        </div>
    </div>
</div>
@endif
{!! Form::close() !!}

{!! Form::open(['action' => $action, 'method' => $method, 'id'=>'examination-form']) !!}
<input type="hidden" name="examination_id" value="{{ @$examination->id}}">
<div class="row">
	<div class="col-6">
		<div class="form-group row">
			{{ Form::label('', 'Applicant', ['class' => 'col-12 col-sm-3 col-form-label text-sm-right']) }}
			<div class="col-12 col-sm-8 col-lg-6">
				<select name="applicant_id" class="form-control form-control-xs" id="applicant_id" {{ (@$examination) ? 'disabled' : '' }}>
					<option value="0">Select applicant</option>
					@if(@$qualified)
						@foreach($qualified as $qualify)
						<option value="{{$qualify->id}}" data-email={{ Crypt::decrypt($qualify->email_address) }} {{ ($qualify->id == @$examination->applicant_id) ? 'selected' : '' }}>{{ $qualify->getFullName()}}</option>
						@endforeach
					@endif
				</select>
			</div>
		</div>

		<div class="form-group row">
		    <label class="col-12 col-sm-3 col-form-label text-sm-right"> Examination Date </label>
		    <div class="col-12 col-sm-7 col-md-5 col-lg-6">
		        <div data-min-view="2" data-date-format="yyyy-mm-dd" class="input-group date datetimepicker">
		            <input size="16" type="text" value="{{@$examination->exam_date}}" name="exam_date"
		                   class="form-control form-control-sm" >
		            <div class="input-group-append">
		                <button class="btn btn-primary"><i class="icon-th mdi mdi-calendar"></i></button>
		            </div>
		        </div>
		    </div>
		</div>

		<div class="form-group row">
			{{ Form::label('','Time', ['class' => 'col-12 col-sm-3 col-form-label text-sm-right']) }}
			<div class="col-12 col-sm-8 col-lg-6">
				{{ Form::text('exam_time', @$examination->exam_time, [
					'class' => 'form-control form-control-sm',
					'required' => true
				]) }}
			</div>
		</div>

		<div class="form-group row">
			{{ Form::label('','Location', ['class' => 'col-12 col-sm-3 col-form-label text-sm-right']) }}
			<div class="col-12 col-sm-8 col-lg-6">
				{{ Form::text('exam_location', @$examination->exam_location, [
					'class' => 'form-control form-control-sm',
					'required' => true
				]) }}
			</div>
		</div>

		<div class="form-group row">
			{{ Form::label('','Email Address', ['class' => 'col-12 col-sm-3 col-form-label text-sm-right']) }}
			<div class="col-12 col-sm-8 col-lg-6">
				{{ Form::text('email', (@$examination->applicant->email_address) ? Crypt::decrypt($examination->applicant->email_address) : '', [
					'id' => 'email',
					'class' => 'form-control form-control-sm',
					'readonly' => true
				]) }}
			</div>
		</div>

		<div class="form-group row">
			{{ Form::label('','Confirmation', ['class' => 'col-12 col-sm-3 col-form-label text-sm-right']) }}
			<div class="col-12 col-sm-8 col-lg-6">
              <div class="switch-button switch-button-success switch-button-yesno">
              	 @if(@$examination->confirmed == 1)
                  <input type="checkbox" name="confirmed" id="confirmed" checked="true"><span>
                 @else
                 <input type="checkbox" name="confirmed" id="confirmed"><span>
                 @endif
                 <label for="confirmed"></label></span>
              </div>
           </div>
		</div>
	</div>

	<div class="col-6">
		<div class="form-group row">
			<label class="col-12 col-sm-3 col-form-label text-sm-right font-weight-bold"> If Reschedule </label>
		</div>

		<div class="form-group row">
		    <label class="col-12 col-sm-3 col-form-label text-sm-right"> Date </label>
		    <div class="col-12 col-sm-7 col-md-5 col-lg-6">
		        <div data-min-view="2" data-date-format="yyyy-mm-dd" class="input-group date datetimepicker">
		            <input size="16" type="text" value="{{ @$examination->resched_exam_date }}" name="resched_exam_date"
		                   class="form-control form-control-sm">
		            <div class="input-group-append">
		                <button class="btn btn-primary"><i class="icon-th mdi mdi-calendar"></i></button>
		            </div>
		        </div>
		    </div>
		</div>

		<div class="form-group row">
			{{ Form::label('','Time', ['class' => 'col-12 col-sm-3 col-form-label text-sm-right']) }}
			<div class="col-12 col-sm-8 col-lg-6">
				{{ Form::text('resched_exam_time', @$examination->resched_exam_time, [
					'class' => 'form-control form-control-sm',
				]) }}
			</div>
		</div>

		<div class="form-group row">
			{{ Form::label('','Confirmation', ['class' => 'col-12 col-sm-3 col-form-label text-sm-right']) }}
			<div class="col-12 col-sm-8 col-lg-6">
              <div class="switch-button switch-button-success switch-button-yesno">
              	 @if(@$examination->notifiy_resched_exam == 1)
                  <input type="checkbox" name="notifiy_resched_exam" id="notifiy_resched_exam" checked="true"><span>
                 @else
                 <input type="checkbox" name="notifiy_resched_exam" id="notifiy_resched_exam"><span>
                 @endif
                 <label for="notifiy_resched_exam"></label></span>
              </div>
           </div>
		</div>


		<hr>

		<div class="form-group row">
			<label class="col-12 col-form-label text-sm-center font-weight-bold"> Written Exam Results </label>
		</div>

		<div class="form-group row">
			{{ Form::label('','Average Raw Score', ['class' => 'col-12 col-sm-3 col-form-label text-sm-right']) }}
			<div class="col-12 col-sm-8 col-lg-6">
				{{ Form::text('average_raw_score', @$examination->average_raw_score, [
					'class' => 'form-control form-control-sm text-right',
					'placeholder' => 'Average Raw Score'
				]) }}
			</div>
		</div>

		<div class="form-group row">
			{{ Form::label('','Sub Total Rating', ['class' => 'col-12 col-sm-3 col-form-label text-sm-right']) }}
			<div class="col-12 col-sm-8 col-lg-6">
				{{ Form::text('sub_total_rating', @$examination->sub_total_rating, [
					'class' => 'form-control form-control-sm text-right',
					'placeholder' => '%'
				]) }}
			</div>
		</div>
	</div>
</div>

<hr>

<div class="row ">
	<div class="col-12">
		<div class="form-group row mb-0 pb-0">
			{{ Form::label('','Examination Status', ['class' => 'col-12 col-sm-2 col-form-label text-sm-right']) }}
			<div class="col-12 col-sm-8 col-lg-3">
				{{ Form::select('exam_status', config('params.examination_status'), @$examination->exam_status,[
					'class' => 'form-control form-control-xs',
					'id' => 'exam_status',
					'placeholder' => 'Select examination status'
				]) }}

			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-5 text-right">
		<a class="btn btn-success mt-2" style="color: #fff;height: 30px;" id="send_mail">
			<i class="mdi mdi-mail-send"></i>
			Notify
		</a>
	</div>
</div>


<div class="form-group row text-right">
    <div class="col col-sm-12 ">
        {{ Form::submit('Submit', ['id' => 'job-submit', 'class'=>'btn btn-primary btn-space']) }}
        {{ Form::reset('Clear Form', ['id'=>'clear-form', 'class'=>'btn btn-space btn-danger']) }}
    </div>
</div>
{!! Form::close() !!}

@section('scripts')
    @include('examinations._form-script')
@endsection
