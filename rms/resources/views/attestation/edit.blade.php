@extends('layouts.app')

@section('content')
    <div class="page-head">
        <h2 class="page-head-title">Attestation</h2>
    </div>

    <!-- Assumption Form -->
    <div class="row">
        <div class="col-md-12">
            <div class="card card-border-color card-border-color-primary">
                <div class="card-header card-header-divider">
                    <span class="card-subtitle">You can create an attestation in the form below.</span></div>
                <div class="card-body">
                    @include('attestation._form', [
                        'action' => ['AttestationController@update', $attestation->id],
                        'method' => 'PATCH',
                        'attestation' => $attestation
                    ])
                </div>
            </div>
        </div>
    </div>
@endsection
