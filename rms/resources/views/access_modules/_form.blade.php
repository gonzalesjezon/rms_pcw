@section('css')
    <link rel="stylesheet" type="text/css"
        href="{{ URL::asset('beagle-assets/lib/datetimepicker/css/bootstrap-datetimepicker.min.css') }}">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.min.css"
          rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('beagle-assets/lib/summernote/summernote-bs4.css') }}" />
@endsection


{!! Form::open(['action' => $action, 'method' => $method, 'id' => 'form']) !!}

<div class="row form-group">
	{!! Form::label('','Access Name',['class' => 'col-12 col-sm-1 col-form-label text-sm-right']) !!}
	<div class="col-3">
		{!! Form::text('access_name', @$access_module->access_name,[
			'class' => 'form-control form-control-sm',
			'required' => 'true'
		]) !!}
	</div>
</div>

<div class="row form-group">
	<div class="col-5">
		<table class="table">
			<thead class="text-center">
				<tr>
					<th>Module Name</th>
					<th>Allowed</th>
				</tr>
			</thead>
			<tbody>
                @foreach($modules as $key => $module)
                <tr>
                    <td>{!! $module->description !!}</td>
                    <td  style="width: 10px;" >
                        <!-- <div class="col-12 col-sm- col-lg-6 "> -->
                          <div class="switch-button switch-button-success switch-button-yesno">
                            @if(isset($access_right[$module->id]))
                            <input type="checkbox" name="access_right[{{$module->id}}]" id="swt{{$key}}" checked><span>
                             <input type="hidden" name="access_id[{{$module->id}}]" value="{{ $access_right[$module->id] }}">
                            @else
                            <input type="checkbox" name="access_right[{{$module->id}}]" id="swt{{$key}}"><span>
                            @endif
                            <label for="swt{{$key}}"></label></span>
                          </div>
                         <!-- </div> -->
                    </td>
                </tr>
                @endforeach
            </tbody>
		</table>
	</div>
</div>

<hr>
<input type="hidden" name="id" value="{{ @$access_module->id }}">
<div class="form-group row text-right">
    <div class="col col-sm-10 col-lg-9 offset-sm-1 offset-lg-0">
        {{ Form::submit('Submit', ['id' => 'job-submit', 'class'=>'btn btn-primary btn-space']) }}
        {{ Form::reset('Clear Form', ['id'=>'clear-form', 'class'=>'btn btn-space btn-danger']) }}
    </div>
</div>

{!! Form::close() !!}

@section('scripts')
    <!-- JS Libraries -->
    <script src="{{ URL::asset('beagle-assets/lib/moment.js/min/moment.min.js')}}"></script>
    <script src="{{ URL::asset('beagle-assets/lib/datetimepicker/js/bootstrap-datetimepicker.min.js') }}"
            type="text/javascript"></script>
    <script src="{{ URL::asset('beagle-assets/lib/summernote/summernote-bs4.min.js') }}"
            type="text/javascript"></script>
    <script src="{{ URL::asset('beagle-assets/lib/summernote/summernote-ext-beagle.js') }}"
            type="text/javascript"></script>
    <script src="{{ URL::asset('beagle-assets/js/app-form-wysiwyg.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('beagle-assets/lib/select2/js/select2.min.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('beagle-assets/lib/select2/js/select2.full.min.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('beagle-assets/lib/bootstrap-slider/bootstrap-slider.min.js') }}"
            type="text/javascript"></script>
    <script src="{{ URL::asset('beagle-assets/js/app-form-elements.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('beagle-assets/lib/parsley/parsley.min.js') }}" type="text/javascript"></script>
    <script>
      $(document).ready(function() {
        //initialize the javascript
        App.init();
        App.formElements();
        $('#form').parsley(); // frontend validation

      });
    </script>
@endsection
