@extends('layouts.app')

@section('content')
    <div class="page-head">
        <h2 class="page-head-title">Edit User</h2>
    </div>

    <!-- Job Form -->
    <div class="row">
        <div class="col-md-12">
            <div class="card card-border-color card-border-color-primary">
                <div class="card-header card-header-divider">
                    <span class="card-subtitle">You can edit user in the form below.</span>
                </div>
                <div class="card-body">
                    @include('users._form', [
                        'action' => ['UsersController@update', $user->id],
                        'method' => 'PATCH',
                        'access_module' => $access_module
                    ])
                </div>
            </div>
        </div>
    </div>
@endsection
