@extends('layouts.app')

@section('content')
    <div class="page-head">
        <h2 class="page-head-title">Create User</h2>
    </div>

    <!-- Matrix Form -->
    <div class="row">
        <div class="col-md-12">
            <div class="card card-border-color card-border-color-primary">
                <div class="card-header card-header-divider">
                    <span class="card-subtitle" style="display: initial !important;">You can create users in the form belwo.</span>
                </div>
                <div class="card-body">
                     @include('users._form', [
                          'action' => $action,
                          'method' => 'POST',
                          'access_module' => $access_module
                      ])
                </div>
            </div>
        </div>
    </div>
@endsection
