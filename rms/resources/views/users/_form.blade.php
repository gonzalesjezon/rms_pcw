@section('css')
   <link rel="stylesheet" type="text/css"
        href="{{ URL::asset('beagle-assets/lib/datetimepicker/css/bootstrap-datetimepicker.min.css') }}">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.min.css"
          rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('beagle-assets/lib/summernote/summernote-bs4.css') }}" />
@endsection


 {!! Form::open(['action' => $action, 'method' => $method, 'id' => 'form']) !!}

<div class="row form-group">
	{!! Form::label('','Name', ['class'=>'col-12 col-sm-2 col-form-label text-sm-right']) !!}
	<div class="col-3">
		{!! Form::text('name', @$user->name,[
			'class'=>'form-control form-control-sm',
			'required' => 'true',
			'readonly' => (@$user->id == 1) ? true : false
		]) !!}
	</div>
</div>

<div class="row form-group">
	{!! Form::label('','Username', ['class'=>'col-12 col-sm-2 col-form-label text-sm-right']) !!}
	<div class="col-3">
		{!! Form::text('username', @$user->username,[
		'class'=>'form-control form-control-sm',
		'required' => 'true',
		'readonly' => (@$user->id == 1) ? true : false
		]) !!}
	</div>
</div>

<div class="row form-group">
	{!! Form::label('','Password', ['class'=>'col-12 col-sm-2 col-form-label text-sm-right']) !!}
	<div class="col-3">
		{!! Form::input('password','password','',[
			'class'=>'form-control form-control-sm',
			'required' => 'true'
		]) !!}
	</div>
</div>

<div class="row form-group">
	{!! Form::label('','Confirm Password', ['class'=>'col-12 col-sm-2 col-form-label text-sm-right']) !!}
	<div class="col-3">
		{!! Form::input('password','password_confirmation','',['class'=>'form-control form-control-sm']) !!}
	</div>
</div>

<div class="row form-group">
	{!! Form::label('','Access Type', ['class'=>'col-12 col-sm-2 col-form-label text-sm-right']) !!}
	<div class="col-3">
		<select class="form-control form-control-xs" name="access_type_id">
			<option>Select access type</option>
			@foreach($access_module as $access)
			<option value="{{$access->id}}" {{ (@$user->access_type_id == $access->id) ? 'selected' : '' }} >{!! $access->access_name!!}</option>
			@endforeach
		</select>
	</div>
</div>

<div class="form-group row text-right">
  <div class="col col-sm-12">
    {{ Form::submit('Save', ['id' => 'attestation-submit', 'class'=>'btn btn-success btn-space']) }}
    {{ Form::reset('Cancel', ['id'=>'clear-form', 'class'=>'btn btn-space btn-danger']) }}
  </div>
</div>

{!! Form::close() !!}

@section('scripts')
  <!-- JS Libraries -->
  	 <script src="{{ URL::asset('beagle-assets/lib/moment.js/min/moment.min.js')}}"></script>
	<script src="{{ URL::asset('beagle-assets/lib/datetimepicker/js/bootstrap-datetimepicker.min.js') }}"
	        type="text/javascript"></script>
	<script src="{{ URL::asset('beagle-assets/lib/summernote/summernote-bs4.min.js') }}"
	        type="text/javascript"></script>
	<script src="{{ URL::asset('beagle-assets/lib/summernote/summernote-ext-beagle.js') }}"
	        type="text/javascript"></script>
	<script src="{{ URL::asset('beagle-assets/js/app-form-wysiwyg.js') }}" type="text/javascript"></script>
	<script src="{{ URL::asset('beagle-assets/lib/select2/js/select2.min.js') }}" type="text/javascript"></script>
	<script src="{{ URL::asset('beagle-assets/lib/select2/js/select2.full.min.js') }}" type="text/javascript"></script>
	<script src="{{ URL::asset('beagle-assets/lib/bootstrap-slider/bootstrap-slider.min.js') }}"
	        type="text/javascript"></script>
	<script src="{{ URL::asset('beagle-assets/js/app-form-elements.js') }}" type="text/javascript"></script>
	<script src="{{ URL::asset('beagle-assets/lib/parsley/parsley.min.js') }}" type="text/javascript"></script>
  <script>
    $(document).ready(function() {
      //initialize the javascript
      App.init();
      App.formElements();
       $('#form').parsley(); //frontend validation
    });
  </script>
@endsection
