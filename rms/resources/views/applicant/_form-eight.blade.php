{{--PRIMARY--}}
<div class="form-group row font-weight-bold" style="font-size: 10px;">
  <div class="col-3 text-center">TITLE OF LEARNING AND DEVELOPMENT INTERVENTIONS/TRAINING PROGRAMS</div>
  <div class="col-2 text-center">INCLUSIVE DATES OF ATTENDANCE</div>
  <div class="col-1 text-center">NUMBER OF HOURS</div>
  <div class="col-3 text-center">TYPE OF LD</div>
  <div class="col-3 text-left">CONDUCTED/SPONSORED BY</div>
</div>
<div class="row">
  <div class="col-12 text-left">
    <a href="#" id="add_training" class="btn btn-sm btn-info">Add</a>
  </div>
</div>

<div class="row text-center mt-2">
  <div class="col-3">
    <span style="font-size: 10px;">(Write in full/Do not abbreviate)</span>
  </div>
  <div class="col-1">
    <span style="font-size: 10px;">FROM</span>
  </div>
  <div class="col-1">
    <span style="font-size: 10px;">TO</span>
  </div>
  <div class="col-1">
    <span style="font-size: 10px;">&nbsp;</span>
  </div>
  <div class="col-3">
    <span style="font-size: 10px;">(Managerial/Supervisor/Technical/etc.)</span>
  </div>
  <div class="col-3 text-left">
    <span style="font-size: 10px;">(Write in full/Do not abbreviate)</span>
  </div>
</div>

<?php
    $training_ctr     = 0;
    $training_ctr2    = count($applicant->training);
?>

@if(count($applicant->training) > 0)

@foreach($applicant->training as $key => $value)

<?php $training_ctr += 1; ?>

<input type="hidden" name="training[{{$key}}][id]" value="{{$value->id}}">
<div class="row mt-1 {{ ($training_ctr2 == $training_ctr) ? 'training' : '' }}">
  <div class="col-3 text-center">
    <input type="text" name="training[{{$key}}][title_learning_programs]" class="form-control form-control-sm" value="{{$value->title_learning_programs}}">
    {!! $errors->first('training[1][title_learning_programs]', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
  </div>
  <div class="col-1 pl-1 pr-1 text-center">
    <input type="text" name="training[{{$key}}][inclusive_date_from]" class="form-control form-control-sm" value="{{$value->inclusive_date_from}}">
    {!! $errors->first('training[1][inclusive_date_from]', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
  </div>
  <div class="col-1 pl-1 pr-1 text-center font-weight-bold">
    <input type="text" name="training[{{$key}}][inclusive_date_to]" class="form-control form-control-sm" value="{{$value->inclusive_date_to}}">
    {!! $errors->first('training[1][inclusive_date_to]', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
  </div>
  <div class="col-1 text-center font-weight-bold">
    <input type="text" name="training[{{$key}}][number_hours]" class="form-control form-control-sm" value="{{$value->number_hours}}" required>
    {!! $errors->first('training[1][number_hours]', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
  </div>
  <div class="col-3 text-center">
    <input type="text" name="training[{{$key}}][ld_type]" class="form-control form-control-sm" value="{{$value->ld_type}}">
    {!! $errors->first('training[1][ld_type]', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
  </div>
  <div class="col-3 text-center">
    <input type="text" name="training[{{$key}}][sponsored_by]" class="form-control form-control-sm col-9 pr-0 mr-1" value="{{$value->sponsored_by}}" style="display: inline-block;">
    {!! $errors->first('training[1][sponsored_by]', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}

    <a  class="btn btn-danger col-2 remove" data-id="{{$value->id}}" data-level="training"><i class="icon mdi mdi-delete" style="color:#fff !important;"></i></a>
  </div>
</div>
@endforeach
<input type="hidden" id="training" value="{{$training_ctr}}">
@else
<div class="row training">
  <div class="col-3 text-center">
    {{ Form::text('training[1][title_learning_programs]', '', [
            'class' => 'form-control form-control-sm',
        ])
    }}
    {!! $errors->first('training[1][title_learning_programs]', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
  </div>
  <div class="col-1 text-center">
    {{ Form::text('training[1][inclusive_date_from]', '', [
            'class' => 'form-control form-control-sm',
        ])
    }}
    {!! $errors->first('training[1][inclusive_date_from]', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
  </div>
  <div class="col-1 text-center font-weight-bold">
    {{ Form::text('training[1][inclusive_date_to]', '', [
            'class' => 'form-control form-control-sm',
        ])
    }}
    {!! $errors->first('training[1][inclusive_date_to]', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
  </div>
  <div class="col-1 text-center font-weight-bold">
    {{ Form::text('training[1][number_hours]', '', [
            'class' => 'form-control form-control-sm',
            'required' => 'true',
        ])
    }}
    {!! $errors->first('training[1][number_hours]', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
  </div>
  <div class="col-3 text-center">
    {{ Form::text('training[1][ld_type]', '', [
            'class' => 'form-control form-control-sm',
        ])
    }}
    {!! $errors->first('training[1][ld_type]', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
  </div>
  <div class="col-3 pr-1 text-left">
    {{ Form::text('training[1][sponsored_by]', '', [
            'class' => 'form-control form-control-sm col-9',
        ])
    }}
    {!! $errors->first('training[1][sponsored_by]', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
  </div>
</div>
@endif




<div class="form-group row text-right">
  <div class="col-12">
    <!-- {{ Form::button('Previous', ['class'=>'btn btn-secondary btn-space wizard-previous', 'data-wizard' => '#wizard1']) }} -->
    
    {{ Form::reset('Clear Form', ['class'=>'btn btn-space btn-danger']) }}
    {{ Form::submit('Save', ['class'=>'btn btn-space btn-primary d-none', 'id'=> 'save_applicant' ]) }}  
  </div>
</div>