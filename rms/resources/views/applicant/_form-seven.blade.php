{{--PRIMARY--}}
<div class="form-group row font-weight-bold" style="font-size: 10px;">
    <div class="col-2 text-center">INCLUSIVE DATES (mm/dd/yyyy)</div>
    <div class="col-1"></div>
    <div class="col-2 text-center">POSITION TITLE</div>
    <div class="col-2 text-center">DEPARTMENT/AGENCY/OFFICE/COMPANY</div>
    <div class="col-1 text-center">MONTHLY SALARY</div>
    <div class="col-1 text-center">SALARY/JOB/SALARY GRADE</div>
    <div class="col-1 text-center">STATUS OF APPOINTMENT</div>
    <div class="col-1 text-left">GOV'T SERVICE (Y/N)</div>
    <div class="col-1"></div>
</div>

<div class="row mb-2">
  <div class="col-12 text-left">
    <a href="#" id="add_workexperience" class="btn btn-sm btn-info">Add</a>
  </div>
</div>

<?php
    $experience_ctr     = 0;
    $experience_ctr2    = count($applicant->workexperience);
?>

@if($experience_ctr2 > 0)

<div class="row text-center mt-2" style="font-size: 10px;">
    <div class="col-2">
        <div class="row">
            <div class="col-6">From</div>
            <div class="col-6">To</div>
        </div>
    </div>
    <div class="col-1"></div>
    <div class="col-2 pl-0 pr-0">(Write in full/Do not abbreviate)</div>
    <div class="col-2 pl-0 pr-0">(Write in full/Do not abbreviate)</div>
    <div class="col-1"></div>
    <div class="col-1"></div>
    <div class="col-2"></div>
    <div class="col-1"></div>
</div>

@foreach($applicant->workexperience as $key => $value)
<?php $experience_ctr += 1; ?>

<input type="hidden" name="work_experience[{{$key}}][id]" value="{{$value->id}}">
<div class="row {{ ($experience_ctr2 == $experience_ctr) ? 'work_experience_'.$experience_ctr : '' }} mt-2">
    <div class="col-2 text-center pl-0 pr-0">
        <div class="row">
            <div class="col-6 pr-1">
                <input size="16" type="text"  name="work_experience[{{$key}}][inclusive_date_from]"
                           class="form-control form-control-sm datetimepicker date" style="font-size: 8pt;" data-min-view="2" data-date-format="yyyy-mm-dd" value="{{$value->inclusive_date_from}}">

                {!! $errors->first('work_experience[$key][inclusive_date_from]', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
            </div>
            <div class="col-6 pl-1">
                <input size="16" type="text"  name="work_experience[{{$key}}][inclusive_date_to]"
                           class="form-control form-control-sm datetimepicker date" style="font-size: 8pt;" data-min-view="2" data-date-format="yyyy-mm-dd" value="{{$value->inclusive_date_to}}">

                {!! $errors->first('work_experience[$key][inclusive_date_to]', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
            </div>
        </div>
    </div>
    <div class="col-1 font-weight-bold">
        <div class="custom-control custom-checkbox mt-2">
          <input class="custom-control-input" type="checkbox" id="exp_check{{$key}}" name="work_experience[{{$key}}][present_work]" {{ ($value->present_work) ? 'checked' : '' }} >
          <label class="custom-control-label" for="exp_check{{$key}}" style="font-size: 7pt;" >Present <br> Work</label>
        </div>
    </div>
    <div class="col-2 pr-1 pl-0 text-center font-weight-bold">
        <input type="text" name="work_experience[{{$key}}][position_title]" class="form-control form-control-sm" value="{{$value->position_title}}">

        {!! $errors->first('work_experience[$key][position_title]', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
    </div>
    <div class="col-2 pr-0 pl-1 text-center font-weight-bold">
        <input type="text" name="work_experience[{{$key}}][department]" class="form-control form-control-sm" value="{{$value->department}}">

        {!! $errors->first('work_experience[$key][department]', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
    </div>
    <div class="col-1 pl-1 pr-1 text-center">
        <input type="text" name="work_experience[{{$key}}][monthly_salary]" class="form-control form-control-sm" value="{{$value->monthly_salary}}">

        {!! $errors->first('work_experience[$key][monthly_salary]', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
    </div>
    <div class="col-1 pl-1 pr-1  text-center">
        <input type="text" name="work_experience[{{$key}}][salary_grade]" class="form-control form-control-sm" value="{{$value->salary_grade}}">

        {!! $errors->first('work_experience[$key][salary_grade]', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
    </div>
    <div class="col-1 pl-1 pr-1  text-center">
        <input type="text" name="work_experience[{{$key}}][status_of_appointment]" class="form-control form-control-sm" value="{{$value->status_of_appointment}}">

        {!! $errors->first('work_experience[$key][status_of_appointment]', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
    </div>
    <div class="col-1 pl-1 pr-1">
        <div class="switch-button switch-button-yesno mt-1">
          <input type="checkbox" name="work_experience[{{$key}}][govt_service]" id="swt{{$key}}" {{ ($value->govt_service) ? 'checked' : '' }} >
          <span><label for="swt{{$key}}"></label></span>
        </div>
    </div>

    <div class="col-1 pl-1 pr-1 text-center">
        <a  class="btn btn-danger col-5 remove" data-id="{{$value->id}}" data-level="experience"><i class="icon mdi mdi-delete" style="color:#fff !important;"></i></a>
    </div>
</div>
@endforeach

<input type="hidden" id="experience" value="{{$experience_ctr}}">
@else
<div class="row work_experience">
    <div class="col-2 text-center pl-0 pr-0">
        <div class="row">
            <div class="col-6 pr-1">
                <span style="font-size: 10px;">FROM</span>
                <input size="16" type="text"  name="work_experience[0][inclusive_date_from]"
                           class="form-control form-control-sm datetimepicker date" style="font-size: 8pt;" data-min-view="2" data-date-format="yyyy-mm-dd" >

                {!! $errors->first('work_experience[0][inclusive_date_from]', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
            </div>
            <div class="col-6 pl-1">
                <span style="font-size: 10px;">TO</span>
                <input size="16" type="text"  name="work_experience[0][inclusive_date_to]"
                           class="form-control form-control-sm datetimepicker date" style="font-size: 8pt;" data-min-view="2" data-date-format="yyyy-mm-dd" >
                {!! $errors->first('work_experience[0][inclusive_date_to]', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
            </div>
        </div>
    </div>
    <div class="col-1 mt-4"> 
        <div class="custom-control custom-checkbox mt-2">
          <input class="custom-control-input" type="checkbox" id="exp_check0" name="work_experience[0][present_work]">
          <label class="custom-control-label" for="exp_check0" style="font-size: 7pt;" >Present <br> Work</label>
        </div>
    </div>
    <div class="col-2 pr-1 pl-0 text-center font-weight-bold">
        <span style="font-size: 10px;">(Write in full/Do not abbreviate)</span>
        {{ Form::text('work_experience[0][position_title]', '', [
                'class' => 'form-control form-control-sm',
            ])
        }}
        {!! $errors->first('work_experience[0][position_title]', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
    </div>
    <div class="col-2 pr-0 pl-1 text-center font-weight-bold">
        <span style="font-size: 10px;">(Write in full/Do not abbreviate)</span>
        {{ Form::text('work_experience[0][department]', '', [
                'class' => 'form-control form-control-sm',
            ])
        }}
        {!! $errors->first('work_experience[0][department]', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
    </div>
    <div class="col-1 pl-1 pr-1 text-center">
        <span>&nbsp;</span>
        {{ Form::text('work_experience[0][monthly_salary]', '', [
                'class' => 'form-control form-control-sm',
            ])
        }}
        {!! $errors->first('work_experience[0][monthly_salary]', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
    </div>
    <div class="col-1 pl-1 pr-1  text-center">
        <span>&nbsp;</span>
        {{ Form::text('work_experience[0][salary_grade]', '', [
                'class' => 'form-control form-control-sm',
            ])
        }}
        {!! $errors->first('work_experience[0][salary_grade]', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
    </div>
    <div class="col-1 pl-1 pr-1  text-center">
        <span>&nbsp;</span>
        {{ Form::text('work_experience[0][status_of_appointment]', '', [
                'class' => 'form-control form-control-sm',
            ])
        }}
        {!! $errors->first('work_experience[0][status_of_appointment]', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
    </div>
    <div class="col-1 pl-1 pr-1">
        <div class="switch-button switch-button-yesno mt-4">
          <input type="checkbox" name="work_experience[0][govt_service]" id="swt0"  >
          <span><label for="swt0"></label></span>
        </div>
    </div>
</div>
@endif


<div class="form-group row text-right">
    <div class="col-12">
        {{ Form::button('Next Step', ['id' => 'btn-form-one', 'class'=>'btn btn-primary btn-space wizard-next', 'data-wizard' => '#wizard1']) }}
        {{ Form::reset('Clear Form', ['class'=>'btn btn-space btn-danger']) }}
    </div>
</div>