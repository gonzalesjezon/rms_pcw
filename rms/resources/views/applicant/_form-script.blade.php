<!-- JS Libraries -->
<script src="{{ URL::asset('beagle-assets/lib/jquery-ui/jquery-ui.min.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('beagle-assets/lib/moment.js/min/moment.min.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('beagle-assets/js/app-form-elements.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('beagle-assets/lib/select2/js/select2.min.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('beagle-assets/lib/select2/js/select2.full.min.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('beagle-assets/lib/bootstrap-slider/bootstrap-slider.min.js') }}"
        type="text/javascript"></script>
<script src="{{ URL::asset('beagle-assets/lib/datetimepicker/js/bootstrap-datetimepicker.min.js') }}"
        type="text/javascript"></script>
<script src="{{ URL::asset('beagle-assets/js/app-booking.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('beagle-assets/lib/fuelux/js/wizard.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('beagle-assets/js/app-form-wizard.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('beagle-assets/lib/parsley/parsley.min.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('js/sweetalert.min.js') }}" type="text/javascript"></script>

<script>
  $(document).ready(function() {
    //initialize the javascript
    App.init();
    App.formElements();
    App.wizard();

    // run the parsely validation before changing forms
    $('.wizard-next, .wizard-previous').click(function() {
      tabs.validate(this, 8);
    });

    let tabs = {
      validate: function(element, noOfTabs) {
        // validate each element in current form tab
        var noError = true;
        var result = true;
        $(element).closest('[data-step]').find('input, textarea, select').each(function() {
          $(this).parsley().validate();
          result = $(this).parsley().isValid();
          if (!result) {
            noError = result;
          }
        });

        // set css class active and complete tabs
        let step = $(element).closest('[data-step]').data('step');
        if (!noError) {
          for (i = 1; i <= noOfTabs; i++) {
            if (i != step) {
              $(`[data-step*="${i}"]`).removeClass('active complete');
            }
            else {
              $(`[data-step*="${i}"]`).addClass('active complete');
            }
          }
        }
      },
    };

    let vocational_ctr = 0;
    let voc_ctr = 0;
    $('#add_vocational').click(function(e) {
      vocational_ctr += 1;
      let vocationalHtml = '' +
        `<div class="form-group row">` +
        `<label for="primary_name" class="col-form-label text-sm-right col">-</label>` +
        `<div class="col-2 pr-0">` +
        `<input class="form-control form-control-sm" name="vocational[${vocational_ctr}][school_name]" type="text">` +
        `</div>` +
        `<div class="col-1"></div>` +
        `<div class="col-2 pr-0 pl-0">` +
        `<input class="form-control form-control-sm" name="vocational[${vocational_ctr}][course]" type="text">` +
        `</div>` +
        `<div class="col-1 text-center font-weight-bold">` +
        `<input size="16" type="text"  name="vocational[${vocational_ctr}][attendance_from]"
                           class="form-control form-control-sm datetimepicker date" style="font-size: 8pt;" data-min-view="2" data-date-format="yyyy-mm-dd" placeholder="From">`+
        `</div>` +
        `<div class="col-1 text-center font-weight-bold">` +
        `<input size="16" type="text"  name="vocational[${vocational_ctr}][attendance_to]"
                           class="form-control form-control-sm datetimepicker date" style="font-size: 8pt;" data-min-view="2" data-date-format="yyyy-mm-dd" placeholder="From">`+
        `</div>` +
         `<div class="col-1 text-center font-weight-bold pr-0">` +
        `<input size="16" type="text"  name="work_experience[${vocational_ctr}][attendance_from]"
                           class="form-control form-control-sm datetimepicker date" style="font-size: 8pt;" data-min-view="2" data-date-format="yyyy-mm-dd" placeholder="From">`+
        `</div>` +
        `<div class="col-1 text-center font-weight-bold pr-0">` +
        `<input size="16" type="text"  name="work_experience[${vocational_ctr}][attendance_to]"
                           class="form-control form-control-sm datetimepicker date" style="font-size: 8pt;" data-min-view="2" data-date-format="yyyy-mm-dd" placeholder="To">`+
        `</div>` +
        `<div class="col-2">` +
        `<input class="form-control form-control-sm col-6 pr-0 mr-1" name="vocational[${vocational_ctr}][awards]" type="text" style="display:inline-block">` +
            `<button class="btn btn-danger col-3 remove"><i class="icon mdi mdi-delete"></i></button>`+
        `</div>` +
        `</div>`;

      if(vocational_ctr == 1){
        $(vocationalHtml).insertAfter('.vocational');
        voc_ctr = 1;
      }else{
        ctr = Number(vocational_ctr) - 1
        $(vocationalHtml).insertAfter('.vocational_'+ctr);
      }
      e.preventDefault();
    });

    let workexperience_ctr = Number('{{ count(@$applicant->workexperience) }}');
    let exp_ctr            = 0;
    $('#add_workexperience').click(function(e) {
      workexperience_ctr += 1;
      let workexperienceHtml = '' +
        `<div class="row mt-2  work_experience_${workexperience_ctr}">` +
        `<div class="col-2 text-center pl-0 pr-0">` +
        `<div class="row">` +
            `<div class="col-6 pr-1">` +
            `<input size="16" type="text"  name="work_experience[${workexperience_ctr}][inclusive_date_from]" class="form-control form-control-sm datetimepicker date" style="font-size: 8pt;" data-min-view="2" data-date-format="yyyy-mm-dd">`+
            `</div>` +
            `<div class="col-6 pr-1">` +
            `<input size="16" type="text" name="work_experience[${workexperience_ctr}][inclusive_date_to]"
                           class="form-control form-control-sm datetimepicker date" style="font-size: 8pt;" data-min-view="2" data-date-format="yyyy-mm-dd">`+
            `</div>` +
        `</div>` +
        `</div>` +
        `<div class="col-1">` + 
            `<div class="custom-control custom-checkbox mt-2">` +
              `<input class="custom-control-input" type="checkbox" id="exp_check${workexperience_ctr}" name="work_experience[${workexperience_ctr}][present_work]">` +
              `<label class="custom-control-label" for="exp_check${workexperience_ctr}" style="font-size: 7pt;" >Present <br> Work</label>` +
            `</div>` +
        `</div>` +
        `<div class="col-2 pr-1 pl-0 text-center font-weight-bold">` +
        `<input class="form-control form-control-sm" name="work_experience[${workexperience_ctr}][position_title]" type="text">` +
        `</div>` +
        `<div class="col-2 pr-0 pl-1 text-center font-weight-bold">` +
        `<input class="form-control form-control-sm" name="work_experience[${workexperience_ctr}][department]" type="text">` +
        `</div>` +
        `<div class="col-1 pl-1 pr-1 text-center">` +
        `<input class="form-control form-control-sm" name="work_experience[${workexperience_ctr}][monthly_salary]" type="text">` +
        `</div>` +
        `<div class="col-1 pl-1 pr-1 text-center">` +
        `<input class="form-control form-control-sm" name="work_experience[${workexperience_ctr}][salary_grade]" type="text">` +
        `</div>` +
        `<div class="col-1 pl-1 pr-1 text-center">` +
        `<input class="form-control form-control-sm" name="work_experience[${workexperience_ctr}][status_of_appointment]" type="text">` +
        `</div>` +
        `<div class="col-1 pl-1 pr-1">` +
            `<div class="switch-button switch-button-yesno mt-1">` +
                `<input type="checkbox" name="work_experience[${workexperience_ctr}}][govt_service]" id="swt${workexperience_ctr}"><span>` +
                `<label for="swt${workexperience_ctr}"></label></span>` +
            `</div>` +
        `</div>` +
        `<div class="col-1 pl-1 pr-1 text-center">` +
            `<button class="btn btn-danger col-5 remove"><i class="icon mdi mdi-delete"></i></button>`+
        `</div>` +
        `</div>`;

      if(workexperience_ctr == 1){
        $(workexperienceHtml).insertAfter('.work_experience');
        exp_ctr = 1;
      }else{
        ctr = Number(workexperience_ctr) - 1
        $(workexperienceHtml).insertAfter('.work_experience_'+ctr);
      }
      e.preventDefault();
    });

    let eligibility_ctr = Number('{{ count(@$applicant->eligibility) }}');
    let eli_ctr = 0;
    $('#add_eligibility').click(function(e) {

      eligibility_ctr = (eligibility_ctr == 1) ? 0 : eligibility_ctr;
      eligibility_ctr += 1; 

      let eligibilityHtml = '' +
        `<div class="row mt-2 eligibility_${eligibility_ctr} ">` +
        `<div class="col-2">` +
        `{{ Form::select('eligibility[${eligibility_ctr}][eligibility_ref]', config('params.eligibility_type'), '', [
                'class' => 'form-control form-control-xs',
                'placeholder' => 'Select Eligibility',
                'required' => true,
            ])
        }}` +
        `</div>` +
        `<div class="col-2">` +
        `<input class="form-control form-control-sm" name="eligibility[${eligibility_ctr}][other_speficy]" type="text" placeholder="Specify">` +
        `</div>` +
        `<div class="col-2">` +
        `<input class="form-control form-control-sm" name="eligibility[${eligibility_ctr}][rating]" type="text">` +
        `</div>` +
        `<div class="col-2 text-center font-weight-bold">` +
        `<input class="form-control form-control-sm" name="eligibility[${eligibility_ctr}][exam_date]" type="text">` +
        `</div>` +
        `<div class="col-1 text-center font-weight-bold">` +
        `<input class="form-control form-control-sm" name="eligibility[${eligibility_ctr}][exam_place]" type="text">` +
        `</div>` +
        `<div class="col-1 text-center">` +
        `<input class="form-control form-control-sm" name="eligibility[${eligibility_ctr}][license_number]" type="text">` +
        `</div>` +
        `<div class="col-2 text-center">` +
        `<input class="form-control form-control-sm col-8 pr-0 mr-1" name="eligibility[${eligibility_ctr}][license_validity]" type="text" style="display:inline-block">` +
        `<button class="btn btn-danger col-3 remove"><i class="icon mdi mdi-delete"></i></button>`+
        `</div>` +
        `</div>`;

      if(eligibility_ctr == 1){
        $(eligibilityHtml).insertAfter('.eligibility');
        eli_ctr = 1;
      }else{
        ctr = Number(eligibility_ctr) - 1
        $(eligibilityHtml).insertAfter('.eligibility_'+ctr);
      }
      e.preventDefault();
    });


    let college_ctr = $('#college_ctr').val();
    $('#add_college').click(function(e) {
      college_ctr = Number(college_ctr) + 1;
      let collegeHTML = '' +
        `<div class="form-group row">` +
        `<label for="primary_name" class="col-form-label text-sm-right col">-</label>` +
        `<div class="col-2 pr-0">` +
        `<input class="form-control form-control-sm" name="college[${college_ctr}][school_name]" type="text">` +
        `</div>` +
        `<div class="col-1"></div>` +
        `<div class="col-2 pr-0 pl-0">` +
        `<input class="form-control form-control-sm" name="college[${college_ctr}][course]" type="text">` +
        `</div>` +
        `<div class="col-1 text-center font-weight-bold">` +
        `<input size="16" type="text"  name="college[${vocational_ctr}][attendance_from]"
                           class="form-control form-control-sm datetimepicker date" style="font-size: 8pt;" data-min-view="2" data-date-format="yyyy-mm-dd" placeholder="From">`+
        `</div>` +
        `<div class="col-1 text-center font-weight-bold">` +
        `<input size="16" type="text"  name="college[${vocational_ctr}][attendance_to]"
                           class="form-control form-control-sm datetimepicker date" style="font-size: 8pt;" data-min-view="2" data-date-format="yyyy-mm-dd" placeholder="To">`+
        `</div>` +
        `<div class="col-1">` +
        `<input class="form-control form-control-sm" name="college[${college_ctr}][level]" type="text">` +
        `</div>` +
        `<div class="col-1">` +
        `<input class="form-control form-control-sm" name="college[${college_ctr}][graduated]" type="text">` +
        `</div>` +
        `<div class="col-2">` +
        `<input class="form-control form-control-sm col-6 pr-0 mr-1" name="college[${college_ctr}][awards]" type="text" style="display:inline-block">` +
        `<button class="btn btn-danger col-3 remove"><i class="icon mdi mdi-delete"></i></button>`+
        `</div>` +
        `</div>`;

      $(collegeHTML).insertAfter('.college');
      e.preventDefault();
    });

    let graduate_studies_ctr = $('#graduate_ctr').val();
    $('#add_graduate_studies').click(function(e) {
      graduate_studies_ctr = Number(graduate_studies_ctr) + 1;
      let graduateStudiesHTML = '' +
        `<div class="form-group row">` +
        `<label for="primary_name" class="col-form-label text-sm-right col">-</label>` +
        `<div class="col-2 pr-0">` +
        `<input class="form-control form-control-sm" name="graduate[${graduate_studies_ctr}][school_name]" type="text">` +
        `</div>` +
        `<div class="col-1"></div>` +
        `<div class="col-2 pr-0 pl-0">` +
        `<input class="form-control form-control-sm" name="graduate[${graduate_studies_ctr}][course]" type="text">` +
        `</div>` +
        `<div class="col-1 text-center font-weight-bold">` +
        `<input size="16" type="text"  name="graduate[${graduate_studies_ctr}][attendance_from]"
                           class="form-control form-control-sm datetimepicker date" style="font-size: 8pt;" data-min-view="2" data-date-format="yyyy-mm-dd" placeholder="From">`+
        `</div>` +
        `<div class="col-1 text-center font-weight-bold">` +
        `<input size="16" type="text"  name="graduate[${graduate_studies_ctr}][attendance_to]"
                           class="form-control form-control-sm datetimepicker date" style="font-size: 8pt;" data-min-view="2" data-date-format="yyyy-mm-dd" placeholder="To">`+
        `</div>` +
        `<div class="col-1">` +
        `<input class="form-control form-control-sm" name="graduate[${graduate_studies_ctr}][level]" type="text">` +
        `</div>` +
        `<div class="col-1">` +
        `<input class="form-control form-control-sm" name="graduate[${graduate_studies_ctr}][graduated]" type="text">` +
        `</div>` +
        `<div class="col-2">` +
        `<input class="form-control form-control-sm col-6 pr-0 mr-1" name="graduate[${graduate_studies_ctr}][awards]" type="text" style="display:inline-block">` +
        `<button class="btn btn-danger col-3 remove"><i class="icon mdi mdi-delete"></i></button>`+
        `</div>` +
        `</div>`;

      $(graduateStudiesHTML).insertAfter('.graduate-studies');
      e.preventDefault();
    });

    let training_ctr = Number('{{ count(@$applicant->training) }}');
    let tr_ctr = 0;
    $('#add_training').click(function(e) {
      training_ctr += 1;
      let trainingHtml = '' +
        `<div class="row mt-2 training_${training_ctr}">` +
        `<div class="col-3 text-center">` +
        `<input class="form-control form-control-sm" name="training[${training_ctr}][title_learning_programs]" type="text">` +
        `</div>` +
        `<div class="col-1 pl-1 pr-1 text-center">` +
        `<input class="form-control form-control-sm" name="training[${training_ctr}][inclusive_date_from]" type="text">` +
        `</div>` +
        `<div class="col-1 pl-1 pr-1 text-center font-weight-bold">` +
        `<input class="form-control form-control-sm" name="training[${training_ctr}][inclusive_date_to]" type="text">` +
        `</div>` +
        `<div class="col-1 text-center font-weight-bold">` +
        `<input class="form-control form-control-sm" name="training[${training_ctr}][number_hours]" type="text" required>` +
        `</div>` +
        `<div class="col-3 text-center">` +
        `<input class="form-control form-control-sm" name="training[${training_ctr}][ld_type]" type="text">` +
        `</div>` +
        `<div class="col-3 pl-1 text-center">` +
        `<input class="form-control form-control-sm col-9 pr-0 mr-1" name="training[${training_ctr}][sponsored_by]" type="text" style="display:inline-block">` +
            `<button class="btn btn-danger col-2 remove"><i class="icon mdi mdi-delete"></i></button>`+
        `</div>` +
        `</div>`;

      if(training_ctr == 1){
        $(trainingHtml).insertAfter('.training');
        tr_ctr = 1;
      }else{
        ctr = Number(training_ctr) - 1
        $(trainingHtml).insertAfter('.training_'+ctr);
      }
      e.preventDefault();
    });

    $(document).on('focus', '.datetimepicker', function(){
        $(this).datetimepicker({
            dateFormat: 'dd-mm-yy', //maybe you want something like this
            showButtonPanel: true
        });
    });

    $('#swt6').change(function(){

        let houseNumber = $('#house_number').val();
        let street      = $('#street').val();
        let subdivision = $('#subdivision').val();
        let barangay    = $('#barangay').val();
        let city        = $("#city").val();
        let province    = $('#province').val();

        if($(this).is(':checked')){
            $('#permanent_house_number').val(houseNumber);
            $('#permanent_street').val(street);
            $('#permanent_subdivision').val(subdivision);
            $('#permanent_barangay').val(barangay);
            $('#permanent_city').val(city);
            $('#permanent_province').val(province);
        }else{
            $('.clear-form-input').val('');
        }
    });

    let applicant_id = `{{ @$applicant->id }}`;

    $(document).on('click','.remove',function(){
        id            = $(this).data('id');
        level         = $(this).data('level');
        base_url = `{{ url('applicant') }}`;

        if(id){
            Swal.fire({
                title: "Confirm Delete?",
                type: "warning",
                showCancelButton: true,
                confirmButtonClass: "btn-warning",
                confirmButtonText: "Yes",
                cancelButtonText: "No",
            }).then(function(isConfirm){
                if(isConfirm.value == true){
                    $.ajax({
                        url:base_url+'/delete',
                        data:{
                            '_token':`{{ csrf_token() }}`,
                            'id':id,
                            'level':level,
                        },
                        type:'POST',
                        dataType:'JSON',
                        success:function(result){
                            Swal.fire({
                              title: 'Deleted Successfully',
                              type: "success",
                              showCancelButton: false,
                              confirmButtonClass: "btn-success",
                              confirmButtonText: "OK",
                              closeOnConfirm: false
                            });

                            window.location.href = base_url+'/'+applicant_id+'/edit';
                        }
                    })
                }else{
                    return false;
                }
            });
        }else{

            if(vocational_ctr > 0){
                vocational_ctr -= 1;
            }else{
                voc_ctr = 0;
            }

            if(eligibility_ctr > 0){
                eligibility_ctr -= 1;
            }else{
                eli_ctr = 0;
            }

            if(workexperience_ctr > 0){
                workexperience_ctr -= 1;
            }else{
                exp_ctr = 0;
            }

            if(training_ctr > 0){
                training_ctr -= 1;
            }else{
                tr_ctr = 0;
            }
            $(this).closest('.row').remove();
        }

    });

    $('#select_qualified').change(function(){
        val = $(this).find(':selected').val();

        $('#send_mail').addClass('d-none');
        if(val > 1)
        {
            $('#send_mail').removeClass('d-none');
        }

        $('#selected_qualified').val(val);
    })

    $('#select_qualified').trigger('change');

    $('.btn-save').click(function(){
        $('#save_applicant').trigger('click');
    })

    $('#send_mail').click(function(){
        Swal.fire({
          title: 'Are you sure?',
          text: "You won't be able to revert this!",
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Yes'
        }).then((result) => {
          if (result.value) {

            var arr = {};
            $("[class*='form-control']").each(function () {
                var obj_name = $(this).attr("name");
                var value    = $(this).val();
                arr[obj_name] = value;
            });

            $.ajax({
                url:`{{ url('applicant/sendMail') }}`,
                data:{
                    'data':arr,
                    '_token':`{{ csrf_token() }}`
                },
                type:'POST',
                dataType:'JSON',
                success:function(result){
                    Swal.fire(
                      'Sent Successfully!',
                      'Your mail has been sent.',
                      'success'
                    )

                }
            })
          }

        });

    });

  });
</script>