{{--PRIMARY--}}

<div class="form-group row font-weight-bold text-center">
  <div class="col-2 offset-1 text-center font-pt">NAME OF SCHOOL <br> (Write in Full)</div>
  <div class="col-2 offset-1 font-pt">DEGREE/COURSE <br> (Write in Full)</div>
  <div class="col-2 text-center font-pt">Period of Attendance</div>
  <div class="col-1 font-pt">Highest Level/Units Earned</div>
  <div class="col-1 font-pt">Year Graduated</div>
  <div class="col-2 font-pt">Scholarship/Academic <br> Honors Received</div>
</div>


@if(count($applicant->education) > 0)

  @foreach($applicant->education as $key => $educ)

  @if($educ->educ_level == 1)
  <div class="form-group row">
    <div class="col-form-label col-1 font-weight-bold mt-4">
      Primary
    </div>
    <input type="hidden" name="primary[0][id]" value="{{$educ->id}}">
    <input type="hidden" name="primary[0][applicant_id]" value="{{$educ->applicant_id}}">
    <div class="col-2 mt-4 pr-0">
      {{ Form::text('primary[0][school_name]', $educ->school_name, [
              'class' => 'form-control form-control-sm',
              'required' => 'true'
          ])
      }}
      {!! $errors->first('XXXXX', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
    </div>
    <div class="col-1"></div>
    <div class="col-2 mt-4 pr-0 pl-0">
      {{ Form::text('primary[0][course]', $educ->course, [
              'class' => 'form-control form-control-sm',
          ])
      }}
      {!! $errors->first('XXXXX', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
    </div>
    <div class="col-1 mt-4 text-center font-weight-bold pr-0">
      <input size="16" type="text"  name="primary[0][attendance_from]"
                             class="form-control form-control-sm datetimepicker date" style="font-size: 8pt;" data-min-view="2" data-date-format="yyyy-mm-dd" placeholder="From" value="{{ @$educ->attendance_from }}">
      {!! $errors->first('XXXXX', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
    </div>
    <div class="col-1 mt-4  text-center font-weight-bold pr-0">
      <input size="16" type="text"  name="primary[0][attendance_to]"
                             class="form-control form-control-sm datetimepicker date" style="font-size: 8pt;" data-min-view="2" data-date-format="yyyy-mm-dd" placeholder="To" value="{{ @$educ->attendance_to }}">
      {!! $errors->first('XXXXX', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
    </div>
    <div class="col-1 mt-4">
      {{ Form::text('primary[0][level]', $educ->level, [
              'class' => 'form-control form-control-sm',
          ])
      }}
      {!! $errors->first('XXXXX', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
    </div>
    <div class="col-1 mt-4">
      {{ Form::text('primary[0][graduated]', $educ->graduated, [
              'class' => 'form-control form-control-sm',
          ])
      }}
      {!! $errors->first('XXXXX', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
    </div>
    <div class="col-2 mt-4">
      {{ Form::text('primary[0][awards]', $educ->awards, [
              'class' => 'form-control form-control-sm col-9 pr-0 mr-1',
          ])
      }}
      {!! $errors->first('XXXXX', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
    </div>
  </div>
  @endif

  @if($educ->educ_level == 2)

  {{--SECONDARY--}}
  <input type="hidden" name="secondary[0][id]" value="{{$educ->id}}">
  <input type="hidden" name="secondary[0][applicant_id]" value="{{$applicant->id}}">
  <div class="form-group row">
    {{ Form::label('primary_name', 'Secondary', ['class'=>'col-form-label col-1 font-weight-bold col']) }}
    <div class="col-2 pr-0">
      {{ Form::text('secondary[0][school_name]', $educ->school_name, [
              'class' => 'form-control form-control-sm',
              'required' => 'true'
          ])
      }}
      {!! $errors->first('XXXXX', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
    </div>
    <div class="col-1"></div>
    <div class="col-2 pr-0 pl-0">
      {{ Form::text('secondary[0][course]', $educ->course, [
              'class' => 'form-control form-control-sm',
          ])
      }}
      {!! $errors->first('XXXXX', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
    </div>
    <div class="col-1 text-center font-weight-bold pr-0">
      <input size="16" type="text"  name="secondary[0][attendance_from]"
                             class="form-control form-control-sm datetimepicker date" style="font-size: 8pt;" data-min-view="2" data-date-format="yyyy-mm-dd" placeholder="From" value="{{ @$educ->attendance_from }}">
      {!! $errors->first('XXXXX', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
    </div>
    <div class="col-1 text-center font-weight-bold pr-0">
      <input size="16" type="text"  name="secondary[0][attendance_to]"
                             class="form-control form-control-sm datetimepicker date" style="font-size: 8pt;" data-min-view="2" data-date-format="yyyy-mm-dd" placeholder="To" value="{{ @$educ->attendance_to }}">
      {!! $errors->first('XXXXX', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
    </div>
    <div class="col-1">
      {{ Form::text('secondary[0][level]', $educ->level, [
              'class' => 'form-control form-control-sm',
          ])
      }}
      {!! $errors->first('XXXXX', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
    </div>
    <div class="col-1">
      {{ Form::text('secondary[0][graduated]', $educ->graduated, [
              'class' => 'form-control form-control-sm',
          ])
      }}
      {!! $errors->first('XXXXX', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
    </div>
    <div class="col-2">
      {{ Form::text('secondary[0][awards]', $educ->awards, [
              'class' => 'form-control form-control-sm col-9 pr-0 mr-1',
          ])
      }}
      {!! $errors->first('XXXXX', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
    </div>
  </div>

  @endif
  @endforeach

@else

<div class="form-group row">
  <div class="col-form-label col-1 font-weight-bold mt-4">
    Primary
  </div>
  <input type="hidden" name="primary[0][applicant_id]" value="{{$applicant->id}}">
  <div class="col-2 mt-4 pr-0">
    {{ Form::text('primary[0][school_name]', '', [
            'class' => 'form-control form-control-sm',
            'required' => true,
        ])
    }}
    {!! $errors->first('XXXXX', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
  </div>
  <div class="col-1"></div>
  <div class="col-2 mt-4 pr-0 pl-0">
    {{ Form::text('primary[0][course]', '', [
            'class' => 'form-control form-control-sm',
        ])
    }}
    {!! $errors->first('XXXXX', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
  </div>
  <div class="col-1 text-center font-weight-bold pr-0 mt-4">
    <input size="16" type="text"  name="primary[0][attendance_from]"
                           class="form-control form-control-sm datetimepicker date" style="font-size: 8pt;" data-min-view="2" data-date-format="yyyy-mm-dd" placeholder="From">
    {!! $errors->first('XXXXX', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
  </div>
  <div class="col-1 text-center font-weight-bold pr-0 mt-4">
    <input size="16" type="text"  name="primary[0][attendance_to]"
                           class="form-control form-control-sm datetimepicker date" style="font-size: 8pt;" data-min-view="2" data-date-format="yyyy-mm-dd" placeholder="To">
    {!! $errors->first('XXXXX', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
  </div>
  <div class="col-1 mt-4">
    {{ Form::text('primary[0][level]', '', [
            'class' => 'form-control form-control-sm',
        ])
    }}
    {!! $errors->first('XXXXX', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
  </div>
  <div class="col-1 mt-4">
    {{ Form::text('primary[0][graduated]', '', [
            'class' => 'form-control form-control-sm',
        ])
    }}
    {!! $errors->first('XXXXX', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
  </div>
  <div class="col-2 mt-4">
    {{ Form::text('primary[0][awards]', '', [
            'class' => 'form-control form-control-sm col-9 pr-0 mr-1',
        ])
    }}
    {!! $errors->first('XXXXX', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
  </div>
</div>

{{--SECONDARY--}}
<input type="hidden" name="secondary[0][applicant_id]" value="{{$applicant->id}}">
<div class="form-group row">
  {{ Form::label('primary_name', 'Secondary', ['class'=>'col-form-label col-1 font-weight-bold col']) }}
  <div class="col-2 pr-0">
    {{ Form::text('secondary[0][school_name]', '', [
            'class' => 'form-control form-control-sm',
            'required' => true,
        ])
    }}
    {!! $errors->first('XXXXX', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
  </div>
  <div class="col-1"></div>
  <div class="col-2 pr-0 pl-0">
    {{ Form::text('secondary[0][course]', '', [
            'class' => 'form-control form-control-sm',
        ])
    }}
    {!! $errors->first('XXXXX', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
  </div>
  <div class="col-1 text-center font-weight-bold pr-0">
    <input size="16" type="text"  name="secondary[0][attendance_from]"
                           class="form-control form-control-sm datetimepicker date" style="font-size: 8pt;" data-min-view="2" data-date-format="yyyy-mm-dd" placeholder="From">
    {!! $errors->first('XXXXX', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
  </div>
  <div class="col-1 text-center font-weight-bold pr-0">
    <input size="16" type="text"  name="secondary[0][attendance_to]"
                           class="form-control form-control-sm datetimepicker date" style="font-size: 8pt;" data-min-view="2" data-date-format="yyyy-mm-dd" placeholder="To">
    {!! $errors->first('XXXXX', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
  </div>
  <div class="col-1">
    {{ Form::text('secondary[0][level]', '', [
            'class' => 'form-control form-control-sm',
        ])
    }}
    {!! $errors->first('XXXXX', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
  </div>
  <div class="col-1">
    {{ Form::text('secondary[0][graduated]', '', [
            'class' => 'form-control form-control-sm',
        ])
    }}
    {!! $errors->first('XXXXX', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
  </div>
  <div class="col-2">
    {{ Form::text('secondary[0][awards]', '', [
            'class' => 'form-control form-control-sm col-9 pr-0 mr-1',
        ])
    }}
    {!! $errors->first('XXXXX', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
  </div>
</div>
@endif

{{--VOCATIONAL--}}
<div class="row">
  <div class="offset-1 col-1">
    <a href="#" id="add_vocational" class="btn btn-sm btn-info">Add</a>
  </div>
</div>

<?php
    $college_ctr      = 0;
    $college_ctr2     = 0;
    $vocational_ctr   = 0;
    $vocational_ctr2  = 0;
    $graduate_ctr     = 0;
    $graduate_ctr2    = 0;
?>

@if(count($applicant->education) > 0)

  @foreach($applicant->education as $key => $educ)
    @if($educ->educ_level == 3)
    <?php $vocational_ctr2 += 1; ?>
    @elseif($educ->educ_level == 4)
    <?php $college_ctr2 += 1; ?>
    @elseif($educ->educ_level == 5)
    <?php $graduate_ctr2 += 1; ?>
    @endif
  @endforeach

@endif

@if(count($applicant->education) > 0)
<?php $vocational = ''; ?>
@foreach($applicant->education as $key => $educ)
  @if($educ->educ_level == 3)
  <input type="hidden" name="vocational[{{$key}}][id]" value="{{$educ->id}}">
  <input type="hidden" name="vocational[{{$key}}][applicant_id]" value="{{$applicant->id}}">
  <?php
    $vocational = ($vocational == '') ? 'Vocational' : '-';
    $vocational_ctr += 1;
  ?>
  <div class="form-group row {{ ($vocational_ctr2 == $vocational_ctr) ? 'vocational' : '' }}">
    {{ Form::label('primary_name', $vocational, ['class'=>'col-form-label col-1 font-weight-bold col']) }}
    <div class="col-2 pr-0">
      <input type="text" name="vocational[{{$key}}][school_name]" class="form-control form-control-sm" value="{{$educ->school_name}}">
    </div>
    <div class="col-1"></div>
    <div class="col-2 pr-0 pl-0">
      <input type="text" name="vocational[{{$key}}][course]" class="form-control form-control-sm" value="{{$educ->course}}">
    </div>
    <div class="col-1 text-center font-weight-bold pr-0">
      <input size="16" type="text"  name="vocational[{{$key}}][attendance_from]"
                           class="form-control form-control-sm datetimepicker date" style="font-size: 8pt;" data-min-view="2" data-date-format="yyyy-mm-dd" placeholder="From" value="{{$educ->attendance_from}}">
    </div>
    <div class="col-1 text-center font-weight-bold pr-0">
      <input size="16" type="text"  name="vocational[{{$key}}][attendance_to]"
                           class="form-control form-control-sm datetimepicker date" style="font-size: 8pt;" data-min-view="2" data-date-format="yyyy-mm-dd" placeholder="To" value="{{$educ->attendance_to}}">
    </div>
    <div class="col-1">
      <input type="text" name="vocational[{{$key}}][level]" class="form-control form-control-sm" value="{{$educ->level}}">
    </div>
    <div class="col-1">
      <input type="text" name="vocational[{{$key}}][graduated]" class="form-control form-control-sm" value="{{$educ->graduated}}">
    </div>
    <div class="col-2">
      <input type="text" name="vocational[{{$key}}][awards]" class="form-control form-control-sm col-6 pr-0 mr-1" value="{{$educ->awards}}" style="display: inline-block;">
      <a  class="btn btn-danger col-3 remove" data-id="{{$educ->id}}" data-level="education"><i class="icon mdi mdi-delete" style="color:#fff !important;"></i></a>
    </div>
  </div>
  @endif
@endforeach
<input type="hidden" id="vocational_ctr" value="{{$vocational_ctr}}" >
@endif

@if(!in_array(3,$has_educ))
<input type="hidden" name="vocational[0][applicant_id]" value="{{$applicant->id}}">
<div class="form-group row vocational">
  {{ Form::label('primary_name', 'Vocational', ['class'=>'col-form-label col-1 font-weight-bold col']) }}
  <div class="col-2 pr-0">
    {{ Form::text('vocational[0][school_name]', '', [
            'class' => 'form-control form-control-sm',
        ])
    }}
  </div>
  <div class="col-1"></div>
  <div class="col-2 pr-0 pl-0">
    {{ Form::text('vocational[0][course]', '', [
            'class' => 'form-control form-control-sm',
        ])
    }}
  </div>
  <div class="col-1 text-center font-weight-bold pr-0">
    <input size="16" type="text"  name="vocational[0][attendance_frpm]"
                           class="form-control form-control-sm datetimepicker date" style="font-size: 8pt;" data-min-view="2" data-date-format="yyyy-mm-dd" placeholder="From">
  </div>
  <div class="col-1 text-center font-weight-bold pr-0">
    <input size="16" type="text"  name="vocational[0][attendance_to]"
                           class="form-control form-control-sm datetimepicker date" style="font-size: 8pt;" data-min-view="2" data-date-format="yyyy-mm-dd" placeholder="To">
  </div>
  <div class="col-1">
    {{ Form::text('vocational[0][level]', '', [
            'class' => 'form-control form-control-sm',
        ])
    }}
  </div>
  <div class="col-1">
    {{ Form::text('vocational[0][graduated]', '', [
            'class' => 'form-control form-control-sm',
        ])
    }}
  </div>
  <div class="col-2">
    {{ Form::text('vocational[0][awards]', '', [
            'class' => 'form-control form-control-sm col-9 pr-0 mr-1',
        ])
    }}
  </div>
</div>
@endif

{{--COLLEGE--}}
<div class="row">
  <div class="offset-1 col-1">
    <a href="#" id="add_college" class="btn btn-sm btn-info">Add</a>
  </div>
</div>

@if(count($applicant->education) > 0)
<?php
  $college       = '';
?>

@foreach($applicant->education as $key => $educ)

@if($educ->educ_level == 4)
<?php
  $college = ($college == '') ? 'College' : '-';

  $college_ctr += 1;

?>

<input type="hidden" name="college[{{$key}}][id]" value="{{$educ->id}}">
<input type="hidden" name="college[{{$key}}][applicant_id]" value="{{$applicant->id}}">

<div class="form-group row {{ ($college_ctr2 == $college_ctr || $college_ctr == 1) ? 'college' : '' }}">
  {{ Form::label('primary_name', $college, ['class'=>'col-form-label col-1 font-weight-bold col']) }}
  <div class="col-2 pr-0">
    <input type="text" name="college[{{$key}}][school_name]" class="form-control form-control-sm" value="{{$educ->school_name}}">
  </div>
  <div class="col-1"></div>
  <div class="col-2 pr-0 pl-0">
    <input type="text" name="college[{{$key}}][course]" class="form-control form-control-sm" value="{{$educ->course}}">
  </div>
  <div class="col-1 text-center font-weight-bold pr-0">
    <input size="16" type="text"  name="college[{{$key}}][attendance_from]"
                           class="form-control form-control-sm datetimepicker date" style="font-size: 8pt;" data-min-view="2" data-date-format="yyyy-mm-dd" placeholder="From" value="{{$educ->attendance_from}}">
  </div>
  <div class="col-1 text-center font-weight-bold pr-0">
    <input size="16" type="text"  name="college[{{$key}}][attendance_to]"
                           class="form-control form-control-sm datetimepicker date" style="font-size: 8pt;" data-min-view="2" data-date-format="yyyy-mm-dd" placeholder="To" value="{{$educ->attendance_to}}">
  </div>
  <div class="col-1">
    <input type="text" name="college[{{$key}}][level]" class="form-control form-control-sm" value="{{$educ->level}}">
  </div>
  <div class="col-1">
    <input type="text" name="college[{{$key}}][graduated]" class="form-control form-control-sm" value="{{$educ->graduated}}">
  </div>
  <div class="col-2">
    <input type="text" name="college[{{$key}}][awards]" class="form-control form-control-sm col-6 pr-0 mr-1" value="{{$educ->awards}}" style="display: inline-block;">
    <a  class="btn btn-danger col-3 remove" data-id="{{$educ->id}}" data-level="education"><i class="icon mdi mdi-delete" style="color:#fff !important;"></i></a>
  </div>
</div>
@endif

@endforeach
<input type="hidden" id="college_ctr" value="{{$college_ctr}}" >
@endif

@if(!in_array(4,$has_educ))
<input type="hidden" name="college[0][applicant_id]" value="{{$applicant->id}}">
<div class="form-group row college">
  <label class="col-1 col-form-label text-sm-right">College <span style="color:red;">*</span> </label>
  <div class="col-2 pr-0">
    {{ Form::text('college[0][school_name]', '', [
            'class' => 'form-control form-control-sm',
        ])
    }}
  </div>
  <div class="col-1"></div>
  <div class="col-2 pr-0 pl-0">
    {{ Form::text('college[0][course]', '', [
            'class' => 'form-control form-control-sm',
        ])
    }}
  </div>
  <div class="col-1 text-center font-weight-bold pr-0">
    <input size="16" type="text"  name="college[0][attendance_from]"
                           class="form-control form-control-sm datetimepicker date" style="font-size: 8pt;" data-min-view="2" data-date-format="yyyy-mm-dd" placeholder="From" >
  </div>
  <div class="col-1 text-center font-weight-bold pr-0">
    <input size="16" type="text"  name="college[0][attendance_to]"
                           class="form-control form-control-sm datetimepicker date" style="font-size: 8pt;" data-min-view="2" data-date-format="yyyy-mm-dd" placeholder="To">
  </div>
  <div class="col-1">
    {{ Form::text('college[0][level]', '', [
            'class' => 'form-control form-control-sm',
        ])
    }}
  </div>
  <div class="col-1">
    {{ Form::text('college[0][graduated]', '', [
            'class' => 'form-control form-control-sm',
        ])
    }}
  </div>
  <div class="col-2">
    {{ Form::text('college[0][awards]', '', [
            'class' => 'form-control form-control-sm col-9 pr-0 mr-1',
        ])
    }}
  </div>
</div>
@endif

{{--GRADUATE STUDIES--}}
<div class="row">
  <div class="offset-1 col-1">
    <a href="#" id="add_graduate_studies" class="btn btn-sm btn-info">Add</a>
  </div>
</div>


@if(count($applicant->education) > 0)
<?php $graduate = ''; ?>
@foreach($applicant->education as $key => $educ)
  @if($educ->educ_level == 5)
  <input type="hidden" name="graduate[{{$key}}][id]" value="{{$educ->id}}">
  <input type="hidden" name="graduate[{{$key}}][applicant_id]" value="{{$applicant->id}}">
  <?php
      $graduate = ($graduate == '') ? 'Graduate <br> Studies' : '-';
      $graduate_ctr += 1;
  ?>
  <div class="form-group row {{ ($graduate_ctr2 == $graduate_ctr) ? 'graduate-studies' : '' }} ">
    <label class="col-form-label col-1 font-weight-bold col">{!! $graduate !!}</label>
    <div class="col-2 pr-0">
      <input type="text" name="graduate[{{$key}}][school_name]" class="form-control form-control-sm" value="{{$educ->school_name}}">
    </div>
    <div class="col-1 font-weight-bold">
        <div class="custom-control custom-checkbox mt-2">
          <input class="custom-control-input" type="checkbox" id="check{{$key}}" name="graduate[{{$key}}][ongoing]" {{ ($educ->ongoing) ? 'checked' : '' }}>
          <label class="custom-control-label" for="check{{$key}}" style="font-size: 7pt;" >Ongoing</label>
        </div>
    </div>
    <div class="col-2 pr-0 pl-0">
      <input type="text" name="graduate[{{$key}}][course]" class="form-control form-control-sm" value="{{$educ->course}}">
    </div>
    <div class="col-1 text-center font-weight-bold pr-0">
      <input size="16" type="text"  name="graduate[{{$key}}][attendance_from]"
                             class="form-control form-control-sm datetimepicker date" style="font-size: 8pt;" data-min-view="2" data-date-format="yyyy-mm-dd" placeholder="From" value="{{$educ->attendance_from}}">
    </div>
    <div class="col-1 text-center font-weight-bold pr-0">
      <input size="16" type="text"  name="graduate[{{$key}}][attendance_to]"
                             class="form-control form-control-sm datetimepicker date" style="font-size: 8pt;" data-min-view="2" data-date-format="yyyy-mm-dd" placeholder="To" value="{{$educ->attendance_to}}">
    </div>
    <div class="col-1">
      <input type="text" name="graduate[{{$key}}][level]" class="form-control form-control-sm" value="{{$educ->level}}">
    </div>
    <div class="col-1">
      <input type="text" name="graduate[{{$key}}][graduated]" class="form-control form-control-sm" value="{{$educ->graduated}}">
    </div>
    <div class="col-2">
      <input type="text" name="graduate[{{$key}}][awards]" class="form-control form-control-sm col-6 pr-0 mr-1" value="{{$educ->awards}}" style="display: inline;">
      <a  class="btn btn-danger col-3 remove" data-id="{{$educ->id}}" data-level="education"><i class="icon mdi mdi-delete" style="color:#fff !important;"></i></a>
    </div>
  </div>
  @endif

@endforeach
<input type="hidden" id="graduate_ctr" value="{{$graduate_ctr}}" >
@endif

@if(!in_array(5,$has_educ))
<input type="hidden" name="graduate[0][applicant_id]" value="{{$applicant->id}}">
<div class="form-group row graduate-studies">
  <label class="col-form-label col-1 font-weight-bold col">Graduate <br> Studies</label>
  <div class="col-2 pr-0">
    {{ Form::text('graduate[0][school_name]', '', [
            'class' => 'form-control form-control-sm',
        ])
    }}
  </div>
  <div class="col-1 font-weight-bold">
      <div class="custom-control custom-checkbox mt-2">
        <input class="custom-control-input" type="checkbox" id="check0" name="graduate[0][ongoing]" >
        <label class="custom-control-label" for="check0" style="font-size: 7pt;" >Ongoing</label>
      </div>
  </div>
  <div class="col-2 pr-0 pl-0">
    {{ Form::text('graduate[0][course]', '', [
            'class' => 'form-control form-control-sm',
        ])
    }}
  </div>
  <div class="col-1 text-center font-weight-bold pr-0">
    <input size="16" type="text"  name="graduate[0][attendance_from]"
                           class="form-control form-control-sm datetimepicker date" style="font-size: 8pt;" data-min-view="2" data-date-format="yyyy-mm-dd" placeholder="From">
  </div>
  <div class="col-1 text-center font-weight-bold pr-0">
    <input size="16" type="text"  name="graduate[0][attendance_to]"
                           class="form-control form-control-sm datetimepicker date" style="font-size: 8pt;" data-min-view="2" data-date-format="yyyy-mm-dd" placeholder="To">
  </div>
  <div class="col-1">
    {{ Form::text('graduate[0][level]', '', [
            'class' => 'form-control form-control-sm',
        ])
    }}
  </div>
  <div class="col-1">
    {{ Form::text('graduate[0][graduated]', '', [
            'class' => 'form-control form-control-sm',
        ])
    }}
  </div>
  <div class="col-2">
    {{ Form::text('graduate[0][awards]', '', [
            'class' => 'form-control form-control-sm col-9 pr-0 mr-1',
        ])
    }}
  </div>
</div>
@endif
<div class="form-group row text-right">
  <div class="col-12">
    {{ Form::button('Next Step', ['id' => 'btn-form-one', 'class'=>'btn btn-primary btn-space wizard-next', 'data-wizard' => '#wizard1']) }}
    {{ Form::reset('Clear Form', ['class'=>'btn btn-space btn-danger']) }}
  </div>
</div>