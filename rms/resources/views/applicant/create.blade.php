@extends('layouts.app')

@section('content')
    <div class="page-head">
        <h2 class="page-head-title">Add Applicant</h2>
    </div>

    <!-- Applicant Form -->
    <div class="row">
        <div class="col-md-12">
            <div class="card card-border-color card-border-color-primary">
                <div class="card-header card-header-divider">

                    <div class="card-group">
                        <div class="col-7">
                            <span class="card-subtitle col-form-label">Apply for the position below</span>
                        </div>

                        <div class="col-2 text-right">
                            <span class="card-subtitle col-form-label">Applicant Status</span>
                        </div>
                        <div class="col-2">
                            {{ Form::select('applicant_status', config('params.applicant_status'), $applicant->qualified, [
                                'class' => 'form-control form-control-xs',
                                'placeholder' => 'Select status',
                                'id' => 'select_qualified',
                                'required' => true,
                            ])
                        }}
                        </div>
                        <div class="col-1 mt-1">
                            <button class="btn btn-primary btn-save"><i class="mdi mdi-save"></i> Save</button>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    @include('applicant._form', [
                        'action' => $action,
                        'method' => 'POST',
                        'applicant' => $applicant,
                    ])
                </div>
            </div>
        </div>
    </div>
@endsection
