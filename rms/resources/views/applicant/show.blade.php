@extends('layouts.app')

@section('content')
    <div class="page-head">
        <h2 class="page-head-title">Applicant Details</h2>
        <nav aria-label="breadcrumb" role="navigation">
            <ol class="breadcrumb page-head-nav">
                <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Dashboard</a></li>
                <li class="breadcrumb-item active"><a href="{{ route('applicant.index' ) }}">
                        Applicants ( {{ $applicant->getFullName() }} )</a></li>
            </ol>
        </nav>
    </div>

    <!-- Applicant Form -->
    @if(!$documentView)
        <div class="row">
            <div class="col">
                <div class="card card-border-color card-border-color-primary">
                    <div class="card-header">
                        <div class="btn-group mb-4" role="group" aria-label="Basic example">
                            <a href="{{ route('dashboard') }}" class="btn btn-success"><i
                                        class="icon icon-left mdi mdi-home"></i> Dashboard</a>
                            <a href="{{ route('applicant.index') }}" class="btn btn-success"><i
                                        class="icon icon-left mdi mdi-view-list-alt"></i> Applicants</a>
                            <a href="{{ route('applicant.create') }}" class="btn btn-success"><i
                                        class="icon icon-left mdi mdi-account-add"></i> Add an applicant</a>
                        </div>
                        <div class="tools">
                            <button type="button" data-toggle="dropdown" class="btn btn-secondary dropdown-toggle"
                                    aria-expanded="false">
                                <i class="icon icon-left mdi mdi-settings-square"></i> Options <span
                                        class="icon-dropdown mdi mdi-chevron-down"></span>
                            </button>
                            <div role="menu" class="dropdown-menu" x-placement="bottom-start"
                                 style="position: absolute; transform: translate3d(0px, 30px, 0px); top: 0px; left: 0px; will-change: transform;">
                                <a href="{{ route('applicant.edit', $applicant->id) }}" class="dropdown-item"><i
                                            class="icon icon-left mdi mdi-edit"></i>Edit this applicant</a>
                                <div class="dropdown-divider"></div>
                                {!! Form::open([
                                    'method'=>'DELETE',
                                    'url' => ['applicant', $applicant->id],
                                    'style' => 'display:inline'
                                ]) !!}
                                {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> Delete this applicant', array(
                                    'type' => 'submit',
                                    'style' => 'color: #504e4e',
                                    'class' => 'dropdown-item',
                                    'title' => 'Delete Job Post',
                                    'onclick'=>'return confirm("Confirm delete?")'
                                ))!!}
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="user-info-list card">
            <div class="card-header card-header-divider">
                {{ $applicant->getFullName() }}
                <span class="card-subtitle">
                Applied For: {!! ($applicant->job->status == 'plantilla') ? $applicant->job->plantilla_item->position->Name : $applicant->job->cos_position_title !!} / Applied On: {{ $applicant->created_at }}
                </span>
            </div>
            <div class="card-body">
                <table class="no-border no-strip skills">
                    <tbody class="no-border-x no-border-y">
                    <tr>
                        <td class="icon"><span class="mdi mdi-case"></span></td>
                        <td class="item">Position Applied For:</td>
                        <td>{!! ($applicant->job->status == 'plantilla') ? $applicant->job->plantilla_item->position->Name : $applicant->job->cos_position_title !!}</td>
                    </tr>
                    <tr>
                        <td class="icon"><span class="mdi mdi-collection-item-2"></span></td>
                        <td class="item">Application Reference No.</td>
                        <td>{{ $applicant->reference_no }}</td>
                    </tr>
                    <tr>
                        <td class="icon"><span class="mdi mdi-email"></span></td>
                        <td class="item">Email</td>
                        <td>{{ Crypt::decrypt($applicant->email_address) }}</td>
                    </tr>
                    <tr>
                        <td class="icon"><span class="mdi mdi-alert-polygon"></span></td>
                        <td class="item">Status</td>
                        <td>{{ $applicant->job->status }}</td>
                    </tr>
                    <tr>
                        <td class="icon"><span class="mdi mdi-phone"></span></td>
                        <td class="item">Mobile</td>
                        <td>{{ $applicant->mobile_number }}</td>
                    </tr>
                    <tr>
                        <td class="icon"><span class="mdi mdi-phone"></span></td>
                        <td class="item">Profile Picture</td>
                        <td>
                            @if($applicant->image_path)
                                <img src="{{ asset('storage/images/' . $applicant->image_path) }}"
                                     class="img-thumbnail rounded float-left" width="200" height="200"
                                     alt="profile picture">
                            @endif
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="row mb-4">
            <div class="col">
                <div class="card-group"> 
                    <div class="card card-contrast" style="box-shadow: none !important;">
                        <div class="card-header card-header-contrast">
                            Applicant Personal Details
                            <span class="card-subtitle">Detailed information regarding applicant</span>
                        </div>
                        <div class="card-body">
                            <label class="font-weight-bold">Personal Information</label>
                            <table class="no-border no-strip skills">
                                <tbody class="no-border-x no-border-y">
                                <tr>
                                    <td>Birthday</td>
                                    <td> :</td>
                                    <td>{{ Crypt::decrypt($applicant->birthday)  }}</td>
                                </tr>
                                <tr>
                                    <td>Birth Place</td>
                                    <td> :</td>
                                    <td>{{ $applicant->birth_place }}</td>
                                </tr>
                                <tr>
                                    <td>Gender</td>
                                    <td> :</td>
                                    <td>{{ $applicant->gender }}</td>
                                </tr>
                                <tr>
                                    <td>Civil Status</td>
                                    <td> :</td>
                                    <td>{{ $applicant->civil_status }}</td>
                                </tr>
                                <tr>
                                    <td>Citizenship</td>
                                    <td> :</td>
                                    <td>{{ @$applicant->citizenOf->name }}</td>
                                </tr>
                                <tr>
                                    <td>Filipino</td>
                                    <td> :</td>
                                    <td>{{ $applicant->filipino ? 'yes' : 'no' }}</td>
                                </tr>
                                <tr>
                                    <td>Naturalized</td>
                                    <td> :</td>
                                    <td>{{ $applicant->naturalized ? 'yes' : 'no' }}</td>
                                </tr>
                                <tr>
                                    <td>Height</td>
                                    <td> :</td>
                                    <td>{{ $applicant->height }}</td>
                                </tr>
                                <tr>
                                    <td>Weight</td>
                                    <td> :</td>
                                    <td>{{ $applicant->weight }}</td>
                                </tr>
                                <tr>
                                    <td>Blood Type</td>
                                    <td> :</td>
                                    <td>{{ $applicant->blood_type }}</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div class="card card-contrast" style="box-shadow: none !important;">
                        <div class="card-header card-header-contrast">
                            &nbsp;
                            <span class="card-subtitle">&nbsp;</span>
                        </div>
                        <div class="card-body">
                            <label class="font-weight-bold">Present Address</label>
                            <table class="no-border no-strip skills">
                                <tbody class="no-border-x no-border-y">
                                <tr>
                                    <td>House Number</td>
                                    <td> :</td>
                                    <td>{{ Crypt::decrypt($applicant->house_number) }}</td>
                                </tr>
                                <tr>
                                    <td>Street</td>
                                    <td> :</td>
                                    <td>{{ Crypt::decrypt($applicant->street) }}</td>
                                </tr>
                                <tr>
                                    <td>Subdivision</td>
                                    <td> :</td>
                                    <td>{{ Crypt::decrypt($applicant->subdivision) }}</td>
                                </tr>
                                <tr>
                                    <td>Barangay</td>
                                    <td> :</td>
                                    <td>{{ Crypt::decrypt($applicant->barangay) }}</td>
                                </tr>
                                <tr>
                                    <td>City</td>
                                    <td> :</td>
                                    <td>{{ Crypt::decrypt($applicant->city) }}</td>
                                </tr>
                                <tr>
                                    <td>Province</td>
                                    <td> :</td>
                                    <td>{{ Crypt::decrypt($applicant->province) }}</td>
                                </tr>
                                <tr>
                                    <td>Zip Code</td>
                                    <td> :</td>
                                    <td>{{ Crypt::decrypt($applicant->zip_code) }}</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div class="card card-contrast" style="box-shadow: none !important;">
                        <div class="card-header card-header-contrast">
                            &nbsp;
                            <span class="card-subtitle">&nbsp;</span>
                        </div>
                        <div class="card-body">
                            <table class="no-border no-strip skills">
                                <label class="font-weight-bold">Permanent Address</label>
                                <tbody class="no-border-x no-border-y">
                                <tr>
                                    <td>House Number</td>
                                    <td> :</td>
                                    <td>{{ Crypt::decrypt($applicant->permanent_house_number) }}</td>
                                </tr>
                                <tr>
                                    <td>Street</td>
                                    <td> :</td>
                                    <td>{{ Crypt::decrypt($applicant->permanent_street) }}</td>
                                </tr>
                                <tr>
                                    <td>Subdivision</td>
                                    <td> :</td>
                                    <td>{{ Crypt::decrypt($applicant->permanent_subdivision) }}</td>
                                </tr>
                                <tr>
                                    <td>Barangay</td>
                                    <td> :</td>
                                    <td>{{ Crypt::decrypt($applicant->permanent_barangay) }}</td>
                                </tr>
                                <tr>
                                    <td>City</td>
                                    <td> :</td>
                                    <td>{{ Crypt::decrypt($applicant->permanent_city) }}</td>
                                </tr>
                                <tr>
                                    <td>Province</td>
                                    <td> :</td>
                                    <td>{{ Crypt::decrypt($applicant->permanent_province) }}</td>
                                </tr>
                                <tr>
                                    <td>Zip Code</td>
                                    <td> :</td>
                                    <td>{{ Crypt::decrypt($applicant->permanent_zip_code) }}</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- <div class="row">
            <div class="col">
                <div class="card card-contrast">
                    <div class="card-header card-header-contrast">
                        Government ID's
                        <span class="card-subtitle">Government Related Details</span>
                    </div>
                    <div class="card-body">
                        <table class="no-border no-strip skills">
                            <tbody class="no-border-x no-border-y">
                            <tr>
                                <td>Pagibig</td>
                                <td> :</td>
                                <td>{{ $applicant->pagibig }}</td>
                            </tr>
                            <tr>
                                <td>GSIS Number</td>
                                <td> :</td>
                                <td>{{ $applicant->gsis }}</td>
                            </tr>
                            <tr>
                                <td>Philhealth</td>
                                <td> :</td>
                                <td>{{ $applicant->philhealth }}</td>
                            </tr>
                            <tr>
                                <td>Tin Number</td>
                                <td> :</td>
                                <td>{{ $applicant->tin }}</td>
                            </tr>
                            <tr>
                                <td>SSS Number</td>
                                <td> :</td>
                                <td>{{ $applicant->sss }}</td>
                            </tr>
                            <tr>
                                <td>Government Issue ID</td>
                                <td> :</td>
                                <td>{{ $applicant->govt_issued_id }}</td>
                            </tr>
                            <tr>
                                <td>Government Issued ID Number</td>
                                <td> :</td>
                                <td>{{ $applicant->govt_id_issued_number }}</td>
                            </tr>
                            <tr>
                                <td>Government ID place of issuance</td>
                                <td> :</td>
                                <td>{{ $applicant->govt_id_issued_place }}</td>
                            </tr>
                            <tr>
                                <td>Government ID Date Issued</td>
                                <td> :</td>
                                <td>{{ $applicant->govt_id_date_issued }}</td>
                            </tr>
                            <tr>
                                <td>Government ID Valid Until</td>
                                <td> :</td>
                                <td>{{ $applicant->govt_id_valid_until }}</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div> -->

        <div class="row">
            <div class="col">
                <div class="card card-contrast">
                    <div class="card-header card-header-contrast">
                        Qualification
                        <span class="card-subtitle">Detailed applicants qualifications</span>
                    </div>
                    <div class="card-body">
                        <table class="no-border no-strip skills">
                            <tbody class="no-border-x no-border-y">
                            <tr>
                                <td style="vertical-align: top">Education</td>
                                <td style="vertical-align: top">:</td>
                                <td style="vertical-align: top;">
                                    @if($applicant->education)
                                        <?php
                                            $educ_3 = false;
                                            $educ_4 = false;
                                            $educ_5 = false;
                                         ?>
                                        @foreach($applicant->education as $education)

                                        @if($education->educ_level == 1)
                                        <p class="font-weight-bold p-0 m-0 ml-2">Primary School</p>
                                        <p class="p-0 m-0 ml-4">{!! $education->school_name !!}</p>
                                        @endif

                                        @if($education->educ_level == 2)
                                        <p class="font-weight-bold p-0 m-0 ml-2">Secondary School</p>
                                        <p class="p-0 m-0 ml-4">{!! $education->school_name !!}</p>
                                        @endif

                                        @if($education->educ_level == 3)
                                            @if($educ_3 == false)
                                            <p class="font-weight-bold p-0 m-0 ml-2">Vocational School</p>
                                            <?php $educ_3 = true; ?>
                                            @endif
                                        <p class="p-0 m-0 ml-4">{!! $education->school_name !!}</p>
                                        @endif

                                        @if($education->educ_level == 4)
                                            @if($educ_4 == false)
                                            <p class="font-weight-bold p-0 m-0 ml-2">College School</p>
                                            <?php $educ_4 = true; ?>
                                            @endif
                                        <p class="p-0 m-0 ml-4">{!! $education->school_name !!} {!! $education->course !!}</p>
                                        @endif

                                        @if($education->educ_level == 5)
                                            @if($educ_5 == false)
                                                <p class="font-weight-bold p-0 m-0 ml-2">Graduate School</p>
                                                <?php $educ_5 = true; ?>
                                            @endif
                                        <p class="p-0 m-0 ml-4">{!! $education->school_name !!} {!! $education->course !!}</p>
                                        @endif

                                        @endforeach
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <td style="vertical-align: top;">Work Experience</td>
                                <td style="vertical-align: top;"> :</td>
                                <td style="vertical-align: top;">
                                    @if($applicant->workexperience)
                                        @foreach($applicant->workexperience as $experience)
                                        <p class="p-0 m-0 ml-2">{!! $experience->position_title !!}</p>
                                        @endforeach
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <td style="vertical-align: top;">Training</td>
                                <td style="vertical-align: top;"> :</td>
                                <td style="vertical-align: top;">
                                    @if($applicant->training)
                                        @foreach($applicant->training as $training)
                                        <p class="p-0 m-0 ml-2">{!! $training->title_learning_programs !!}</p>
                                        @endforeach
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <td style="vertical-align: top;">Eligibility</td>
                                <td style="vertical-align: top;"> :</td>
                                <td style="vertical-align: top;">
                                    @if($applicant->eligibility)
                                        @foreach($applicant->eligibility as $eligibility)
                                        <p class="p-0 m-0 ml-2">{!! @$eligibility->name !!}</p>
                                        @endforeach
                                    @endif
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    @endif

    <div class="row">
        <div class="col">
            <div class="card card-contrast">
                <div class="card-header card-header-contrast">
                    Attached Documents
                    <span class="card-subtitle">All applicant documents attached below</span>
                </div>
                <div class="card-body">
                    <!-- 1 -->
                    @if($applicant->application_letter_path)
                        <div class="form-group row">
                            <div class="col-6 mt-1"><span class="badge badge-success">Application Letter</span></div>
                            <div class="col-0 p-1">
                                <a href="{{ asset('documents/' . $applicant->application_letter_path) }}" target="_blank" class="badge badge-info"><span class="icon icon-left mdi mdi-eye"></span> View</a>
                            </div>
                            <div class="col-0 p-1">
                                <a href="{{ asset('documents/' . $applicant->application_letter_path) }}" class="badge badge-success" download>
                                    <span class="icon mdi mdi-download"> Download</span>
                                </a>
                            </div>
                        </div>
                    @endif

                    <!-- 2 -->
                    @if($applicant->pds_path)
                        <div class="form-group row">
                            <div class="col-6 mt-1"><span class="badge badge-success">Fully Accomplished personat Data Sheet (revise d 2017)</span></div>
                            <div class="col-0 p-1">
                                <a href="{{ asset('documents/' . $applicant->pds_path) }}" target="_blank" class="badge badge-info"><span class="icon icon-left mdi mdi-eye"></span> View</a>
                            </div>
                            <div class="col-0 p-1">
                                <a href="{{ asset('documents/' . $applicant->pds_path) }}" download  class="badge badge-success" >
                                    <span class="icon mdi mdi-download"> Download</span>
                                </a>
                            </div>
                        </div>
                    @endif

                    <!-- 3 -->
                    @if($applicant->curriculum_vitae_path)
                        <div class="form-group row">
                            <div class="col-6 mt-1"><span class="badge badge-success">Curriculum Vitae with detailed job description;</span></div>
                            <div class="col-0 p-1">
                                <a href="{{ asset('documents/' . $applicant->curriculum_vitae_path) }}" target="_blank" class="badge badge-info"><span class="icon icon-left mdi mdi-eye"></span> View</a>
                            </div>
                            <div class="col-0 p-1">
                                <a href="{{ asset('documents/' . $applicant->curriculum_vitae_path) }}" download  class="badge badge-success" >
                                    <span class="icon mdi mdi-download"> Download</span>
                                </a>
                            </div>
                        </div>
                    @endif

                    <!-- 4 -->
                    @if($applicant->tor_path)
                        <div class="form-group row">
                            <div class="col-6 mt-1"><span class="badge badge-success">Copy of Transcript of Records and Diploma;</span></div>
                            <div class="col-0 p-1">
                                <a href="{{ asset('documents/' . $applicant->tor_path) }}" target="_blank" class="badge badge-info"><span class="icon icon-left mdi mdi-eye"></span> View</a>
                            </div>
                            <div class="col-0 p-1">
                                <a href="{{ asset('documents/' . $applicant->tor_path) }}" download class="badge badge-success" >
                                    <span class="icon mdi mdi-download"> Download</span>
                                </a>
                            </div>
                        </div>
                    @endif

                    <!-- 5 -->
                    @if($applicant->training_certificate_path)
                        <div class="form-group row">
                            <div class="col-6 mt-1"><span class="badge badge-success">Copy of Certificate of training/seminars attended;</span></div>
                            <div class="col-0 p-1">
                                <a href="{{ asset('documents/' . $applicant->training_certificate_path) }}" target="_blank" class="badge badge-info"><span class="icon icon-left mdi mdi-eye"></span> View</a>
                            </div>
                            <div class="col-0 p-1">
                                <a href="{{ asset('documents/' . $applicant->training_certificate_path) }}" download class="badge badge-success" >
                                    <span class="icon mdi mdi-download"> Download</span>
                                </a>
                            </div>
                        </div>
                    @endif

                    <!-- 6 -->
                    @if($applicant->employment_certificate_path)
                        <div class="form-group row">
                            <div class="col-6 mt-1"><span class="badge badge-success">Copy of Certificate of Employment</span></div>
                            <div class="col-0 p-1">
                                <a href="{{ asset('documents/' . $applicant->application_letter_path) }}" target="_blank" class="badge badge-info"><span class="icon icon-left mdi mdi-eye"></span> View</a>
                            </div>
                            <div class="col-0 p-1">
                                <a href="{{ asset('documents/' . $applicant->employment_certificate_path) }}" download class="badge badge-success" >
                                    <span class="icon mdi mdi-download"> Download</span>
                                </a>
                            </div>
                        </div>
                    @endif

                    <!-- 7 -->
                    @if($applicant->info_sheet_path)
                        <div class="form-group row">
                            <div class="col-6 mt-1"><span class="badge badge-success">Performance Evaluation (if applicable)</span></div>
                            <div class="col-0 p-1">
                                <a href="{{ asset('documents/' . $applicant->info_sheet_path) }}" target="_blank" class="badge badge-info"><span class="icon icon-left mdi mdi-eye"></span> View</a>
                            </div>
                            <div class="col-0 p-1">
                                <a href="{{ asset('documents/' . $applicant->info_sheet_path) }}" download class="badge badge-success" >
                                    <span class="icon mdi mdi-download"> Download</span>
                                </a>
                            </div>
                        </div>
                    @endif

                    <!-- 8 -->
                    @if($applicant->coe_path)
                        <div class="form-group row">
                            <div class="col-6 mt-1"><span class="badge badge-success">Certificate of etigibitity/rating/ticense (if appticabte)</span></div>
                            <div class="col-0 p-1">
                                <a href="{{ asset('documents/' . $applicant->coe_path) }}" target="_blank" class="badge badge-info"><span class="icon icon-left mdi mdi-eye"></span> View</a>
                            </div>
                            <div class="col-0 p-1">
                                <a href="{{ asset('documents/' . $applicant->coe_path) }}" download class="badge badge-success" >
                                    <span class="icon mdi mdi-download"> Download</span>
                                </a>
                            </div>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{ URL::asset('beagle-assets/js/app-tables-datatables.js') }}" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            //initialize the javascript
            App.init();
        });
    </script>
@endsection
