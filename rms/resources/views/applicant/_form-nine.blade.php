<div class="row form-group">
		<div class="col-12">
			<p>Pursuant to: (a) Indigenous People's Act (RA 8371); (b) Magna Carta for Disabled Persons (RA 7277); and (c) Solo Parents Welfare Act of 2000 (RA 8972), please answer the following items:</p>
		</div>
</div>

<div class="row form-group">
		<div class="col-3">
			<p class="col-form-label text-left">A. Are you a member of any indigenous group?</p> 
		</div>
    <div class="col-1 pt-1">
      <div class="switch-button switch-button-success switch-button-yesno">
          <input type="checkbox" id="swt01" {{ @$applicant->indigenous_member ? 'checked' : '' }}><span>
          <label for="swt01"></label></span>
      </div>
    </div>
    <div class="col-2 pl-0 ml-0">
    	<label class="col-form-label">If YES, Please Specify :</label>
    </div>
    <div class="col-3">
    	<input type="text" name="indigenous_member" class="form-control form-control-sm" value="{{ $applicant->indigenous_member }}">
    </div>
</div>

<div class="row form-group">
		<div class="col-3">
			<p class="col-form-label text-left">B. Are you a person with disability?</p> 
		</div>
    <div class="col-1 pt-1">
      <div class="switch-button switch-button-success switch-button-yesno">
          <input type="checkbox" id="swt02" {{ @$applicant->person_id_no ? 'checked' : '' }} ><span>
          <label for="swt02"></label></span>
      </div>
    </div>
    <div class="col-2 pl-0 ml-0">
    	<label class="col-form-label">If YES, Please Specify ID No :</label>
    </div>
    <div class="col-3">
    	<input type="text" name="person_id_no" class="form-control form-control-sm" placeholder="ID Number" value="{{ $applicant->person_id_no }}">
    </div>
</div>

<div class="row form-group">
		<div class="col-3">
			<p class="col-form-label text-left">C Are you a solo parent?</p> 
		</div>
    <div class="col-1 pt-1">
      <div class="switch-button switch-button-success switch-button-yesno">
          <input type="checkbox" id="swt03" {{ @$applicant->solo_id_no ? 'checked' : '' }}><span>
          <label for="swt03"></label></span>
      </div>
    </div>
    <div class="col-2 pl-0 ml-0">
    	<label class="col-form-label">If YES, Please Specify ID No :</label>
    </div>
    <div class="col-3">
    	<input type="text" name="solo_id_no" class="form-control form-control-sm" placeholder="ID Number" value="{{ $applicant->solo_id_no }}">
    </div>
</div>

