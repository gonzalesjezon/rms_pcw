
<ol>
    <li>
        <div class="row">
            <div class="col-5">
                <label class="col-form-label">Application Letter</label>
            </div>

            <div class="col-7">
                  <input id="application_letter_path" type="file" name="application_letter_path" data-multiple-caption="{count} files selected" multiple=""
                       class="inputfile">
                <label for="application_letter_path" class="btn-primary"> <i class="mdi mdi-upload"></i><span>Browse files...</span></label>
                <span class="badge badge-primary">{{ @$applicant->application_letter_path }}</span>  
            </div>
        </div>
    </li>

    <li>

        <div class="row">
            <div class="col-5">
                <label class="col-form-label">Fully Accomplished personat Data Sheet (revise d 2017)</label>
            </div>

            <div class="col-7">
                  <input id="pds_path" type="file" name="pds_path" data-multiple-caption="{count} files selected" multiple=""
                   class="inputfile">
            <label for="pds_path" class="btn-primary"> <i class="mdi mdi-upload"></i><span>Browse files...</span></label>
            <span class="badge badge-primary">{{ @$applicant->pds_path }}</span>
            </div>
        </div>
        
    </li>

    <li>

        <div class="row">
            <div class="col-5">
                <label class="col-form-label">Curriculum Vitae with detailed job description;</label>
            </div>

            <div class="col-7">
                <input id="curriculum_vitae_path" type="file" name="curriculum_vitae_path" data-multiple-caption="{count} files selected" multiple=""
                   class="inputfile">
                <label for="curriculum_vitae_path" class="btn-primary"> <i class="mdi mdi-upload"></i><span>Browse files...</span></label>
                <span class="badge badge-primary"></span>
            </div>
        </div>
        
    </li>

    <li>

        <div class="row">
            <div class="col-5">
                <label class="col-form-label">Copy of Transcript of Records and Diploma;</label>
            </div>

            <div class="col-7">
                <input id="tor_path" type="file" name="tor_path" data-multiple-caption="{count} files selected" multiple=""
                   class="inputfile">
                <label for="tor_path" class="btn-primary"> <i class="mdi mdi-upload"></i><span>Browse files...</span></label>
                <span class="badge badge-primary">{{ @$applicant->tor_path }}</span>
            </div>
        </div>
        
    </li>

    <li>

        <div class="row">
            <div class="col-5">
                <label class="col-form-label"> Copy of Certificate of training/seminars attended; </label>
            </div>

            <div class="col-7">
                <input id="training_certificate_path" type="file" name="training_certificate_path" data-multiple-caption="{count} files selected" multiple=""
                   class="inputfile">
                <label for="training_certificate_path" class="btn-primary"> <i class="mdi mdi-upload"></i><span>Browse files...</span></label>
                <span class="badge badge-primary">{{ @$applicant->training_certificate_path }}</span>
            </div>
        </div>
        
    </li>

    <li>

        <div class="row">
            <div class="col-5">
                <label class="col-form-label"> Copy of Certificate of Employment </label>
            </div>

            <div class="col-7">
                <input id="employment_certificate_path" type="file" name="employment_certificate_path" data-multiple-caption="{count} files selected" multiple=""
                   class="inputfile">
                <label for="employment_certificate_path" class="btn-primary"> <i class="mdi mdi-upload"></i><span>Browse files...</span></label>
                <span class="badge badge-primary">{{ @$applicant->employment_certificate_path }}</span>
            </div>
        </div>
        
    </li>

    <li>

        <div class="row">
            <div class="col-5">
                <label class="col-form-label">  Performance Evaluation (if applicable) </label>
            </div>

            <div class="col-7">
                <input id="info_sheet_path" type="file" name="info_sheet_path" data-multiple-caption="{count} files selected" multiple=""
                   class="inputfile">
                <label for="info_sheet_path" class="btn-primary"> <i class="mdi mdi-upload"></i><span>Browse files...</span></label>
                <span class="badge badge-primary">{{ @$applicant->info_sheet_path }}</span>
            </div>
        </div>
        
    </li>

    <li>

        <div class="row">
            <div class="col-5">
                <label class="col-form-label"> Certificate of etigibitity/rating/ticense (if appticabte) </label>
            </div>

            <div class="col-7">
                <input id="coe_path" type="file" name="coe_path" data-multiple-caption="{count} files selected" multiple=""
                   class="inputfile">
                <label for="coe_path" class="btn-primary"> <i class="mdi mdi-upload"></i><span>Browse files...</span></label>
                <span class="badge badge-primary">{{ @$applicant->coe_path }}</span>
            </div>
        </div>
        
    </li>
</ol>

<div class="form-group row text-right">
    <div class="col col-sm-10 col-lg-9 offset-sm-1 offset-lg-0">
        {{ Form::button('Next Step', ['id' => 'btn-form-one', 'class'=>'btn btn-primary btn-space wizard-next', 'data-wizard' => '#wizard1']) }}
        {{ Form::reset('Clear Form', ['class'=>'btn btn-space btn-danger']) }}
    </div>
</div>
