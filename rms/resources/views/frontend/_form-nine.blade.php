<div class="row form-group">
		<div class="col-12">
			<p>Pursuant to: (a) Indigenous People's Act (RA 8371); (b) Magna Carta for Disabled Persons (RA 7277); and (c) Solo Parents Welfare Act of 2000 (RA 8972), please answer the following items:</p>
		</div>
</div>

<div class="row form-group">
		<div class="col-3">
			<p class="col-form-label text-left">A. Are you a member of any indigenous group?</p> 
		</div>
    <div class="col-1 pt-1">
      <div class="switch-button switch-button-success switch-button-yesno">
          <input type="checkbox" id="swt01"><span>
          <label for="swt01"></label></span>
      </div>
    </div>
    <div class="col-2 pl-0 ml-0">
    	<label class="col-form-label">If YES, Please Specify :</label>
    </div>
    <div class="col-3">
    	<input type="text" name="indigenous_member" class="form-control form-control-sm">
    </div>
</div>

<div class="row form-group">
		<div class="col-3">
			<p class="col-form-label text-left">B. Are you a person with disability?</p> 
		</div>
    <div class="col-1 pt-1">
      <div class="switch-button switch-button-success switch-button-yesno">
          <input type="checkbox" id="swt02"><span>
          <label for="swt02"></label></span>
      </div>
    </div>
    <div class="col-2 pl-0 ml-0">
    	<label class="col-form-label">If YES, Please Specify ID No :</label>
    </div>
    <div class="col-3">
    	<input type="text" name="person_id_no" class="form-control form-control-sm" placeholder="ID Number">
    </div>
</div>

<div class="row form-group">
		<div class="col-3">
			<p class="col-form-label text-left">C Are you a solo parent?</p> 
		</div>
    <div class="col-1 pt-1">
      <div class="switch-button switch-button-success switch-button-yesno">
          <input type="checkbox" id="swt03"><span>
          <label for="swt03"></label></span>
      </div>
    </div>
    <div class="col-2 pl-0 ml-0">
    	<label class="col-form-label">If YES, Please Specify ID No :</label>
    </div>
    <div class="col-3">
    	<input type="text" name="solo_id_no" class="form-control form-control-sm" placeholder="ID Number">
    </div>
</div>

<div class="form-group row text-right">
  <div class="col-12">
    {{ Form::button('Previous', ['class'=>'btn btn-secondary btn-space wizard-previous btn-form-nine btn-prev', 'data-wizard' => '#wizard1']) }}
    {{ Form::submit('Submit', ['class'=>'btn btn-space btn-primary']) }}
  </div>
</div>
