{{--PRIMARY--}}
<div class="form-group row font-weight-bold" style="font-size: 10px;">
    <div class="col-2 text-center">INCLUSIVE DATES (mm/dd/yyyy)</div>
    <div class="col-1"></div>
    <div class="col-2 text-center">POSITION TITLE</div>
    <div class="col-2 text-center">DEPARTMENT/AGENCY/OFFICE/COMPANY</div>
    <div class="col-1 text-center">MONTHLY SALARY</div>
    <div class="col-1 text-center">SALARY/JOB/ <br> SALARY GRADE</div>
    <div class="col-1 text-center">STATUS OF APPOINTMENT</div>
    <div class="col-1 text-left">GOV'T SERVICE (Y/N)</div>
    <div class="col-1"></div>
</div>


<div class="row mb-4">
  <div class="col-12 text-left">
    <a href="#" id="add_workexperience" class="btn btn-sm btn-info">Add</a>
  </div>
</div>


<div class="row work_experience">
    <div class="col-2 text-center pr-0 pl-0">
        <div class="row">
            <div class="col-6 pr-1">
                <span style="font-size: 8pt !important;">FROM</span>
                    <input size="16" type="text"  name="work_experience[0][inclusive_date_from]"
                           class="form-control form-control-sm datetimepicker date" style="font-size: 8pt;" data-min-view="2" data-date-format="yyyy-mm-dd">
                {!! $errors->first('work_experience[0][inclusive_date_from]', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
            </div>
            <div class="col-6 pl-1">
                <span style="font-size: 8pt !important;">TO</span>
                    <input size="16" type="text"  name="work_experience[0][inclusive_date_to]"
                           class="form-control form-control-sm datetimepicker date" style="font-size: 8pt;" data-min-view="2" data-date-format="yyyy-mm-dd">
                {!! $errors->first('work_experience[0][inclusive_date_to]', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
            </div>
        </div>
    </div>
    <div class="col-1 font-weight-bold">
        <div class="custom-control custom-checkbox mt-6">
          <input class="custom-control-input" type="checkbox" id="exp_check0" name="work_experience[0][present_work]">
          <label class="custom-control-label" for="exp_check0" style="font-size: 7pt;" >Present <br> Work</label>
        </div>
    </div>
    <div class="col-2 pr-1 pl-0 text-center font-weight-bold">
        <span style="font-size: 10px;">(Write in full/Do not abbreviate)</span>
        {{ Form::text('work_experience[0][position_title]', '', [
                'class' => 'form-control form-control-sm',
            ])
        }}
        {!! $errors->first('work_experience[0][position_title]', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
    </div>
    <div class="col-2 pr-0 pl-1 text-center font-weight-bold">
        <span style="font-size: 10px;">(Write in full/Do not abbreviate)</span>
        {{ Form::text('work_experience[0][department]', '', [
                'class' => 'form-control form-control-sm',
            ])
        }}
        {!! $errors->first('work_experience[0][department]', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
    </div>
    <div class="col-1 pl-1 pr-1 text-center">
        <span>&nbsp;</span>
        {{ Form::text('work_experience[0][monthly_salary]', '', [
                'class' => 'form-control form-control-sm',
            ])
        }}
        {!! $errors->first('work_experience[0][monthly_salary]', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
    </div>
    <div class="col-1 pl-1 pr-1  text-center">
        <span>&nbsp;</span>
        {{ Form::text('work_experience[0][salary_grade]', '', [
                'class' => 'form-control form-control-sm',
            ])
        }}
        {!! $errors->first('work_experience[0][salary_grade]', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
    </div>
    <div class="col-1 pl-1 pr-1  text-center">
        <span>&nbsp;</span>
        {{ Form::text('work_experience[0][status_of_appointment]', '', [
                'class' => 'form-control form-control-sm',
            ])
        }}
        {!! $errors->first('work_experience[0][status_of_appointment]', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
    </div>
    <div class="col-1 pl-1 pr-1">
        <div class="switch-button switch-button-yesno mt-4">
          <input type="checkbox" name="work_experience[0][govt_service]" id="swt8"><span>
            <label for="swt8"></label></span>
        </div>
        {!! $errors->first('work_experience[0][govt_service]', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
    </div>
</div>


<div class="form-group row text-right">
  <div class="col-12">
    {{ Form::button('Previous', ['class'=>'btn btn-secondary btn-space wizard-previous btn-form-seven btn-prev', 'data-wizard' => '#wizard1']) }}
        {{ Form::button('Next Step', ['class'=>'btn btn-primary btn-space wizard-next btn-form-seven', 'data-wizard' => '#wizard1']) }}
    {{ Form::reset('Clear Form', ['class'=>'btn btn-space btn-danger']) }}
  </div>
</div>
