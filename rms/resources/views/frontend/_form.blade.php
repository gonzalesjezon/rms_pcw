@section('css')
  <link rel="stylesheet" type="text/css"
        href="{{ URL::asset('beagle-assets/lib/datetimepicker/css/bootstrap-datetimepicker.min.css') }}">
  <link rel="stylesheet" type="text/css" href="{{ URL::asset('beagle-assets/lib/select2/css/select2.min.css') }}">
  <link rel="stylesheet" type="text/css"
        href="{{ URL::asset('beagle-assets/lib/bootstrap-slider/css/bootstrap-slider.min.css') }}">
@endsection

<div class="row wizard-row">
  <div class="col-md-12 fuelux">
    <div class="block-wizard">
      <div id="wizard1" class="wizard wizard-ux complete">
        <div class="steps-container">
          <ul class="steps" style="margin-left: 0">
            <li data-step="1" class="active">Personal Info<span class="chevron"></span></li>
            <li data-step="2" >Personal Details<span class="chevron"></span></li>
            <li data-step="3" >Address<span class="chevron"></span></li>
            <li data-step="4" >Education<span class="chevron"></span></li>
            <li data-step="5" >Eligibility<span class="chevron"></span></li>
            <li data-step="6" >Work Experience<span class="chevron"></span></li>
            <li data-step="7" >Training<span class="chevron"></span></li>
            <li data-step="8" >Attachments<span class="chevron"></span></li>
            <li data-step="9" >Question<span class="chevron"></span></li>
          </ul>
        </div>
       <!--  <div class="actions">
          <button type="button" class="btn btn-xs btn-prev btn-secondary"><i
              class="icon mdi mdi-chevron-left"></i> Prev
          </button>
          <button type="button" data-last="Finish" class="btn btn-xs btn-next btn-secondary">Next<i
              class="icon mdi mdi-chevron-right"></i></button>
        </div> -->

        {!! Form::open(['action' => $action,'method' => $method,'id' => 'applicant-form-one', 'files' => true ,
            'data-parsley-namespace' => "data-parsley-", 'data-parsley-validate' => '',
            'class' => 'form-horizontal group-border-dashed'
        ]) !!}

        <div class="step-content">
          {{--Personal Info--}}
          <div data-step="1" class="step-pane">
            @include('frontend._form-one', ['applicant' => @$applicant, 'jobs' => $jobs])
          </div>

          {{--Personal Details--}}
          <div data-step="2" class="step-pane">
            @include('frontend._form-two', ['applicant' => @$applicant])
          </div>

          {{--Address--}}
          <div data-step="3" class="step-pane">
            @include('frontend._form-three', ['applicant' => @$applicant])
          </div>

          {{--Education--}}
          <div data-step="4" class="step-pane">
            @include('frontend._form-four', ['applicant' => @$applicant])
          </div>

          {{--Eligibility--}}
          <div data-step="5" class="step-pane">
            @include('frontend._form-five', ['applicant' => @$applicant])
          </div>

          {{--Work Experience--}}
          <div data-step="6" class="step-pane">
            @include('frontend._form-six', ['applicant' => @$applicant])
          </div>

          {{--Training--}}
          <div data-step="7" class="step-pane">
            @include('frontend._form-seven', ['applicant' => @$applicant])
          </div>

          {{--Attachments--}}
          <div data-step="8" class="step-pane">
            @include('frontend._form-eight', ['applicant' => @$applicant])
          </div>

          <div data-step="9" class="step-pane">
            @include('frontend._form-nine', ['applicant' => @$applicant])
          </div>
        </div>
        {{ Form::hidden('_token',csrf_token())}}

        {!! Form::close() !!}
      </div>
    </div>
  </div>
</div>

@section('scripts')
  @include('frontend._form-script')
@endsection
