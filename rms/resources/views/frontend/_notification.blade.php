<!-- Nifty Modal-->
<div id="md-flipH" class="full-width modal-container modal-effect-8">
    <div class="modal-content modal-certificate px-1">
        <div class="modal-header">
            <button type="button" data-dismiss="modal" aria-hidden="true" class="close modal-close"><span
                        class="mdi mdi-close"></span></button>
        </div>

        <div id="printThis" class="modal-body rounded">
            <div class="row mb-4">
                <div class="col text-center" style="color:#7030a0;"><h4><strong>OPPORTUNITIES IN PHILIPPINE COMMISSION ON WOMEN</strong></h4></div>
            </div>
            <hr>
            <div class="row mb-4">
                <div class="col-md-12">
                    <p class="text-justify">
                        In connection with the collection of personal information, and sensitive personal information (“Personal Data”) by PCW, the undersigned hereby gives consent as a Data Subject under the Implementing Rules and Regulations of the Data Privacy Act (“IRR”), its amendments or equivalent succeeding laws, rules, and regulations. 
                    </p>
                    <p class="text-justify">
                       The Data Subject hereby acknowledges being informed and consents to the following:
                    </p>
                    <ol>
                        <li>The Data Subject’s Personal Data are being or have been collected and processed, including the existence of automated decision making and  profiling.</li>
                        <li>The Personal Data is being collected for the following purposes:</li>
                    </ol>

                    <ol style="list-style: lower-alpha;">
                         <li>Background Check</li>
                         <li>Employment Verification</li>
                         <li>Employee Requirement</li>
                         <li>Employment Record</li>
                    </ol>

                    <p class="text-justify">The Personal Data will be retained from date of application until the declared, specified and legitimate purpose has been achieved or the processing relevant to the purpose has been terminated.</p>

                    <p class="text-justify">
                        I hereby acknowledge that I have been fully informed of the foregoing and that I give my consent to and ratify the collection and processing of my Personal Data by PCW. 
                    </p>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 text-center ">
                    <label class="form-check-label form-check-inline">
                        <input type="radio"  name="i_agree" class="custom-radio" value="yes" >&nbsp; I agree
                    </label>
                    <label class="form-check-label form-check-inline">
                        <input type="radio"  name="i_agree" class="custom-radio" value="no">&nbsp; I disagree
                    </label>
                </div>
            </div>

            <div class="row text-right">
                <div class="col-md-12">
                     <form method="POST" action="{{ route('frontend.post_job') }}">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" name="id" id="job_id">
                     <button class="btn btn-success btn-space" id="btn-submit">Submit</button>
                     </form>
                </div>
            </div>
        </div>
        <div class="modal-footer"></div>
    </div>
</div>