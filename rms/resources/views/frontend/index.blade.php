@extends('frontend.layouts.app')

@section('css')
  <link rel="stylesheet" type="text/css"
        href="{{ URL::asset('beagle-assets/lib/datatables/datatables.net-bs4/css/dataTables.bootstrap4.css') }}" />
        <link rel="stylesheet" type="text/css" href="{{ URL::asset('css/styles.css') }}" />
@endsection

@section('content')
    <div class="bg-light">
        <div class="main-content container-fluid bg-light">
          @include('layouts._flash-message')
            <div class="card card-table">
                <div class="card-header">
                  <h3 class="mb-4">JOB OPPORTUNITIES</h3>
                </div>
                <div class="card-body">
                    <table id="table1" class="table table-striped table-hover table-fw-widget">
                        <thead>
                            <tr class="text-center">
                                <th>Position Title</th>
                                <th>Salary Grade</th>
                                <th>Compensation</th>
                                <th>Status of Employment</th>
                                <th>Division/Section</th>
                                <th>Job Details</th>
                                <th>Deadline of Application</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($jobs as $key => $job)
                            <tr>
                                <td style="vertical-align: top" class="text-center" >
                                    <a href="{{ route('frontend.show',['id'=>$job->id] ) }}"  target="_blank"> 
                                      {!! @$job->plantilla_item->position->Name !!} 
                                    </a>
                                </td>
                                <td style="vertical-align: top" class="text-center">
                                    {!! 'SG '.@$job->plantilla_item->salary_grade->Name !!}
                                </td> 
                                <td style="vertical-align: top" class="text-center">
                                  @if($job->status == 'plantilla')
                                  {{ number_format($job->plantilla_item->basic_salary,2)}} / month <br>
                                  plus {{ number_format($job->pera_amount,2) }} / PERA
                                  @else
                                  {{ number_format($job->plantilla_item->basic_salary,2)}} / month <br>
                                  plus 20% premium
                                  @endif

                                </td>
                                <td style="vertical-align: top" class="text-center">
                                  {!! @$job->plantilla_item->employee_status->Name !!}
                                </td>
                                <td style="vertical-align: top">
                                  {!! @$job->plantilla_item->division->Name !!}
                                </td>
                                <td style="vertical-align: top" class="text-center">
                                     <a href="{{ route('frontend.show',['id'=> Crypt::encrypt($job->id)] ) }}" class="btn btn-info mt-3" target="_blank"><i class="icon icon-left mdi mdi-eye"></i> View</a>
                                </td>
                                <td style="vertical-align: top;" class="text-center">
                                    <span style="font-size: 12px;">{{ $job->deadline_date }}</span> <br>
                                    <button data-modal="md-flipH" class="btn btn-info md-trigger show" data-job_id="{{$job->id}}"> Apply Now</button>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>

            <div class=" form-group row">
              <div class="col-md-12">
                  <h4 class="font-weight-bold">Attach the following:</h4>
              </div>
            </div>

            <div class="form-group row mb-1">
              <div class="col-md-12">
                <ol>
                  <li>Fully Accomplished Personal Data Sheet (revised 2017); </li>
                  <li>Curriculum Vitae with detailed job description;  </li>
                  <li>Copy of Transcript of Records and Diploma; </li>
                  <li>Copy of Certificate of training/seminars attended; </li>
                  <li>Copy of Certificate of Employment  </li>
                  <li>Performance Evaluation (if applicable) </li>
                  <li>Certificate of eligibility/rating/license (if applicable)  </li>
                </ol>

              </div>
            </div>

        </div>
    </div>

@include('frontend._notification')
@endsection

@section('scripts')
  <script src="{{ URL::asset('beagle-assets/lib/datatables/datatables.net/js/jquery.dataTables.js') }}"
          type="text/javascript"></script>
  <script src="{{ URL::asset('beagle-assets/lib/datatables/datatables.net-bs4/js/dataTables.bootstrap4.js') }}"
          type="text/javascript"></script>
  <script src="{{ URL::asset('beagle-assets/lib/datatables/datatables.net-buttons/js/dataTables.buttons.min.js') }}"
          type="text/javascript"></script>
  <script src="{{ URL::asset('beagle-assets/lib/datatables/datatables.net-buttons/js/buttons.html5.min.js') }}"
          type="text/javascript"></script>
  <script src="{{ URL::asset('beagle-assets/lib/datatables/datatables.net-buttons/js/buttons.flash.min.js') }}"
          type="text/javascript"></script>
  <script src="{{ URL::asset('beagle-assets/lib/datatables/datatables.net-buttons/js/buttons.print.min.js') }}"
          type="text/javascript"></script>
  <script src="{{ URL::asset('beagle-assets/lib/datatables/datatables.net-buttons/js/buttons.colVis.min.js') }}"
          type="text/javascript"></script>
  <script src="{{ URL::asset('beagle-assets/lib/datatables/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js') }}"
          type="text/javascript"></script>
  <script src="{{ URL::asset('beagle-assets/js/app-tables-datatables.js') }}" type="text/javascript"></script>
  <script src="{{ URL::asset('beagle-assets/lib/jquery.niftymodals/dist/jquery.niftymodals.js') }}"
            type="text/javascript"></script>
  <script type="text/javascript">
    $(document).ready(function() {
      //initialize the javascript
      App.init();
      App.dataTables();

      $('input[name=i_agree]').prop('checked',false);

      $.fn.niftyModal('setDefaults', {
          overlaySelector: '.modal-overlay',
          contentSelector: '.modal-content',
          closeSelector: '.modal-close',
          classAddAfterOpen: 'modal-show',
      });

      var jobId;
      $(document).on('click','.show',function(){
        jobId = $(this).data('job_id');
      });

      var check;
      $('input[name=i_agree]').on('change',function(){
        check = $(this).val();
      });

      $('#btn-submit').on('click',function(){

         if(check == 'yes'){
            $('#job_id').val(jobId);
            $('.mdi-close').trigger('click');
         }else{
            return false;
         }

      })

    });
  </script>
@endsection
