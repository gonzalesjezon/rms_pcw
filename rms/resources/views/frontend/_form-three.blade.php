    <div class="row">
        <div class="col-12">
            <h5 class="col-12 col-sm-3 text-sm-right font-weight-bold">Present Address:</h5>
        </div>
    </div>
    <div class="form-group row">
        <label class="col-12 col-sm-3 col-form-label text-sm-right">House Number <span style="color:red;">*</span> </label>
        <div class="col-12 col-sm-8 col-lg-6">
            {{ Form::text('house_number', @$applicant->house_number, [
                    'class' => 'form-control form-control-sm',
                    'placeholder' => 'House Number',
                    'id' => 'house_number',
                    'required' => 'true'
                ])
            }}
        </div>
    </div>

    <div class="form-group row">
        <label class="col-12 col-sm-3 col-form-label text-sm-right">Street </label>
        <div class="col-12 col-sm-8 col-lg-6">
            {{ Form::text('street', @$applicant->street, [
                    'class' => 'form-control form-control-sm',
                    'id' => 'street',
                    'placeholder' => 'Street',
                ])
            }}
        </div>
    </div>

    <div class="form-group row">
        <label class="col-12 col-sm-3 col-form-label text-sm-right">Subdivision </label>
        <div class="col-12 col-sm-8 col-lg-6">
            {{ Form::text('subdivision', @$applicant->subdivision, [
                    'class' => 'form-control form-control-sm',
                    'id' => 'subdivision',
                    'placeholder' => 'Subdivision',
                ])
            }}
        </div>
    </div>

    <div class="form-group row">
        <label class="col-12 col-sm-3 col-form-label text-sm-right">Barangay <span style="color:red;">*</span> </label>
        <div class="col-12 col-sm-8 col-lg-6">
            {{ Form::text('barangay', @$applicant->barangay, [
                    'class' => 'form-control form-control-sm',
                    'id' => 'barangay',
                    'placeholder' => 'Barangay/District',
                    'required' => 'true'
                ])
            }}
        </div>
    </div>

    <div class="form-group row">
        <label class="col-12 col-sm-3 col-form-label text-sm-right">City <span style="color:red;">*</span> </label>
        <div class="col-12 col-sm-8 col-lg-6">
            {{ Form::text('city', @$applicant->city, [
                    'class' => 'form-control form-control-sm',
                    'id' => 'city',
                    'placeholder' => 'City',
                    'required' => true,
                ])
            }}
        </div>
    </div>

    <div class="form-group row">
        <label class="col-12 col-sm-3 col-form-label text-sm-right">Province <span style="color:red;">*</span> </label>
        <div class="col-12 col-sm-8 col-lg-6">
            {{ Form::text('province', @$applicant->province, [
                    'class' => 'form-control form-control-sm',
                    'id' => 'province',
                    'placeholder' => 'Province',
                    'required' => true,
                ])
            }}
        </div>
    </div>

    <div class="form-group row">
        <label class="col-12 col-sm-3 col-form-label text-sm-right">Zip Code</label>
        <div class="col-12 col-sm-8 col-lg-6">
            {{ Form::text('zip_code', @$applicant->zip_code, [
                    'class' => 'form-control form-control-sm',
                    'placeholder' => 'Zip Code',
                ])
            }}
        </div>
    </div>

<!--     <div class="form-group row">
        {{ Form::label('country_id', 'Country', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
        <div class="col-12 col-sm-8 col-lg-6">
            {{ Form::select('country', $countries, @$applicant->country, [
                    'class' => 'form-control form-control-xs',
                    'placeholder' => 'Country',
                    'required' => true,
                ])
            }}
        </div>
    </div> -->

    <div class="dropdown-divider"></div>

    <div class="row">
        <h5 class="col-12 col-sm-3 text-sm-right font-weight-bold">Permanent Address:</h5>
    </div>

    <div class="row">
        {{ Form::label('', 'Same as above?', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
        <div class="col-12 col-sm-8 col-lg-6 offset-0 mt-2">
              <div class="switch-button switch-button-success switch-button-yesno">
                    <input type="checkbox" name="same_address" id="swt6"><span>
                    <label for="swt6"></label></span>
              </div>
          </div>
    </div>

    <div class="form-group row">
        {{ Form::label('permanent_house_number', 'House Number', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
        <div class="col-12 col-sm-8 col-lg-6">
            {{ Form::text('permanent_house_number', @$applicant->permanent_house_number, [
                    'class' => 'form-control form-control-sm clear-form-input',
                    'placeholder' => ' House Number',
                ])
            }}
        </div>
    </div>
    <div class="form-group row">
        {{ Form::label('permanent_street', 'Street', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
        <div class="col-12 col-sm-8 col-lg-6">
            {{ Form::text('permanent_street', @$applicant->permanent_street, [
                    'class' => 'form-control form-control-sm clear-form-input',
                    'placeholder' => ' Street',
                ])
            }}
        </div>
    </div>
    <div class="form-group row">
        {{ Form::label('permanent_subdivision', ' Subdivision', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
        <div class="col-12 col-sm-8 col-lg-6">
            {{ Form::text('permanent_subdivision', @$applicant->permanent_subdivision, [
                    'class' => 'form-control form-control-sm clear-form-input',
                    'placeholder' => 'Subdivision',
                ])
            }}
        </div>
    </div>

    <div class="form-group row">
        {{ Form::label('permanent_barangay', 'Barangay', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
        <div class="col-12 col-sm-8 col-lg-6">
            {{ Form::text('permanent_barangay', @$applicant->permanent_barangay, [
                    'class' => 'form-control form-control-sm clear-form-input',
                    'placeholder' => 'Barangay',
                ])
            }}
        </div>
    </div>

    <div class="form-group row">
        {{ Form::label('permanent_city', 'City', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
        <div class="col-12 col-sm-8 col-lg-6">
            {{ Form::text('permanent_city', @$applicant->permanent_city, [
                    'class' => 'form-control form-control-sm clear-form-input',
                    'placeholder' => 'City',
                ])
            }}
        </div>
    </div>

    <div class="form-group row">
        {{ Form::label('permanent_province', 'Province', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
        <div class="col-12 col-sm-8 col-lg-6">
            {{ Form::text('permanent_province', @$applicant->permanent_province, [
                    'class' => 'form-control form-control-sm clear-form-input',
                    'placeholder' => 'Province',
                ])
            }}
        </div>
    </div>
<!--
    <div class="form-group row">
        {{ Form::label('permanent_country_id', 'Country', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
        <div class="col-12 col-sm-8 col-lg-6">
            {{ Form::select('permanent_country', $countries, @$applicant->permanent_country, [
                    'class' => 'form-control form-control-xs',
                    'placeholder' => 'Country',
                ])
            }}
        </div>
    </div> -->
    <div class="form-group row">
        {{ Form::label('', 'Zip Code', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
        <div class="col-12 col-sm-8 col-lg-6">
            {{ Form::text('permanent_zip_code', @$applicant->permanent_zip_code, [
                    'class' => 'form-control form-control-sm clear-form-input',
                    'placeholder' => 'Zip Code',
                ])
            }}
        </div>
    </div>


    <div class="form-group row text-right">
        <div class="col col-sm-10 col-lg-12">
            {{ Form::button('Previous', ['class'=>'btn btn-secondary btn-space btn-prev wizard-previous btn-form-four', 'data-wizard' => '#wizard1']) }}
            {{ Form::button('Next Step', ['class'=>'btn btn-primary btn-space wizard-next btn-form-four', 'data-wizard' => '#wizard1']) }}
            {{ Form::reset('Clear Form', ['class'=>'btn btn-space btn-danger']) }}
        </div>
    </div>  