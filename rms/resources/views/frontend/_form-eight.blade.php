
<ol>
    <li>
        <div class="row">
            <div class="col-4">
                <label class="col-form-label">Application Letter</label>

                <p>Note <span class="font-italic">Application Letter must be addressed to:</span></p>
                <p>
                    <span class="font-weight-bold">{!! (@$jobs->appointer) ? $jobs->appointer->getFullName() : '' !!}</span> <br>
                    {!! (@$jobs->appointer) ? $jobs->appointer->employeeinfo->position->Name : '' !!} <br>
                    Philippine Commission on Women <br>
                    1145 JP Laurel St. San Miguel Manila 
                </p>
            </div>

            <div class="col-6">
                  <input id="application_letter_path" type="file" name="application_letter_path" data-multiple-caption="{count} files selected" multiple=""
                       class="inputfile" required>
                <label for="application_letter_path" class="btn-primary"> <i class="mdi mdi-upload"></i><span>Browse files...</span></label>
                <span class="badge badge-primary">{{ @$applicant->application_letter_path }}</span>  
            </div>
        </div>
    </li>

    <li>

        <div class="row">
            <div class="col-4">
                <label class="col-form-label">Fully Accomplished Personal Data Sheet (revised 2017)</label>
            </div>

            <div class="col-6">
                  <input id="pds_path" 
                    type="file" name="pds_path" data-multiple-caption="{count} files selected" 
                    multiple=""
                   class="inputfile"
                   required 
                   >
            <label for="pds_path" class="btn-primary"> <i class="mdi mdi-upload"></i><span>Browse files...</span></label>
            <span class="badge badge-primary">{{ @$applicant->pds_path }}</span>
            </div>
        </div>
        
    </li>

    <li>

        <div class="row">
            <div class="col-4">
                <label class="col-form-label">Curriculum Vitae with detailed job description;</label>
            </div>

            <div class="col-6">
                <input id="curriculum_vitae_path" type="file" name="curriculum_vitae_path" data-multiple-caption="{count} files selected" multiple=""
                   class="inputfile" required>
                <label for="curriculum_vitae_path" class="btn-primary"> <i class="mdi mdi-upload"></i><span>Browse files...</span></label>
                <span class="badge badge-primary"></span>
            </div>
        </div>
        
    </li>

    <li>

        <div class="row">
            <div class="col-4">
                <label class="col-form-label">Copy of Transcript of Records and Diploma;</label>
            </div>

            <div class="col-6">
                <input id="tor_path" type="file" name="tor_path" data-multiple-caption="{count} files selected" multiple=""
                   class="inputfile" required>
                <label for="tor_path" class="btn-primary"> <i class="mdi mdi-upload"></i><span>Browse files...</span></label>
                <span class="badge badge-primary">{{ @$applicant->tor_path }}</span>
            </div>
        </div>
        
    </li>

    <li>

        <div class="row">
            <div class="col-4">
                <label class="col-form-label"> Copy of Certificate of training/seminars attended; </label>
            </div>

            <div class="col-6">
                <input id="training_certificate_path" type="file" name="training_certificate_path" data-multiple-caption="{count} files selected" multiple=""
                   class="inputfile" required>
                <label for="training_certificate_path" class="btn-primary"> <i class="mdi mdi-upload"></i><span>Browse files...</span></label>
                <span class="badge badge-primary">{{ @$applicant->training_certificate_path }}</span>
            </div>
        </div>
        
    </li>

    <li>

        <div class="row">
            <div class="col-4">
                <label class="col-form-label"> Copy of Certificate of Employment </label>
            </div>

            <div class="col-6">
                <input id="employment_certificate_path" type="file" name="employment_certificate_path" data-multiple-caption="{count} files selected" multiple=""
                   class="inputfile" required>
                <label for="employment_certificate_path" class="btn-primary"> <i class="mdi mdi-upload"></i><span>Browse files...</span></label>
                <span class="badge badge-primary">{{ @$applicant->employment_certificate_path }}</span>
            </div>
        </div>
        
    </li>

    <li>

        <div class="row">
            <div class="col-4">
                <label class="col-form-label">  Performance Evaluation (if applicable) </label>
            </div>

            <div class="col-6">
                <input id="info_sheet_path" type="file" name="info_sheet_path" data-multiple-caption="{count} files selected" multiple=""
                   class="inputfile">
                <label for="info_sheet_path" class="btn-primary"> <i class="mdi mdi-upload"></i><span>Browse files...</span></label>
                <span class="badge badge-primary">{{ @$applicant->info_sheet_path }}</span>
            </div>
        </div>
        
    </li>

    <li>

        <div class="row">
            <div class="col-4">
                <label class="col-form-label"> Certificate of Eligibility/Rating/License (if applicable) 
                </label>
            </div>

            <div class="col-6">
                <input id="coe_path" type="file" name="coe_path" data-multiple-caption="{count} files selected" multiple=""
                   class="inputfile" {{ (@$jobs->status == 'plantilla') ? 'required' : '' }}>
                <label for="coe_path" class="btn-primary"> <i class="mdi mdi-upload"></i><span>Browse files...</span></label>
                <span class="badge badge-primary">{{ @$applicant->coe_path }}</span>
            </div>
        </div>
        
    </li>
</ol>


<div class="form-group row text-right">
  <div class="col-12">
    {{ Form::button('Previous', ['class'=>'btn btn-secondary btn-space wizard-previous btn-form-nine btn-prev', 'data-wizard' => '#wizard1']) }}
    {{ Form::button('Next Step', ['class'=>'btn btn-primary btn-space wizard-next btn-form-nine', 'data-wizard' => '#wizard1']) }}
  </div>
</div>
