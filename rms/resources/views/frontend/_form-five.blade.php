{{--PRIMARY--}}
<div class="form-group row font-weight-bold" style="font-size: 8pt;">
  <div class="col-2 text-center">CAREER SERVICE
  </div>
  <div class="col-2 text-center">OTHER SPECIFY</div>
  <div class="col-2 text-center">RATING <br> (If Applicable)</div>
  <div class="col-2 text-center" >DATE OF EXAMINATION/ <br> CONFERMENT</div>
  <div class="col-1 text-center pl-0 pr-0 ml-0 mr-0">PLACE <br> OF EXAMINATION/ <br> CONFERMENT</div>
  <div class="col-1"></div>
  <div class="col-2 text-left">LICENSE <br> (If Applicable)</div>
</div>

<div class="row">
  <div class="col-12 text-left">
    <label id="add_eligibility" class="btn btn-sm btn-info col-form-label">Add</label>
  </div>
</div>


<div class="row eligibility">
  <div class="col-2 mt-4">
    {{ Form::select('eligibility[1][eligibility_ref]', config('params.eligibility_type'), '', [
                'class' => 'form-control form-control-xs',
                'placeholder' => 'Select Eligibility',
                'required' => true,
            ])
        }}
    {!! $errors->first('eligibility[1][eligibility_ref]', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
  </div>

  <div class="col-2 mt-4">
    {{ Form::text('eligibility[1][other_specify]', '', [
            'class' => 'form-control form-control-sm',
            'placeholder' => 'Specify'
        ])
    }}
    {!! $errors->first('eligibility[1][other_specify]', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
  </div>


  <div class="col-2 mt-4">
    {{ Form::text('eligibility[1][rating]', '', [
            'class' => 'form-control form-control-sm',
        ])
    }}
    {!! $errors->first('eligibility[1][rating]', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
  </div>
  <div class="col-2 text-center font-weight-bold mt-4">
    {{ Form::text('eligibility[1][exam_date]', '', [
            'class' => 'form-control form-control-sm',
        ])
    }}
    {!! $errors->first('eligibility[1][exam_date]', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
  </div>
  <div class="col-1 text-center font-weight-bold mt-4">
    {{ Form::text('eligibility[1][exam_place]', '', [
            'class' => 'form-control form-control-sm',
        ])
    }}
    {!! $errors->first('eligibility[1][exam_place]', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
  </div>
  <div class="col-1 text-center">
    NUMBER
    {{ Form::text('eligibility[1][license_number]', '', [
            'class' => 'form-control form-control-sm',
        ])
    }}
    {!! $errors->first('eligibility[1][license_number]', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
  </div>
  <div class="col-2 text-left">
    Date of Validity
    {{ Form::text('eligibility[1][license_validity]', '', [
            'class' => 'form-control form-control-sm col-9 pr-0 mr-1',
        ])
    }}
    {!! $errors->first('eligibility[1][license_validity]', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
  </div>
</div>

<div class="form-group row text-right">
  <div class="col-12">
    {{ Form::button('Previous', ['class'=>'btn btn-secondary btn-space wizard-previous btn-form-six btn-prev', 'data-wizard' => '#wizard1']) }}
    {{ Form::button('Next Step', ['class'=>'btn btn-primary btn-space wizard-next btn-form-six', 'data-wizard' => '#wizard1']) }}
    {{ Form::reset('Clear Form', ['class'=>'btn btn-space btn-danger']) }}
  </div>
</div>