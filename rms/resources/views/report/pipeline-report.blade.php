@extends('layouts.print')

@section('css')
<style type="text/css">
  @media print{
    .table>thead>tr>th{
      vertical-align: middle !important;
      padding: 4px !important;
      font-size: 8pt !important;
    }
  }
  .table>thead>tr>th{
    vertical-align: middle !important;
    padding: 4px !important;
    font-size: 8pt !important;
  }
</style>
@endsection

@section('content')

<div id="reports" style="width: 960px;margin: auto;font-size:8pt;font-family: Arial, Helvetica, sans-serif;">

  <table class="table table-bordered">
    <thead class="text-center">
      @include('report.includes._header')
      <tr>
        <th>POSITION</th>
        <th>NUMBER OF <br> APPLICANTS</th>
        <th>NOT QUALIFIED</th>
        <th>NO. OF <br> APPLICANTS <br> FOR EXAM</th>
        <th>NO. OF <br> APPLICANTS <br> FOR REFERENCE</th>
        <th>NO. OF <br>  APPLICANTS <br> WHO <br> WITHDREW <br> APPLICATION</th>
        <th>NO. OF <br> APPLICANTS <br> WHO <br> PASSED THE EXAM</th>
        <th>NO. OF <br> APPLICANTS <br> WHO <br> PASSED THE INTERVIEW</th>
        <th>NO. OF <br>  APPLICANTS <br> WHO <br> WITHDREW APPLICATION</th>
      </tr>
    </thead>
    <tbody>
      @foreach($jobs as $job)
      <tr class="text-center">
        <td class="text-left" nowrap>{!! strtoupper(@$job->plantilla_item->position->Name)  !!}</td>
        <td>{!! count($job->no_of_applicants) !!}</td>
        <td>{!! count($job->not_qualified_applicant) !!}</td>
        <td>{!! count($job->qualified_applicant) !!}</td>
        <td>{!! count($job->applicant_for_reference) !!}</td>
        <td>{!! count($job->applicant_widthrawn) !!}</td>
        <td>{!! count($job->pass_exam) !!}</td>
        <td>{!! count($job->pass_interview) !!}</td>
        <td></td>
      </tr>
      @endforeach
    </tbody>
  </table>

</div>

 <div class="form-group row text-right d-print-none">
    <div class="col col-sm-10 col-lg-9 offset-sm-1 offset-lg-3">
      {{ Form::button('Print', ['id' => 'evaluation-report', 'class'=>'btn btn-primary btn-space', 'type'=>'submit']) }}
      {{ Form::reset('Cancel', ['class'=>'btn btn-space btn-danger']) }}
    </div>
  </div>
@endsection

@section('scripts')
<script type="text/javascript">
  $(document).ready(function() {
    $('#evaluation-report').click(function() {
      window.print();
    });
  });
</script>
@endsection