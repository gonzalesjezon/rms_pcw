@extends('layouts.print')

@section('css')
<style type="text/css">
  @media print{
    @page{
      size: a4 landscape;
    }
    .table>thead>tr>th, .table>tbody>tr>td{
      padding: 3px !important;
    }

    .table>thead>tr>th,
    .table>tbody>tr>td{
      border: 1px solid #333 !important;
    }
  }
  .table>thead>tr>th, .table>tbody>tr>td{
    padding: 3px !important;
  }

  .table>thead>tr>th,
  .table>tbody>tr>td{
    border: 1px solid #333 !important;
  }
</style>
@endsection

@section('content')

<div class="row text-right d-print-none">
  <div class="col col-sm-10 col-lg-9 offset-sm-1 offset-lg-3">
    <button class="btn btn-primary btn-space" id="evaluation-report" type="submit"><i class="mdi mdi-print"></i> Print</button>
    <!-- {{ Form::reset('Cancel', ['class'=>'btn btn-space btn-danger']) }} -->
  </div>
</div>

<br>

<div id="reports" style="margin: auto;font-size: 12px;font-family: Arial, Helvetica, sans-serif;">
  <div class="row mb-2">
    <div class="col-sm-3">CS Form No. 1 <br> <i>Revised 2018</i></div>
    <div class="col-sm-9 text-right"><span class="border border-dark p-1">For Use of Accredited Agencies Only</span></div>
  </div>

  <div class="row mb-1">
    <div class="col-sm-12 text-center"><h4><b>APPOINTMENT TRANSMITTAL  AND ACTION FORM</b></h4></div>
  </div>

  <div class="row mb-6">
    <div class="col-sm-1 text-right">Agency</div>
    <div class="col-sm-4 border-bottom border-dark text-center">PHILIPPINE COMMISSION ON WOMEN</div>

    <div class="col-sm-3 text-right"></div>

    <div class="col-sm-2 text-right">CSCFO In-charge:</div>
    <div class="col-sm-2 border-bottom border-dark">&nbsp;</div>
  </div>

  <div class="row mb-1">
    <div class="col-sm-12">INSTRUCTIONS:</div>
  </div>
  <div class="row mb-1">
  	<div class="col-sm-6">
  		<ol>
  			<li>Fill-out the data needed in the form completely and accurately.</li>
  			<li>Do not abbreviate entries in the form.</li>
  			<li>Accomplish the Checklist of Common Requirements and sign the certification.</li>
  			<li>Submit the duly accomplished form in electronic and printed copy (2 copies) to the CSC Field Office-in-Charge together with the original CSC copy of appointments and supporting documents. </li>
  		</ol>
  	 </div>

  	 <div class="col-sm-2"></div>
  	 <div class="col-sm-4 border border-dark text-left" style="padding-right: 0px !important;padding-left: 0px !important;">
  	 	<p class="mb-0 p-0 bg-thead border border-dark border-top-0 border-left-0 border-right-0 border-bottom-1">For CSC RO/FO's Use:</p>
  	 	<p>Date Received:</p>
  	 </div>
  </div>

  <div class="row mb-1">
      <div class="col-sm-12">
        <table id="table1" class="table table-striped table-hover table-fw-widget table-bordered" style="font-size: 10px;">
          <thead>
            <tr class="text-center">
              <th rowspan="2">No</th>
              <th colspan="4">Name of Appointers</th>
              <th rowspan="2" style="vertical-align: middle;">Position Title <br> (indicate parenthetical title, if applicable) </th>
              <th rowspan="2" style="vertical-align: middle;">Salary/ <br>JOB/ <br>Pay Grade</th>
              <th rowspan="2" style="vertical-align: middle;">Employment <br> Status</th>
              <th rowspan="2" style="vertical-align: middle;">
                PERIOD OF EMPLOYMENT  <br>
                (for Temporary, Casual/ Contractual Appointments) <br>
                (mm/dd/yyyy to mmdd/yyyy)
              </th>
              <th rowspan="2" style="vertical-align: middle;">NATURE OF <br> APPOINTMENT</th>
              <th rowspan="2" style="vertical-align: middle;width: 80px !important">
                DATE OF <br>
                ISSUANCE <br>
                (mm/dd/yyyy )
              </th>
              <th colspan="2">Publication</th>
              <th colspan="3">CSC Action</th>
              <th rowspan="2" style="vertical-align: middle;">Agency Receiving <br> Officer</th>
            </tr>
            <tr class="text-center">
              <th style="vertical-align: middle;">Last Name</th>
              <th style="vertical-align: middle;">First Name</th>
              <th style="vertical-align: middle;">Name Extension <br> (Jr./III)</th>
              <th style="vertical-align: middle;">Middle Name</th>
              <th style="vertical-align: middle;">
                DATE <br>
                indicate period of <br>
                publication <br>
                 (mm/dd/yyyy to mmdd/yyyy)
              </th>
              <th style="vertical-align: middle;">
                MODE <br>
                CSC Bulletin of <br>
                Vacant Positions,  <br>
                Agency Website,  Newspaper, etc.)
              </th>
              <th>
                A - Approved <br>
                or  <br>
                D - Disapproved <br>
                or <br>
                N - Noted
              </th>
              <th style="vertical-align: middle;width: 80px !important;">
                Date of Action <br>
                (mm/dd/yyyy )
              </th>
              <th style="vertical-align: middle;">
                Date of Release
                (mm/dd/yyyy )
              </th>
            </tr>
          </thead>
          <tbody>
            @foreach($appointment as $key => $issued)
              <tr class="text-center">
                <td>{!! $key+1 !!}</td>
                <td class="text-left">{!! Crypt::decrypt($issued->applicant->last_name) !!}</td>
                <td class="text-left">{!! Crypt::decrypt($issued->applicant->first_name) !!}</td>
                <td class="text-left">{!! Crypt::decrypt($issued->applicant->extension_name) !!}</td>
                <td class="text-left">{!! Crypt::decrypt($issued->applicant->middle_name) !!}</td>
                <td nowrap>{{  $issued->applicant->job->plantilla_item->position->Name }}</td>
                <td nowrap>{{ @$issued->applicant->job->plantilla_item->item_number}}</td>
                <td>{{ $issued->applicant->job->plantilla_item->salary_grade->Name}}</td>
                <td nowrap>{{ $issued->applicant->job->plantilla_item->employee_status->Name }}</td>
                <td nowrap>{{ config('params.nature_of_appointment.'.$issued->nature_of_appointment)}}</td>
                <td>{{ $issued->period_emp_from }} - {{$issued->period_emp_to}}</td>
                <td>{{ $issued->publication_date_from}} - {{ $issued->publication_date_to}}</td>
                <td>{{ config('params.publication.'.$issued->applicant->publication)}}</td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
              </tr>
              @endforeach
          </tbody>
        </table>
      </div>
  </div>

    <?php
      $hrmo         = explode('|', @$signatory->hrmo);

    ?>

  <div class="row mb-4">
      <div class="col-sm-5" style="margin-left: 6em !important;">
      	<p class="font-weight-bold mb-2">CERTIFCATION</p>
      	<p class="mb-4">This is to certify that the information contained in this form are true, correct and complete.</p>
        <p class="text-center mb-0 pb-0">&nbsp; {!! @$hrmo[0]  !!}</p>
      	<p class="border border-bottom border-dark pb-0 mb-0"></p>
      	<p class="text-center p-0 m-0 mb-2"><span>HMO</span></p>
      	<p>Date ____________________________</p>
      </div>
  </div>

  <div class="col-sm-12 border-dark border" style="padding-right: 0px !important;padding-left: 0px !important;">
  	<p class="border border-bottom-1 border-dark border-top-0 border-left-0 border-right-0 p-0 m-0">REMARKS/COMMENTS/RECOMMENDATIONS: (e.g. Reasons for Disapproval of Appointment)</p>
  	<p class="border border-bottom-1 border-dark border-top-0 border-left-0 border-right-0 p-0 m-0">&nbsp;</p>
  	<p class="p-0 m-0">&nbsp;</p>
  </div>

  <div style="page-break-before: always;"></div>
  <div class="row mb-4">
      <div class="col-sm-12">
          <table id="table3" class="table table-bordered">
              <thead>
                  <tr class="text-center">
                    <th colspan="2">CHECKLIST OF COMMON REQUIREMENTS</th>
                    <th>HRMO</th>
                    <th>CSC FO</th>
                  </tr>
                  <tr>
                    <th colspan="4">Instructions: Put a check if the requirements are complete. If incomplete, use the space provided to indicate the name of appointee and the lacking requirement/s.</th>
                  </tr>
              </thead>
              <tbody>
                  <tr>
                     <td class="text-center">1</td>
                     <td nowrap>
                        APPOINTMENT FORMS (CS Form No. 33-A, Revised 2018) - Original CSC copy of appointment form
                     </td>
                     <td></td>
                     <td></td>
                  </tr>
                  <tr>
                     <td class="text-center">2</td>
                     <td>
                        PLANTILLA OF CASUAL APPOINTMENT (CSC Form No. 34-B or D) - Original CSC copy
                     </td>
                     <td></td>
                     <td></td>
                  </tr>
                  <tr>
                     <td class="text-center">3</td>
                     <td>
                        PERSONAL DATA SHEET (CS Form No. 212, Revised 2017)
                     </td>
                     <td></td>
                     <td></td>
                  </tr>
                  <tr>
                     <td class="text-center">4</td>
                     <td>
                        ORIGINAL COPY OF AUTHENTICATED CERTIFICATE OF ELIGIBILITY/ RATING/ LICENSE - Except if the eligibility has been previously authenticated in 2004 or onward and recorded
                     </td>
                     <td></td>
                     <td></td>
                  </tr>
                  <tr>
                     <td class="text-center">4</td>
                     <td>
                        POSITION DESCRIPTION FORM (DBM-CSC Form No. 1, Revised 2017)
                     </td>
                     <td></td>
                     <td></td>
                  </tr>
                  <tr>
                     <td class="text-center">6</td>
                     <td>
                        OATH OF OFFICE (CS Form No. 32, Revised 2017)
                     </td>
                     <td></td>
                     <td></td>
                  </tr>
                  <tr>
                     <td class="text-center">7</td>
                     <td>
                        CERTIFICATE OF ASSUMPTION TO DUTY (CS Form No. 4)
                     </td>
                     <td></td>
                     <td></td>
                  </tr>

                  <tr>
                    <td colspan="2"></td>
                    <td>
                        <p class="text-justify mb-6" style="text-indent: 20px;">This is to certify that I have checked the veracity, authenticity and completeness of all the requirements in support of the appointments attached herein.</p>

                        <div class="row">
                          <div class="col-2"></div>
                          <div class="col-8 text-center">&nbsp; {!! @$hrmo[0]  !!}</div>
                          <div class="col-2"></div>
                        </div>

                        <div class="row">
                          <div class="col-2"></div>
                          <div class="col-8  border-top border-dark text-center">
                            <span >HRMO</span>
                          </div>
                          <div class="col-2"></div>
                        </div>
                    </td>
                    <td>
                        <p class="text-justify mb-6" style="text-indent: 20px;">This is to certify that I have checked all the requirements in support of the appointments attached herein and found these to be  [  ] complete /  [  ]   lacking.</p>

                        <div class="row">
                          <div class="col-2"></div>
                          <div class="col-8  border-top border-dark text-center">
                            <span >CSC FO Receiving Officer</span>
                          </div>
                          <div class="col-2"></div>
                        </div>
                    </td>
                  </tr>
              </tbody>

          </table>
      </div>
  </div>

</div>

 
@endsection

@section('scripts')
<script type="text/javascript">
  $(document).ready(function() {
    $('#evaluation-report').click(function() {
      window.print();
    });
  });
</script>
@endsection