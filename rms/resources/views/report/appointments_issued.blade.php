@extends('layouts.print')

@section('css')
<style type="text/css">
  @media print{
    @page{
      size: a4 landscape;
    }
    .table>thead>tr>th,
    .table>tbody>tr>td{
      border: 1px solid #333 !important;
    }
    .table>thead>tr>th{
      padding: 3px !important;;
    }
  }
  .table>thead>tr>th, .table>tbody>tr>td{
    padding: 3px !important;
  }
  .table>thead>tr>th,
  .table>tbody>tr>td{
    border: 1px solid #333 !important;
  }
</style>
@endsection

@section('content')

<div id="reports" style="margin: auto;font-size: 12px;font-family: Arial, Helvetica, sans-serif;">
  <div class="row mb-2">
    <div class="col-sm-8">CS Form No. 2 <br> <i>Revised 2018</i></div>
    <div class="col-sm-4 text-right border border-dark p-1 text-center font-weight-bold font-italic">For Use of Accredited Agencies Only</div>
  </div>

  <div class="row mb-1">
    <div class="col-sm-12 text-center"><h4><b>REPORT ON APPOINTMENTS ISSUED (RAI)</b></h4></div>
  </div>

  <div class="row mb-2">
    <div class="col-sm-12 text-center">For the month of {{ $print_date }}</div>
  </div>

  <div class="row mb-6">
    <div class="col-sm-10 text-right">Date received by CSCFO:</div>
    <div class="col-sm-2 border-bottom border-dark">&nbsp;</div>
  </div>

  <div class="row mb-6">
    <div class="col-sm-1 text-right">Agency</div>
    <div class="col-sm-4 border-bottom border-dark">PHILIPPINE COMMISSION ON WOMEN</div>

    <div class="col-sm-2 text-right">CSC Resolution No: </div>
    <div class="col-sm-1 border-bottom border-dark">&nbsp;</div>

    <div class="col-sm-2 text-right">CSCFO In-charge:</div>
    <div class="col-sm-2 border-bottom border-dark">&nbsp;</div>
  </div>

  <div class="row mb-1">
    <div class="col-sm-10">INSTRUCTIONS: (1) Fill-out the data needed in the form completely and accurately. </div>
  </div>
  <div class="row mb-1">
    <div class="col-sm-10" style="text-indent: 96px;">(2) Do not abbreviate entries in the form.</div>
  </div>
  <div class="row mb-1">
    <div class="col-sm-10" style="text-indent: 96px;">(3) Accomplish the Checklist of Common Requirements and sign the certification.</div>
  </div>
  <div class="row mb-1">
    <div class="col-sm-10" style="text-indent: 96px;">(4) Submit the duly accomplished form in electronic and printed copy (2 copies) to the CSC Field Office-in-Charge </div>
  </div>
  <div class="row mb-3">
    <div class="col-sm-10" style="text-indent: 105px;">together with the original CSC copy of appointments and supporting documents within the 30th day of the succeeding month. </div>
  </div>

  <div class="row mb-6">
    <div class="col-sm-12"><b>Pertinent data on appointment issued</b></div>
  </div>

  <div class="row mb-1">
      <div class="col-sm-12">
        <table id="table1" class="table table-bordered" style="font-size: 10px;">
          <thead>
            <tr class="text-center">
              <th rowspan="2"></th>
              <th rowspan="2" style="vertical-align: middle;">Date Issued / <br> Effectivity Date</th>
              <th colspan="4">Name of Appointers</th>
              <th rowspan="2" style="vertical-align: middle;">Position <br> Title</th>
              <th rowspan="2" style="vertical-align: middle;">Item No</th>
              <th rowspan="2" style="vertical-align: middle;">Salary/ <br>JOB/ <br>Pay Grade</th>
              <th rowspan="2" style="vertical-align: middle;">Salary Rate <br>(Annual)</th>
              <th rowspan="2" style="vertical-align: middle;">Employement <br> Status</th>
              <th rowspan="2" style="vertical-align: middle;">PERIOD OF EMPLOYMENT <br> (for Temporary, <br> Casual/ <br> Contractual Appointments) </th>
              <th rowspan="2" style="vertical-align: middle;">NATURE OF <br> APPOINTMENT</th>
              <th colspan="2">Publication</th>
              <th colspan="3">CSC Action</th>
              <th rowspan="2" style="vertical-align: middle;">Agency Receiving <br> Officer</th>
            </tr>
            <tr class="text-center">
              <th style="vertical-align: middle;">Last Name</th>
              <th style="vertical-align: middle;">First Name</th>
              <th style="vertical-align: middle;">Name Extension <br> (Jr./III)</th>
              <th style="vertical-align: middle;">Middle Name</th>
              <th>DATE indicate period of publication <br> (mm/dd/yyyy to mm/dd/yyyy)</th>
              <th style="vertical-align: middle;">MODE <br> (CSC Bulletin of Vacant Positions) </th>
              <th>V-Validated <br> INV- Invalidated</th>
              <th style="vertical-align: middle;">Date of Action</th>
              <th style="vertical-align: middle;">Date of Release</th>
            </tr>
            <tr class="text-center">
              <th></th>
              <th>(1)</th>
              <th colspan="4">(2)</th>
              <th>(3)</th>
              <th>(4)</th>
              <th>(5)</th>
              <th>(6)</th>
              <th>(7)</th>
              <th>(8)</th>
              <th>(9)</th>
              <th>(10)</th>
              <th>(11)</th>
              <th>(12)</th>
              <th>(13)</th>
              <th>(14)</th>
              <th>(15)</th>
            </tr>
          </thead>
          <tbody>
            @foreach($appointments as $key => $value)
            <tr>
              <td></td>
              <td>{{ $value->issued_from }}</td>
              <td>{{ $value->applicant->last_name}}</td>
              <td>{{ $value->applicant->first_name}}</td>
              <td>{{ $value->applicant->extension_name}}</td>
              <td>{{ $value->applicant->middle_name}}</td>
              <td nowrap>{{ $value->applicant->job->plantilla_item->position->Name}}</td>
              <td nowrap>{{ $value->applicant->job->plantilla_item->Name}}</td>
              <td>{{ $value->applicant->job->plantilla_item->salary_grade->Name}}</td>
              <td>{{ number_format($value->applicant->job->plantilla_item->basic_salary * 12,2) }}</td>
              <td>{{ $value->applicant->job->plantilla_item->employee_status->Name }}</td>
              <td>{{ $value->period_emp_from }} - {{$value->period_emp_to}}</td>
              <td>{{ config('params.nature_of_appointment.'.$value->nature_of_appointment)}}</td>
              <td>{{ $value->publication_date_from}} - {{ $value->publication_date_to}}</td>
              <td>{{ config('params.publication.'.$value->applicant->publication)}}</td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
            </tr>
            @endforeach
          </tbody>
        </table>
      </div>
  </div>
  <div class="row mb-4">
      <div class="col-sm-4"><b>CERTIFCATION</b></div>
      <div class="col-sm-4"><b>CERTIFCATION</b></div>
      <div class="col-sm-4"><b>Post-Audited By</b></div>
  </div>

  <div class="row mb-4">
      <div class="col-sm-4">
        <p style="text-indent: 20px;font-size: 12px;padding: 10px;" class="text-justify">
            This is to certify that the information contained in this
        report are true, correct and complete based on the Plantilla
        of Personnel and appointment/s issued.
        </p>
      </div>
      <div class="col-sm-4">
          <p style="text-indent: 20px;font-size: 12px;padding: 10px;" class="text-justify">
           This is to certify that the appointment/s issued
            is/are in accordance with existing Civil Service Law,
            rules and regulations.
          </p>
      </div>
      <div class="col-sm-4">
      </div>
  </div>


  <?php
    $hrmo         = explode('|', $signatory->hrmo);
    $agencyHead   = explode('|', $signatory->agency_head);

  ?>

  <div class="row mb-4 text-center">
      <div class="col-sm-4">
          <p class="border-bottom border-dark pb-0 mb-0">&nbsp; {!! @$hrmo[0] !!}</p>
          <p class="font-weight-bold">
            Highest Ranking HRMO
          </p>
      </div>
      <div class="col-sm-4">
          <p class="border-bottom border-dark pb-0 mb-0">&nbsp; {!! @$agencyHead[0] !!}</p>
          <p class="font-weight-bold">
            Agency Head or Authorized Official
          </p>
      </div>
      <div class="col-sm-4">
          <p class="border-bottom border-dark pb-0 mb-0">&nbsp; </p>
          <p class="font-weight-bold">
            CSC Official
          </p>
      </div>
  </div>

  <div class="row mb-4">
      <div class="col-sm-12">
          <table id="table2" class="table table-striped table-hover table-fw-widget table-bordered">
            <thead>
                <tr>
                  <th>REMARKS/COMMENTS/RECOMMENDATIONS (e.g. Reasons for Invalidation):</th>
                </tr>
                <tr>
                  <th></th>
                </tr>
                <tr>
                  <th></th>
                </tr>
            </thead>
          </table>
      </div>
  </div>

  <div class="row mb-4">
      <div class="col-sm-12">
          <table id="table3" class="table table-bordered">
              <thead>
                  <tr class="text-center">
                    <th colspan="2">CHECKLIST OF COMMON REQUIREMENTS</th>
                    <th>HRMO</th>
                    <th>CSC FO</th>
                  </tr>
                  <tr>
                    <td colspan="4" style="border:1px solid #333;">Instructions: Put a check if the requirements are complete. If incomplete, use the space provided to indicate the name of appointee and the lacking requirement/s.</td>
                  </tr>
              </thead>
              <tbody>
                  <tr>
                     <td class="text-center">1</td>
                     <td>
                        APPOINTMENT FORMS (CS Form No. 33-B, Revised 2018) - Original CSC copy of appointment form
                     </td>
                     <td></td>
                     <td></td>
                  </tr>
                  <tr>
                     <td class="text-center">2</td>
                     <td>
                        PLANTILLA OF CASUAL APPOINTMENT (CSC Form No. 34-B or D) - Original CSC copy
                     </td>
                     <td></td>
                     <td></td>
                  </tr>
                  <tr>
                     <td class="text-center">3</td>
                     <td>
                        PERSONAL DATA SHEET (CS Form No. 212, Revised 2017)
                     </td>
                     <td></td>
                     <td></td>
                  </tr>
                  <tr>
                     <td class="text-center">4</td>
                     <td>
                        ORIGINAL COPY OF AUTHENTICATED CERTIFICATE OF ELIGIBILITY/ RATING/ LICENSE - Except if the eligibility has been previously authenticated in 2004 or onward and recorded
                     </td>
                     <td></td>
                     <td></td>
                  </tr>
                  <tr>
                     <td class="text-center">4</td>
                     <td>
                        POSITION DESCRIPTION FORM (DBM-CSC Form No. 1, Revised 2017)
                     </td>
                     <td></td>
                     <td></td>
                  </tr>
                  <tr>
                     <td class="text-center">6</td>
                     <td>
                        OATH OF OFFICE (CS Form No. 32, Revised 2017)
                     </td>
                     <td></td>
                     <td></td>
                  </tr>
                  <tr>
                     <td class="text-center">7</td>
                     <td>
                        CERTIFICATE OF ASSUMPTION TO DUTY (CS Form No. 4)
                     </td>
                     <td></td>
                     <td></td>
                  </tr>
                  <tr>
                    <td colspan="2"></td>
                    <td>
                        <p class="text-justify mb-6" style="text-indent: 20px;">This is to certify that I have checked the veracity, authenticity and completeness of all the requirements in support of the appointments attached herein.</p>
                          
                        <div class="row">
                          <div class="col-2"></div>
                          <div class="col-8 text-center">&nbsp; {!! @$hrmo[0] !!}</div>
                          <div class="col-2"></div>
                        </div>

                        <div class="row">
                          <div class="col-2"></div>
                          <div class="col-8  border-top border-dark text-center">
                            <span >HRMO</span>
                          </div>
                          <div class="col-2"></div>
                        </div>
                    </td>
                    <td>
                        <p class="text-justify mb-6" style="text-indent: 20px;">This is to certify that I have checked all the requirements in support of the appointments attached herein and found these to be  [  ] complete /  [  ]   lacking.</p>
                        <div class="row">
                          <div class="col-2"></div>
                          <div class="col-8  border-top border-dark text-center">
                            <span >CSC FO Receiving Officer</span>
                          </div>
                          <div class="col-2"></div>
                        </div>
                    </td>
                  </tr>
              </tbody>

          </table>
      </div>
  </div>

</div>

 <div class="form-group row text-right d-print-none">
    <div class="col col-sm-10 col-lg-9 offset-sm-1 offset-lg-3">
      {{ Form::button('Print', ['id' => 'evaluation-report', 'class'=>'btn btn-primary btn-space', 'type'=>'submit']) }}
      {{ Form::reset('Cancel', ['class'=>'btn btn-space btn-danger']) }}
    </div>
  </div>
@endsection

@section('scripts')
<script type="text/javascript">
  $(document).ready(function() {
    $('#evaluation-report').click(function() {
      window.print();
    });
  });
</script>
@endsection