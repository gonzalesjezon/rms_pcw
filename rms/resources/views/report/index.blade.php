@extends('layouts.app')

@section('css')
  <link rel="stylesheet" type="text/css" href="{{ URL::asset('beagle-assets/lib/datetimepicker/css/bootstrap-datetimepicker.min.css') }}">
@endsection

@section('content')
  <div class="page-head">
    <h2 class="page-head-title">Reports</h2>
  </div>
  <div class="main-content container-fluid">
    <div class="row">
      <div class="col-sm-12">
        <div class="card card-table">
          <div class="card-header">
          </div>
          <div class="card-body p-2">
            <div class="form-group row">
              {{ Form::label('reports_name', 'Reports Name', [
                  'class'=>'col-12 col-sm-2 col-form-label text-sm-right'
               ])}}
              <div class="col-12 col-sm-8 col-lg-6">
                {{ Form::select('reports_name', $reports, '', [
                        'class' => 'form-control form-control-xs',
                        'placeholder' => 'Select report',
                        'required' => true,
                    ])
                }}
              </div>
            </div>

            <div class="form-group row d-none hide hide-all" id="select_position">
              {{ Form::label('select_job', 'Position', [
                  'class'=>'col-12 col-sm-2 col-form-label text-sm-right'
               ])}}
              <div class="col-12 col-sm-8 col-lg-5">
                <select class="form-control form-control-xs unselect" name="select_job" id="select_job">
                  @foreach($jobs as $job)
                  <option value="{{$job->id}}" data-sg="{{ @$job->plantilla_item->salary_grade->Name }}">{{ ($job->status == 'plantilla') ? strtoupper($job->plantilla_item->position->Name) : strtoupper($job->plantilla_item->position->Name) }}</option>
                  @endforeach
                </select>
              </div>
            </div>

            <div class="form-group row d-none hide hide-all" id="select_applicant">
              {{ Form::label('applicant_name', 'Select Applicant', [
                  'class'=>'col-12 col-sm-2 col-form-label text-sm-right'
               ])}}
              <div class="col-12 col-sm-8 col-lg-5">
                <select class="form-control form-control-xs unselect">
                  <option value="0">Select applicant</option>
                  @foreach($applicants as $applicant)
                  <option>{!! $applicant->getFullName() !!}</option>
                  @endforeach
                </select>
              </div>
            </div>

            <div class="form-group row d-none hide hide-all" id="cos_applicant">
              {{ Form::label('applicant_name', 'Select Applicant', [
                  'class'=>'col-12 col-sm-2 col-form-label text-sm-right'
               ])}}
              <div class="col-12 col-sm-8 col-lg-5">
                <select class="form-control form-control-xs unselect" id="select_cos">
                  <option value="0">Select applicant</option>
                  @foreach($cos as $applicant)
                  <option value="{{ $applicant->id }}">{!! $applicant->getFullName() !!}</option>
                  @endforeach
                </select>
              </div>
            </div>

            <div class="row form-group hide hide-all d-none issued transmittal" >
              {{ Form::label('', 'HRMO', [
                  'class'=>'col-12 col-sm-2 col-form-label text-sm-right'
               ])}}
              <div class="col-12 col-sm-8 col-lg-6">
                <select class="form-control form-control-xs unselect" id="hrmo">
                  @foreach($employees as  $employee)
                    @if($employee)
                      <option value="{{ $employee->RefId }}" data-sign="{{ $employee->getFullName().'|'.@$employee->employeeinfo->position->Name  }}">{!! $employee->getFullName() !!}</option>
                    @endif
                  @endforeach
                </select>
              </div>
            </div>

            <div class="row form-group hide hide-all d-none issued" >
              {{ Form::label('', 'Agency Head', [
                  'class'=>'col-12 col-sm-2 col-form-label text-sm-right'
               ])}}
              <div class="col-12 col-sm-8 col-lg-6">
                <select class="form-control form-control-xs unselect" id="agency_head">
                  @foreach($employees as  $employee)
                    @if($employee)
                      <option value="{{ $employee->RefId }}" data-sign="{{ $employee->getFullName().'|'.@$employee->employeeinfo->position->Name  }}">{!! $employee->getFullName() !!}</option>
                    @endif
                  @endforeach
                </select>
              </div>
            </div>

            <div class="form-group row d-none hide hide-all" id="job_status">
              {{ Form::label('job_status', 'Select Job Status', [
                  'class'=>'col-12 col-sm-2 col-form-label text-sm-right'
               ])}}
              <div class="col-12 col-sm-8 col-lg-5">
                <select id="select_status" class="form-control form-control-xs">
                  <option></option>
                  <option value="plantilla">Plantilla</option>
                  <option value="nonplantilla">Non Plantilla</option>
                </select>
              </div>
            </div>

            <div class="row form-group d-none hide hide-all" id="get_month">
              {{ Form::label('', 'Select Month', [
                  'class'=>'col-12 col-sm-2 col-form-label text-sm-right'
               ])}}
              <div class="col-12 col-sm-8 col-lg-5">
                {{ Form::select('', $months, '', [
                        'class' => 'form-control form-control-xs unselect',
                        'required' => true,
                    ])
                }}
              </div>
            </div>

            <div class="row form-group d-none hide hide-all select_period">
              {{ Form::label('', 'Transaction Period', [
                  'class'=>'col-12 col-sm-2 col-form-label text-sm-right'
               ])}}
              <div class="col-12 col-sm-8 col-md-5 col-lg-4 col-xl-3">
                <div data-min-view="2" data-date-format="yyyy-mm-dd HH:ii" class="input-group date datetimepicker">
                  <input size="16" type="text" value=""
                         class="form-control form-control-sm" id="from_date">
                  <div class="input-group-append">
                    <button class="btn btn-primary"><i class="icon-th mdi mdi-calendar"></i></button>
                  </div>
                </div>
              </div>

              <div class="col-12 col-sm-8 col-md-5 col-lg-4 col-xl-3">
                <div data-min-view="2" data-date-format="yyyy-mm-dd HH:ii" class="input-group date datetimepicker">
                  <input size="16" type="text" value=""
                         class="form-control form-control-sm" id="to_date">
                  <div class="input-group-append">
                    <button class="btn btn-primary"><i class="icon-th mdi mdi-calendar"></i></button>
                  </div>
                </div>
              </div>
            </div>



            <div class="row form-group d-none hide hide-all" id="display_signatory">
              {{ Form::label('', 'Signatory', [
                  'class'=>'col-12 col-sm-2 col-form-label text-sm-right'
               ])}}
              <div class="col-12 col-sm-8 col-lg-5">
                <select class="form-control form-control-xs unselect" id="select_signatory">
                  @foreach($employees as  $employee)
                    @if($employee)
                      <option value="{{ $employee->RefId }}" data-sign="{{ $employee->getFullName().'|'.@$employee->employeeinfo->position->Name  }}">{!! $employee->getFullName() !!}</option>
                    @endif
                  @endforeach
                </select>
              </div>
            </div>

            <div class="row form-group hide hide-all d-none cdm" >
              {{ Form::label('', 'Certified Correct', [
                  'class'=>'col-12 col-sm-2 col-form-label text-sm-right'
               ])}}
              <div class="col-12 col-sm-8 col-lg-3">
                <select class="form-control form-control-xs unselect" id="certified">
                  @foreach($employees as  $employee)
                    @if($employee)
                      <option value="{{ $employee->RefId }}" data-sign="{{ $employee->getFullName().'|'.@$employee->employeeinfo->position->Name  }}">{!! $employee->getFullName() !!}</option>
                    @endif
                  @endforeach
                </select>
              </div>

              {{ Form::label('', 'Noted By', [
                  'class'=>'col-12 col-sm-1 col-form-label text-sm-right'
               ])}}
              <div class="col-12 col-sm-8 col-lg-3">
                <select class="form-control form-control-xs unselect" id="noted">
                  @foreach($employees as  $employee)
                    @if($employee)
                      <option value="{{ $employee->RefId }}" data-sign="{{ $employee->getFullName().'|'.@$employee->employeeinfo->position->Name  }}">{!! $employee->getFullName() !!}</option>
                    @endif
                  @endforeach
                </select>
              </div>
            </div>

            <div class="row form-group hide hide-all d-none cdm" >
              {{ Form::label('', 'HRMPSB Secretariat', [
                  'class'=>'col-12 col-sm-2 col-form-label text-sm-right'
               ])}}
              <div class="col-12 col-sm-8 col-lg-3">
                <select class="form-control form-control-xs unselect" id="secretariat">
                  @foreach($employees as  $employee)
                    @if($employee)
                      <option value="{{ $employee->RefId }}" data-sign="{{ $employee->getFullName().'|'.@$employee->employeeinfo->position->Name  }}">{!! $employee->getFullName() !!}</option>
                    @endif
                  @endforeach
                </select>
              </div>

            
              <label class="col-12 col-sm-1 col-form-label text-sm-left">HRMPSB <br> Chairperson</label>
              <div class="col-12 col-sm-8 col-lg-3">
                <select class="form-control form-control-xs unselect" id="chairperson">
                  @foreach($employees as  $employee)
                    @if($employee)
                      <option value="{{ $employee->RefId }}" data-sign="{{ $employee->getFullName().'|'.@$employee->employeeinfo->position->Name  }}">{!! $employee->getFullName() !!}</option>
                    @endif
                  @endforeach
                </select>
              </div>
            </div>

            <div class="row form-group hide hide-all d-none cdm" >
              {{ Form::label('', 'End-User', [
                  'class'=>'col-12 col-sm-2 col-form-label text-sm-right'
               ])}}
              <div class="col-12 col-sm-8 col-lg-3">
                <select class="form-control form-control-xs unselect" id="end_user">
                  @foreach($employees as  $employee)
                    @if($employee)
                      <option value="{{ $employee->RefId }}" data-sign="{{ $employee->getFullName().'|'.@$employee->employeeinfo->position->Name  }}">{!! $employee->getFullName() !!}</option>
                    @endif
                  @endforeach
                </select>
              </div>

            
              <label class="col-12 col-sm-1 col-form-label text-sm-left">EA <br> Representative</label>
              <div class="col-12 col-sm-8 col-lg-3">
                <select class="form-control form-control-xs unselect" id="ea_rep">
                  @foreach($employees as  $employee)
                    @if($employee)
                      <option value="{{ $employee->RefId }}" data-sign="{{ $employee->getFullName().'|'.@$employee->employeeinfo->position->Name  }}">{!! $employee->getFullName() !!}</option>
                    @endif
                  @endforeach
                </select>
              </div>
            </div>

            <div class="row form-group hide hide-all d-none cdm">
              {{ Form::label('', 'HRMPSB Member', [
                  'class'=>'col-12 col-sm-2 col-form-label text-sm-right'
               ])}}
              <div class="col-12 col-sm-8 col-lg-3">
                <select class="form-control form-control-xs unselect" id="member">
                  @foreach($employees as  $employee)
                    @if($employee)
                      <option value="{{ $employee->RefId }}" data-sign="{{ $employee->getFullName().'|'.@$employee->employeeinfo->position->Name  }}">{!! $employee->getFullName() !!}</option>
                    @endif
                  @endforeach
                </select>
              </div>
            </div>

            <div class="form-group row">
              <label class="col-12 col-sm-2 col-form-label text-sm-right">Print Date </label>
              <div class="col-12 col-sm-8 col-md-5 col-lg-4 col-xl-3">
                <div data-min-view="2" data-date-format="yyyy-mm-dd" class="input-group date datetimepicker">
                  <input size="16" type="text" value="" name="print_date"
                         class="form-control form-control-sm" id="print_date">
                  <div class="input-group-append">
                    <button class="btn btn-primary"><i class="icon-th mdi mdi-calendar"></i></button>
                  </div>
                </div>
              </div>
            </div>

<!--             <div class="form-group row">
              <label class="col-12 col-sm-3 col-md-1 col-form-label text-sm-right"> Sex </label>
              <div class="col-12 col-sm-7 col-md-1 col-lg-1 col-xl-1">
                <div class="icon-container">
                  <div class="icon"><span class="mdi mdi-female"></span></div><span class="icon-class"> </span>
                </div>
              </div>
              <div class="col-12 col-sm-7 col-md-1 col-lg-1 col-xl-1">
                <div class="icon-container">
                  <div class="icon"><span class="mdi mdi-male"></span></div><span class="icon-class"> </span>
                </div>
              </div>
            </div> -->

            <hr>

            <div class="form-group row">
              <div class="col-4 offset-6">
                <buton class="btn btn-secondary" id="preview">Preview</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection

@section('scripts')
  <script src="{{ URL::asset('beagle-assets/lib/datetimepicker/js/bootstrap-datetimepicker.min.js') }}"
          type="text/javascript"></script>
  <script src="{{ URL::asset('beagle-assets/js/app-form-elements.js') }}" type="text/javascript"></script>
  <script src="{{ URL::asset('beagle-assets/lib/select2/js/select2.min.js') }}" type="text/javascript"></script>
  <script src="{{ URL::asset('beagle-assets/lib/select2/js/select2.full.min.js') }}" type="text/javascript"></script>
  <script src="{{ URL::asset('beagle-assets/lib/bootstrap-slider/bootstrap-slider.min.js') }}"
          type="text/javascript"></script>
  <script type="text/javascript">
    $(document).ready(function() {
      //initialize the javascript
      App.init();
      App.formElements();

      var reportName;
      var bool = false;
      $(document).on('change','#reports_name',function(){
          bool = false;
          jobId  = '';
          printDate = '';
          level = '';

          $('.unselect').val([]);

          reportName = $(this).find(':selected').val();
          $('.hide-all').addClass('d-none');

          switch(reportName){
            case 'selection_lineup':
              $('#select_position').removeClass('d-none');
              bool = true;
              break;
            case 'checklist':
              $('#select_applicant').removeClass('d-none');
              bool = true;
              break;
            case 'resignation_acceptance':
              $('#select_applicant').removeClass('d-none');
              bool = true;
              break;
            case 'preliminary_evaluation':
              $('#select_position').removeClass('d-none');
              bool = true;
              break;
            case 'comparative-report':
              $('#select_position').removeClass('d-none');
              $('#select_applicant').addClass('d-none');
              bool = true;
              break;
            case 'oath_office':
              $('#select_applicant').removeClass('d-none');
              bool = true;
              break;
            case 'appointment_form_regulated':
              $('#select_applicant').removeClass('d-none');
              bool = true;
              break;
            case 'medical_certificate':
              $('#select_applicant').removeClass('d-none');
              bool = true;
              break;
            case 'background-checking':
              $('#select_applicant').removeClass('d-none');
              bool = true;
              break;
            case 'erasures_alteration':
              $('#select_applicant').removeClass('d-none');
              bool = true;
              break;
            case 'publication_vacant_position':
              $('.select_period').removeClass('d-none');
              $('#display_signatory').removeClass('d-none');
              bool = true;
              break;
            case 'matrix_qualification':
              $('#select_position').removeClass('d-none');
              bool = true;
              break;
            case 'psb_matrix_qualification':
              $('#select_position').removeClass('d-none');
              bool = true;
              break;
            case 'matrix_qualification_3':
              $('#select_position').removeClass('d-none');
              bool = true;
              break;
            case 'appointment-transmital':
              $('.select_period').removeClass('d-none');
              $('.transmittal').removeClass('d-none');

              bool = true;
              break;
            case 'cos_agreement':
              $('#cos_applicant').removeClass('d-none');
              bool = true;
              break;
            case 'pipeline-report':
              $('#get_month').removeClass('d-none');
              $('#job_status').removeClass('d-none');
              bool = true;
              break;
            case 'status_of_vacancy':
              $('#get_month').removeClass('d-none');
              break;
            case 'comparative_matrix':
              $('#select_position').removeClass('d-none');
              $('.cdm').removeClass('d-none');
              $('#get_month').removeClass('d-none');
              break;
            case 'shortlisted':
              $('#get_month').removeClass('d-none');
              break;
            case 'appointments_issued':
              $('.issued').removeClass('d-none');
              $('.select_period').removeClass('d-none');
              bool = true;
              break;
            default:
              $('.hide').addClass('d-none')
              bool = false;
              break;
          }
      });

      var jobId;
      var Level;
      $(document).on('change','#select_job',function(){
          jobId = $(this).find(':selected').val();
          sg = $(this).find(':selected').data('sg');
          bool = true;

          if(Number(sg) < 10){
            Level = '1st Level';
          }
          else if(Number(sg) > 9 && Number(sg) < 24)
          {
            Level = '2nd Level';
          }
          else if(Number(sg) > 23)
          {
           Level = '3rd Level'; 
          }

      })

      var appId;
      $(document).on('change','#select_applicant',function(){
          appId = $(this).find(':selected').val();
          bool = true;

      });

      var cosAppId;
      $(document).on('change','#select_cos',function(){
          cosAppId = $(this).find(':selected').val();
          bool = true;

      });

      var month;
      $(document).on('change','#get_month',function(){
          month = $(this).find(':selected').val();
          bool = true;

      })

      var jobStatus;
      $(document).on('change','#job_status',function(){
          jobStatus = $(this).find(':selected').val();
          bool = true;

      })

      var signatoryId;
      $(document).on('change','#select_signatory',function(){
          signatoryId = $(this).find(':selected').val();
          bool = true;
      });

      var certified = "";
      $(document).on('change','#certified',function(){
          certified = $(this).find(':selected').data('sign');
          bool = true;
      });

      var noted = "";
      $(document).on('change','#noted',function(){
          noted = $(this).find(':selected').data('sign');
          bool = true;
      });

      var secretariat = "";
      $(document).on('change','#secretariat',function(){
          secretariat = $(this).find(':selected').text();
          bool = true;
      });

      var chairperson = "";
      $(document).on('change','#chairperson',function(){
          chairperson = $(this).find(':selected').text();
          bool = true;
      });

      var end_user = "";
      $(document).on('change','#end_user',function(){
          end_user = $(this).find(':selected').text();
          bool = true;
      });

      var ea_rep = "";
      $(document).on('change','#ea_rep',function(){
          ea_rep = $(this).find(':selected').text();
          bool = true;
      });

      var member = "";
      $(document).on('change','#member',function(){
          member = $(this).find(':selected').text();
          bool = true;
      });



      var printDate;
      $(document).on('change','#print_date',function(){
        printDate = $(this).val();
      });

      var id;
      var param;
      var param2;
      var selected_month;
      $(document).on('click','#preview',function(){
          param     = "";
          level     = "";
          sign      = "";

          let hrmo        = $('#hrmo :selected').data('sign');
          let agencyHead  = $('#agency_head :selected').data('sign');
          let cscOfficial = $('#csc_official :selected').data('sign');
          let fromDate = $('#from_date').val();
          let toDate   = $('#to_date').val();

          if(jobId){
            id = jobId;
          }else{
            id = appId;
          }

          if(cosAppId){
            id = cosAppId;
          }

          arrSign = [];

          switch(reportName)
          {
            case 'comparative_matrix':
              arrSign = {
                'certified':certified, 
                'noted':noted,
                'secretariat':secretariat,
                'chairperson':chairperson,
                'end_user':end_user,
                'ea_rep':ea_rep,
                'member':member,
              };
              break;

            case 'appointments_issued':
              arrSign = {
                'hrmo':hrmo, 
                'agency_head':agencyHead,
                'from':fromDate,
                'to':toDate,
              };
              break;

            case 'appointment-transmital':
              arrSign = {
                'hrmo':hrmo, 
                'from':fromDate,
                'to':toDate,
              };
              break;

            case 'publication_vacant_position':
              arrSign = {
                'hrmo':hrmo, 
                'from':fromDate,
                'to':toDate,
              };
              break;
          }

          param = (id) ? 'id='+id+'&' : '';
          param2 = (month) ? 'month='+month+'&' : '';
          date = (printDate) ? 'date='+printDate+'&' : '';
          status = (jobStatus) ? 'status='+jobStatus+'&' : '';
          signatory = (signatoryId) ? 'signatory_id='+signatoryId+'&' : '';
          level = (Level) ? 'level='+Level+'&' : '';
          sign = (!Array.isArray(arrSign) || !arrSign.length) ? 'sign='+JSON.stringify(arrSign)+'&' : '';

          var href = "";
          if(bool == true){
            if(param || date || param2 || sign){

              href = window.location+'/'+reportName+'?'+param+date+param2+status+signatory+level+sign;

            }else{
              alert('Select parameter first!');
              return false;
            }
          }else{

              href = window.location+'/'+reportName;

          }

          window.open(href, '_blank');
      });

    });
  </script>
@endsection
