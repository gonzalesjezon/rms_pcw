@extends('layouts.print')

@section('css')
<style type="text/css">
  @media print{
  	@page{
  		size:landscape !important;
  	}
    .table>thead>tr>th{
      vertical-align: middle !important;
      padding: 4px !important;
      font-size: 8pt !important;
    }

    .table>thead>tr>th,
    .table>tbody>tr>td{
      border: 1px solid #333 !important;
    }
  }
  .table>thead>tr>th{
    vertical-align: middle !important;
    padding: 4px !important;
    font-size: 8pt !important;
  }
  .table>thead>tr>th,
  .table>tbody>tr>td{
    border: 1px solid #333 !important;
  }
</style>
@endsection

@section('content')

<div id="reports" style="width: 100%;margin: auto; font-size: 8pt;font-family: Arial, Helvetica, sans-serif;">

  <table class="table table-bordered">
    <thead class="text-center">
      @include('report.includes._header')
      <tr>
      	<th colspan="3">PERMANENT</th>
      	<th colspan="3">CASUAL</th>
      	<th colspan="3">CONTRACTUAL</th>
      	<th colspan="3">TOTAL NO. OF PERMANENT, <br> CASUAL & CONTRACTUAL</th>
      	<th colspan="3">CONTRACT OF SERVICE</th>
      	<th colspan="3">JOB ORDER</th>
      	<th colspan="3">TOTAL POSITION </th>
      </tr>
      <tr>
      	<th>F</th>
      	<th>UF</th>
      	<th>T</th>
      	<th>F</th>
      	<th>UF</th>
      	<th>T</th>
      	<th>F</th>
      	<th>UF</th>
      	<th>T</th>
      	<th>F</th>
      	<th>UF</th>
      	<th>T</th>
      	<th>F</th>
      	<th>UF</th>
      	<th>T</th>
      	<th>F</th>
      	<th>UF</th>
      	<th>T</th>
      	<th>F</th>
      	<th>UF</th>
      	<th>T</th>
      </tr>
    </thead>
    <tbody class="text-center">
      <tr>
        <td>{{ $response['perma_f'] }}</td>
        <td>{{ $response['perma_uf'] }}</td>
        <td>{{ ($response['perma_uf'] + $response['perma_f'])  }}</td>
        <td>{{ $response['cas_f'] }}</td>
        <td>{{ $response['cas_uf'] }}</td>
        <td>{{ ($response['cas_uf'] + $response['cas_f'])  }}</td>
        <td>{{ $response['cont_f'] }}</td>
        <td>{{ $response['cont_uf'] }}</td>
        <td>{{ ($response['cont_uf'] + $response['cont_f'])  }}</td>
        <td>
          {!! $subFirstF = ($response['perma_f'] + $response['cas_f'] + $response['cont_f']) !!}
        </td>
        <td>
          {!! $subFirstUF = ($response['perma_uf'] + $response['cas_uf'] + $response['cont_uf']) !!}
        </td>
        <td>
          {!! $totalFirst = ($subFirstF + $subFirstUF)  !!}
        </td>
        <td>{{ $response['cos_f'] }}</td>
        <td>{{ $response['cos_uf'] }}</td>
        <td>{{ ($response['cos_uf'] + $response['cos_f'])  }}</td>
        <td>{{ $response['jo_f'] }}</td>
        <td>{{ $response['jo_uf'] }}</td>
        <td>{{ ($response['jo_uf'] + $response['jo_f'])  }}</td>
        <td>
           @php 

            $netF   = 0; 
            $neTUF = 0;

           @endphp
          {!! $netF = ($response['perma_f'] + $response['cas_f'] + $response['cont_f'] + $response['cos_f'] + $response['jo_f']) !!}
        </td>
        <td>
          {!! $netUF = ($response['perma_uf'] + $response['cas_uf'] + $response['cont_uf'] + $response['cos_uf'] + $response['jo_uf']) !!}
        </td>
        <td>
          {!! ($netF + $neTUF) !!}
        </td>
      </tr>
    </tbody>
  </table>

</div>

 <div class="form-group row text-right d-print-none">
    <div class="col col-sm-10 col-lg-9 offset-sm-1 offset-lg-3">
      {{ Form::button('Print', ['id' => 'evaluation-report', 'class'=>'btn btn-primary btn-space', 'type'=>'submit']) }}
      {{ Form::reset('Cancel', ['class'=>'btn btn-space btn-danger']) }}
    </div>
  </div>
@endsection

@section('scripts')
<script type="text/javascript">
  $(document).ready(function() {
    $('#evaluation-report').click(function() {
      window.print();
    });
  });
</script>
@endsection