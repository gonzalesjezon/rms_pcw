@extends('layouts.print')

@section('css')
@endsection

@section('content')
<div class="reports" style="width: 960px;margin: auto;font-size: 12px;font-family: Arial, Helvetica, sans-serif;">

	<div class="row mb-4">
		<div class="col-sm-12 text-center">
			<p style="font-size:1.8em; ">Office of the Vice President of <br> The Philippines</p>
		</div>
	</div>

	<div class="row mb-1">
		<div class="col-sm-4 text-right">Title Report</div>
		<div class="col-sm-1"></div>
		<div class="col-sm-2 text-left">Report on Separation</div>
	</div>
	<div class="row mb-1">
		<div class="col-sm-4 text-right">Period Covered</div>
		<div class="col-sm-1"></div>
		<div class="col-sm-2 text-left"></div>
	</div>
	<div class="row mb-1">
		<div class="col-sm-4 text-right">Office</div>
		<div class="col-sm-1"></div>
		<div class="col-sm-2 text-left"></div>
	</div>

	<div class="row mb-4">
		<div class="col-sm-12">
			<table class="table table-bordered">
				<thead class="text-center font-weight-bold border">
					<tr>
						<th></th>
						<th style="vertical-align: middle;">NAME</th>
						<th style="vertical-align: middle;">POSITION TITLE</th>
						<th style="vertical-align: middle;">SALARY GRADE</th>
						<th style="vertical-align: middle;">LEVEL OF POSITION</th>
						<th style="vertical-align: middle;">STATUS OF APPOINTMENT</th>
						<th style="vertical-align: middle;">EFFECTIVE DATE OF SEPARATION</th>
						<th style="vertical-align: middle;">MODE OF SEPARATION</th>
					</tr>
				</thead>
			</table>
		</div>
	</div>

	<div class="row mb-8">
		<div class="col-2">Prepared By:</div>
		<div class="col-6"></div>
		<div class="col-4">Certified Correct:</div>
	</div>

	<div class="row mb-4">
		<div class="col-3 text-center">
			<p class="font-weight-bold m-0">SHELLY MONIQUE P. PETRAS</p>
			<p class="m-0">Administrative Officer IV</p>
			<p class="m-0">Human Resources Management Unit</p>
		</div>
		<div class="col-5"></div>
		<div class="col-4 text-center">
			<p class="font-weight-bold m-0">JENNIFER J. TAN</p>
			<p class="m-0">Director IV</p>
			<p class="m-0"> Administrative and Financial Services Office</p>
		</div>
	</div>

	<div class="row">
		<div class="col-3">Date: ___________________</div>
		<div class="col-5"></div>
		<div class="col-3">Date: ___________________</div>
	</div>

</div>


 <div class="form-group row text-right">
    <div class="col col-sm-10 col-lg-9 offset-sm-1 offset-lg-3 d-print-none">
      {{ Form::button('Print', ['id' => 'evaluation-report', 'class'=>'btn btn-primary btn-space', 'type'=>'submit']) }}
      {{ Form::reset('Cancel', ['class'=>'btn btn-space btn-danger']) }}
    </div>
  </div>
@endsection

@section('scripts')
<script type="text/javascript">
  $(document).ready(function() {
    $('#evaluation-report').click(function() {
      window.print();
    });
  });
</script>
@endsection