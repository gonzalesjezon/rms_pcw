@extends('layouts.print')

@section('css')
<style type="text/css">
	.table2>tbody>tr>td{
		border: none !important;
		padding: 1px;
		vertical-align: top !important;
	}

	.table3>thead>tr>th, .table3>tbody>tr>td{
		padding: 2px !important;
		border: 1px solid #333 !important;
		vertical-align: middle;
	}

	@media print{
		.table2>tbody>tr>td{
			border: none !important;
			padding: 1px;
			vertical-align: top !important;
		}

		.table3>thead>tr>th, .table3>tbody>tr>td{
			padding: 2px !important;
			border: 1px solid #333 !important;
			vertical-align: middle;
		}
	}
</style>
@endsection

@section('content')

<div class="form-group row text-right d-print-none mb-6">
  <div class="col col-sm-10 col-lg-9 offset-sm-1 offset-lg-3">
    {{ Form::button('Print', ['id' => 'evaluation-report', 'class'=>'btn btn-primary btn-space', 'type'=>'submit']) }}
    {{ Form::reset('Cancel', ['class'=>'btn btn-space btn-danger']) }}
  </div>
</div>

<div id="reports" style="width: 960px;margin: auto; font-size: 12px;font-family: Arial, Helvetica, sans-serif;">

  <div class="row mb-6">
  	<div class="col-sm-12 text-center">
  		<p class="mb-0 pb-0 font-weight-bold">PHILIPPINE COMMISSION ON WOMEN</p>
  		<p class="font-weight-bold">HUMAN RESOURCE MERIT, PROMOTION AND SELECTION BOARD</p>
  		<p class="font-weight-bold">COMPARATIVE DATA MATRIX</p>
  	</div>
  </div>

  <table class="table table2 border-0">
  	<tbody>
  		<tr>
  			<td style="width: 110px;" class="font-weight-bold">Vacant Position</td>
  			<td style="width: 30em;">: 
  				<span class="pl-3">{!! @$job->plantilla_item->position->Name !!}</span>
  			</td>
  			<td style="width: 12em;" class="font-weight-bold">Qualification Standards</td>
  		</tr>

  		<tr>
  			<td style="width: 110px;" class="font-weight-bold">Item Number</td>
  			<td style="width: 30em;">: 
  				<span class="pl-3">{!! (@$job->status == 'plantilla') ? $job->plantilla_item->item_number : 'NA' !!}</span>
  			</td>
  			<td class="font-weight-bold">Education</td>
  			<td>
  				<p class="text-justify mb-0 pb-0">{!! strip_tags(@$job->education) !!}</p>
  			</td>
  		</tr>

  		<tr>
  			<td style="width: 120px;" class="font-weight-bold">SG / Monthly Salary</td>
  			<td style="width: 30em;">: 
  				<span class="pl-3">{!! @$job->plantilla_item->salary_grade->Name.' / '.number_format(@$job->plantilla_item->basic_salary,2) !!}</span>
  			</td>
  			<td class="font-weight-bold">Experience</td>
  			<td>
  				<p class="text-justify mb-0 pb-0">{!! strip_tags(@$job->experience) !!}</p>
  			</td>
  		</tr>

  		<tr>
  			<td colspan="2"/td>
  			<td class="font-weight-bold">Training</td>
  			<td>
  				<p class="text-justify mb-0 pb-0">{!! strip_tags(@$job->training) !!}</p>
  			</td>
  		</tr>

  		<tr>
  			<td colspan="2"></td>
  			<td class="font-weight-bold">Eligibility</td>
  			<td>
  				<p class="text-justify">{!! strip_tags(@$job->elgibiility) !!}</p>
  			</td>
  		</tr>
  	</tbody>
  </table>

	<?php
		$col1 = "";
		$col2 = "";

		switch ($level) {
			case '1st Level':
				$col1 = '35%';
				$col2 = '20%';
				break;
			
			case '2nd Level':
				$col1 = '30%';
				$col2 = '25%';
				break;

			case '3rd Level':
				$col1 = '25%';
				$col2 = '30%';
				break;
		}
	?>

  <table class="table table3 table-bordered">
  	<thead class="text-center font-weight-bold">
  		<tr>
  			<th colspan="2">(1)</th>
  			<th>(2)</th>
  			<th>(3)</th>
  			<th colspan="4">(4) ETE</th>
  			<th colspan="2">(5)</th>
  			<th colspan="2">(6)</th>
  			<th>(7)</th>
  			<th>8</th>
  		</tr>
  		<tr>
  			<th rowspan="2">No</th>
  			<th rowspan="2">Name of Applicants</th>
  			<th rowspan="2">Appropriate <br> Eligibility</th>
  			<th rowspan="2">Performance <br> Rating</th>
  			<th>15 %</th>
  			<th>10 %</th>
  			<th>20 %</th>
  			<th>45 %</th>
  			<th colspan="2">Written Exam ({!! $col1 !!})</th>
  			<th colspan="2">CBI ({!! $col2 !!})</th>
  			<th>100%</th>
  			<th rowspan="2">Remarks</th>
  		</tr>
  		<tr>
  			<th>Education</th>
  			<th>Training</th>
  			<th>Experience</th>
  			<th>Sub Total Rating</th>
  			<th>Average Score</th>
  			<th>Sub Total Rating</th>
  			<th>Raw Score</th>
  			<th>Sub Total Rating</th>
  			<th>Total Rating</th>
  		</tr>
  	</thead>

  	<tbody>
  		@foreach($applicants as $key => $applicant)
  		<tr>
  			<td>{!! $key+1 !!}</td>
  			<td nowrap>{!! $applicant->getFullName() !!}</td>
  			<td nowrap>{!! config('params.eligibility_type.'.@$applicant->latest_eligibility->eligibility_ref) !!}</td>
  			<td >{!! $applicant->gwa !!}</td>
  			<td class="text-center" >{!! @$applicant->applicant_rating->education_points !!}</td>
  			<td class="text-center" >{!! @$applicant->applicant_rating->training_points !!}</td>
  			<td class="text-center" >{!! @$applicant->applicant_rating->experience_points !!}</td>
  			<td class="text-center" >{!! @$applicant->applicant_rating->total_points !!}</td>
  			<td class="text-center" >{!! @$applicant->examination->average_raw_score !!}</td>
  			<td class="text-center" >{!! @$applicant->examination->sub_total_rating !!}</td>
  			<td class="text-center" >{!! @$applicant->interview->average_raw_score !!}</td>
  			<td class="text-center" >{!! @$applicant->interview->sub_total_rating !!}</td>
  			<td class="text-center">
  				{!! (@$applicant->applicant_rating->total_points + @$applicant->examination->sub_total_rating + @$applicant->interview->sub_total_rating) !!}
  			</td>
  			<td></td>
  		</tr>
  		@endforeach
  	</tbody>
  </table>

  <div class="row mb-8">
  	<div class="col-3">Certified Correct:</div>
  	<div class="col-1"></div>
  	<div class="col-3">Noted By</div>
  </div>

  <?php 
    $certified = explode('|', $signatory->certified);
    $noted = explode('|', $signatory->noted);

  ?>

  <div class="row mb-8">
    <div class="col-3">
      <span class="font-weight-bold">{!! @$certified[0] !!}</span> <br>
      {!! @$certified[1] !!}
    </div>
    <div class="col-1"></div>
    <div class="col-3">
      <span class="font-weight-bold">{!! @$noted[0] !!}</span> <br>
      {!! @$noted[1] !!}
    </div>
  </div>

  <div class="row m-1 text-center font-weight-bold" style="font-size: 8pt;">
    <div class="col-3">{!! @$signatory->secretariat !!}</div>
    <div class="col-2">{!! @$signatory->chairperson !!}</div>
    <div class="col-3">{!! @$signatory->end_user !!}</div>
    <div class="col-2">{!! @$signatory->ea_rep !!}</div>
    <div class="col-2">{!! @$signatory->member !!}</div>
  </div>

  <div class="row m-1 text-center">
  	<div class="col-3">HRMPSB Secretariat</div>
  	<div class="col-2">HRMPSB Chairperson</div>
  	<div class="col-3">End-User</div>
  	<div class="col-2">EA Representative</div>
  	<div class="col-2">HRMPSB Member</div>
  </div>

</div>
@endsection

@section('scripts')
<script type="text/javascript">
  $(document).ready(function() {
    $('#evaluation-report').click(function() {
      window.print();
    });
  });
</script>
@endsection