
<tr>
	@php
	$col = 0;
	$new_col = 1;
		switch($report)
		{
			case 'pipeline-report':
				$col = 6;
				break;
			case 'status_of_vacancy':
				$col = 17;
				break;
			case 'work_force_complement':
				$col = 18;
				break;
			case 'shortlisted':
				$col = 7;
				$new_col = 3;
				break;
		}

	@endphp
	<th rowspan="3" colspan="{{$new_col}}">
		<img src="{{ url('img/pcc-logo-small2.png') }}" style="height: 80px;">
	</th>
	
	<th rowspan="3" colspan="{{$col}}" class="text-center">
	@if($report == 'pipeline-report')
	<h3>Pipeline Report</h3>
	@elseif($report == 'status_of_vacancy')
	<h3>Status Of Vacancy</h3>
	@elseif($report == 'shortlisted')
	<h3>Shortlisted</h3>
	@else
	<h3>Workforce Complement</h3>
	@endif

	</th>
	<th colspan="2" class="text-left">Document Code: HRDMS-R-004</th>
</tr>
<tr>
	<th colspan="2" class="text-left">Date: <span id="print_date"></span></th>
</tr>
<tr>
	<th class="text-left">Revision No</th>
	<th colspan="2" class="text-left">Page</th>
</tr>