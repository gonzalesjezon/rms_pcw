@extends('layouts.print')

@section('css')
<style type="text/css">
	.table2>tbody>tr>td{
		border: none !important;
		padding: 1px;
		vertical-align: top !important;
	}

	.table3>thead>tr>th, .table3>tbody>tr>td{
		padding: 2px !important;
		border: 1px solid #333 !important;
		vertical-align: middle;
	}

	.v-top{
		vertical-align: top !important;
	}

	@media print{
		.table2>tbody>tr>td{
			border: none !important;
			padding: 1px;
			vertical-align: top !important;
		}

		.table3>thead>tr>th, .table3>tbody>tr>td{
			padding: 2px !important;
			border: 1px solid #333 !important;
			vertical-align: middle;
		}

		.v-top{
			vertical-align: top !important;
		}
	}
</style>
@endsection

@section('content')

<div class="form-group row text-right d-print-none mb-6">
  <div class="col col-sm-10 col-lg-9 offset-sm-1 offset-lg-3">
    {{ Form::button('Print', ['id' => 'evaluation-report', 'class'=>'btn btn-primary btn-space', 'type'=>'submit']) }}
    {{ Form::reset('Cancel', ['class'=>'btn btn-space btn-danger']) }}
  </div>
</div>

<div id="reports" style="width: 960px;margin: auto; font-size: 12px;font-family: Arial, Helvetica, sans-serif;">
	
	<table class="table table3">
		<thead class="text-center">
			@include('report.includes._header')
			<tr>
				<th colspan="12">PROFILE OF APPLICANTS</th>
			</tr>
			<tr>
				<th style="width: 5%;">NO</th>
				<th style="width: 15%;">NAME</th>
				<th style="width: 5%;">AGE</th>
				<th style="width: 5%;">SEX</th>
				<th>ADDRESS</th>
				<th>PHONE NUMBER</th>
				<th>EMAIL ADDRESS</th>
				<th>SPECIAL NEEDS (IF ANY)</th>
				<th>EDUCATION</th>
				<th>TRAINING (only relevant to the position applied)</th>
				<th>EXPERIENCE (only relevant to the position applied)</th>
				<th>ELIGIBILITY</th>
			</tr>
		</thead>
		<tbody>
			@foreach($applicants as $key => $applicant)
			<tr>
				<td class="text-center v-top">{!! $key+1 !!}</td>
				<td nowrap class="v-top">{!! $applicant->getFullName() !!}</td>
				<td class="text-center v-top">{!! Carbon\Carbon::now()->diffInYears(Crypt::decrypt($applicant->birthday)) !!}</td>
				<td class="v-top">{!! $applicant->gender !!}</td>
				<td nowrap class="v-top">
					{!!  $applicant->getAddress() !!}
				</td>
				<td class="v-top">{!! @$applicant->mobile_number !!}</td>
				<td class="v-top">{!! (@$applicant) ? Crypt::decrypt(@$applicant->email_address) : '' !!}</td>
				<td class="v-top">
					{!! (@$applicant->person_id_no) ? 'PWD' : 'NA' !!}
				</td>
				<td class="v-top" nowrap>
					{!! @$applicant->highestEducation->course !!}
				</td>

				<td class="v-top" nowrap>
					{!! @$applicant->applicant_rating->training_remarks !!}
				</td>

				<td class="v-top" nowrap>
					{!! @$applicant->applicant_rating->experience_remarks !!}
				</td>

				<td class="v-top" nowrap>
						{!! config('params.eligibility_type.'.@$applicant->latest_eligibility->eligibility_ref) !!} <br>
						{!! @$applicant->latest_eligibility->other_specify !!}
				</td>

			</tr>
			@endforeach
		</tbody>
	</table>
  
</div>

@endsection

@section('scripts')
<script type="text/javascript">
  $(document).ready(function() {
    $('#evaluation-report').click(function() {
      window.print();
    });
  });
</script>
@endsection