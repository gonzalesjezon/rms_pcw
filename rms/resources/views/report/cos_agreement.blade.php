@extends('layouts.print')

@section('css')
<style type="text/css">
  @media print{
    .table>thead>tr>th{
      vertical-align: middle !important;
      padding: 4px !important;
      font-size: 8pt !important;
    }
  }
  .table>thead>tr>th{
    vertical-align: middle !important;
    padding: 4px !important;
    font-size: 8pt !important;
  }

</style>
@endsection

@section('content')

<div class="form-group row text-right d-print-none mb-6">
    <div class="col col-sm-10 col-lg-9 offset-sm-1 offset-lg-3">
      {{ Form::button('Print', ['id' => 'evaluation-report', 'class'=>'btn btn-primary btn-space', 'type'=>'submit']) }}
      {{ Form::reset('Cancel', ['class'=>'btn btn-space btn-danger']) }}
    </div>
  </div>

<div id="reports" style="width: 960px;margin: auto;font-size:12pt;font-family: Arial, Helvetica, sans-serif;">
	<h4 class="text-center font-weight-bold mb-8">
		CONTRACT OF SERVICE AGREEMENT
	</h4>

	<h4 class="font-weight-bold">KNOW ALL PERSONS BY THESE PRESENTS:</h4>
	<p class="offset-1">This <b>Contract of Service Agreement</b> is made and executed by and between:</p>

	<p style="text-indent: 4.9em;" class="text-justify">
		The <b>PHILIPPINE COMMISSION ON WOMEN</b>, a government agency under the Office of the President, with office address at 1145 J. P. Laurel St., San Miguel, Manila, Philippines, represented by its Executive Director, <b>EMMELINE L. VERZOSA</b>, hereinafter referred to as “ <b>PCW</b> ”;
	</p>

	<p class="text-center">-and-</p>

	<p style="text-indent: 4.9em;" class="text-justify mb-6">
		<b>{!! (@$applicant) ? strtoupper(@$applicant->getFullName()) : '' !!}</b>, Filipino of legal age, and a resident of {!! (@$applicant) ? $applicant->getAddress() : '' !!}, hereinafter referred to as the <b>“ {!! (@$applicant) ? @$applicant->job->plantilla_item->position->Name : '' !!} ”</b>
	</p>

	<p class="text-center font-weight-bold">WITNESSETH THAT:</p>

	<p style="text-indent: 4.9em;" class="text-justify">
		<b>WHEREAS</b>, under Republic Act 9710 or the Magna Carta of Women, the PCW is mandated to coordinate with and assist government agencies at national and local levels to effectively address gender concerns in their development processes—from planning, programming, budgeting, monitoring and evaluation;
	</p>

	<p style="text-indent: 4.9em;" class="text-justify">
		<b>WHEREAS</b>, due to its limited number of employees, and taking into account the work necessary to carry out its functions, the PCW, specifically the Human Resource Management and Development Section under Administrative and Finance Division, is unable to meet the demands of its workload as identified in the Human Resource Management and Development Section Work and Financial Plan;
	</p>

	<p style="text-indent: 4.9em;" class="text-justify">
		<b>WHEREAS</b>, the PCW requires the services of the {!! (@$applicant) ? @$applicant->job->plantilla_item->position->Name : '' !!} to provide administrative and clerical support to the Supply Section, in order to ensure that PCW efficiently and effectively delivers all its outstanding commitments;
	</p>

	<p style="text-indent: 4.9em;" class="text-justify">
		<b>WHEREAS</b>, the {!! (@$applicant) ? @$applicant->job->plantilla_item->position->Name : '' !!} was chosen, having been found competent to render the required services and to deliver the outputs described herein, based on the assessment made on her academic qualifications, related work experience, technical skills, attitude, and satisfactory performance in her previous work engagement with PCW.
	</p>

	<p style="text-indent: 4.9em;" class="text-justify mb-6">
		<b>NOW</b>, <b>THEREFORE</b>, for and in consideration of the above premises, both parties hereby declare and agree to the following:
	</p>

	<p class="text-center font-weight-bold mb-0 pb-0">Section 1</p>
	<p class="text-center font-weight-bold">Work Description</p>

	<p style="text-indent: 4.9em;" class="text-justify">
		This Contract of Service Agreement covers the engagement of the {!! (@$applicant) ? @$applicant->job->plantilla_item->position->Name : '' !!}, who shall provide regular driving services to the PCW, in accordance with the requirements specified in the Terms of Reference (TOR), hereto attached, and made an integral part of this document as <b>Annex “A”</b>.
	</p>

	<!-- <div style="page-break-after: always;"></div> -->

	<p class="text-center font-weight-bold mb-0 pb-0">Section 2</p>
	<p class="text-center font-weight-bold">EFFECTIVITY PERIOD OF THE CONTRACT</p>

	<p style="text-indent: 4.9em;" class="text-justify mb-6">
		This Contract of Service Agreement shall commence on <b>{!! (@$applicant) ? date('F d Y',strtotime($applicant->job->created_at)) : '' !!}</b> and shall expire on <b>{!! (@$applicant) ? date('F d Y',strtotime($applicant->job->deadline_date)) : '' !!}</b>, unless sooner terminated based on the provisions of Section 6 of this contract.  Renewal of this Contract of Service Agreement shall only be made upon the submission of the performance assessment and written recommendation from the {!! (@$applicant) ? @$applicant->job->plantilla_item->position->Name : '' !!}’s immediate supervisor, endorsed by the Chief Administrative Officer, explicitly stating therein the period for renewal, and upon filing of the latest Income Tax Return and payment of the tax due.
	</p>

	<p class="text-center font-weight-bold mb-0 pb-0">Section 3</p>
	<p class="text-center font-weight-bold">OBLIGATIONS OF PARTIES</p>

	<p style="text-indent: 4.9em;" class="text-justify">
		<b>3.1</b> The {!! (@$applicant) ? @$applicant->job->plantilla_item->position->Name : '' !!} shall perform the following tasks in accordance with the Terms of Reference:
	</p>

	<div class="row">
		<div class="col-2"></div>
		<div class="col-10">
			<p class="text-justify">
				<b>3.1.1</b> Focal for Student Training Program (STP);
			</p>

			<p class="text-justify">
				<b>3.1.2</b> Coordinate Student Training Program applications to different divisions;
			</p>

			<p class="text-justify">
				<b>3.1.3</b> Screen applicants in accordance with the need of the end-user and the required skills and competencies required to be gained by the Student Training Program applicant/s;
			</p>

			<p class="text-justify">
				<b>3.1.4</b> Coordinate with different universities for partnership on Student Training Programs;
			</p>

			<p class="text-justify">
				<b>3.1.5</b> Updates and monitors database relative to STP;
			</p>

			<p class="text-justify">
				<b>3.1.6</b> Ensures and monitors collection of Evaluation Form from students;
			</p>

			<p class="text-justify">
				<b>3.1.7</b> Administer Employee Engagement Survey (EES)  and Job Stress Survey (JSS);
			</p>

			<p class="text-justify">
				<b>3.1.8</b> Prepare reports on the result of EES and JSS;
			</p>

			<p class="text-justify">
				<b>3.1.9</b> Develop and communicate positive employee relations strategies and retention initiatives that foster and promote a culture of excellence;
			</p>

			<p class="text-justify">
				<b>3.1.10</b> Prepare employee engagement/relations activities and assist such as but not limited to Annual Teambuilding, Annual Health and Wellness Plan including Sports Festival, Family Day, Annual Physical Examiniation, etc. and assist the Head on activity implementation;
			</p>

			<p class="text-justify">
				<b>3.1.11</b> Evaluate the effectiveness of employee relations/engagement programs;
			</p>

			<p class="text-justify">
				<b>3.1.12</b> Develop newsletters regarding employee enagement activities; 
			</p>

			<p class="text-justify">
				<b>3.1.13</b> Ensures the validity and reliability of the list of employees for rewards and recognition; 
			</p>

			<p class="text-justify">
				<b>3.1.14</b> Prepares minutes of meeting and resolution during awards committee meeting;
			</p>

			<p class="text-justify">
				<b>3.1.15</b> Assist on logistical preparation for awards and recognition and assist the Head during Anniversary Celebration;
			</p>

			<p class="text-justify">
				<b>3.1.16</b> Ensure the submission of Employee Exit Survey of all resigning and retiring employees;
			</p>

			<p class="text-justify">
				<b>3.1.17</b> Update the database of Employee Exit Survey and provide statistical and written analysis/report on the reason/s for resigning;
			</p>

			<p class="text-justify">
				<b>3.1.18</b> Assist the Section Head on the completion of PRIME-HRM and ISO requirements;
			</p>

			<p class="text-justify">
				<b>3.1.19</b> Perform other related tasks assigned by the Immediate Supervisor.
			</p>
		</div>
	</div>

	<p style="text-indent: 4.9em;" class="text-justify">
		<b>3.2</b> The PCW shall:
	</p>

	<div class="row mb-6">
		<div class="col-2"></div>
		<div class="col-10">
			<p class="text-justify">
				<b>3.2.1</b> Pay the {!! (@$applicant) ? @$applicant->job->plantilla_item->position->Name : '' !!} a monthly salary, in accordance with the computation provided in Section 4.1 of this Contract of Service Agreement;
			</p>

			<p class="text-justify">
				<b>3.2.2</b> Make available the relevant information, supplies and materials that may be necessary to effectively carry out the {!! (@$applicant) ? @$applicant->job->plantilla_item->position->Name : '' !!}’s tasks; and
			</p>

			<p class="text-justify">
				<b>3.2.3</b> Provide directions and guidelines on the delivery of administrative support services and other outputs.
			</p>
		</div>
	</div>

	<p class="text-center font-weight-bold mb-0 pb-0">Section 4</p>
	<p class="text-center font-weight-bold">SALARY AND TERMS OF PAYMENT</p>

	<p style="text-indent: 4.9em;" class="text-justify">
		<b>4.1</b> The {!! (@$applicant) ? @$applicant->job->plantilla_item->position->Name : '' !!} shall be paid a daily salary rate of <b>{!! strtoupper($whole) !!} PESOS {!! ($decimal) ? 'AND '.strtoupper($decimal).' CENTAVOS' : '' !!}  (Php {!! (@$applicant) ? number_format($applicant->job->professional_fees) : '' !!})</b> and a 20% premium, computed in accordance with the following formula, subject to the applicable government mandatory withholding taxes:
	</p>

	<p style="text-indent: 4.9em;" class="font-italic">Salary = Daily salary rate x 1.20 x number of days actually rendered during the computation period</p>

	<p style="text-indent: 4.9em;" class="text-justify">
		<b>4.2</b> The daily salary rate shall be based on the equivalent daily salary of the comparable permanent government position (fourth tranche), as indicated in Executive Order No. 201, s. 2016 . 
	</p>

	<p style="text-indent: 4.9em;" class="text-justify">
		<b>4.3</b> The salary of the {!! (@$applicant) ? @$applicant->job->plantilla_item->position->Name : '' !!} shall be processed twice a month (every 15th and 30th), upon completion and submission of the Accomplishment Report (with applicable supporting documents), copy of the Daily Time Record duly signed by her and her immediate Supervisor, and Certificate of <br>
		________________________ 
	</p>

	<p class="offset-1" style="font-size: 10pt;">Modifying the Salary Schedule for Civilian Government Personnel and Authorizing the Grant of Additional Benefits for Both Civilian and Military and Uniformed Personnel </p>

	<p class="text-justify">
		Outputs and Services Rendered, duly reviewed and endorsed by the immediate supervisor and approved by the Chief Administrative Officer, in accordance with the provisions of Annex A (Terms of Reference). 
	</p>

	<p style="text-indent: 4.9em;" class="text-justify">
		<b>4.4</b> The {!! (@$applicant) ? @$applicant->job->plantilla_item->position->Name : '' !!} shall be paid for work rendered in excess of the regular work hours as stated in Section 5.1, provided that she is authorized to do so and she complies with the requirements of Section 5.2. The payment for work rendered in excess of the regular work hours shall be computed based on the fraction of the daily salary rate. Similarly, for the {!! (@$applicant) ? @$applicant->job->plantilla_item->position->Name : '' !!}’s inability to complete the prescribed eight-hour work day, a salary deduction, computed based on the fraction of the daily salary rate, shall likewise be imposed.
	</p>

	<p style="text-indent: 4.9em;" class="text-justify">
		<b>4.5</b> The {!! (@$applicant) ? @$applicant->job->plantilla_item->position->Name : '' !!} shall not be paid during the Regular and Special (Non-working) Holidays, as enumerated under Section 1 of Proclamation No. 555  (hereinafter referred to as “declared holidays”), except when actual work is rendered during the said holidays/ special non-working days. In which case, the day actually rendered shall be integrated in the computation of salary as provided in Section 4.1 hereof.  
	</p>

	<p style="text-indent: 4.9em;" class="text-justify mb-6">
		<b>4.6</b> In case of holidays/ special non-working days not included in the enumeration of Section 1, Proclamation No. 555 or when there are suspensions of work due to calamities and other unforeseen events (hereinafter referred to as “non-declared holidays”), the same shall be paid and credited as a day actually rendered and shall be integrated in the computation of the salary, as provided in Section 4.1 hereof. 
	</p>

	<p class="text-center font-weight-bold mb-0 pb-0">Section 5</p>
	<p class="text-center font-weight-bold">WORK ARRANGEMENTS</p>

	<p style="text-indent: 4.9em;" class="text-justify mb-6">
		<b>5.1</b> The {!! (@$applicant) ? @$applicant->job->plantilla_item->position->Name : '' !!} is required to report to PCW, five (5) days in a week, eight (8) hours per day, following the flexi-time schedule of PCW, from Mondays to Fridays, except non-working holidays, exclusive of the daily one-hour lunch break from 12:00nn to 01:00pm.
	</p>

	<p style="text-indent: 4.9em;" class="text-justify mb-6">
		<b>5.2</b> In the event that the {!! (@$applicant) ? @$applicant->job->plantilla_item->position->Name : '' !!} is required to perform tasks on a weekend or extend beyond the prescribed eight (8) hours of work, he must secure a duly signed authority to work from his immediate supervisor and submit an Accomplishment Report. Similarly, if he is required to attend to official business or PCW activities applicable, must be first secured, and upon return, he must submit a Certificate of Appearance or other required documents.
	</p>

	<p style="text-indent: 4.9em;" class="text-justify mb-6">
		<b>5.3</b> This Contract of Service Agreement shall not create an employer-employee relationship between PCW and the {!! (@$applicant) ? @$applicant->job->plantilla_item->position->Name : '' !!} and the services to be rendered by the latter shall not be credited as government service. Furthermore, the {!! (@$applicant) ? @$applicant->job->plantilla_item->position->Name : '' !!} shall not be entitled to the benefits enjoyed by the regular personnel of the PCW.
	</p>

	<p class="text-center font-weight-bold mb-0 pb-0">Section 6</p>
	<p class="text-center font-weight-bold">TERMINATION OF CONTRACT</p>

	<p style="text-indent: 4.9em;" class="text-justify mb-6">
		<b>6.1</b> For the duration of this Contract of Service Agreement, PCW, through the Administrative and Finance Division, shall have the right to conduct periodic evaluation of the {!! (@$applicant) ? @$applicant->job->plantilla_item->position->Name : '' !!} performance, services delivered and outputs submitted and to terminate this contract due to breach of contract, unsatisfactory performance and/or other reasons detrimental to the interest of PCW.
	________________________ 
	</p>

	<p class="offset-1" style="font-size: 10pt;">  Declaring the Regular Holidays and Special Non- working days for 2019 </p>

	<p style="text-indent: 4.9em;" class="text-justify mb-6">
		<b>6.2</b> This Contract of Service Agreement shall be automatically terminated at the date set forth under Section 2, hereof.
	</p>

	<p style="text-indent: 4.9em;" class="text-justify mb-6">
		<b>6.3</b> Over the same period, the {!! (@$applicant) ? @$applicant->job->plantilla_item->position->Name : '' !!} may initiate the termination of this Contract of Service Agreement provided that:
	</p>

	<div class="row mb-6">
		<div class="col-2"></div>
		<div class="col-10">
			<p class="text-justify">
				<b>6.3.1</b> A written notice for the termination of contract is submitted, stating therein the reasons for the termination of the contract, at least thirty (30) calendar days prior to the proposed date of effectivity for contract termination; and
			</p>

			<p class="text-justify">
				<b>6.3.2.</b> The written notice has been received, accepted and approved in writing by PCW.
			</p>
		</div>
	</div>

	<p class="text-center font-weight-bold mb-0 pb-0">Section 7</p>
	<p class="text-center font-weight-bold">AUTHORITY TO ENTER INTO AGREEMENT</p>

	<p style="text-indent: 4.9em;" class="text-justify mb-6">
		The {!! (@$applicant) ? @$applicant->job->plantilla_item->position->Name : '' !!} shall have no right and authority to enter into agreements on behalf of the PCW in any manner and pledge credit on its behalf in any way. 
	</p>

	<p class="text-center font-weight-bold mb-0 pb-0">Section 8</p>
	<p class="text-center font-weight-bold">INSURANCE GUARANTEE</p>

	<p style="text-indent: 4.9em;" class="text-justify mb-6">
		The {!! (@$applicant) ? @$applicant->job->plantilla_item->position->Name : '' !!} assures the PCW that he is responsible for his personal life/accident insurance and frees the PCW of any liability or obligation in case of untoward incidents that he may experience in the course of performing his services.
	</p>	

	<p class="text-center font-weight-bold mb-0 pb-0">Section 9</p>
	<p class="text-center font-weight-bold">DRUG USE POLICY</p>

	<p style="text-indent: 4.9em;" class="text-justify mb-6">
		The {!! (@$applicant) ? @$applicant->job->plantilla_item->position->Name : '' !!} must be negative of drug use and must not engage or be caught peddling drugs. Drug testing is mandatory and if the {!! (@$applicant) ? @$applicant->job->plantilla_item->position->Name : '' !!} has been found positive for drug use and caught peddling drugs, the latter shall not be entitled to renewal of this Contract of Service Agreement. The {!! (@$applicant) ? @$applicant->job->plantilla_item->position->Name : '' !!} shall perform effective and efficient service free from the hazards of drug use in the workplace.
	</p>	

	<p class="text-center font-weight-bold mb-0 pb-0">Section 10</p>
	<p class="text-center font-weight-bold">CONFIDENTIALITY</p>

	<p style="text-indent: 4.9em;" class="text-justify mb-6">
		The {!! (@$applicant) ? @$applicant->job->plantilla_item->position->Name : '' !!} agrees that he shall not disclose or divulge to any person or entity, even after the termination of this Contract of Service Agreement, any confidential information, including but not limited to sensitive personal information involving PCW employees, clients, and stakeholders. Breach of this confidentiality agreement shall render the {!! (@$applicant) ? @$applicant->job->plantilla_item->position->Name : '' !!} liable for criminal and civil liabilities under the applicable laws and shall be a ground for the pre-termination of this contract.
	</p>

	<div style="page-break-after: always;"></div>

	<p style="text-indent: 4.9em;" class="text-justify mb-6">
		<b>IN WITNESS WHEREOF</b>, the parties have hereunto set their hands on this 10th day of June 2019 in the City of Manila, Philippines.
	</p>

	<div class="row font-weight-bold mb-8">
		<div class="col-5 text-center">
			PHILIPPINE COMMISION ON WOMEN
		</div>
		<div class="col-2"></div>
		<div class="col-5 text-center">
			HUMAN RESOURCE MANAGEMENT  OFFICER 
		</div>
	</div>

	<div class="row mb-8">
		<div class="col-5 border-top border-dark text-center">
			<span class="font-weight-bold">EMMELINE L. VERZOSA</span> <br>
			Executive Director III
		</div>
		<div class="col-2"></div>
		<div class="col-5 border-top border-dark text-center font-weight-bold">
			{!! (@$applicant) ? strtoupper($applicant->getFullName()) : '' !!}
		</div>
	</div>

	<div class="row mb-8">
		<div class="col-12 text-center font-weight-bold">
			SIGNED IN THE PRESENCE OF:
		</div>
	</div>

	<div class="row mb-8">
		<div class="col-5 border-top border-dark text-center">
			<span class="font-weight-bold">	CECILE B. GUTIERREZ</span> <br>
			Deputy Director for Management Services	
		</div>
		<div class="col-2"></div>
		<div class="col-5 border-top border-dark text-center">
			<span class="font-weight-bold">LOLITA E. ETRATA</span> <br>
			Chief Administrative Officer
		</div>
	</div>

	<div class="row mb-8">
		<div class="col-12 font-weight-bold text-center">
			CERTIFICATE OF AVAILABILITY OF FUNDS
		</div>
	</div>


	<div class="row">
		<div class="col-4"></div>
		<div class="col-4 border-dark border-top text-center">
			<span class="font-weight-bold">CHARY GRACE C. CATUBAG</span> <br>
			Accountant III
		</div>
		<div class="col-4"></div>
	</div>

	<div style="page-break-after: always;"></div>

	<div class="row mb-6">
		<div class="col-12 font-weight-bold text-center">
			ACKNOWLEDGMENT
		</div>
	</div>

	<p class="font-weight-bold">
		REPUBLIC OF THE PHILIPPINES	) <br>
		CITY OF MANILA		       	) S.S.
	</p>

	<p style="text-indent: 4.9em;" class="text-justify mb-6">
		<b>BEFORE ME</b>, a notary public for and in the City of Manila personally appeared the following:
	</p>

	<table class="table table-bordered">
		<thead class="text-center font-weight-bold">
			<tr>
				<td>NAME</td>
				<td>ID TYPE/ ID NUMBER</td>
				<td>DATE/PLACE ISSUED</td>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td class="font-weight-bold">EMMELINE L. VERZOSA</td>
				<td>PASSPORT NO. S0011288A</td>
				<td>
					Date of Issuance: August 18, 2017 <br>
					Place of Issuance: DFA, Manila <br>
					Expiration Date: August 17, 2022
				</td>
			</tr>
			<tr>
				<td class="font-weight-bold">KEN HAROLD M. TURALBA</td>
				<td>TIN NO. 327-133-802</td>
				<td></td>
			</tr>
		</tbody>
	</table>

	<p class="text-justify">
		who are both known to me to be the same persons who executed the foregoing instrument and acknowledged before me that the same is their true and voluntary act and deed and that of the entities they represent at this instance.
	</p>

	<p style="text-indent: 4.9em;" class="text-justify mb-6">
		This instrument consists of six (6) pages including this page wherein this acknowledgement is written, and is signed by the parties and their instrumental witnesses on each and every page thereof.
	</p>

	<p style="text-indent: 4.9em;" class="text-justify mb-6">
		<b>WITNESS MY HAND AND SEAL</b>, this 10th day of June 2019 in the City of Manila, Philippines.
	</p>

	Doc No.__________; <br>
	Page No._________; <br>
	Book No._________; <br>
	Series of 2019






</div>

 
@endsection

@section('scripts')
<script type="text/javascript">
  $(document).ready(function() {
    $('#evaluation-report').click(function() {
      window.print();
    });
  });
</script>
@endsection