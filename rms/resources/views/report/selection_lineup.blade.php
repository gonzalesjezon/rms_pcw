@extends('layouts.print')

@section('css')
@endsection

@section('content')
<div class="reports" style="width: 960px;margin: auto;font-size: 12px;font-family: Arial, Helvetica, sans-serif;">
	<div class="row mb-2">
		NAME OF APPLICANTS AND THEIR QUALIFICATIONS
	</div>

	<div class="row mb-2">
		Position to be Filled: {{$jobs->psipop->position_title}}
	</div>

	<div class="row mb-1">
		MINIMUM QUALIFICATION
	</div>

	<div class="row mb-1">
		<div class="col-12">
			@if($jobs->education)
			- {!! $jobs->education !!}
			@endif
		</div>
	</div>

	<div class="row mb-1">
		<div class="col-12">
			@if($jobs->training)
			- {!! $jobs->training !!}
			@endif
		</div>
	</div>

	<div class="row mb-1">
		<div class="col-12">
			@if($jobs->experience)
			- {!! $jobs->experience !!}
			@endif
		</div>
	</div>

	<div class="row mb-1">
		<div class="col-12">
			@if($jobs->eligibility)
			- {!! $jobs->eligibility !!}
			@endif
		</div>
	</div>

	<div class="row mb-2">
		<div class="col-6">
			@if($jobs->compentency_1)
			- <b>Core Compentencies</b> <br>
			{!! $jobs->compentency_1 !!}
			@endif
		</div>
		<div class="col-6">
			@if($jobs->compentency_2)
			- <b>Functional Compentencies</b> <br>
			{!! $jobs->compentency_2 !!}
			@endif
		</div>
	</div>

	<div class="row mb-2">
		<div class="col-6">
			@if($jobs->compentency_3)
			- <b>General FUnctions</b> <br>
			{!! $jobs->compentency_3 !!}
			@endif
		</div>

		<div class="col-6">
			@if($jobs->compentency_4)
			- <b>Specific Duties & Responsibilities</b> <br>
			{!! $jobs->compentency_4 !!}
			@endif
		</div>
	</div>

	<div class="row mb-2">
		<div class="col-6">
			@if($jobs->compentency_5)
			- <b>Secondary FUnctions</b> <br>
			{!! $jobs->compentency_5 !!}
			@endif
		</div>
	</div>

	<div class="row mb-2">
		<div class="col-12">
			<table class="table table-striped table-fw-widget table-bordered">
				<thead>
					<tr class="text-center">
						<th>No.</th>
						<th>Name of Applicant</th>
						<th>GWA</th>
						<th>Education</th>
						<th>Work Experience</th>
						<th>Training</th>
						<th>Eligibility</th>
						<th>Remarks</th>
					</tr>
				</thead>
				<tbody>
					@foreach($recommend as $key => $value)
					<tr>
						<td class="text-center" style="vertical-align: top;">{{ $value->id }}</td>
						<td style="vertical-align: top;">{{ $value->applicant->getFullName()}}</td>
						<td style="vertical-align: top;">{{ $value->gwa }}</td>
						<td>
							<ul>
								@foreach($value->applicant->education as $key => $educ)
								<li>{{ $educ->course }}</li>
								@endforeach
							</ul>
						</td>
						<td style="vertical-align: top;">
							<ul>
								@foreach($value->applicant->workexperience as $key => $el)
								<li>{{ $el->position_title }}</li>
								@endforeach
							</ul>
						</td>
						<td style="vertical-align: top;">
							<ul>
								@foreach($value->applicant->training as $key => $el)
								<li>{{ $el->title_learning_programs }}</li>
								@endforeach
							</ul>
						</td>
						<td style="vertical-align: top;"><ul>
								@foreach($value->applicant->eligibility as $key => $el)
								<li>{{ $el->name }}</li>
								@endforeach
							</ul></td>
						<td></td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>

</div>


 <div class="form-group row text-right d-print-none">
    <div class="col col-sm-10 col-lg-9 offset-sm-1 offset-lg-3">
      {{ Form::button('Print', ['id' => 'evaluation-report', 'class'=>'btn btn-primary btn-space', 'type'=>'submit']) }}
      {{ Form::reset('Cancel', ['class'=>'btn btn-space btn-danger']) }}
    </div>
  </div>
@endsection

@section('scripts')
<script type="text/javascript">
  $(document).ready(function() {
    $('#evaluation-report').click(function() {
      window.print();
    });
  });
</script>
@endsection