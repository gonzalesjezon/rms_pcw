@extends('layouts.print')

@section('css')
<style type="text/css">
  @media print{
  	@page{
  		size:landscape !important;
  	}
    .table>thead>tr>th{
      vertical-align: middle !important;
      padding: 2px !important;
      font-size: 8pt !important;
    }

    .table>thead>tr>th,
    .table>tbody>tr>td{
      border: 1px solid #333 !important;
    }
  }
  .table>thead>tr>th{
    vertical-align: middle !important;
    padding: 2px !important;
    font-size: 8pt !important;
  }

  .table>thead>tr>th,
  .table>tbody>tr>td{
    border: 1px solid #333 !important;
  }
</style>
@endsection

@section('content')

<div class="form-group row text-right d-print-none">
  <div class="col col-sm-10 col-lg-9 offset-sm-1 offset-lg-3">
    {{ Form::button('Print', ['id' => 'evaluation-report', 'class'=>'btn btn-primary btn-space', 'type'=>'submit']) }}
    {{ Form::reset('Cancel', ['class'=>'btn btn-space btn-danger']) }}
  </div>
</div>

<div id="reports" style="max-width: 100%;max-height: 100%;margin: auto;font-size:8pt;font-family: Arial, Helvetica, sans-serif;">

  <table class="table table-bordered" >
    <thead class="text-center">
      @include('report.includes._header')
      <tr>
      	<th colspan="6"><i>PERSONAL INFORMATION</i></th>
      	<th colspan="13"><i>RSP Activities</i></th>
      	<th rowspan="3">Remarks</th>
      </tr>
      <tr>
      	<th rowspan="2">Position Title</th>
      	<th rowspan="2">SG</th>
      	<th rowspan="2">Status of Employment</th>
      	<th rowspan="2">Last Incumbent</th>
      	<th rowspan="2">Mode of Separation</th>
      	<th rowspan="2">Date Vacated</th>
      	<th>Date Published in CSC</th>
      	<th>Date of Posting in Job Sourcing Platforms (Facebook, Jobstreet etc.)</th>
      	<th>Deadline of Posting</th>
      	<th>Number of Received Applications</th>
      	<th>Number of Shortlisted Applicants</th>
      	<th>Date of Sourcing Applicants</th>
      	<th>Date of Exam</th>
      	<th>Date of Conduct of Background Check</th>
      	<th>Date of HRMPSB Screening</th>
      	<th>Date of Approval of Application</th>
      	<th>Date of Signing of Appointment</th>
      	<th>Date of Assumption</th>
      	<th>Over-All Time To Fill (Turn-Around Time)</th>
      </tr>
      <tr>
      	<th colspan="13">&nbsp;</th>
      </tr>
    </thead>
    <tbody>
      @foreach($jobs as $job)
      
      <tr>
        <td>{!! @$job->plantilla_item->position->Name !!}</td>
        <td>{!! (@$job->status == 'plantilla') ? $job->plantilla_item->salary_grade->Name : 'NA'  !!}</td>
        <td nowrap>{!! @$job->plantilla_item->employee_status->Name !!}</td>
        <td nowrap>{!! @$job->plantilla_item->incumbent_name !!}</td>
        <td nowrap>{!! @$job->plantilla_item->mode_of_separation !!}</td>
        <td nowrap>{!! @$job->plantilla_item->date_vacated !!}</td>
        <td nowrap>{!! ($job->approved_date) ? date('Y-m-d', strtotime(@$job->approved_date)) : '' !!}</td>
        <td></td>
        <td nowrap>{!! date('Y-m-d', strtotime(@$job->deadline_date)) !!}</td>
        <td class="text-center">{!! count(@$job->applicants) !!}</td>
        <td class="text-center">{!! count(@$job->qualified_applicant) !!}</td>
        <td nowrap class="text-center">{!! @$job->dateFirstQualified->date_qualified !!}</td>
        <td nowrap class="text-center">{!! @$job->getDate->firstExam->exam_date !!}</td>
        <td nowrap class="text-center">{!! @$job->getDate->firstBChecking->created_at !!}</td>
        <td nowrap class="text-center">{!! @$job->getDate->firstInterview->interview_date !!}</td>
        <td nowrap class="text-center">{!! @$job->getDate->firstInterviewApproved->date_approved !!}</td>
        <td nowrap class="text-center">{!! @$job->getDate->dateAppointmentSigned->date_sign !!}</td>
        <td nowrap class="text-center">{!! @$job->getDate->dateAssumption->assumption_date !!}</td>
        <td nowrap class="text-center">
          <?php
            $assumptionDate = (@$job->getDate->dateAssumption) ? new DateTime($job->getDate->dateAssumption->assumption_date) : new DateTime(date('Y-m-d',time()));
            $publishDate    = ($job->publish_date) ? new DateTime(@$job->publish_date) : new DateTime(date('Y-m-d',time()));

            $interval = $publishDate->diff(@$assumptionDate);
            $year     = $interval->format('%y');
            $month    = $interval->format('%m');
            $days     = $interval->format('%d');
          ?>
          {!! ($month != 0) ? $month.' Month/s & '.$days.' day/s' : '' !!}
        </td>
        <td></td>
      </tr>
      @endforeach
    </tbody>
  </table>

</div>

@endsection

@section('scripts')
<script type="text/javascript">
  $(document).ready(function() {
    $('#evaluation-report').click(function() {
      window.print();
    });
  });
</script>
@endsection