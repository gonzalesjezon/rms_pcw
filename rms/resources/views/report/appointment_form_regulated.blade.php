@extends('layouts.print')

@section('css')
@endsection

@section('content')

@if($applicant)
<div id="reports" style="width: 960px;margin: auto; font-size: 12px;font-family: Arial, Helvetica, sans-serif;">
    <div class="report1" style="border: 10px solid #b1acac;padding: 20px;">
      <div class="row mb-1">
        <div class="col-3">CS Form No. 33-A <br> Revised 2018</div>
        <div class="col-6"></div>
        <div class="col-3 text-right">(Stamp of Date of Receipt)</div>
      </div>

      <div class="row mb-6">
        <div class="col-12 text-center">
            <div style="font-size: 16px;" class="font-weight-bold">Republic of the Philippines</div>
            <div style="font-size: 14px;">Philippine Commssion on Women</div>
            <div>1145 J.P Laurel St. San Miguel Manila</div>
        </div>
      </div>

      <div class="row mb-4">
        <div class="col-12">
            <p class="font-weight-bold">Mr./Mrs./ Ms.: {!! $applicant->applicant->getFullName() !!}  </p>
        </div>
      </div>

      <div class="row">
          <div class="col-3 text-right pr-0">You are hereby appointed as</div>
          <div class="col-4 border border-dark border-top-0 border-right-0 border-left-0 text-center pr-0 pl-0">{!! $applicant->applicant->job->psipop->position_title !!}</div>
          <div class="col-1 text-center pr-0 pl-0">(SG/JG/PG)</div>
          <div class="col-4 border border-dark border-top-0 border-right-0 border-left-0 text-center">{!! $applicant->applicant->job->psipop->salary_grade !!}</div>
      </div>

      <div class="row mb-2">
          <div class="col-3"></div>
          <div class="col-4 text-center">(Position Titlte)</div>
      </div>

      <div class="row">
          <div class="col-1 text-left pl-3 pr-0">under</div>
          <div class="col-6 border border-dark border-top-0 border-right-0 border-left-0 text-center pl-0 pr-0">{!! config('params.employee_status.'.$applicant->employee_status) !!}</div>
          <div class="col-1 text-center pl-0 pr-0">status at the</div>
          <div class="col-4 border border-dark border-top-0 border-right-0 border-left-0 text-center">{!! $applicant->applicant->job->psipop->division->name !!}</div>
      </div>

      <div class="row mb-2">
          <div class="col-8 text-center">(Permanent,Temporary,etc.)</div>
          <div class="col-4 text-center">(Office/Department/Unit)</div>
      </div>

      <div class="row mb-6">
        <div class="col-2 text-left pr-0">
            with compensation rate of
        </div>
        <div class="col-4  border border-dark border-top-0 border-right-0 border-left-0 text-center">{{ $number_in_word }}</div>
        <div class="pl-0 pr-0">(P</div>
        <div class="col-3 border border-dark border-top-0 border-right-0 border-left-0 text-center pl-0 pr-0">{{ number_format($applicant->applicant->job->monthly_basic_salary,2) }}</div>
        <div class="col-2 pl-0 pr-0 text-left">) pesos per month.</div>
      </div>


      <div class="row">
          <div class="col-3 text-right pr-0">The nature of appointment is</div>
          <div class="col-4 border border-dark border-top-0 border-right-0 border-left-0 text-center pr-0 pl-0">{!! config('params.nature_of_appointment.'.$applicant->nature_of_appointment) !!}</div>
          <div class="col-1 text-center pr-0 pl-0">vice</div>
          <div class="col-4 border border-dark border-top-0 border-right-0 border-left-0 text-center pl-0 pr-0"></div>
      </div>

      <div class="row mb-2">
          <div class="col-10 text-center pr-0 pl-0">(Original, Promotion, etc.)    </div>
      </div>

      <div class="row mb-1">
        <div class="col-1 text-left pr-0">
            who
        </div>
        <div class="col-3 border border-dark border-top-0 border-right-0 border-left-0 text-center pr-0 pl-0"></div>
        <div class="pr-0 pl-0 text-center">with Plantilla Item No.</div>
        <div class="col-4 border border-dark border-top-0 border-right-0 border-left-0 text-center pr-0 pl-0">{!! $applicant->applicant->job->psipop->item_number !!}</div>
      </div>

      <div class="row mb-2">
          <div class="col-5 text-center pr-0 pl-0">(Transferred, Retired, etc.)    </div>
      </div>

      <div class="row mb-4">
          <div class="col-1 pr-0">Page No. </div>
          <div class="col-2 border border-dark border-top-0 border-right-0 border-left-0"></div>
      </div>

      <div class="row mb-8">
          <div class="col-7 text-right pr-0">This appointment shall take effect on the date of signing by the appointing officer/authority.</div>
      </div>

      <div style="height: 10em;"></div>

      <div class="row mb-6">
        <div class="col-7"></div>
        <div class="col-5">Very truly yours,</div>
      </div>


      <div class="row">
        <div class="col-7"></div>
        <div class="col-3 text-center">{!! $applicant->appointing_officer !!}</div>
      </div>

      <div class="row mb-6">
        <div class="col-7"></div>
        <div class="col-3 border-top text-center"> Appointing Officer/Authority</div>
      </div>


      <div class="row">
        <div class="col-7"></div>
        <div class="col-3 text-center">{!! $applicant->date_sign !!}</div>
      </div>

      <div class="row mb-1">
        <div class="col-7"></div>
        <div class="col-3 border-top text-center"> Date of Signing</div>
      </div>

      <div style="height: 30em;"></div>

  </div>

  <div class="report1" style="border: 10px solid #b1acac;padding: 10px;">
        <div class="row mb-8">
            <div class="col-12 font-weight-bold">CSC ACTION:</div>
        </div>

        <div class="row mb-1">
            <div class="col-1"></div>
            <div class="col-3 border-top"></div>
        </div>
        <div class="row mb-8">
            <div class="col-1"></div>
            <div class="col-3 text-center font-weight-bold">Authorize Official</div>
        </div>

        <div class="row mb-1">
            <div class="col-1"></div>
            <div class="col-3 border-top"></div>
        </div>
        <div class="row mb-5">
            <div class="col-1"></div>
            <div class="col-3 text-center font-weight-bold">Date</div>
        </div>
        <div class="row mb-2">
            <div class="col-12 text-right"><i>(Stamp of Date of Release)</i></div>
        </div>
  </div>
    <div style="page-break-before: always;"></div>

    <br>

    <div class="report1" style="border: 10px solid #b1acac;padding: 10px;">
        <div class="row mb-2">
            <div class="col-12 font-weight-bold text-center"><h4>Certification</h4></div>
        </div>

        <div class="row mb-4">
            <div class="col-12">
                <p style="text-indent: 50px;" class="text-justify">This is to certify that all requirements and supporting papers pursuant to CSC MC No. ________have been complied with, reviewed and found to be in order.</p>
                <p style="text-indent: 50px;" class="text-justify">
                    The position was published at <b>{!! config('params.publication.'.$applicant->applicant->publication) !!}</b> from ___________ to _________,
                    20_____ and posted in _____________________________________ from___________ to__________, 20_____ in consonance with RA No. 7041. The assessment by the Human Resource Merit Promotion and Selection Board (HRMPSB) started on ______________, 20_____.
                </p>
            </div>
        </div>

        <div class="row">
            <div class="col-7"></div>
            <div class="col-3 text-center">{!! $applicant->hrmo !!}</div>
          </div>

        <div class="row mb-6">
            <div class="col-7"></div>
            <div class="col-3 border-top text-center"> Highest Ranking HRMO</div>
          </div>
    </div>

    <div class="report1" style="border: 10px solid #b1acac;padding: 10px;">
        <div class="row mb-2">
            <div class="col-12 font-weight-bold text-center"><h4>Certification</h4></div>
        </div>

        <div class="row mb-4">
            <div class="col-12">
                <p style="text-indent: 50px;" class="text-justify">This      is     to       certify      that       the      appointee      has     been     screened     and     found     qualified by the majority of the HRMPSB during the deliberation held on {!! date('F d, Y',strtotime($applicant->chairperson_date_sign)) !!}. </p>
            </div>
        </div>


        <div class="row">
            <div class="col-7"></div>
            <div class="col-3 text-center"> {!! $applicant->chairperson !!}</div>
          </div>

        <div class="row mb-6">
            <div class="col-7"></div>
            <div class="col-3 border-top text-center"> Chairperson, HRMPSB</div>
          </div>
    </div>

    <br>
    <div class="report1" style="border: 10px solid #b1acac;">
        <div class="row mb-2 ml-0 mr-0" style="background-color: #b1acac;">
            <div class="col-12 font-weight-bold text-center"><h4>CSC Notation</h4></div>
        </div>

        <div class="row mb-6">
            <div class="col-12 border-top"></div>
        </div>

        <div class="row mb-6">
            <div class="col-12 border-top"></div>
        </div>

        <div class="row mb-6">
            <div class="col-12 border-top"></div>
        </div>

        <div class="row mb-6">
            <div class="col-12 border-top"></div>
        </div>

        <div class="row mb-6">
            <div class="col-12 border-top"></div>
        </div>

    </div>
    <div class="report1" style="border: 10px solid #b1acac;padding: 10px;">
        <div class="row mb-6"></div>
        <div class="row mb-4">
            <div class="col-12">
                <p style="text-indent: 50px;" class="text-justify">ANY ERASURE OR ALTERATION ON THE CSC ACTION SHALL NULLIFY OR INVALIDATE THIS APPOINTMENT EXCEPT IF THE ALTERATION WAS AUTHORIZED BY THE COMMISSION. </p>
            </div>
        </div>
    </div>
    <br>
    <div class="report1" style="border: 10px solid #b1acac;padding: 10px;">

        <div class="row mb-1">
            <div class="col-6">
                <div class="mb-6"></div>
                <ul style="list-style: none;">
                    <li>Original Copy   -   for the Appointee</li>
                    <li>Original Copy   -   for the Civil Service Commission</li>
                    <li>Original Copy   -   for the Agency</li>
                </ul>
            </div>
            <div class="col-6 text-center">
                <p class="font-weight-bold">Acknowledgement</p>
                <p>Received original/photocopy of appointment on_____________</p>
            </div>
        </div>

        <div class="row mb-6">
            <div class="col-7"></div>
            <div class="col-3 border-top text-center"> Highest Ranking HRMO</div>
          </div>
    </div>

</div>
@endif
 <div class="form-group row text-right d-print-none">
    <div class="col col-10 col-lg-9 offset-1 offset-lg-3">
      {{ Form::button('Print', ['id' => 'evaluation-report', 'class'=>'btn btn-primary btn-space', 'type'=>'submit']) }}
      {{ Form::reset('Cancel', ['class'=>'btn btn-space btn-danger']) }}
    </div>
  </div>
@endsection

@section('scripts')
<script type="text/javascript">
  $(document).ready(function() {
    $('#evaluation-report').click(function() {
      window.print();
    });
  });
</script>
@endsection