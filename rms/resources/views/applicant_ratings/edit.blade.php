@extends('layouts.app')

@section('content')
    <div class="page-head">
        <h2 class="page-head-title">Applicant Rating</h2>
    </div>

    <!-- Job Form -->
    <div class="row">
        <div class="col-md-12">
            <div class="card card-border-color card-border-color-primary">
                <div class="card-header card-header-divider">
                    <span class="card-subtitle">You can update the applicant rating in the form below.</span>
                </div>
                <div class="card-body">
                    @include('applicant_ratings._form', [
                        'action' => ['ApplicantRatingController@update', $rating->id],
                        'method' => 'PATCH',
                        'applicants' => $applicants,
                        'rating' => $rating,
                    ])
                </div>
            </div>
        </div>
    </div>
@endsection
