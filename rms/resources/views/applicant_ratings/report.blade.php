@extends('layouts.print')

@section('css')
<style type="text/css">
	.v-top{
		vertical-align: top !important;
	}

	.v-middle{
		vertical-align: middle !important;
	}

	.table>tbody>tr>td{
		border: 0 !important;
		padding: 2px;
	}

	.width-150px{
		width: 150px !important;
	}

	.width-em-28{
 		width: 28.8em !important;
 	}

 	@media print{
 		.width-em-28{
	 		width: 28.8em !important;
	 	}
 	}


</style>
@endsection

@section('content')
 <div class="form-group row text-right d-print-none">
    <div class="col col-sm-10 col-lg-9 offset-sm-1 offset-lg-3">
      {{ Form::button('Print', ['id' => 'evaluation-report', 'class'=>'btn btn-primary btn-space', 'type'=>'submit']) }}
      {{ Form::reset('Cancel', ['class'=>'btn btn-space btn-danger']) }}
    </div>
  </div>

<div class="reports" style="width: 760px;margin: auto;font-size: 14px;font-family: Arial, Helvetica, sans-serif;">

	<?php 

		$level = "";
		switch ($rating->position_level) {
			case '1st Level':
				$level = 'FIRST LEVEL POSITION';
				break;

			case '2nd Level':
				$level = 'SECOND LEVEL POSITION';
				break;

			case '3rd Level':
				$level = 'THIRD LEVEL POSITION';
				break;
			
		}

	?>

	<div class="row border-bottom border-dark mb-3">
		<div class="col-12">
			<h3 class="font-weight-bold mt-1 mb-4 text-center" style="font-size: 14px;">RATING FORM ({!! $level !!})</h3>
			<h3 class="mb-1 pb-1 text-center" style="font-size: 14px;">EVALUTION OF EDUCATION, TRAINING AND EXPERIENCE (ETE)</h3>
			<h3 class="mb-8  pt-0 mt-0 text-center" style="font-size: 14px;">FOR THE POSITION OF <u>{!! (@$rating->applicant->job->status == 'plantilla') ? @$rating->applicant->job->plantilla_item->position->Name : @$rating->applicant->job->plantilla_item->position->Name !!}</u></h3>
			<table class="table border-0">
				<tbody>
					<tr>
						<td class="width-150px">NAME</td>
						<td>: {!! @$rating->applicant->getFullName() !!}</td>
					</tr>
					<tr>
						<td class="width-150px">CURRENT POSITION</td>
						<td>: </td>
					</tr>
					<tr>
						<td class="width-150px">OFFICE</td>
						<td>: {!! (@$rating->applicant->job->status == 'plantilla') ? @$rating->applicant->job->plantilla_item->office->Name : 'NA' !!}</td>
					</tr>
					<tr>
						<td class="width-150px">SG</td>
						<td>: {!! (@$rating->applicant->job->plantilla_item->salary_grade) ? @$rating->applicant->job->plantilla_item->salary_grade->Name : 'NA' !!}</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>

<p class="pt-0 mt-0 {{ ($rating->position_level == '3rd Level') ? '' : 'd-none' }}">	Note: Applicants shall meet the <u>minimum of 36 points</u> to proceed to with the special exams, aptitude test and CBI.</p>

<div class="row">
	<div class="col-6 text-center">
		CRITERIA
	</div>
	<div class="col-6 text-center">
		POINTS
	</div>
</div>

<!-- First Level  -->
<div class="first_level {{ ($rating->position_level == '1st Level') ? '' : 'd-none' }}">
	<div class="row mb-3">
		<div class="col-6">
			<p class="font-weight-bold pb-0 mb-0">A. EDUCATION</p>
			<p class="mb-1 pb-1 pl-4">(at least 2 years in college for SG 8 and 9; <br> High school graduate for SG 7 below)</p>
			<table class="table ml-4">
				<tbody>
					<tr>
						<td class="width-em-28">Doctoral Degree</td>
						<td>15%</td>
					</tr>
					<tr>
						<td class="width-em-28">Masteral’s Degree</td>
						<td>14%</td>
					</tr>
					<tr>
						<td class="width-em-28">At least 24 MA Units and above</td>
						<td>13%</td>
					</tr>
					<tr>
						<td class="width-em-28">At least 10-18 MA Units</td>
						<td>12%</td>
					</tr>
					<tr>
						<td class="width-em-28">At least 3-9 Ma Units</td>
						<td>11%</td>
					</tr>
					<tr>
						<td class="width-em-28">Bachelor’s degree </td>
						<td>10%</td>
					</tr>
					<tr>
						<td class="width-em-28">Completion of 2 years in College </td>
						<td>9%</td>
					</tr>
				</tbody>
			</table>
		</div>

		<div class="col-6">
			<div class="form-group row">
	          <div class="col-12 col-sm-8 col-lg-6	offset-3 text-center">
	          	{!! @$rating->education_points !!}
	          </div>
	      </div>
		</div>
	</div>
	
	<div class="row">
		<div class="col-6">
			<p class="font-weight-bold pb-0 mb-0">B. TRAINING</p>
			<p class="mb-1 pb-1 pl-4">(only relevant training shall be considered)</p>
			<table class="table ml-4">
				<tbody>
					<tr>
						<td class="width-em-28">5 days or more </td>
						<td>10%</td>
					</tr>
					<tr>
						<td class="width-em-28">3-4 days</td>
						<td>9%</td>
					</tr>
					<tr>
						<td class="width-em-28">1-2 days</td>
						<td>8%</td>
					</tr>
					<tr>
						<td class="width-em-28">Half day</td>
						<td>7%</td>
					</tr>
				</tbody>
			</table>
		</div>

		<div class="col-6">
			<div class="form-group row">
	          <div class="col-12 col-sm-8 col-lg-6	offset-3 text-center">
	          	{!! @$rating->training_points !!}
	          </div>
	      </div>
		</div>
	</div>

	<div class="row mb-4">
		<div class="col-6">
			<p class="font-weight-bold mb-0 pb-0">C. EXPERIENCE</p>
			<p class="mb-1 pb-1 pl-4">(only relevant training shall be considered)</p>
			<table class="table ml-4">
				<tbody>
					<tr>
						<td class="width-em-28">6 years or more </td>
						<td>20%</td>
					</tr>
					<tr>
						<td class="width-em-28">5 years</td>
						<td>19%</td>
					</tr>
					<tr>
						<td class="width-em-28">4 years</td>
						<td>18%</td>
					</tr>
					<tr>
						<td class="width-em-28">3 years</td>
						<td>17%</td>
					</tr>
					<tr>
						<td class="width-em-28">2 years</td>
						<td>16%</td>
					</tr>
					<tr>
						<td class="width-em-28">1 year</td>
						<td>15%</td>
					</tr>
				</tbody>
			</table>
		</div>

		<div class="col-6">
			<div class="form-group row">
	          <div class="col-12 col-sm-8 col-lg-6	offset-3 text-center">
	          	{!! @$rating->experience_points !!}
	          </div>
	      </div>
		</div>
	</div>
</div>	

<!-- Second Level -->
<div class="second_level {{ ($rating->position_level == '2nd Level') ? '' : 'd-none' }}">
	<div class="row mb-3">
		<div class="col-6">
			<p class="font-weight-bold pb-0 mb-0">A. EDUCATION</p>
			<p class="mb-1 pb-1 pl-4">(graduate of any bachelor’s degree course relevant to the job)</p>
			<table class="table ml-4">
				<tbody>
					<tr>
						<td class="width-em-28">Doctoral Degree</td>
						<td>15%</td>
					</tr>
					<tr>
						<td class="width-em-28">Masteral’s Degree</td>
						<td>14%</td>
					</tr>
					<tr>
						<td class="width-em-28">At least 24 MA Units and above</td>
						<td>13%</td>
					</tr>
					<tr>
						<td class="width-em-28">At least 10-18 MA Units</td>
						<td>12%</td>
					</tr>
					<tr>
						<td class="width-em-28">At least 3-9 Ma Units</td>
						<td>11%</td>
					</tr>
					<tr>
						<td class="width-em-28">Bachelor’s degree </td>
						<td>10%</td>
					</tr>
				</tbody>
			</table>
		</div>

		<div class="col-6">
			<div class="form-group row">
	          <div class="col-12 col-sm-8 col-lg-6	offset-3 text-center">
	          	{!! @$rating->education_points !!}
	          </div>
	      </div>
		</div>
	</div>

	@php
	 $sg = @$rating->applicant->job->plantilla_item->salary_grade->Name;
	@endphp

	<div class="row">
		<div class="col-6">
			<p class="font-weight-bold pb-0 mb-0">B. TRAINING</p>
			<p class="mb-1 pb-1 pl-4">(only relevant training shall be considered)</p>
			<table class="table ml-4">
				<tbody>
						<tr class="first {{ ($sg > 9 && $sg < 14) ? '' : 'd-none' }}">
							<td colspan="2" class="pb-3 pt-2">
								(SG 10 to 13)
							</td>
						</tr>
						<tr class="first {{ ($sg > 9 && $sg < 14) ? '' : 'd-none' }}">
							<td class="width-em-28">26 Hours and Above</td>
							<td>10%</td>
						</tr>
						<tr class="first {{ ($sg > 9 && $sg < 14) ? '' : 'd-none' }}">
							<td class="width-em-28">16-24 Hours</td>
							<td>9%</td>
						</tr>
						<tr class="first {{ ($sg > 9 && $sg < 14) ? '' : 'd-none' }}">
							<td class="width-em-28">12-24 Hours</td>
							<td>8%</td>
						</tr>
						<tr class="first {{ ($sg > 9 && $sg < 14) ? '' : 'd-none' }}">
							<td class="width-em-28">4-8 Hours</td>
							<td>7%</td>
						</tr>
	 
					<tr class="second {{ ($sg > 13 && $sg < 18) ? '' : 'd-none' }}">
						<td colspan="2" class="pb-3 pt-2">
							(SG 14 to 17)
						</td>
					</tr>
					<tr class="second {{ ($sg > 13 && $sg < 18) ? '' : 'd-none' }}">
						<td class="width-em-28">36 Hours and Above</td>
						<td>10%</td>
					</tr>
					<tr class="second {{ ($sg > 13 && $sg < 18) ? '' : 'd-none' }}">
						<td class="width-em-28">26-32 Hours</td>
						<td>9%</td>
					</tr>
					<tr class="second {{ ($sg > 13 && $sg < 18) ? '' : 'd-none' }}">
						<td class="width-em-28">12-24 Hours</td>
						<td>8%</td>
					</tr>
					<tr class="second {{ ($sg > 13 && $sg < 18) ? '' : 'd-none' }}">
						<td class="width-em-28">4-8 Hours</td>
						<td>7%</td>
					</tr>

					<tr class="third {{ ($sg > 17) ? '' : 'd-none' }}">
						<td colspan="2" class="pb-3 pt-2">
							(SG 18 to 23)
						</td>
					</tr>
					<tr class="third {{ ($sg > 17) ? '' : 'd-none' }}">
						<td class="width-em-28">41 Hours and Above</td>
						<td>10%</td>
					</tr>
					<tr class="third {{ ($sg > 17) ? '' : 'd-none' }}">
						<td class="width-em-28">32-40 Hours</td>
						<td>9%</td>
					</tr>
					<tr class="third {{ ($sg > 17) ? '' : 'd-none' }}">
						<td class="width-em-28">20-28 Hours</td>
						<td>8%</td>
					</tr>
					<tr class="third {{ ($sg > 17) ? '' : 'd-none' }}">
						<td class="width-em-28">8-16 Hours</td>
						<td>7%</td>
					</tr>
					
				</tbody>
			</table>
		</div>

		<div class="col-6">
				<div class="form-group row">
	          <div class="col-12 col-sm-8 col-lg-6	offset-3 text-center">
	          	{!! @$rating->training_points !!}
	          </div>
	      </div>
		</div>
	</div>

	<div class="row mb-4">
		<div class="col-6">
			<p class="font-weight-bold mb-0 pb-0">C. EXPERIENCE</p>
			<p class="mb-1 pb-1 pl-4">(only relevant training shall be considered)</p>
			<table class="table ml-4">
				<tbody>
					<tr class="first {{ ($sg > 9 && $sg < 14) ? '' : 'd-none' }}">
						<td colspan="2" class="pb-3 pt-2">
							(SG 10 to 13)
						</td>
					</tr>
					<tr class="first {{ ($sg > 9 && $sg < 14) ? '' : 'd-none' }}">
						<td class="width-em-28">5 years or more </td>
						<td>20%</td>
					</tr>
					<tr class="first {{ ($sg > 9 && $sg < 14) ? '' : 'd-none' }}">
						<td class="width-em-28">4 years</td>
						<td>19%</td>
					</tr>
					<tr class="first {{ ($sg > 9 && $sg < 14) ? '' : 'd-none' }}">
						<td class="width-em-28">3 years</td>
						<td>18%</td>
					</tr>
					<tr class="first {{ ($sg > 9 && $sg < 14) ? '' : 'd-none' }}">
						<td class="width-em-28">2 years</td>
						<td>17%</td>
					</tr>
					<tr class="first {{ ($sg > 9 && $sg < 14) ? '' : 'd-none' }}">
						<td class="width-em-28">1 years</td>
						<td>16%</td>
					</tr>
					<tr class="first {{ ($sg > 9 && $sg < 14) ? '' : 'd-none' }}">
						<td class="width-em-28">6 months</td>
						<td>15%</td>
					</tr>

					<tr class="second {{ ($sg > 13 && $sg < 18) ? '' : 'd-none' }}">
						<td colspan="2" class="pb-3 pt-2">
							(SG 14 to 17)
						</td>
					</tr>
					<tr class="second {{ ($sg > 13 && $sg < 18) ? '' : 'd-none' }}">
						<td class="width-em-28">6 years or more </td>
						<td>20%</td>
					</tr>
					<tr class="second {{ ($sg > 13 && $sg < 18) ? '' : 'd-none' }}">
						<td class="width-em-28">5 years</td>
						<td>19%</td>
					</tr>
					<tr class="second {{ ($sg > 13 && $sg < 18) ? '' : 'd-none' }}">
						<td class="width-em-28">4 years</td>
						<td>18%</td>
					</tr>
					<tr class="second {{ ($sg > 13 && $sg < 18) ? '' : 'd-none' }}">
						<td class="width-em-28">3 years</td>
						<td>17%</td>
					</tr>
					<tr class="second {{ ($sg > 13 && $sg < 18) ? '' : 'd-none' }}">
						<td class="width-em-28">2 years</td>
						<td>16%</td>
					</tr>
					<tr class="second {{ ($sg > 13 && $sg < 18) ? '' : 'd-none' }}">
						<td class="width-em-28">1 year</td>
						<td>15%</td>
					</tr>

					<tr class="third {{ ($sg > 17) ? '' : 'd-none' }}">
						<td colspan="2" class="pb-3 pt-2">
							(SG 18 to 23)
						</td>
					</tr>
					<tr class="third {{ ($sg > 17) ? '' : 'd-none' }}">
						<td class="width-em-28">7 years or more </td>
						<td>20%</td>
					</tr>
					<tr class="third {{ ($sg > 17) ? '' : 'd-none' }}">
						<td class="width-em-28">6 years</td>
						<td>19%</td>
					</tr>
					<tr class="third {{ ($sg > 17) ? '' : 'd-none' }}">
						<td class="width-em-28">5 years</td>
						<td>18%</td>
					</tr>
					<tr class="third {{ ($sg > 17) ? '' : 'd-none' }}">
						<td class="width-em-28">4 years</td>
						<td>17%</td>
					</tr>
					<tr class="third {{ ($sg > 17) ? '' : 'd-none' }}">
						<td class="width-em-28">3 years</td>
						<td>16%</td>
					</tr>
					<tr class="third {{ ($sg > 17) ? '' : 'd-none' }}">
						<td class="width-em-28">2 year</td>
						<td>15%</td>
					</tr>
				</tbody>
			</table>
		</div>

		<div class="col-6">
			<div class="form-group row">
	          <div class="col-12 col-sm-8 col-lg-6	offset-3 text-center">
	          	{!! @$rating->experience_points !!}
	          </div>
	      </div>
		</div>
	</div>
</div>

<div class="third_level {{ ($rating->position_level == '3rd Level') ? '' : 'd-none' }}" >
	
	<div class="row mb-3">
		<div class="col-6">
			<p class="font-weight-bold pb-0 mb-0">A. EDUCATION (15 Points)</p>
			<p class="mb-1 pb-1 pl-4">(graduate of any bachelor’s degree course relevant to the job)</p>
			<table class="table ml-4">
				<tbody>
					<tr>
						<td class="width-em-28">Doctoral Degree</td>
						<td>15%</td>
					</tr>
					<tr>
						<td class="width-em-28">Masteral’s Degree</td>
						<td>14%</td>
					</tr>
					<tr>
						<td class="width-em-28">At least 24 MA Units and above</td>
						<td>13%</td>
					</tr>
					<tr>
						<td class="width-em-28">At least 10-18 MA Units</td>
						<td>12%</td>
					</tr>
					<tr>
						<td class="width-em-28">At least 3-9 Ma Units</td>
						<td>11%</td>
					</tr>
					<tr>
						<td class="width-em-28">Bachelor’s degree </td>
						<td>10%</td>
					</tr>
				</tbody>
			</table>
		</div>

		<div class="col-6">
			<div class="form-group row">
	          <div class="col-12 col-sm-8 col-lg-6	offset-3 text-center">
	          	{!! @$rating->education_points !!}
	          </div>
	      </div>
		</div>
	</div>

	<div class="row">
		<div class="col-6">
			<p class="font-weight-bold pb-0 mb-0">B. TRAINING (10 Points)</p>
			<p class="mb-1 pb-1 pl-4">(only relevant training shall be considered)</p>
			<table class="table ml-4">
				<tbody>
					<tr>
						<td class="width-em-28">80 Hours and Above </td>
						<td>10%</td>
					</tr>
					<tr>
						<td class="width-em-28">64-76 Hours</td>
						<td>9%</td>
					</tr>
					<tr>
						<td class="width-em-28">44-60 Hours</td>
						<td>8%</td>
					</tr>
					<tr>
						<td class="width-em-28">40 Hours</td>
						<td>7%</td>
					</tr>
				</tbody>
			</table>
		</div>

		<div class="col-6">
			<div class="form-group row">
	          <div class="col-12 col-sm-8 col-lg-6	offset-3 text-center">
	          	{!! @$rating->training_points !!}
	          </div>
	      </div>
		</div>
	</div>

	<div class="row mb-4">
		<div class="col-6">
			<p class="font-weight-bold mb-0 pb-0">C. EXPERIENCE (20 Points)</p>
			<p class="mb-1 pb-1 pl-4">(only relevant training shall be considered)</p>
			<table class="table ml-4">
				<tbody>
					<tr>
						<td class="width-em-28">10 years or more </td>
						<td>20%</td>
					</tr>
					<tr>
						<td class="width-em-28">9 years</td>
						<td>19%</td>
					</tr>
					<tr>
						<td class="width-em-28">8 years</td>
						<td>18%</td>
					</tr>
					<tr>
						<td class="width-em-28">7 years</td>
						<td>17%</td>
					</tr>
					<tr>
						<td class="width-em-28">6 years</td>
						<td>16%</td>
					</tr>
					<tr>
						<td class="width-em-28">5 years</td>
						<td>15%</td>
					</tr>
				</tbody>
			</table>
		</div>

		<div class="col-6">
			<div class="form-group row">
	          <div class="col-12 col-sm-8 col-lg-6	offset-3 text-center">
	          	{!! @$rating->experience_points !!}
	          </div>
	      </div>
		</div>
	</div>

</div>


<div class="row">
	<div class="col-6 text-right">
		<p class="font-weight-bold">TOTAL POINTS</p>
	</div>
	<div class="col-6">
		<div class="form-group row">
	        <div class="col-12 col-sm-8 col-lg-6	offset-3 text-center">
	        	{!! @$rating->total_points !!}
	        </div>
	    </div>
	</div>
</div>

</div>


@endsection

@section('scripts')
<script type="text/javascript">
  $(document).ready(function() {
    $('#evaluation-report').click(function() {
      window.print();
    });
  });
</script>
@endsection