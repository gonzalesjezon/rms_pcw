
<div class="row mb-3">
	<div class="col-6">
		<p class="font-weight-bold">A. EDUCATION (15 Points)</p>
		<p class="mb-1 pb-1">(graduate of any bachelor’s degree course relevant to the job)</p>
		<table class="table">
			<tbody>
				<tr>
					<td class="width-em-28">Doctoral Degree</td>
					<td>15%</td>
				</tr>
				<tr>
					<td class="width-em-28">Masteral’s Degree</td>
					<td>14%</td>
				</tr>
				<tr>
					<td class="width-em-28">At least 24 MA Units and above</td>
					<td>13%</td>
				</tr>
				<tr>
					<td class="width-em-28">At least 10-18 MA Units</td>
					<td>12%</td>
				</tr>
				<tr>
					<td class="width-em-28">At least 3-9 Ma Units</td>
					<td>11%</td>
				</tr>
				<tr>
					<td class="width-em-28">Bachelor’s degree </td>
					<td>10%</td>
				</tr>
			</tbody>
		</table>
	</div>

	<div class="col-3">
		<div class="form-group row">
          <div class="col-12">
          	{{ Form::text('education_points_3', @$rating->education_points, [
          		'class' => 'form-control form-control-sm input text-right',
          		'placeholder' => '0.00'
          	]) }}
          </div>
      </div>
	</div>
</div>

<div class="row">
	<div class="col-6">
		<p class="font-weight-bold">B. TRAINING (10 Points)</p>
		<p class="mb-1 pb-1">(only relevant training shall be considered)</p>
		<table class="table">
			<tbody>
				<tr>
					<td class="width-em-28">80 Hours and Above </td>
					<td>10%</td>
				</tr>
				<tr>
					<td class="width-em-28">64-76 Hours</td>
					<td>9%</td>
				</tr>
				<tr>
					<td class="width-em-28">44-60 Hours</td>
					<td>8%</td>
				</tr>
				<tr>
					<td class="width-em-28">40 Hours</td>
					<td>7%</td>
				</tr>
			</tbody>
		</table>
	</div>

	<div class="col-3">
		<div class="form-group row">
          <div class="col-12">
          	{{ Form::text('training_points_3', @$rating->training_points, [
          		'class' => 'form-control form-control-sm input text-right',
          		'placeholder' => '0.00'
          	]) }}
          </div>
      </div>
	</div>

	<div class="col-3">
		<div class="form-group row">
          <div class="col-12">
          	<div id="training_remarks_3" name="training_remarks"></div>
                {{ Form::textarea('training_remarks_3', @$rating->training_remarks,['id'=>'training_remarks_3-text', 'class'=>'d-none']) }}
          </div>
      </div>
	</div>
</div>

<div class="row mb-4">
	<div class="col-6">
		<p class="font-weight-bold">C. EXPERIENCE (20 Points)</p>
		<p class="mb-1 pb-1">(only relevant training shall be considered)</p>
		<table class="table">
			<tbody>
				<tr>
					<td class="width-em-28">10 years or more </td>
					<td>20%</td>
				</tr>
				<tr>
					<td class="width-em-28">9 years</td>
					<td>19%</td>
				</tr>
				<tr>
					<td class="width-em-28">8 years</td>
					<td>18%</td>
				</tr>
				<tr>
					<td class="width-em-28">7 years</td>
					<td>17%</td>
				</tr>
				<tr>
					<td class="width-em-28">6 years</td>
					<td>16%</td>
				</tr>
				<tr>
					<td class="width-em-28">5 years</td>
					<td>15%</td>
				</tr>
			</tbody>
		</table>
	</div>

	<div class="col-3">
		<div class="form-group row">
          <div class="col-12">
          	{{ Form::text('experience_points_3', @$rating->experience_points, [
          		'class' => 'form-control form-control-sm input text-right',
          		'placeholder' => '0.00'
          	]) }}
          </div>
      </div>
	</div>

	<div class="col-3">
		<div class="form-group row">
          <div class="col-12">
          	<div id="experience_remarks_3" name="experience_remarks"></div>
                {{ Form::textarea('experience_remarks_3', @$rating->experience_remarks,['id'=>'experience_remarks_3-text', 'class'=>'d-none']) }}
          </div>
      </div>
	</div>
</div>

<div class="row">
	<div class="col-6 text-center">
		<p class="font-weight-bold">TOTAL POINTS</p>
	</div>
	<div class="col-6">
		<div class="form-group row">
	        <div class="col-12 col-sm-8 col-lg-6	offset-3">
	        	{{ Form::text('total_points', @$rating->total_points, [
	        		'class' => 'form-control form-control-sm text-right total_points',
	        		'placeholder' => '0.00',
	        		'readonly' => 'true',
	        	]) }}
	        </div>
	    </div>
	</div>
</div>