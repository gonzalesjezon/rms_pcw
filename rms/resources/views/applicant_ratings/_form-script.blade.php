<script src="{{ URL::asset('beagle-assets/lib/moment.js/min/moment.min.js')}}"></script>
<script src="{{ URL::asset('beagle-assets/lib/datetimepicker/js/bootstrap-datetimepicker.min.js') }}"
        type="text/javascript"></script>
<script src="{{ URL::asset('beagle-assets/lib/summernote/summernote-bs4.min.js') }}"
        type="text/javascript"></script>
<script src="{{ URL::asset('beagle-assets/lib/summernote/summernote-ext-beagle.js') }}"
        type="text/javascript"></script>
<script src="{{ URL::asset('beagle-assets/js/app-form-wysiwyg.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('beagle-assets/lib/select2/js/select2.min.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('beagle-assets/lib/select2/js/select2.full.min.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('beagle-assets/lib/bootstrap-slider/bootstrap-slider.min.js') }}"
        type="text/javascript"></script>
<script src="{{ URL::asset('beagle-assets/js/app-form-elements.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('beagle-assets/lib/parsley/parsley.min.js') }}" type="text/javascript"></script>
<script type="text/javascript">
  $(document).ready(function() {
    //initialize the javascript
    App.init();
    App.formElements();
    $('#applicant-form').parsley(); //frontend validation

    let data = {
      '#training_remarks_1': `{!! @$rating->training_remarks !!}`,
      '#experience_remarks_1': `{!! @$rating->experience_remarks !!}`,
      '#training_remarks_2': `{!! @$rating->training_remarks !!}`,
      '#experience_remarks_2': `{!! @$rating->experience_remarks !!}`,
      '#training_remarks_3': `{!! @$rating->training_remarks !!}`,
      '#experience_remarks_3': `{!! @$rating->experience_remarks !!}`,
    };

    // initialize editors for each data element
    App.textEditors(Object.keys(data));

    // when validation fails, get data from hidden input texts
    // and set as value for wysiwyg editor
    for (var key in data) {
      if (data.hasOwnProperty(key)) {
        if (data[key] === '') {
          data[key] = $(`${key}-text`).html();
        }
        setData(key, data[key]);
      }
    }

    // set data for wysiwyg editors
    function setData(selector = '', data = '') {
      $(selector).next('.note-editor').find('.note-editing-area > .note-editable').html(data);
    }

    $('.note-toolbar').remove();
    // on form submit, get data from wysiwyg editors
    // pass to hidden input elements
    $('#job-submit').click(function() {
      for (let key in data) {
        if (data.hasOwnProperty(key)) {
          let element = getData(key);
          $(`${key}-text`).val(element);
        }
      }
    });

    function getData(selector = '') {
      let data = $(selector).next('.note-editor').find('.note-editing-area > .note-editable');
      return data.html();
    }

    // MULTIPLE INPUT FIELDS SUMMATION
    $(document).on('keyup','.input',function(){
        var sum = 0;
        $('.input').each(function(){
            sum += +$(this).val();
        });
        $('.total_points').val(sum);
    });

    $('#applicant_id').on('change',function(){
        position = $(this).find(':selected').data('position');
        office   = $(this).find(':selected').data('office');
        sg = $(this).find(':selected').data('sg');
        // Hide all forms
        $('.first').addClass('d-none');
        $('.second').addClass('d-none');
        $('.third').addClass('d-none');
        $('#first_level').addClass('d-none');
        $('#second_level').addClass('d-none');
        $('#third_level').addClass('d-none');

        level = "";
        if(sg !== 'NA')
        {
            if(Number(sg) < 10)
            {
              level = '1st Level';
              $('#first_level').removeClass('d-none');
            }
            else if(Number(sg) > 9 && Number(sg) < 24)
            {
              level = '2nd Level';
              if(Number(sg) > 9 && Number(sg) < 14)
              {
                $('.first').removeClass('d-none');
              }
              else if(Number(sg) > 13 && Number(sg) < 18)
              {
                $('.second').removeClass('d-none');
              }
              else if(Number(sg) > 17)
              {
                $('.third').removeClass('d-none');
              }
              $('#second_level').removeClass('d-none');

            }else if(Number(sg) > 23){
              level = '3rd Level';
              $('#third_level').removeClass('d-none');
            }

        }else{
          level = 'NA';
        }

        $('#level').val(level);
        $('#office').val(office);
        $('#position_applied').val(position);
        $('#sg').val(sg);
    });

    $("#applicant_id").trigger('change');

    

  });
</script>