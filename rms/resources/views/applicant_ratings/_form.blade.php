@section('css')
<link rel="stylesheet" type="text/css"
        href="{{ URL::asset('beagle-assets/lib/datetimepicker/css/bootstrap-datetimepicker.min.css') }}">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.min.css"
          rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('beagle-assets/lib/summernote/summernote-bs4.css') }}" />

 <style type="text/css">
 	.table>tbody>tr>td{
 		border: 0 !important;
 		padding: 2px;
 	}

 	.width-em-28{
 		width: 28.8em !important;
 	}
 </style>
@endsection


{!! Form::open(['action' => $action, 'method' => $method, 'id'=>'applicant-form']) !!}
<input type="hidden" name="id" value="{{ @$rating->id }}">
<div class="row">
    <div class="col-6">

    	@if(@$rating)
    		<div class="form-group row {{ $errors->has('applicant_id') ? 'has-error' : ''}}">
            {{ Form::label('applicant_id', 'Applicant Name', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
            <div class="col-12 col-sm-8 col-lg-6">
               {{ Form::text('', @$rating->applicant->getFullName(), [
            		'class' => 'form-control form-control-sm',
            		'readonly' => 'true'
            	]) }}
            </div>
        </div>
    	@else
	    	<div class="form-group row {{ $errors->has('applicant_id') ? 'has-error' : ''}}">
		        {{ Form::label('applicant_id', 'Select Applicant', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
		        <div class="col-12 col-sm-8 col-lg-6">
		            <select name="applicant_id" id="applicant_id" class="form-control form-control-xs" required>
							      <option value="0">Select applcaint</option>
							      @foreach($applicants as $applicant)
							      <option value="{{$applicant->id }}" data-position="{{ ($applicant->job->status == 'plantilla') ? $applicant->job->plantilla_item->position->Name : $applicant->job->plantilla_item->position->Name  }} " data-office="{{ ($applicant->job->status == 'plantilla') ? @$applicant->job->plantilla_item->office->Name : 'NA'  }} " data-sg="{{ ($applicant->job->status == 'plantilla') ? @$applicant->job->plantilla_item->salary_grade->Name : 'NA'  }} " {{ ($applicant->id == @$rating->applicant_id) ? 'selected' : '' }}>{{ $applicant->getFullName() }}</option>
							      @endforeach

						    </select>
		        {!! $errors->first('applicant_id', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
		        </div>
		    </div>	
    	@endif

    </div>

    <div class="col-6">
			<div class="form-group row">
	            {{ Form::label('', 'Current Position', [
	            'class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
	          <div class="col-12 col-sm-8 col-lg-6">
	          	{{ Form::text('', '', [
	          		'class' => 'form-control form-control-sm',
	          		'readonly' => 'true'
	          	])}}
	          </div>
	      </div>
		</div>

</div>

<div class="row">
		<div class="col-6">
			<div class="form-group row">
	            {{ Form::label('', 'Position Applied', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
	          <div class="col-12 col-sm-8 col-lg-6">
	          	{{ Form::text('', (@$rating->applicant->job->status == 'plantilla') ? @$rating->applicant->job->plantilla_item->position->Name : @$rating->applicant->job->cos_position_title, [
	          		'class' => 'form-control form-control-sm',
	          		'readonly' => 'true',
	          		'id' => 'position_applied'
	          	]) }}
	          </div>
	      </div>
		</div>

		<div class="col-6">
			<div class="form-group row">
	            {{ Form::label('', 'Office', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
	          <div class="col-12 col-sm-8 col-lg-6">
	          	{{ Form::text('', (@$rating->applicant->job->status == 'plantilla') ? @$rating->applicant->job->plantilla_item->office->Name : 'NA', [
	          		'class' => 'form-control form-control-sm',
	          		'readonly' => 'true',
	          		'id' => 'office'
	          	]) }}
	          </div>
	      </div>
		</div>

</div>

<div class="row">
		<div class="col-6">
			<div class="form-group row">
	            {{ Form::label('', 'Position Level', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
	          <div class="col-12 col-sm-8 col-lg-6">
	          	{{ Form::text('position_level', @$rating->position_level, [
	          		'class' => 'form-control form-control-sm',
	          		'readonly' => 'true',
	          		'id' => 'level'
	          	]) }}
	          </div>
	      </div>
		</div>

		<div class="col-6">
			<div class="form-group row">
	            {{ Form::label('', 'SG', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
	          <div class="col-12 col-sm-8 col-lg-6">
	          	{{ Form::text('', (@$rating->applicant->job->status == 'plantilla') ? @$rating->applicant->job->plantilla_item->salary_grade->Name : 'NA', [
	          		'class' => 'form-control form-control-sm',
	          		'readonly' => 'true',
	          		'id' => 'sg'
	          	]) }}
	          </div>
	      </div>
		</div>
</div>

<hr>

<div class="row">
	<div class="col-6 text-center">
		<p class="font-weight-bold">CRITERIA</p>
	</div>
	<div class="col-3 text-center">
		<p class="font-weight-bold">POINTS</p>
	</div>
	<div class="col-3 text-center">
		<p class="font-weight-bold">REMARKS</p>
	</div>
</div>

<!-- First Level -->
<div id="first_level" class="{{ (@$rating->position_level == '1st Level') ? '' : 'd-none' }}" >
	@include('applicant_ratings._form-first_level')
</div>

<!-- Second Level -->
<div id="second_level" class="{{ (@$rating->position_level == '2nd Level') ? '' : 'd-none' }}">
	@include('applicant_ratings._form-second_level')
</div>

<!-- Third Level -->
<div id="third_level" class="{{ (@$rating->position_level == '3rd Level') ? '' : 'd-none' }}">
	@include('applicant_ratings._form-third_level')
</div>


<div class="form-group row text-right">
    <div class="col col-sm-12 ">
        {{ Form::submit('Submit', ['id' => 'job-submit', 'class'=>'btn btn-primary btn-space']) }}
        {{ Form::reset('Clear Form', ['id'=>'clear-form', 'class'=>'btn btn-space btn-danger']) }}
    </div>
</div>
{!! Form::close() !!}

@section('scripts')
    @include('applicant_ratings._form-script')
@endsection
