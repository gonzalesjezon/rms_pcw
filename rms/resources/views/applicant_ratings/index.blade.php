@extends('layouts.app')

@section('css')
  <link rel="stylesheet" type="text/css"
        href="{{ URL::asset('beagle-assets/lib/datatables/datatables.net-bs4/css/dataTables.bootstrap4.css') }}" />
@endsection

@section('content')
  <div class="page-head">
    <h2 class="page-head-title">Applicant Rating</h2>
  </div>
  <div class="main-content container-fluid">
    <div class="row">
      <div class="col-sm-12">
        <div class="card card-table">
          <div class="card-header">
            <a href="{{ route('applicant_ratings.create') }}" class="btn btn-space btn-primary"
               title="Add a vacant position">
                <i class="icon icon-left mdi mdi-account-add"></i> Add
            </a>

           <!--  <a href="{{ route('examinations.index') }}" class="btn btn-space btn-warning" title="Print">
                <i class="icon icon-left mdi mdi-account-add"></i> Print
            </a> -->
          </div>
          <div class="card-body">
            <table id="table1" class="table table-striped table-hover table-fw-widget">
              <thead>
              <tr>
                <th>Actions</th>
                <th>Applicant Name</th>
                <th>Position Applied</th>
                <th>SG</th>
                <th>Position Level</th>
                <th>Rating</th>
              </tr>
              </thead>
              <tbody>
                 @foreach($ratings as $rating)
                  <tr>
                    <td class="actions text-left">
                      <div class="tools">
                        <button type="button" data-toggle="dropdown"
                                class="btn btn-secondary dropdown-toggle" aria-expanded="false">
                          <i class="icon icon-left mdi mdi-settings-square"> </i> Options
                          <span class="icon-dropdown mdi mdi-chevron-down"> </span>
                        </button>

                        <div role="menu" class="dropdown-menu" x-placement="bottom-start">
                        	<a href="{{ route('applicant_ratings.edit', $rating->id) }}"
                               class="dropdown-item"><i class="icon icon-left mdi mdi-edit"></i>Edit</a>
                            <div class="dropdown-divider"></div>

                            @if(!empty($rating))
		                        <a href="{{route('applicant_ratings.report',[
		                              'id' => @$rating->id,
		                            ])}}"
		                             class="dropdown-item md-trigger"
		                             data-modal="md-flipH"
		                             target="_blank"
		                          >
		                          <i class="icon icon-left mdi mdi-print"> </i>Print
		                        </a>
		                        @else
		                        <a href="#" class="dropdown-item text-danger">
		                          <i class="icon icon-left mdi mdi-print"> </i>Print (Report Empty)
		                        </a>
		                        @endif
		                        <div class="dropdown-divider"></div>

                          {!! Form::open([
                              'method'=>'DELETE',
                              'url' => ['applicant_ratings', $rating->id],
                              'style' => 'display:inline'
                          ]) !!}
                          {!! Form::button('<i class="icon icon-left mdi mdi-delete mr-1"></i> Delete', [
                              'type' => 'submit',
                              'style' => 'color: #504e4e',
                              'class' => 'dropdown-item',
                              'title' => 'Delete Record',
                              'onclick'=>'return confirm("Confirm delete?")'
                          ])!!}
                          <input type="hidden" name="applicant_id" value="{{$rating->applicant_id}}">
                          {!! Form::close() !!}

                        </div>
                      </div>
                    </td>
                    <td>{{ $rating->applicant->getFullName()}}</td>
                    <td>{{ @$rating->applicant->job->plantilla_item->position->Name }}</td>
                    <td>
                    	{{ ($rating->applicant->job->status == 'plantilla') ? $rating->applicant->job->plantilla_item->salary_grade->Name : 'NA' }}
                    </td>
                    <td>{!! @$rating->position_level !!}</td>
                    <td>{!! @$rating->total_points !!}</td>
                  </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection

@section('scripts')
  <script src="{{ URL::asset('beagle-assets/lib/datatables/datatables.net/js/jquery.dataTables.js') }}"
          type="text/javascript"></script>
  <script src="{{ URL::asset('beagle-assets/lib/datatables/datatables.net-bs4/js/dataTables.bootstrap4.js') }}"
          type="text/javascript"></script>
  <script src="{{ URL::asset('beagle-assets/lib/datatables/datatables.net-buttons/js/dataTables.buttons.min.js') }}"
          type="text/javascript"></script>
  <script src="{{ URL::asset('beagle-assets/lib/datatables/datatables.net-buttons/js/buttons.html5.min.js') }}"
          type="text/javascript"></script>
  <script src="{{ URL::asset('beagle-assets/lib/datatables/datatables.net-buttons/js/buttons.flash.min.js') }}"
          type="text/javascript"></script>
  <script src="{{ URL::asset('beagle-assets/lib/datatables/datatables.net-buttons/js/buttons.print.min.js') }}"
          type="text/javascript"></script>
  <script src="{{ URL::asset('beagle-assets/lib/datatables/datatables.net-buttons/js/buttons.colVis.min.js') }}"
          type="text/javascript"></script>
  <script src="{{ URL::asset('beagle-assets/lib/datatables/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js') }}"
          type="text/javascript"></script>
  <script src="{{ URL::asset('beagle-assets/js/app-tables-datatables.js') }}" type="text/javascript"></script>
  <script type="text/javascript">
    $(document).ready(function() {
      //initialize the javascript
      App.init();
      App.dataTables();
    });
  </script>
@endsection
