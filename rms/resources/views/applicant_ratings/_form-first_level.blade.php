
<div class="row mb-3">
	<div class="col-6">
		<p class="font-weight-bold">A. EDUCATION</p>
		<p class="mb-1 pb-1">(at least 2 years in college for SG 8 and 9; <br> High school graduate for SG 7 below)</p>
		<table class="table">
			<tbody>
				<tr>
					<td class="width-em-28">Doctoral Degree</td>
					<td>15%</td>
				</tr>
				<tr>
					<td class="width-em-28">Masteral’s Degree</td>
					<td>14%</td>
				</tr>
				<tr>
					<td class="width-em-28">At least 24 MA Units and above</td>
					<td>13%</td>
				</tr>
				<tr>
					<td class="width-em-28">At least 10-18 MA Units</td>
					<td>12%</td>
				</tr>
				<tr>
					<td class="width-em-28">At least 3-9 Ma Units</td>
					<td>11%</td>
				</tr>
				<tr>
					<td class="width-em-28">Bachelor’s degree </td>
					<td>10%</td>
				</tr>
				<tr>
					<td class="width-em-28">Completion of 2 years in College </td>
					<td>9%</td>
				</tr>
			</tbody>
		</table>
	</div>

	<div class="col-3">
		<div class="form-group row">
          <div class="col-12">
          	{{ Form::text('education_points_1', @$rating->education_points, [
          		'class' => 'form-control form-control-sm input text-right',
          		'placeholder' => '0.00'
          	]) }}
          </div>
      </div>
	</div>
</div>

<div class="row">
	<div class="col-6">
		<p class="font-weight-bold">B. TRAINING</p>
		<p class="mb-1 pb-1">(only relevant training shall be considered)</p>
		<table class="table">
			<tbody>
				<tr>
					<td class="width-em-28">5 days or more </td>
					<td>10%</td>
				</tr>
				<tr>
					<td class="width-em-28">3-4 days</td>
					<td>9%</td>
				</tr>
				<tr>
					<td class="width-em-28">1-2 days</td>
					<td>8%</td>
				</tr>
				<tr>
					<td class="width-em-28">Half day</td>
					<td>7%</td>
				</tr>
			</tbody>
		</table>
	</div>

	<div class="col-3">
		<div class="form-group row">
          <div class="col-12">
          	{{ Form::text('training_points_1', @$rating->training_points, [
          		'class' => 'form-control form-control-sm input text-right',
          		'placeholder' => '0.00'
          	]) }}
          </div>
      </div>
	</div>

	<div class="col-3">
		<div class="form-group row">
          <div class="col-12">
          	<div id="training_remarks_1" name="training_remarks"></div>
                {{ Form::textarea('training_remarks_1', @$rating->training_remarks,['id'=>'training_remarks-text_1', 'class'=>'d-none']) }}
          </div>
      </div>
	</div>
</div>

<div class="row mb-4">
	<div class="col-6">
		<p class="font-weight-bold">C. EXPERIENCE</p>
		<p class="mb-1 pb-1">(only relevant training shall be considered)</p>
		<table class="table">
			<tbody>
				<tr>
					<td class="width-em-28">6 years or more </td>
					<td>20%</td>
				</tr>
				<tr>
					<td class="width-em-28">5 years</td>
					<td>19%</td>
				</tr>
				<tr>
					<td class="width-em-28">4 years</td>
					<td>18%</td>
				</tr>
				<tr>
					<td class="width-em-28">3 years</td>
					<td>17%</td>
				</tr>
				<tr>
					<td class="width-em-28">2 years</td>
					<td>16%</td>
				</tr>
				<tr>
					<td class="width-em-28">1 year</td>
					<td>15%</td>
				</tr>
			</tbody>
		</table>
	</div>

	<div class="col-3">
		<div class="form-group row">
          <div class="col-12">
          	{{ Form::text('experience_points_1', @$rating->experience_points, [
          		'class' => 'form-control form-control-sm input text-right',
          		'placeholder' => '0.00'
          	]) }}
          </div>
      </div>
	</div>

	<div class="col-3">
		<div class="form-group row">
          <div class="col-12">
          	<div id="experience_remarks_1" name="experience_remarks"></div>
                {{ Form::textarea('experience_remarks_1', @$rating->experience_remarks,['id'=>'experience_remarks-text_1', 'class'=>'d-none']) }}
          </div>
      </div>
	</div>
</div>

<div class="row">
	<div class="col-6 text-center">
		<p class="font-weight-bold">TOTAL POINTS</p>
	</div>
	<div class="col-6">
		<div class="form-group row">
	        <div class="col-12 col-sm-8 col-lg-6">
	        	{{ Form::text('total_points', @$rating->total_points, [
	        		'class' => 'form-control form-control-sm text-right total_points',
	        		'placeholder' => '0.00',
	        		'readonly' => 'true',
	        	]) }}
	        </div>
	    </div>
	</div>
</div>