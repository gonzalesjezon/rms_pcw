
<div class="row mb-3">
	<div class="col-6">
		<p class="font-weight-bold">A. EDUCATION</p>
		<p class="mb-1 pb-1">(graduate of any bachelor’s degree course relevant to the job)</p>
		<table class="table">
			<tbody>
				<tr>
					<td class="width-em-28">Doctoral Degree</td>
					<td>15%</td>
				</tr>
				<tr>
					<td class="width-em-28">Masteral’s Degree</td>
					<td>14%</td>
				</tr>
				<tr>
					<td class="width-em-28">At least 24 MA Units and above</td>
					<td>13%</td>
				</tr>
				<tr>
					<td class="width-em-28">At least 10-18 MA Units</td>
					<td>12%</td>
				</tr>
				<tr>
					<td class="width-em-28">At least 3-9 Ma Units</td>
					<td>11%</td>
				</tr>
				<tr>
					<td class="width-em-28">Bachelor’s degree </td>
					<td>10%</td>
				</tr>
			</tbody>
		</table>
	</div>

	<div class="col-3">
		<div class="form-group row">
          <div class="col-12">
          	{{ Form::text('education_points_2', @$rating->education_points, [
          		'class' => 'form-control form-control-sm input text-right',
          		'placeholder' => '0.00'
          	]) }}
          </div>
      </div>
	</div>
</div>

@php
 $sg = @$rating->applicant->job->plantilla_item->salary_grade->Name;
@endphp

<div class="row">
	<div class="col-6">
		<p class="font-weight-bold">B. TRAINING</p>
		<p class="mb-1 pb-1">(only relevant training shall be considered)</p>
		<table class="table">
			<tbody>
					<tr class="first {{ ($sg > 9 && $sg < 14) ? '' : 'd-none' }}">
						<td colspan="2" class="pb-3 pt-2">
							(SG 10 to 13)
						</td>
					</tr>
					<tr class="first {{ ($sg > 9 && $sg < 14) ? '' : 'd-none' }}">
						<td class="width-em-28">26 Hours and Above</td>
						<td>10%</td>
					</tr>
					<tr class="first {{ ($sg > 9 && $sg < 14) ? '' : 'd-none' }}">
						<td class="width-em-28">16-24 Hours</td>
						<td>9%</td>
					</tr>
					<tr class="first {{ ($sg > 9 && $sg < 14) ? '' : 'd-none' }}">
						<td class="width-em-28">12-24 Hours</td>
						<td>8%</td>
					</tr>
					<tr class="first {{ ($sg > 9 && $sg < 14) ? '' : 'd-none' }}">
						<td class="width-em-28">4-8 Hours</td>
						<td>7%</td>
					</tr>
 
				<tr class="second {{ ($sg > 13 && $sg < 18) ? '' : 'd-none' }}">
					<td colspan="2" class="pb-3 pt-2">
						(SG 14 to 17)
					</td>
				</tr>
				<tr class="second {{ ($sg > 13 && $sg < 18) ? '' : 'd-none' }}">
					<td class="width-em-28">36 Hours and Above</td>
					<td>10%</td>
				</tr>
				<tr class="second {{ ($sg > 13 && $sg < 18) ? '' : 'd-none' }}">
					<td class="width-em-28">26-32 Hours</td>
					<td>9%</td>
				</tr>
				<tr class="second {{ ($sg > 13 && $sg < 18) ? '' : 'd-none' }}">
					<td class="width-em-28">12-24 Hours</td>
					<td>8%</td>
				</tr>
				<tr class="second {{ ($sg > 13 && $sg < 18) ? '' : 'd-none' }}">
					<td class="width-em-28">4-8 Hours</td>
					<td>7%</td>
				</tr>

				<tr class="third {{ ($sg > 17) ? '' : 'd-none' }}">
					<td colspan="2" class="pb-3 pt-2">
						(SG 18 to 23)
					</td>
				</tr>
				<tr class="third {{ ($sg > 17) ? '' : 'd-none' }}">
					<td class="width-em-28">41 Hours and Above</td>
					<td>10%</td>
				</tr>
				<tr class="third {{ ($sg > 17) ? '' : 'd-none' }}">
					<td class="width-em-28">32-40 Hours</td>
					<td>9%</td>
				</tr>
				<tr class="third {{ ($sg > 17) ? '' : 'd-none' }}">
					<td class="width-em-28">20-28 Hours</td>
					<td>8%</td>
				</tr>
				<tr class="third {{ ($sg > 17) ? '' : 'd-none' }}">
					<td class="width-em-28">8-16 Hours</td>
					<td>7%</td>
				</tr>
				
			</tbody>
		</table>
	</div>

	<div class="col-3">
		<div class="form-group row">
          <div class="col-12">
          	{{ Form::text('training_points_2', @$rating->training_points, [
          		'class' => 'form-control form-control-sm input text-right',
          		'placeholder' => '0.00'
          	]) }}
          </div>
      </div>
	</div>

	<div class="col-3">
		<div class="form-group row">
          <div class="col-12">
          	<div id="training_remarks_2" name="training_remarks"></div>
                {{ Form::textarea('training_remarks_2', @$rating->training_remarks,['id'=>'training_remarks_2-text', 'class'=>'d-none']) }}
          </div>
      </div>
	</div>
</div>

<div class="row mb-4">
	<div class="col-6">
		<p class="font-weight-bold">C. EXPERIENCE</p>
		<p class="mb-1 pb-1">(only relevant training shall be considered)</p>
		<table class="table">
			<tbody>
				<tr class="first {{ ($sg > 9 && $sg < 14) ? '' : 'd-none' }}">
					<td colspan="2" class="pb-3 pt-2">
						(SG 10 to 13)
					</td>
				</tr>
				<tr class="first {{ ($sg > 9 && $sg < 14) ? '' : 'd-none' }}">
					<td class="width-em-28">5 years or more </td>
					<td>20%</td>
				</tr>
				<tr class="first {{ ($sg > 9 && $sg < 14) ? '' : 'd-none' }}">
					<td class="width-em-28">4 years</td>
					<td>19%</td>
				</tr>
				<tr class="first {{ ($sg > 9 && $sg < 14) ? '' : 'd-none' }}">
					<td class="width-em-28">3 years</td>
					<td>18%</td>
				</tr>
				<tr class="first {{ ($sg > 9 && $sg < 14) ? '' : 'd-none' }}">
					<td class="width-em-28">2 years</td>
					<td>17%</td>
				</tr>
				<tr class="first {{ ($sg > 9 && $sg < 14) ? '' : 'd-none' }}">
					<td class="width-em-28">1 years</td>
					<td>16%</td>
				</tr>
				<tr class="first {{ ($sg > 9 && $sg < 14) ? '' : 'd-none' }}">
					<td class="width-em-28">6 months</td>
					<td>15%</td>
				</tr>

				<tr class="second {{ ($sg > 13 && $sg < 18) ? '' : 'd-none' }}">
					<td colspan="2" class="pb-3 pt-2">
						(SG 14 to 17)
					</td>
				</tr>
				<tr class="second {{ ($sg > 13 && $sg < 18) ? '' : 'd-none' }}">
					<td class="width-em-28">6 years or more </td>
					<td>20%</td>
				</tr>
				<tr class="second {{ ($sg > 13 && $sg < 18) ? '' : 'd-none' }}">
					<td class="width-em-28">5 years</td>
					<td>19%</td>
				</tr>
				<tr class="second {{ ($sg > 13 && $sg < 18) ? '' : 'd-none' }}">
					<td class="width-em-28">4 years</td>
					<td>18%</td>
				</tr>
				<tr class="second {{ ($sg > 13 && $sg < 18) ? '' : 'd-none' }}">
					<td class="width-em-28">3 years</td>
					<td>17%</td>
				</tr>
				<tr class="second {{ ($sg > 13 && $sg < 18) ? '' : 'd-none' }}">
					<td class="width-em-28">2 years</td>
					<td>16%</td>
				</tr>
				<tr class="second {{ ($sg > 13 && $sg < 18) ? '' : 'd-none' }}">
					<td class="width-em-28">1 year</td>
					<td>15%</td>
				</tr>

				<tr class="third {{ ($sg > 17) ? '' : 'd-none' }}">
					<td colspan="2" class="pb-3 pt-2">
						(SG 18 to 23)
					</td>
				</tr>
				<tr class="third {{ ($sg > 17) ? '' : 'd-none' }}">
					<td class="width-em-28">7 years or more </td>
					<td>20%</td>
				</tr>
				<tr class="third {{ ($sg > 17) ? '' : 'd-none' }}">
					<td class="width-em-28">6 years</td>
					<td>19%</td>
				</tr>
				<tr class="third {{ ($sg > 17) ? '' : 'd-none' }}">
					<td class="width-em-28">5 years</td>
					<td>18%</td>
				</tr>
				<tr class="third {{ ($sg > 17) ? '' : 'd-none' }}">
					<td class="width-em-28">4 years</td>
					<td>17%</td>
				</tr>
				<tr class="third {{ ($sg > 17) ? '' : 'd-none' }}">
					<td class="width-em-28">3 years</td>
					<td>16%</td>
				</tr>
				<tr class="third {{ ($sg > 17) ? '' : 'd-none' }}">
					<td class="width-em-28">2 year</td>
					<td>15%</td>
				</tr>
			</tbody>
		</table>
	</div>

	<div class="col-3">
		<div class="form-group row">
          <div class="col-12">
          	{{ Form::text('experience_points_2', @$rating->experience_points, [
          		'class' => 'form-control form-control-sm input text-right',
          		'placeholder' => '0.00'
          	]) }}
          </div>
      </div>
	</div>

	<div class="col-3">
		<div class="form-group row">
          <div class="col-12">
          	<div id="experience_remarks_2" name="experience_remarks"></div>
                {{ Form::textarea('experience_remarks_2', @$rating->experience_remarks,['id'=>'experience_remarks_2-text', 'class'=>'d-none']) }}
          </div>
      </div>
	</div>
</div>

<div class="row">
	<div class="col-6 text-center">
		<p class="font-weight-bold">TOTAL POINTS</p>
	</div>
	<div class="col-3">
		<div class="form-group row">
	        <div class="col-12">
	        	{{ Form::text('total_points', @$rating->total_points, [
	        		'class' => 'form-control form-control-sm text-right total_points',
	        		'placeholder' => '0.00',
	        		'readonly' => 'true',
	        	]) }}
	        </div>
	    </div>
	</div>
</div>