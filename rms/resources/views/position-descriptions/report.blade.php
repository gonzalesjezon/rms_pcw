@extends('layouts.print')

@section('css')
<style type="text/css">
	@media print{
	    @page {
	       size: 7in 9.25in;
	       margin: 27mm 16mm 27mm 16mm;
	    }
		.bg-thead{
		  background-color: #cccaca !important;
		}
	}

	.bg-thead{
	  background-color: #cccaca !important;
	}
</style>
@endsection

@section('content')

@php
$status = @$post_desc->applicant->job->status;
@endphp

<div id="reports" style="width: 960px;margin: auto; font-size: 12px;font-family: Arial, Helvetica, sans-serif;">
	<table class="table table-bordered">
		<tbody >
			<tr>
				<td class="font-weight-bold text-center border border-dark p-1" style="vertical-align: middle;" rowspan="2" colspan="3">
					<p class="m-0 pt-1">Republic of the Philippines</p>
					<p class="m-0">POSITION DESCRIPTION FORM</p>
					<p class="m-0">DBM-CSC Form No. 1 </p>
					<p style="font-size: 10px;" class="p-0">(Revised Version No 1, s. 2017)</p>
				</td>
				<td class="font-weight-bold border border-dark p-0 bg-thead bg-report" style="height: 20px !important;" colspan="3">
					<p class="pl-1 m-0">1. POSITION TITLE (as approved by authorized agency) with parenthetical title</p>
				</td>
			</tr>
			<tr>
				<td class="border border-dark p-0 text-center" colspan="6">{!! @$post_desc->applicant->job->plantilla_item->position->Name !!}</td>
			</tr>
			<tr>
				<td class="font-weight-bold border border-dark p-0 bg-thead bg-report"  colspan="3">
					<p class="pl-1 m-0">2 ITEM NUMBER</p>
				</td>
				<td class="font-weight-bold border border-dark p-0 bg-thead bg-report"  colspan="3" >
					<p class="pl-1 m-0">3 SALARY GRADE</p>
				</td>
			</tr>
			<tr>
				<td class="border border-dark" style="height: 5em;" colspan="3" >{!! @$post_desc->applicant->job->plantilla_item->item_number !!}</td>
				<td class="border border-dark" style="height: 5em;" colspan="3">
				 {!! ($status == 'plantilla') ? 'SG '.@$post_desc->applicant->job->plantilla_item->salary_grade->Name : '' !!}</td>
			</tr>
			<tr>
				<td class="font-weight-bold border border-dark p-0 bg-thead bg-report"  colspan="6">
					<p class="pl-1 m-0">4 FOR LOCAL GOVERNMENT POSITION, ENUMERATE GOVERNMENTAL UNIT AND CLASS</p>
				</td>
			</tr>
			<tr>
				<td class="font-weight-bold border-dark p-0" style="vertical-align: top" colspan="6">
					<div class="row">
						<div class="col-4">
							<ul style="list-style: none;" class="mt-2">
								<li><input type="checkbox"> Province</li>
								<li><input type="checkbox"> City</li>
								<li><input type="checkbox"> Municipality</li>
							</ul>
						</div>

						<div class="col-4">
							<ul style="list-style: none;" class="mt-2">
								<li><input type="checkbox"> 1st Class</li>
								<li><input type="checkbox"> 2nd Class</li>
								<li><input type="checkbox"> 3rd Class</li>
								<li><input type="checkbox"> 4th Class</li>
							</ul>
						</div>

						<div class="col-4">
							<ul style="list-style: none;" class="mt-2">
								<li><input type="checkbox"> 5th Class</li>
								<li><input type="checkbox"> 6th Class</li>
								<li><input type="checkbox"> Special</li>
							</ul>
						</div>
					</div>
				</td>
			</tr>
			<tr>
				<td class="font-weight-bold border border-dark p-0 bg-thead bg-report"  colspan="3">
					<p class="pl-1 m-0">5 DEPARTMENT, CORPORATION OR AGENCY/ LOCAL GOVERNMENT</p>
				</td>
				<td class="font-weight-bold border border-dark p-0 bg-thead bg-report"   colspan="3">
					<p class="pl-1 m-0">6 BUREAU OF OFFICE</p>
				</td>
			</tr>
			<tr>
				<td class="border border-dark" style="height: 5em;" colspan="3"></td>
				<td class="border border-dark" style="height: 5em;" colspan="3"></td>
			</tr>
			<tr>
				<td class="font-weight-bold border border-dark p-0 bg-thead bg-report"  colspan="3">
					<p class="pl-1 m-0">7 DEPARTMENT / BRANCH / DIVISION</p>
				</td>
				<td class="font-weight-bold border border-dark p-0 bg-thead bg-report"  colspan="3" >
					<p class="pl-1 m-0">8 WORKSTATION / PLACE OF WORK</p>
				</td>
			</tr>
			<tr>
				<td class="border border-dark" style="height: 5em;" colspan="3"></td>
				<td class="border border-dark" style="height: 5em;" colspan="3"></td>
			</tr>
			<tr>
				<td class="font-weight-bold border border-dark p-0 bg-thead bg-report" >
					<p class="pl-1 m-0">9 PRESENT APPROP ACT</p>
				</td>
				<td class="font-weight-bold border border-dark p-0 bg-thead bg-report" colspan="2">
					<p class="pl-1 m-0">10 PREVIOUS APPROP ACT</p>
				</td>
				<td class="font-weight-bold border border-dark p-0 bg-thead bg-report" colspan="2">
					<p class="pl-1 m-0">11 SALARY AUTHORIZED</p>
				</td>
				<td class="font-weight-bold border border-dark p-0 bg-thead bg-report"  >
					<p class="pl-1 m-0">12 OTHER COMPENSATION</p>
				</td>
			</tr>
			<tr>
				<td class="border border-dark" style="height: 5em;"></td>
				<td class="border border-dark" style="height: 5em;"  colspan="2"></td>
				<td class="border border-dark" style="height: 5em;"  colspan="2">PHP {!! number_format(@$post_desc->applicant->job->plantilla_item->basic_salary ,2) !!} /MONTH</td>
				<td class="border border-dark" style="height: 5em;">
					@if($status == 'plantilla')
						@if(@$post_desc->applicant->job->pera_amount)
						PERA,
						@endif
						@if(@$post_desc->applicant->job->clothing_amount)
						Clothing Allowance
						@endif
					@endif
				</td>
			</tr>
			<tr>
				<td class="font-weight-bold border border-dark p-0 bg-thead bg-report"  colspan="3">
					<p class="pl-1 m-0">13 POSITION TITLE OF IMMEDIATE SUPERVISOR</p>
				</td>
				<td class="font-weight-bold border border-dark p-0 bg-thead bg-report"  colspan="3" >
					<p class="pl-1 m-0">14 POSITION TITLE OF NEXT HIGHER SUPERVISOR</p>
				</td>
			</tr>
			<tr>
				<td class="border border-dark text-center" style="height: 5em;" colspan="3">{!! @$post_desc->immediate_supervisor !!}</td>
				<td class="border border-dark text-center" style="height: 5em;" colspan="3">{!! @$post_desc->higher_supervisor !!}</td>
			</tr>
			<tr>
				<td class="font-weight-bold border border-dark p-0 bg-thead bg-report"  colspan="7">
					<p class="pl-1 m-0">15 POSITION TITLE, AND ITEM OF THOSE DIRECTLY SUPERVISED</p>
				</td>
			</tr>
			<tr>
				<td class="font-weight-bold border border-dark p-0 text-center" colspan="6">
					<p class="pl-1 m-0" style="font-size: 11px;">(if more than seven (7) list only by their item numbers and titles)</p>
				</td>
			</tr>
			<tr>
				<td class="font-weight-bold border border-dark p-0 text-center" colspan="3">
					<p class="pl-1 m-0">POSITION TITLE</p>
				</td>
				<td class="font-weight-bold border border-dark p-0 text-center" colspan="3" >
					<p class="pl-1 m-0">ITEM NUMBER</p>
				</td>
			</tr>
			<tr>
				<td class="border border-dark" style="height: 5em;" colspan="3"></td>
				<td class="border border-dark" style="height: 5em;" colspan="3"></td>
			</tr>
			<tr>
				<td class="font-weight-bold border border-dark p-0 bg-thead bg-report"  colspan="6">
					<p class="pl-1 m-0">16 MACHINE EQUIPMENT, TOOLS, ETC., USED REGULARY IN PERFORMANCE OF WORK</p>
				</td>
			</tr>
			<tr>
				<td class="border border-dark" style="height: 5em;" colspan="6">{!! @$post_desc->used_tools !!}</td>
			</tr>
			<tr>
				<td class="font-weight-bold border border-dark p-0 bg-thead bg-report"  colspan="6">
					<p class="pl-1 m-0">17 CONTACTS / CLIENTS / STAKEHOLDERS</p>
				</td>
			</tr>
			<tr>
				<td class="font-weight-bold border border-dark p-0 text-center bg-thead bg-report" >
					<p class="pl-1 m-0">17a. Internal</p>
				</td>
				<td class="font-weight-bold border border-dark p-0 text-center bg-thead bg-report" >
					<p class="pl-1 m-0">Occasional</p>
				</td>
				<td class="font-weight-bold border border-dark p-0 text-center bg-thead bg-report" >
					<p class="pl-1 m-0">Frequent</p>
				</td>
				<td class="font-weight-bold border border-dark p-0 text-center bg-thead bg-report" >
					<p class="pl-1 m-0">17b. External</p>
				</td>
				<td class="font-weight-bold border border-dark p-0 text-center bg-thead bg-report" >
					<p class="pl-1 m-0">Occasional</p>
				</td>
				<td class="font-weight-bold border border-dark p-0 text-center bg-thead bg-report" >
					<p class="pl-1 m-0">Frequent</p>
				</td>
			</tr>
			<tr>
				<td class="font-weight-bold border border-dark p-0 border-right-0" >
					<ul style="list-style: none;" class="mt-2" >
						<li>Executive Managerial</li>
						<li>Supervisors</li>
						<li>Non - Supervisors</li>
						<li>Staff</li>
					</ul>
				</td>
				<td class="font-weight-bold border border-dark border-right-0 border-left-0 p-0 text-center" >
					<ul style="list-style: none;" class="col-12 mt-2">
						<li><input type="checkbox"></li>
						<li><input type="checkbox"></li>
						<li><input type="checkbox"></li>
						<li><input type="checkbox"></li>
					</ul>
				</td>
				<td class="font-weight-bold border border-dark border-right-0 border-left-0 p-0 text-center" >
					<ul style="list-style: none;" class="col-12 mt-2">
						<li><input type="checkbox"></li>
						<li><input type="checkbox"></li>
						<li><input type="checkbox"></li>
						<li><input type="checkbox"></li>
					</ul>
				</td>
				<td class="font-weight-bold border border-dark border-right-0 border-left-0 p-0" style="vertical-align: top;">
					<ul style="list-style: none;" class="mt-2" >
						<li>General Public</li>
						<li>Other Agencies</li>
						<li>Others (Please Specify)</li>
					</ul>
				</td>
				<td class="font-weight-bold border border-dark border-right-0 border-left-0 p-0 text-center" style="vertical-align: top;" >
					<ul style="list-style: none;" class="col-12 mt-2">
						<li><input type="checkbox"></li>
						<li><input type="checkbox"></li>
					</ul>
				</td>
				<td class="font-weight-bold border border-dark p-0 border-left-0 text-center" style="vertical-align: top;" >
					<ul style="list-style: none;" class="col-12 mt-2">
						<li><input type="checkbox"></li>
						<li><input type="checkbox"></li>
					</ul>
				</td>
			</tr>
			<tr>
				<td class="font-weight-bold border border-dark p-0 bg-thead bg-report"  colspan="6">
					<p class="pl-1 m-0">18 WORKING CONDITION</p>
				</td>
			</tr>
			<tr>
				<td class="font-weight-bold border border-dark p-0 border-right-0" style="vertical-align: top;" >
					<ul style="list-style: none;" class="mt-2" >
						<li>Office Work</li>
						<li>Field Work</li>
					</ul>
				</td>
				<td class="font-weight-bold border border-dark border-right-0 border-left-0 p-0 text-center" style="vertical-align: top;" >
					<ul style="list-style: none;" class="col-12 mt-2">
						<li><input type="checkbox"></li>
						<li><input type="checkbox"></li>
					</ul>
				</td>
				<td class="font-weight-bold border border-dark border-right-0 border-left-0 p-0 text-center" style="vertical-align: top;" >
					<ul style="list-style: none;" class="col-12 mt-2">
						<li><input type="checkbox"></li>
						<li><input type="checkbox"></li>
					</ul>
				</td>
				<td class="font-weight-bold border border-dark border-left-0 p-0" style="vertical-align: top;" colspan="3">
					<ul style="list-style: none;" class="mt-2" >
						<li>Others (Please Specify)</li>
					</ul>
				</td>
			</tr>
			<tr>
				<td class="font-weight-bold border border-dark p-0 bg-thead bg-report"  colspan="6">
					<p class="pl-1 m-0">19 BRIEF DESCRIPTION OF THE GENERAL FUNCTION OF THE UNIT OR SECTION</p>
				</td>
			</tr>
			<tr>
				<td class="border border-dark" style="height: 5em;" colspan="6">{!! @$post_desc->description_function_unit !!}</td>
			</tr>
			<tr>
				<td class="font-weight-bold border border-dark p-0 bg-thead bg-report"  colspan="6">
					<p class="pl-1 m-0">20 BRIEF DESCRIPTION OF THE GENERAL FUNCTION OF THE POSITION (Job Summary)</p>
				</td>
			</tr>
			<tr>
				<td class="border border-dark" style="height: 5em;" colspan="6">{!! @$post_desc->description_function_position !!}</td>
			</tr>
			<tr>
				<td class="font-weight-bold border border-dark p-0 bg-thead bg-report"  colspan="6">
					<p class="pl-1 m-0">21 QUALIFICATION STANDARDS</p>
				</td>
			</tr>
			<tr>
				<td class="font-weight-bold border border-dark p-0 text-center bg-thead bg-report">
					<p class="pl-1 m-0">21a. Education</p>
				</td>
				<td class="font-weight-bold border border-dark p-0 text-center bg-thead bg-report" colspan="2" >
					<p class="pl-1 m-0">21b. Experience</p>
				</td>
				<td class="font-weight-bold border border-dark p-0 text-center bg-thead bg-report" colspan="2" >
					<p class="pl-1 m-0">21c. Training</p>
				</td>
				<td class="font-weight-bold border border-dark p-0 text-center bg-thead bg-report" >
					<p class="pl-1 m-0">21d. Eligibility</p>
				</td>
			</tr>
			<tr>
				<td class="border border-dark" style="height: 5em;">
					{!! @$post_desc->applicant->job->education !!}
				</td>
				<td class="border border-dark" style="height: 5em;"  colspan="2">
					{!! @$post_desc->applicant->job->experience !!}
				</td>
				<td class="border border-dark" style="height: 5em;"  colspan="2">
					{!! @$post_desc->applicant->job->training !!}
				</td>
				<td class="border border-dark" style="height: 5em;">
					{!! @$post_desc->applicant->job->eligibility !!}
				</td>
			</tr>
			<tr>
				<td class="font-weight-bold border border-dark p-0 text-center bg-thead bg-report"  colspan="4">
					<p class="pl-1 m-0">21e. Core Compentencies</p>
				</td>
				<td class="font-weight-bold border border-dark p-0 text-center bg-thead bg-report" colspan="2" >
					<p class="pl-1 m-0">Compentency Level</p>
				</td>
			</tr>
			<tr>
				<td class="border border-dark" style="height: 5em;" colspan="4">{!! @$post_desc->applicant->job->compentency_1!!}</td>
				<td class="border border-dark" style="height: 5em;"  colspan="2">{!! @$post_desc->compentency_1!!}</td>
			</tr>
			<tr>
				<td class="font-weight-bold border border-dark p-0 text-center bg-thead bg-report"  colspan="4">
					<p class="pl-1 m-0">21f. Leadership Compentencies</p>
				</td>
				<td class="font-weight-bold border border-dark p-0 text-center bg-thead bg-report" colspan="2" >
					<p class="pl-1 m-0">Compentency Level</p>
				</td>
			</tr>
			<tr>
				<td class="border border-dark" style="height: 5em;" colspan="4">{!! @$post_desc->applicant->job->compentency_2!!}</td>
				<td class="border border-dark" style="height: 5em;"  colspan="2">{!! @$post_desc->compentency_2!!}</td>
			</tr>
			<tr>
				<td class="font-weight-bold border border-dark p-0 text-center bg-thead bg-report"  colspan="4">
					<p class="pl-1 m-0">22 STATEMENT OF DUTIES AND RESPONSIBILITIES(Technical Compentencies)</p>
				</td>
				<td class="font-weight-bold border border-dark p-0 text-center bg-thead bg-report" colspan="2" >
					<p class="pl-1 m-0">Compentency Level</p>
				</td>
			</tr>
			<tr>
				<td class="border border-dark p-0 text-center"  colspan="2">
					<p class="pl-1 m-0">Percentage of Working Time</p>
				</td>
				<td class="border border-dark"  colspan="2"></td>
				<td class="border border-dark border-bottom-0" colspan="2">{!! @$post_desc->responsibilities!!}</td>
			</tr>
			<tr>
				<td class="border border-dark" style="height: 5em;" colspan="2">{!! @$post_desc->percentage_work_time!!}</td>
				<td class="border border-dark" style="height: 5em;" colspan="2"></td>
				<td class="border border-dark border-top-0" style="height: 5em;" colspan="2"></td>
			</tr>
			<tr>
				<td class="font-weight-bold border border-dark p-0 bg-thead bg-report"  colspan="6">
					<p class="pl-1 m-0">23 ACKNOWLEDGEMENT AND ACCEPTANCE </p>
				</td>
			</tr>
			<tr>
				<td class="border border-dark border-bottom-0" style="height: 6em; vertical-align: middle;" colspan="6" >
					<p style="text-indent: 3em;">I have received a copy of this position description. It has been discussed with me and I have freely chosen to comply with performance and behavior/conduct expectations contained herein.</p>
				</td>
			</tr>
			<tr>
				<td class="border border-dark border-top-0 border-right-0 text-center" colspan="3" >
					&nbsp;
					<p class="border border-top-1 border-left-0 border-right-0 border-bottom-0 border-dark">Employers Name, Date and Signature</p>
				</td>
				<td class="border border-dark border-top-0 border-left-0 text-center" colspan="3" >
					{!! @$post_desc->supervisor_name!!} &nbsp;
					<p class="border border-top-1 border-left-0 border-right-0 border-bottom-0 border-dark">Supervisors Name, Date and Signature</p>
				</td>
			</tr>
		</tbody>
	</table>

</div>

  <div class="form-group row text-right">
    <div class="col col-sm-10 col-lg-9 offset-sm-1 offset-lg-3 d-print-none">
      {{ Form::button('Print', ['id' => 'evaluation-report', 'class'=>'btn btn-primary btn-space', 'type'=>'submit']) }}
      {{ Form::reset('Cancel', ['class'=>'btn btn-space btn-danger']) }}
    </div>
  </div>
@endsection

@section('scripts')
<script type="text/javascript">
  $(document).ready(function() {
    $('#evaluation-report').click(function() {
      window.print();
    });
  });
</script>
@endsection