@extends('layouts.app')

@section('content')
    <div class="page-head">
        <h2 class="page-head-title">Edit Position Description</h2>
    </div>

    <!-- Applicant Form -->
    <div class="row">
        <div class="col-md-12">
            <div class="card card-border-color card-border-color-primary">
                <div class="card-header card-header-divider">
                    <span class="card-subtitle">You can edit an position description in the form below.</span></div>
                <div class="card-body">
                    @include('position-descriptions._form', [
                        'action'     => ['PositionDescriptionController@update', $form->id],
                        'method'     => 'PATCH',
                        'form' 			 => $form,
                        'option_1'   => $option
                    ])
                </div>
            </div>
        </div>
    </div>
@endsection
