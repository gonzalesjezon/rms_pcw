<script src="{{ URL::asset('beagle-assets/lib/moment.js/min/moment.min.js')}}"></script>
<script src="{{ URL::asset('beagle-assets/lib/datetimepicker/js/bootstrap-datetimepicker.min.js') }}"
        type="text/javascript"></script>
<script src="{{ URL::asset('beagle-assets/lib/summernote/summernote-bs4.min.js') }}"
        type="text/javascript"></script>
<script src="{{ URL::asset('beagle-assets/lib/summernote/summernote-ext-beagle.js') }}"
        type="text/javascript"></script>
<script src="{{ URL::asset('beagle-assets/js/app-form-wysiwyg.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('beagle-assets/lib/select2/js/select2.min.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('beagle-assets/lib/select2/js/select2.full.min.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('beagle-assets/lib/bootstrap-slider/bootstrap-slider.min.js') }}"
        type="text/javascript"></script>
<script src="{{ URL::asset('beagle-assets/js/app-form-elements.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('beagle-assets/lib/parsley/parsley.min.js') }}" type="text/javascript"></script>
<script type="text/javascript">
  $(document).ready(function() {
    //initialize the javascript
    App.init();
    App.formElements();
    $('#job-form').parsley(); //frontend validation

    $('#plantilla_item_id').change(function() {
      position = $(this).find(':selected').data('position');
      office   = $(this).find(':selected').data('office');
      division = $(this).find(':selected').data('division');
      salary_grade = $(this).find(':selected').data('salary_grade');
      basic_salary = $(this).find(':selected').data('basic_salary');
      annual_salary = $(this).find(':selected').data('annual_salary');

      $('#position').val(position);
      $('#office').val(office);
      $('#division').val(division);
      $('#salary_grade').val(salary_grade);
      $('#basic_salary').val(basic_salary);
      $('#annual_salary').val(annual_salary);
      $('#mid_year').val(basic_salary);
      $('#year_end').val(basic_salary);
    });

    $('#plantilla_item_id').trigger('change');

    let data = {
      '#education': `{!! @$job->education !!}`,
      '#experience': `{!! @$job->experience !!}`,
      '#training': `{!! @$job->training !!}`,
      '#eligibility': `{!! @$job->eligibility !!}`,
      '#compentency_1': `{!! @$job->compentency_1 !!}`,
      '#compentency_2': `{!! @$job->compentency_2 !!}`,
      '#compentency_3': `{!! @$job->compentency_3 !!}`,
      '#csc_education': `{!! @$job->csc_education !!}`,
      '#csc_work_experience': `{!! @$job->csc_work_experience !!}`,
      '#csc_eligibility': `{!! @$job->csc_eligibility !!}`,
      '#csc_training': `{!! @$job->csc_training !!}`,
    };

    // initialize editors for each data element
    App.textEditors(Object.keys(data));

    // when validation fails, get data from hidden input texts
    // and set as value for wysiwyg editor
    for (var key in data) {
      if (data.hasOwnProperty(key)) {
        if (data[key] === '') {
          data[key] = $(`${key}-text`).html();
        }
        setData(key, data[key]);
      }
    }

    // set data for wysiwyg editors
    function setData(selector = '', data = '') {
      $(selector).next('.note-editor').find('.note-editing-area > .note-editable').html(data);
    }

    // $('.note-toolbar').remove();
    // on form submit, get data from wysiwyg editors
    // pass to hidden input elements
    $('#job-submit').click(function() {
      for (let key in data) {
        if (data.hasOwnProperty(key)) {
          let element = getData(key);
          $(`${key}-text`).val(element);
        }
      }
    });

    function getData(selector = '') {
      let data = $(selector).next('.note-editor').find('.note-editing-area > .note-editable');
      return data.html();
    }

    // delete form data on clear form
    $('#clear-form').click(function() {
      for (var key in data) {
        if (data.hasOwnProperty(key)) {
          getData(key, '');
        }
      }
    });

    $('#type_personnel').change(function(){
        $('#form_personnel').submit();
    });

  });
</script>