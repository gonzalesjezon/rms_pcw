@extends('layouts.print')

@section('css')
<style type="text/css">
	.v-top{
		vertical-align: top !important;
	}

	.v-middle{
		vertical-align: middle !important;
	}
</style>
@endsection

@section('content')
 <div class="form-group row text-right d-print-none">
    <div class="col col-sm-10 col-lg-9 offset-sm-1 offset-lg-3">
      {{ Form::button('Print', ['id' => 'evaluation-report', 'class'=>'btn btn-primary btn-space', 'type'=>'submit']) }}
      {{ Form::reset('Cancel', ['class'=>'btn btn-space btn-danger']) }}
    </div>
  </div>

<div class="reports" style="width: 760px;margin: auto;font-size: 12px;font-family: Arial, Helvetica, sans-serif;">

		<div class="row mb-4" >
			<div class="col-12">
				<img src="{{ asset('img/header.png') }}" height="96">
			</div>
		</div>

		<div class="row mb-2">
			<div class="col-12">
				<table class="table table-bordered" style="width: 812px;">
					<thead>
						<tr class="text-center">
							<th colspan="7" >
								<h4 class="font-weight-bold">Publication of Vacant Position</h4>
							</th>
						</tr>
						<tr class="text-center">
							<th class="v-middle">Position Title</th>
							<th class="v-middle">Item Number</th>
							<th class="v-middle">Salary Grade</th>
							<th class="v-middle">Compensation</th>
							<th class="v-middle">Status of Employment</th>
							<th class="v-middle">Reporting Line</th>
							<th class="v-middle">Division/Section</th>
						</tr>
					</thead>
					<tbody>
						<tr class="text-center">
							<td class="v-top"  nowrap >{!! @$jobs->plantilla_item->position->Name  !!}</td>
							<td class="v-top" nowrap >{!! @$jobs->plantilla_item->item_number !!}</td>
							<td class="v-top" nowrap >{!! @$jobs->plantilla_item->salary_grade->Name !!}</td>
							<td class="v-top">
								@if($jobs->status == 'plantilla')
	                          	{{ number_format($jobs->plantilla_item->basic_salary,2)}} / month <br>
	                         	plus {{ number_format($jobs->pera_amount,2) }} / PERA
	                         	@else
	                         	{{ number_format($jobs->plantilla_item->basic_salary,2)}} / month <br>
	                         	plus 20% premium
	                         	@endif
							</td>
							<td class="v-top">
								{!! @$jobs->plantilla_item->employee_status->Name !!}
							</td>
							<td class="v-top">{!! @$jobs->reporting_line!!}</td>
							<td class="v-top">{!! @$jobs->plantilla_item->division->Name !!}</td>
						</tr>
					</tbody>
				</table>

				@if($jobs->status == 'plantilla')
				<table class="table table-bordered mb-8" style="width: 812px;">
					<thead class="font-weight-bold text-center">
						<tr>
							<th colspan="4">CSC MINIMUM QUALIFICATIONS</th>
						</tr>
						<tr>
							<th>Education</th>
							<th>Training</th>
							<th>Work Experience</th>
							<th>Eligibility</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>{!! @$jobs->csc_education !!}</td>
							<td>{!! @$jobs->csc_training !!}</td>
							<td>{!! @$jobs->csc_work_experience !!}</td>
							<td>{!! @$jobs->csc_eligibility !!}</td>
						</tr>
					</tbody>
				</table>
				@endif

				<table class="table table-bordered" style="width: 812px;">
					<thead>
						<tr class="text-center">
							<th colspan="5" >PREFERED QUALIFICATION</th>
						</tr>
						<tr class="text-center">
							<th>EDUCATION</th>
							<th>TRAINING</th>
							<th>WORK EXPERIENCE</th>
							<th>ELIGIBILITY</th>
							<th>COMPETENCY</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td class="v-top">{!! $jobs->education !!}</td>
							<td class="v-top">{!! $jobs->training !!}</td>
							<td class="v-top">{!! $jobs->experience !!}</td>
							<td class="v-top">{!! $jobs->eligibility !!}</td>
							<td class="v-top">
								@if($jobs->compentency_1)
									<div class="pl-2 text-justify">{!! $jobs->compentency_1 !!}</div>
								@endif
							</td>
						</tr>
					</tbody>
				</table>

				<table class="table table-bordered" style="width: 812px;">
					<tbody>
						<tr>
							<td>
								<h5 class="font-weight-bold" style="font-size: 15px;">Duties and Responsibilities</h5> 

								@if($jobs->compentency_2)
									<p class="font-weight-bold mb-0 pb-0 pl-4">A. General Functions :</p>
									<div class="pl-6 text-justify">{!! $jobs->compentency_2 !!}</div>
								@endif

								<div style="height:2em;"></div>

								@if($jobs->compentency_3)
									<p class="font-weight-bold mb-0 pb-0 pl-4">B. Specific Duties and Responsibilities :</p>
									<div class="pl-6 text-justify">{!! $jobs->compentency_3 !!}</div>
								@endif
							</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>

		<div class="row">
			<div class="col-12">
				<div class="mb-4 font-weight-bold">Interested and qualified applicants may 
					<a href="{{ route('frontend.create',['id'=>$jobs->id ]) }}">Apply Now.</a>
				</div>

				<p class="font-weight-bold mb-0 pb-0">Please also attach following documents through the link on or before {{ date('F, d Y',strtotime($jobs->deadline_date)) }}</p>
				<p class="font-weight-bold ">The assessment process will start the following day</p>

				<div class="mb-1">
			
				<ol>
	              <li>Fully Accomplished Personal Data Sheet (revised 2017); </li>
	              <li>Curriculum Vitae with detailed job description;  </li>
	              <li>Copy of Transcript of Records and Diploma; </li>
	              <li>Copy of Certificate of training/seminars attended; </li>
	              <li>Copy of Certificate of Employment  </li>
	              <li>Performance Evaluation (if applicable) </li>
	              <li>Certificate of eligibility/rating/license (if applicable)  </li>
	            </ol>
	            <b>Address your application letter to:</b>

	            <div class="mt-5">
		            <span class="font-weight-bold">{!! @$jobs->appointer->getFullName() !!}</span> <br>
			          {!! @$jobs->appointer->position->Name !!} <br>
			          Human Resource Management and Development Section <br>
			          Philippine Commission on Women <br>
			          1145 JP Laurel St. San Miguel Manila
		            </div>
				</div>

				<div class="mt-5">
					<p class="text-justify" style="text-indent: 25px;">
						This Office highly encourage all interested and qualified applicants including persons with disablity (PWD)
						members of indigenous communities, and those from any sexual orientation and gender identities (SOGI)
						Philippine Commission on Women (PCW) comiles with Equal Employment Opportunity Policy (EEOP)
						and that No person with disability shall be denied access to opportunities for suitable employment. A
						qualified employee with disability shall be a subject to the same terms conditions of employment and the
						same compensation privilleges, benefits, fringe benefits, incentives or allowances as a qualified able-bodied
						person.
					</p>
				</div>
			</div>
		</div>
	

</div>


@endsection

@section('scripts')
<script type="text/javascript">
  $(document).ready(function() {
    $('#evaluation-report').click(function() {
      window.print();
    });
  });
</script>
@endsection