    @section('css')
    <link rel="stylesheet" type="text/css"
            href="{{ URL::asset('beagle-assets/lib/datetimepicker/css/bootstrap-datetimepicker.min.css') }}">
        <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.min.css"
              rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="{{ URL::asset('beagle-assets/lib/summernote/summernote-bs4.css') }}" />
    @endsection

    {!! Form::open(['action' => 'JobsController@typePersonnel', 'method'=>'GET', 'id'=>'form_personnel']) !!}
    <div class="row">
        <div class="col-6">
            <div class="form-group row">
                <label class="col-sm-3 col-form-label">Type of Personnel</label>
                <div class="col-12 col-sm-8 col-lg-6">
                    <select class="form-control form-control-xs" name="type_personnel" id="type_personnel" {{ (@$job->id) ? 'disabled' : '' }}>
                        <option>Select type</option>
                        <option value="plantilla" {{ ($status == 'plantilla') ? 'selected'  : '' }}>Plantilla</option>
                        <option value="nonplantilla" {{ ($status == 'nonplantilla') ? 'selected'  : '' }}>Nonplantilla</option>
                    </select>
                </div>
            </div>
        </div>
    </div>
    {!! Form::close() !!}

    {!! Form::open(['action' => $action, 'method' => $method ,'id' => 'create-form']) !!}


    <div class="row">
        <div class="col-6">

            <div class="form-group row {{ $errors->has('plantilla_item_id') ? 'has-error' : ''}}">
                {{ Form::label('', 'Plantilla Item No.', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
                <div class="col-12 col-sm-8 col-lg-6">
                    <select class="form-control form-control-xs" id="plantilla_item_id" name="plantilla_item_id" {{ (@$job->id) ? 'disabled' : '' }}>
                        <option value="0">Select </option>
                        @foreach($items as $item)
                        <option value="{{ @$item->id }}" data-position="{{ @$item->position->Name }}" data-office="{{ @$item->office->Name }}" data-division="{{ @$item->division->Name }}" data-division="{{ @$item->division->Name }}" data-salary_grade="{{ @$item->salary_grade->Name }}" data-basic_salary="{{ number_format(@$item->basic_salary,2) }}" data-annual_salary="{{ number_format(@$item->basic_salary * 12,2) }}" {{ ( @$item->id == @$job->plantilla_item_id) ? 'selected' : '' }} >{!! @$item->item_number !!}</option>
                        @endforeach
                    </select>
                    {!! $errors->first('plantilla_item_id', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
                </div>
            </div>

            <div class="form-group row">
                {{ Form::label('', 'Position Title.', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
                <div class="col-12 col-sm-8 col-lg-6">
                    {{ Form::text('', '', [
                        'class' => 'form-control form-control-sm',
                        'readOnly' => true,
                        'id' => 'position'
                    ])
                    }}
                </div>
            </div>

            <div class="form-group row">
                {{ Form::label('', 'Office', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
                <div class="col-12 col-sm-8 col-lg-6">
                    {{ Form::text('', '',[
                            'class' => 'form-control form-control-xs',
                            'readOnly' => true,
                            'id' => 'office'

                        ])
                    }}
                </div>
            </div>

            <div class="form-group row">
                {{ Form::label('', 'Division', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
                <div class="col-12 col-sm-8 col-lg-6">
                    {{ Form::text('', '',[
                            'class' => 'form-control form-control-xs',
                            'readOnly' => true,
                            'id' => 'division'
                        ])
                    }}
                </div>
            </div>
        </div>

        <div class="col-6">
            <div class="form-group row {{ ($status != 'plantilla') ? 'd-none' : '' }} ">
                {{ Form::label('', 'Salary Grade', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
                <div class="col-12 col-sm-8 col-lg-6">
                    {{ Form::text('', '', [
                        'class' => 'form-control form-control-sm',
                        'readOnly' => true,
                        'id' => 'salary_grade'
                    ])
                    }}
                </div>
            </div>

            <div class="form-group row {{ $errors->has('monthly_basic_salary') ? 'has-error' : ''}}">
                {{ Form::label('monthly_basic_salary', ($status == 'plantilla') ? 'Monthly Basic Salary' : 'Professional Fees', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
                <div class="col-xl-6 col-sm-8 col-lg-6">
                    <input type="text"  id="basic_salary"  class="form-control form-control-sm" placeholder="PHP 0" readonly>
                </div>
            </div>

            <div class="form-group row {{ $errors->has('annual_basic_salary') ? 'has-error' : ''}} {{ ($status != 'plantilla') ? 'd-none' : '' }}">
                {{ Form::label('annual_basic_salary', 'Annual Basic Salary', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
                <div class="col-xl-6 col-sm-8 col-lg-6">
                    {{ Form::text('', '', [
                        'class' => 'form-control form-control-sm',
                        'placeholder' => 'PHP 0',
                        'readOnly' => true,
                        'id' => 'annual_salary'
                    ])
                    }}
                </div>
            </div>

             <div class="form-group row {{ $errors->has('reporting_line') ? 'has-error' : ''}}">
                {{ Form::label('reportling_line', 'Reporting Line', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
                <div class="col-xl-6 col-sm-8 col-lg-6">
                    <input type="text" name="reporting" id="reporting" value="{{@$job->reporting_line}}" class="form-control form-control-sm" >
                    {!! $errors->first('reporting_line', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
                </div>
            </div>

            <div class="form-group row {{ $errors->has('station') ? 'has-error' : ''}}">
                {{ Form::label('station', 'Station', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
                <div class="col-xl-6 col-sm-8 col-lg-6">
                    <input type="text" name="station" id="station_line" value="{{@$job->station}}" class="form-control form-control-sm">
                    {!! $errors->first('station', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
                </div>
            </div>

        </div>
    </div>

    <hr>

    <input type="hidden" name="status" value="{{ $status }}">

    @if($status == 'plantilla')


    <div class="row">
        <div class="col-6">

            <div class="form-group row ">
                {{ Form::label('', 'Additional Allowance', ['class' => 'col-12 col-sm-3 col-form-label text-sm-right font-weight-bold']) }}
            </div>

            <div class="form-group row {{ $errors->has('pera_amount') ? 'has-error' : '' }}">
                {{ Form::label('', 'PERA', ['class' => 'col-12 col-sm-3 col-form-label text-sm-right']) }}
                <div class="col-12 col-sm-8 col-lg-6">
                    {{ Form::text('pera_amount', number_format(2000,2),[
                        'class' => 'form-control form-control-sm',
                        'readonly' => true,
                        'placeholder' => 'PHP 0'
                    ]) }}
                </div>
                {!! $errors->first('pera_amount', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
            </div>

            <div class="form-group row {{ $errors->has('clothing_amount') ? 'has-error' : '' }}">
                {{ Form::label('', 'Clothing Allowance', ['class' => 'col-12 col-sm-3 col-form-label text-sm-right']) }}
                <div class="col-12 col-sm-8 col-lg-6">
                    {{ Form::text('clothing_amount', number_format(6000,2),[
                        'class' => 'form-control form-control-sm',
                        'readonly' => true,
                        'placeholder' => 'PHP 0'
                    ]) }}
                </div>
                {!! $errors->first('clothing_amount', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
            </div>

            <div class="form-group row {{ $errors->has('midyear_amount') ? 'has-error' : '' }}">
                {{ Form::label('', 'Mid-Year Bonus', ['class' => 'col-12 col-sm-3 col-form-label text-sm-right']) }}
                <div class="col-12 col-sm-8 col-lg-6">
                    {{ Form::text('midyear_amount', '',[
                        'class' => 'form-control form-control-sm',
                        'required' => true,
                        'placeholder' => 'PHP 0',
                        'readonly' => 'true',
                        'id' => 'mid_year'
                    ]) }}
                </div>
                {!! $errors->first('midyear_amount', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
            </div>

        </div>


        <div class="col-6">

            <div class="form-group row {{ $errors->has('yearend_amount') ? 'has-error' : '' }} mt-8">
                {{ Form::label('', 'Year-End Bonus', ['class' => 'col-12 col-sm-3 col-form-label text-sm-right']) }}
                <div class="col-12 col-sm-8 col-lg-6">
                    {{ Form::text('yearend_amount', '',[
                        'class' => 'form-control form-control-sm',
                        'required' => true,
                        'placeholder' => 'PHP 0',
                        'readonly' => 'true',
                        'id' => 'year_end'
                    ]) }}
                </div>
                {!! $errors->first('yearend_amount', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
            </div>

            <div class="form-group row {{ $errors->has('cashgift_amount') ? 'has-error' : '' }}">
                {{ Form::label('', 'Cash Gift', ['class' => 'col-12 col-sm-3 col-form-label text-sm-right']) }}
                <div class="col-12 col-sm-8 col-lg-6">
                    {{ Form::text('cashgift_amount', @$job->cashgift_amount,[
                        'class' => 'form-control form-control-sm',
                        'placeholder' => 'PHP 0'
                    ]) }}
                </div>
                {!! $errors->first('cashgift_amount', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
            </div>
        </div>
    </div>

    @else


    <div class="row">
        <div class="col-6">
          <div class="form-group row">
            <label class="col-12 col-sm-3 col-form-label"> Duration </label>
            <div class="col-12 col-sm-8 col-lg-6">
                <div data-min-view="2" data-date-format="yyyy-mm-dd" class="input-group date datetimepicker">
                    <input size="16" type="text" value="{{ @$job->duration_from }}" name="duration_from"
                           class="form-control form-control-sm">
                    <div class="input-group-append">
                        <button class="btn btn-primary"><i class="icon-th mdi mdi-calendar"></i></button>
                    </div>
                </div>
            </div>
          </div>
        </div>
    </div>

    <div class="row">
        <div class="col-6">
          <div class="form-group row">
            <label class="col-12 col-sm-3 col-form-label"> To </label>
            <div class="col-12 col-sm-8 col-lg-6">
                <div data-min-view="2" data-date-format="yyyy-mm-dd" class="input-group date datetimepicker">
                    <input size="16" type="text" value="{{ @$job->duration_to }}" name="duration_to"
                           class="form-control form-control-sm">
                    <div class="input-group-append">
                        <button class="btn btn-primary"><i class="icon-th mdi mdi-calendar"></i></button>
                    </div>
                </div>
            </div>
          </div>
        </div>
    </div>

    @endif

    <hr>

    <div class="form-group row ">
        {{ Form::label('', 'DUTIES RESPONSIBILITIES:', ['class' => 'col-12 col-sm-1 col-form-label text-sm-right font-weight-bold']) }}
    </div>


    <div class="row">
        <div class="col-6">
            <div class="form-group row {{ $errors->has('compentency_2') ? 'has-error' : ''}}">
                {{ Form::label('compentency_2', 'General Functions', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right font-weight-bold']) }}
                <div class="col-9">
                    <div id="compentency_2" name="compentency_2"></div>
                    <textarea name="compentency_2" class="d-none"></textarea>
                    <input type="hidden" name="compentency_2" id="compentency_2-text">
                    {!! $errors->first('compentency_2', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
                </div>
            </div>
        </div>

        <div class="col-6">
            <div class="form-group row {{ $errors->has('compentency_3') ? 'has-error' : ''}}">
                <label class="col-12 col-sm-3 col-form-label text-sm-right font-weight-bold">Specific Duties and <br> Responsibilities:</label>
                <div class="col-9">
                    <div id="compentency_3" name="compentency_3"></div>
                    <textarea name="compentency_3" class="d-none"></textarea>
                    <input type="hidden" name="compentency_3" id="compentency_3-text">
                    {!! $errors->first('compentency_3', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
                </div>
            </div>
        </div>
    </div>

    <hr>

    <div class="form-group row ">
        {{ Form::label('', 'PREFERRED QUALIFICATIONS:', ['class' => 'col-12 col-sm-1 col-form-label text-sm-right font-weight-bold']) }}
    </div>

    <div class="row">
        <div class="col-6">
            <div class="form-group row {{ $errors->has('education') ? 'has-error' : ''}}">
                {{ Form::label('education', 'Education', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right font-weight-bold']) }}
                <div class="col-9">
                    <div id="education" name="education"></div>
                    {{ Form::textarea('education', @$job->education,['id'=>'education-text', 'class'=>'d-none']) }}
                    {!! $errors->first('education', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
                </div>
            </div>
        </div>

        <div class="col-6">
            <div class="form-group row {{ $errors->has('experience') ? 'has-error' : ''}}">
                {{ Form::label('experience', 'Work Experience', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right font-weight-bold']) }}
                <div class="col-9">
                    <div id="experience" name="experience"></div>
                    {{ Form::textarea('experience', @$job->experience,['id'=>'experience-text', 'class'=>'d-none']) }}
                    {!! $errors->first('experience', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-6">
            <div class="form-group row {{ $errors->has('training') ? 'has-error' : ''}}">
                {{ Form::label('training', 'Training', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right font-weight-bold']) }}
                <div class="col-9">
                    <div id="training" name="training"></div>
                    {{ Form::textarea('training', @$job->training,['id'=>'training-text', 'class'=>'d-none']) }}
                    {!! $errors->first('training', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
                </div>
            </div>
        </div>

        <div class="col-6">
            <div class="form-group row {{ $errors->has('eligibility') ? 'has-error' : ''}}">
                <span class="col-12 col-sm-3 col-form-label text-sm-right font-weight-bold"> Eligibility </span>
                <div class="col-9">
                    <div id="eligibility" name="eligibility"></div>
                    {{ Form::textarea('eligibility', @$job->eligibility,['id'=>'eligibility-text', 'class'=>'d-none']) }}
                    {!! $errors->first('eligibility', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-6">
            <div class="form-group row {{ $errors->has('compentency_1') ? 'has-error' : ''}}">
                {{ Form::label('compentency_1', 'Competency', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right font-weight-bold']) }}
                <div class="col-9">
                    <div id="compentency_1" name="compentency_1"></div>
                    <textarea name="compentency_1" class="d-none"></textarea>
                    <input type="hidden" name="compentency_1" id="compentency_1-text">
                    {!! $errors->first('compentency_1', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
                </div>
            </div>
        </div>
    </div>

    <hr>

    <div class="form-group row {{ ($status == 'nonplantilla') ? 'd-none' : '' }}">
        {{ Form::label('', 'CSC MINIMUM QUALIFICATIONS', ['class' => 'col-12 col-sm-1 col-form-label text-sm-right font-weight-bold']) }}
    </div>

    <div class="row {{ ($status == 'nonplantilla') ? 'd-none' : '' }}">
        <div class="col-6">
            <div class="form-group row {{ $errors->has('csc_education') ? 'has-error' : ''}}">
                {{ Form::label('csc_education', 'Education', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right font-weight-bold']) }}
                <div class="col-9">
                    <div id="csc_education" name="csc_education"></div>
                    {{ Form::textarea('csc_education', @$job->csc_education,['id'=>'csc_education-text', 'class'=>'d-none']) }}
                    {!! $errors->first('csc_education', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
                </div>
            </div>
        </div>

        <div class="col-6">
            <div class="form-group row {{ $errors->has('csc_work_experience') ? 'has-error' : ''}}">
                <span class="col-12 col-sm-3 col-form-label text-sm-right font-weight-bold"> Work Experience </span>
                <div class="col-9">
                    <div id="csc_work_experience" name="csc_work_experience"></div>
                    {{ Form::textarea('csc_work_experience', @$job->csc_work_experience,['id'=>'csc_work_experience-text', 'class'=>'d-none']) }}
                    {!! $errors->first('csc_work_experience', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
                </div>
            </div>
        </div>
    </div>

    <div class="row {{ ($status == 'nonplantilla') ? 'd-none' : '' }}">
        <div class="col-6">
            <div class="form-group row {{ $errors->has('csc_eligibility') ? 'has-error' : ''}}">
                {{ Form::label('csc_eligibility', 'Eligibility', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right font-weight-bold']) }}
                <div class="col-9">
                    <div id="csc_eligibility" name="csc_eligibility"></div>
                    {{ Form::textarea('csc_eligibility', @$job->csc_eligibility,['id'=>'csc_eligibility-text', 'class'=>'d-none']) }}
                    {!! $errors->first('csc_eligibility', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
                </div>
            </div>
        </div>

        <div class="col-6">
            <div class="form-group row {{ $errors->has('csc_training') ? 'has-error' : ''}}">
                <span class="col-12 col-sm-3 col-form-label text-sm-right font-weight-bold"> Training </span>
                <div class="col-9">
                    <div id="csc_training" name="csc_training"></div>
                    {{ Form::textarea('csc_training', @$job->csc_training,['id'=>'csc_training-text', 'class'=>'d-none']) }}
                    {!! $errors->first('csc_training', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
                </div>
            </div>
        </div>
    </div>


    <div class="row">
        <div class="col-5">
            <div class="form-group row">
                <label class="col-12 col-sm-4 col-form-label text-sm-right font-weight-bold">Publish</label>
                <div class="col-12 col-sm-8 col-lg-6 pt-1">
                    <div class="switch-button switch-button-success switch-button-yesno">
                        <input type="checkbox" name="publish" id="swt8" {{ @$job->publish ? 'checked' : '' }}><span>
                                            <label for="swt8"></label></span>
                    </div>
                </div>
            </div>

            <div class="form-group row">
                <label class="col-12 col-sm-4 col-form-label text-sm-right"> Deadline Date </label>
                <div class="col-12 col-sm-6">
                    <div data-min-view="2" data-date-format="yyyy-mm-dd" class="input-group date datetimepicker">
                        <input size="16" type="text" value="{{ @$job->deadline_date }}" name="deadline_date"
                               class="form-control form-control-sm">
                        <div class="input-group-append">
                            <button class="btn btn-primary"><i class="icon-th mdi mdi-calendar"></i></button>
                        </div>
                    </div>
                </div>
            </div>

            <div class="form-group row">
                <label class="col-12 col-sm-4 col-form-label text-sm-right"> Application address to: </label>
                <div class="col-12 col-sm-6">
                    <select class="form-control form-control-xs" name="appointer_id">
                        @foreach($employees as $employee)
                            @if($employee)
                            <option value="{{ $employee->RefId }}" {{ ($employee->RefId == @$job->appointer_id) ? 'selected' : '' }}>{!! $employee->getFullName() !!} </option>
                            @endif
                        @endforeach
                    </select>
                </div>
            </div>
        </div>

        <div class="col-7">

            <div class="form-group row">
                {{ Form::label('', 'Publications', ['class'=>'col-12 col-sm-2 col-form-label text-sm-right font-weight-bold']) }}
                <div class="col-10">
                    <div class="row">
                        <label class="col-12 col-sm-4 col-form-label text-sm-right">Agency Website</label>
                        <div class="col-12 col-sm-8 col-lg-6 pt-1">
                            <div class="switch-button switch-button-success switch-button-yesno">
                                <input type="checkbox" name="publication_1" id="swt9" {{ @$job->publication_1 ? 'checked' : '' }}><span>
                                                    <label for="swt9"></label></span>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <label class="col-12 col-sm-4 col-form-label text-sm-right">Newspaper</label>
                        <div class="col-12 col-sm-8 col-lg-6 pt-1">
                            <div class="switch-button switch-button-success switch-button-yesno">
                                <input type="checkbox" name="publication_2" id="swt10" {{ @$job->publication_2 ? 'checked' : '' }}><span>
                                                    <label for="swt10"></label></span>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <label class="col-12 col-sm-4 col-form-label text-sm-right">CSC Bulletin</label>
                        <div class="col-12 col-sm-8 col-lg-2 pt-1">
                            <div class="switch-button switch-button-success switch-button-yesno">
                                <input type="checkbox" name="publication_3" id="swt11" {{ @$job->publication_3 ? 'checked' : '' }}><span>
                                                    <label for="swt11"></label></span>
                            </div>
                        </div>
                        <label class="col-12 col-sm-2 col-form-label text-left pl-0">Approved Date</label>
                        <div class="col-4">
                            <div data-min-view="2" data-date-format="yyyy-mm-dd" class="input-group date datetimepicker">
                                <input size="16" type="text" value="{{ @$job->approved_date }}" name="approved_date"
                                       class="form-control form-control-sm">
                                <div class="input-group-append">
                                    <button class="btn btn-primary"><i class="icon-th mdi mdi-calendar"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <label class="col-12 col-sm-4 col-form-label text-sm-right">Other</label>
                        <div class="col-12 col-sm-8 col-lg-2 pt-1">
                            <div class="switch-button switch-button-success switch-button-yesno">
                                <input type="checkbox" name="publication_4" id="swt12" {{ @$job->publication_4 ? 'checked' : '' }}><span>
                                                    <label for="swt12"></label></span>
                            </div>
                        </div>
                        <label class="col-12 col-sm-2 col-form-label text-left pl-0">Pls. Specify</label>
                        <div class="col-4">
                            <input type="text" name="other_specify" id="other_specify" value="{{@$job->other_specify}}" class="form-control form-control-sm" >
                        </div>
                    </div>
                </div>
            </div>


        </div>
    </div>

    <hr>
    <div class="form-group row text-right">
        <div class="col col-sm-12 ">
            {{ Form::submit('Submit', ['id' => 'job-submit', 'class'=>'btn btn-primary btn-space']) }}
            {{ Form::reset('Clear Form', ['id'=>'clear-form', 'class'=>'btn btn-space btn-danger']) }}
        </div>
    </div>
    {!! Form::close() !!}

    @section('scripts')
        @include('jobs._form-script')
    @endsection
