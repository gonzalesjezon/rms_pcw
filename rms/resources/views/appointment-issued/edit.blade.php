@extends('layouts.app')

@section('content')
    <div class="page-head">
        <h2 class="page-head-title">Edit Issued/Transmital</h2>
    </div>

    <!-- Applicant Form -->
    <div class="row">
        <div class="col-md-12">
            <div class="card card-border-color card-border-color-primary">
                <div class="card-header card-header-divider">
                    <span class="card-subtitle">You can edit an appointment issued in the form below.</span></div>
                <div class="card-body">
                    @include('appointment-issued._form', [
                        'action' => ['AppointmentIssuedController@update', $form->id],
                        'method' => 'PATCH',
                        'form' => $form,
                    ])
                </div>
            </div>
        </div>
    </div>
@endsection
