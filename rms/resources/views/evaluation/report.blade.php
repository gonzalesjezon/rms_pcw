@extends('layouts.print')

@section('css')
@endsection

@section('content')
  <div class="row mb-4">
    <div class="col-sm-3"><img src="{{URL::asset('img/pcc-logo-small.png')}}" class="img-fluid" alt="pcc logo" style="height: 120px;"/></div>
  </div>

  <div class="row mb-4 text-center">
    <div class="col-sm-12"><h2><b>INDIVIDUAL ASSESSMENT FORM</b></h2></div>
  </div>

  <div class="row mb-4 text-left">
    <div class="col-2"><b>Name</b></div>
    <div class="col-3 text-danger">: {{$applicant->getFullname()}}</div>
  </div>

  <div class="row mb-4 text-left">
    <div class="col-2"><b>Present Position</b></div>
    <div class="col-3 text-danger">: (PRESENT POSITION)</div>
  </div>

  <div class="row mb-4 text-left">
    <div class="col-2"><b>Division/Office</b></div>
    <div class="col-3 text-danger">: {{$applicant->job->division}}</div>
  </div>

  <div class="row mb-4 text-left">
    <div class="col-2"><b>Vacant Position/JG/Div./Office</b></div>
    <div class="col-3 text-danger">: {{$applicant->job->title}}</div>
  </div>

  <div class="row mb-4 text-left">
    <div class="col-2"><b>CSC Qualification Standards</b></div>
    <div class="col-3 text-danger">:</div>
  </div>

  <div class="row mb-4 text-left">
    <div class="offset-1 col-1">Education</div>
    <div class="col-8 text-danger">: {!! $applicant->job->education !!}</div>
  </div>

  <div class="row mb-4 text-left">
    <div class="offset-1 col-1">Experience</div>
    <div class="col-8 text-danger">: {!! $applicant->job->experience !!}</div>
  </div>

  <div class="row mb-4 text-left">
    <div class="offset-1 col-1">Eligibility</div>
    <div class="col-8 text-danger">: {!! $applicant->job->eligibility !!}</div>
  </div>

  <div class="row mb-4 text-left">
    <div class="offset-1 col-1">Training</div>
    <div class="col-8 text-danger">: {!! $applicant->job->training !!}</div>
  </div>

  <div class="row mb-4 text-left">
    <div class="col-2"><b>Evaluation Made as of:</b></div>
    <div class="col-3 text-danger">: {{@$evaluation->created_at}}</div>
  </div>

  <div class="row">
    <div class="col text-light bg-secondary">I. PERFORMANCE (40%)</div>
  </div>

  <div class="form-group row">
    <div class="col">For Transferees:</div>
  </div>

  <div class="form-group row">
    <div class="col">
      {{ @$evaluation->performance }}
      <hr>
      <label for="">Rating 1 + Rating 2</label>
    </div>

    <label for=""><h4 class="mt-1">/</h4></label>
    <div class="col">
      {{ @$evaluation->performance_divide }}
      <hr>
    </div>

    <label for=""><h4 class="mt-1">=</h4></label>
    <div class="col">
      {{ @$evaluation->performance_average }}
      <hr>
      <label for="">Average Rating</label>
    </div>

    <div class="offset-1"><h4 class="mt-1">X</h4></div>
    <div class="col">
      {{ @$evaluation->performance_percent}}
      <hr>
      <label for="">POINTS WEIGHT</label>
    </div>

    <label for=""><h4 class="mt-1">=</h4></label>
    <div class="col">
      {{ @$evaluation->performance_score}}
      <hr>
    </div>
  </div>

  <div class="form-group row">
    <div class="col">
      <label for="">* For Non-Transferee, a grade of Satisfactory rating is given.</label>
    </div>
  </div>

  <!-- Education & Training -->
  <div class="form-group row">
    <div class="col text-light bg-secondary">II. EDUCATION & TRAINING (20%)</div>
  </div>

  <div class="form-group row">
    {{ Form::label('eligibility', 'Eligibility', ['class'=>'col-2 mt-2']) }}
    <div class="col-4">
      {{ @$evaluation->eligibility}}
    </div>
  </div>

  <div class="form-group row">
    {{ Form::label('training', 'Training', ['class'=>'col-2 mt-2']) }}
    <div class="col-4">
      {{ @$evaluation->training}}
    </div>
  </div>

  <div class="form-group row">
    {{ Form::label('Seminar', 'Seminar', ['class'=>'col-2 mt-2']) }}
    <div class="col-4">
      {{ @$evaluation->seminar}}
    </div>
  </div>

  <div class="form-group row">
    {{ Form::label('minimum_education_points', 'Minimum Educational Requirement', ['class'=>'col-2 mt-2']) }}
    <div class="col-1">
      {{ @$evaluation->minimum_education_points}}
    </div>
    <div class="col-2">PTS</div>
  </div>

  <div class="form-group row">
    {{ Form::label('minimum_training_points', 'Minimum Training Requirement', ['class'=>'col-2 mt-2']) }}
    <div class="col-1">
      {{ @$evaluation->minimum_training_points}}
    </div>
    <div class="col-2">PTS</div>
  </div>

  <div class="form-group row">
    {{ Form::label('ratings_excess', 'Ratings in Excess of the Minimum:', ['class'=>'col-2 mt-3 font-weight-bold']) }}
  </div>

  <div class="form-group row">
    {{ Form::label('education_points', 'Education', ['class'=>'col-2 mt-2']) }}
    <label for=""><h4 class="mt-3 font-weight-bold">=</h4></label>
    <div class="col-2">
      {{ @$evaluation->education_points}}
      <hr>
      <div>Points Rating</div>
    </div>
  </div>

  <div class="form-group row">
    {{ Form::label('training_points', 'Training', ['class'=>'col-2 mt-2']) }}
    <label for=""><h4 class="mt-3 font-weight-bold">=</h4></label>
    <div class="col-2">
      {{ @$evaluation->training_points}}
      <hr>
      <div>Points Rating</div>
    </div>

    <label for="" class="offset-1 mt-3">=</label>
    <div class="col-2">
      {{ @$evaluation->education_training_total_points}}
      <hr>
      <div>Total Points</div>
    </div>

    <div class="mt-3">X</div>
    <div class="col-2">
      {{ @$evaluation->education_training_percent}}
      <hr>
      <div>POINTS WEIGHT</div>
    </div>

    <label for="" class="mt-3">=</label>
    <div class="col-2">
      {{ @$evaluation->education_training_score}}
      <hr>
    </div>
  </div>

  <!-- Experience & Outstanding Accomplishments -->
  <div class="form-group row">
    <div class="col text-light bg-secondary">III. EXPERIENCE & OUTSTANDING ACCOMPLISHMENTS (20%)</div>
  </div>

  <div class="form-group row">
    {{ Form::label('relevant_positions_held', 'Relevant Positions Held', ['class'=>'col-2 mt-2']) }}
    <div class="col-4">
      {{ @$evaluation->relevant_positions_held}}
    </div>
  </div>

  <div class="form-group row">
    {{ Form::label('minimum_experience_requirement', 'Minimum Experience Requirement', ['class'=>'col-2 mt-2']) }}
    <div class="col-3 offset-1">
      {{ @$evaluation->minimum_experience_requirement}}
    </div>
    <div class="col-2 mt-3">PTS</div>
  </div>

  <div class="form-group row">
    {{ Form::label('additional_points', 'Additional Points (in excess of the minimum requirement)', ['class'=>'col-2 mt-2']) }}
    <div class="col-3 offset-1">
      {{ @$evaluation->additional_points}}
    </div>
    <div class="col-2 mt-3">PTS</div>
  </div>

  <div class="row">
    <label for="" class="offset-3 mt-3">=</label>
    <div class="col-3">
      {{ @$evaluation->experience_accomplishments_total_points}}
      <hr>
      <label for="">Total Points</label>
    </div>

    <label for="" class="mt-3">X</label>
    <div class="col-2">
      {{ @$evaluation->experience_accomplishments_percent}}
      <hr>
      <label for="">POINTS WEIGHT</label>
    </div>

    <label for="" class="mt-3">=</label>
    <div class="col-2">
      {{ @$evaluation->experience_accomplishments_score}}
      <hr>
    </div>
  </div>

  <!-- Potential -->
  <div class="form-group row">
    <div class="col text-light bg-secondary">IV. POTENTIAL (10%)</div>
  </div>

  <div class="form-group row">
    <div class="col">
      {{ @$evaluation->potential}}
      <hr>
      <label for="potential">R1 + R2 + R3 + R4 + R5</label>
    </div>

    <div class="col">
      {{ @$evaluation->potential_average_rating}}
      <hr>
      <label for="potential_average_rating">Average Rating</label>
    </div>

    <div class="col">
      {{ @$evaluation->potential_percentage_rating}}
      <hr>
      <label for="">Percentage Rating</label>
    </div>

    <label for="" class="offset-1"><h4 class="mt-1">X</h4></label>
    <div class="col">
      {{ @$evaluation->potential_percent}}
      <hr>
      <label for="">POINTS WEIGHT</label>
    </div>

    <label for=""><h4 class="mt-1">=</h4></label>
    <div class="col">
      {{ @$evaluation->potential_score}}
      <hr>
    </div>
  </div>

  <!-- Potential -->
  <div class="form-group row">
    <div class="col text-light bg-secondary">V. PSYCHOSOCIAL ATTRIBUTES & PERSONALITY TRAITS (10%)</div>
  </div>

  <div class="form-group row">
    <div class="col">
      {{ @$evaluation->psychosocial}}
      <hr>
      <label for="psychosocial">R1 + R2 + R3 + R4 + R5</label>
    </div>

    <div class="col">
      {{ @$evaluation->psychosocial_average_rating}}
      <hr>
      <label for="psychosocial_average_rating">Average Rating</label>
    </div>

    <div class="col">
      {{ @$evaluation->psychosocial_percentage_rating}}
      <hr>
      <label for="">Percentage Rating</label>
    </div>

    <label for="" class="offset-1"><h4 class="mt-1">X</h4></label>
    <div class="col">
      {{ @$evaluation->psychosocial_percent}}
      <hr>
      <label for="">POINTS WEIGHT</label>
    </div>

    <label for=""><h4 class="mt-1">=</h4></label>
    <div class="col">
      {{ @$evaluation->psychosocial_score}}
      <hr>
    </div>
  </div>

  <hr>

  <div class="form-group row">
    <div class="col-8 text-light bg-secondary">TOTAL</div>
    <div class="col text-light bg-secondary">{{ @$evaluation->total_percent}}</div>
    <div class="col text-light bg-secondary">{{ @$evaluation->total_score}}</div>
  </div>

  <div class="form-group row text-center">
    {{ Form::label('evaluated_by', 'Evaluated By', ['class'=>'col mt-2']) }}
    {{ Form::label('evaluated_by', 'Evaluated By', ['class'=>'col mt-2']) }}
    {{ Form::label('evaluated_by', 'Evaluated By', ['class'=>'col mt-2']) }}
  </div>

  <div class="form-group row text-center">
    <div class="col">
      {{ @$evaluation->evaluated_by}}
      <hr>
    </div>
    <div class="col">
      {{ @$evaluation->reviewed_by}}
      <hr>
    </div>
    <div class="col">
      {{  @$evaluation->noted_by}}
      <hr>
    </div>
  </div>

  <div class="form-group row text-right">
    <div class="col col-sm-10 col-lg-9 offset-sm-1 offset-lg-3">
      {{ Form::button('Print', ['id' => 'evaluation-report', 'class'=>'btn btn-primary btn-space', 'type'=>'submit']) }}
      {{ Form::reset('Cancel', ['class'=>'btn btn-space btn-danger']) }}
    </div>
  </div>
@endsection

@section('scripts')
<script type="text/javascript">
  $(document).ready(function() {
    $('#evaluation-report').click(function() {
      window.print();
    });
  });
</script>
@endsection