@section('css')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.min.css"
          rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('beagle-assets/lib/summernote/summernote-bs4.css') }}" />
@endsection

{!! Form::open(['action' => $action, 'method' => $method, 'id' => 'evaluation-form']) !!}
<div class="form-group row">
    {{ Form::label('reference_no', 'Reference No.', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
    <div class="col-xs-2 col-sm-4 col-lg-2">
        {{ Form::text('reference_no', $applicant->reference_no, [
                'class' => 'form-control',
                'placeholder' => 'Reference No.',
                'readonly' => ($applicant->reference_no) ? true : false
            ])
        }}
    </div>

<!--     <div class="col-xs-2 col-sm-4 offset-lg-3 col-lg-2">
        <a href="{{ route('evaluation.rating',
            [
                'id'=>app('request')->input('id'),
                'reference'=>$applicant->reference_no
            ]) }}"
           class="btn btn-secondary">Evaluation</a>
    </div> -->
</div>

<div class="form-group row">
    {{ Form::label('fullname', 'Applicant Name', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
    <div class="col-12 col-sm-8 col-lg-6">
        {{ Form::text('fullname', $applicant->getFullname(), [
                'class' => 'form-control',
                'placeholder' => 'Applicant Name',
                'readonly' => ($applicant->first_name) ? true : false
            ])
        }}
    </div>
</div>

<div class="form-group row">
    {{ Form::label('present_position', 'Present Position', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
    <div class="col-12 col-sm-8 col-lg-6">
        {{ Form::text('present_position', '', [
                'class' => 'form-control',
                'placeholder' => 'Present Position',
            ])
        }}
    </div>
</div>

<div class="form-group row">
    {{ Form::label('division', 'Division/Office', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
    <div class="col-12 col-sm-8 col-lg-6">
        {{ Form::text('division', '', [
                'class' => 'form-control',
                'placeholder' => 'Division/Office',
            ])
        }}
    </div>
</div>

<div class="form-group row">
    {{ Form::label('title', 'Vacant Position', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
    <div class="col-12 col-sm-8 col-lg-6">
        {{ Form::text('title', ($applicant->job) ? $applicant->job->title : '', [
                'class' => 'form-control',
                'placeholder' => 'Vacant Position',
                'readonly' => ($applicant->job) ? true : false
            ])
        }}
    </div>
</div>

<div class="form-group row">
    {{ Form::label('grade', 'Job Grade', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
    <div class="col-12 col-sm-8 col-lg-6">
        {{ Form::text('grade', ($applicant->job) ? $applicant->job->grade : '', [
                'class' => 'form-control',
                'placeholder' => 'Job Grade',
                'readonly' => ($applicant->job) ? true : false
            ])
        }}
    </div>
</div>

<div class="form-group row">
    <label class="col-12 col-sm-3 col-form-label text-sm-right"> Evaluation Made as of </label>
    <div class="col-12 col-sm-7 col-md-5 col-lg-4 col-xl-3">
        <div data-min-view="2" data-date-format="yyyy-mm-dd" class="input-group date datetimepicker">
            <input size="16" type="text" value="{{ date('Y-m-d',time()) }}" name="birthday"
                   class="form-control form-control-sm"
                   placeholder="Evaluation Made as of"
                    {{($applicant->job) ? 'readonly' : false}}>
            <div class="input-group-append">
                <button class="btn btn-primary"><i class="icon-th mdi mdi-calendar"></i></button>
            </div>
        </div>
    </div>
</div>

<div class="form-group row">
    {{ Form::label('education', 'Education', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
    <div class="col-12 col-sm-8 col-lg-6">
        <div id="education"></div>
    </div>
</div>

<div class="form-group row">
    {{ Form::label('experience', 'Experience', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
    <div class="col-12 col-sm-8 col-lg-6">
        <div id="experience"></div>
    </div>
</div>

<div class="form-group row">
    {{ Form::label('eligibility', 'Eligibility', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
    <div class="col-12 col-sm-8 col-lg-6">
        <div id="eligibility"></div>
    </div>
</div>

<div class="form-group row">
    {{ Form::label('training', 'Training', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
    <div class="col-12 col-sm-8 col-lg-6">
        <div id="training"></div>
    </div>
</div>
{!! Form::close() !!}

@section('scripts')
    <!-- JS Libraries -->
    <script src="{{ URL::asset('beagle-assets/lib/jquery-ui/jquery-ui.min.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('beagle-assets/lib/moment.js/min/moment.min.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('beagle-assets/js/app-form-elements.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('beagle-assets/lib/select2/js/select2.min.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('beagle-assets/lib/select2/js/select2.full.min.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('beagle-assets/lib/bootstrap-slider/bootstrap-slider.min.js') }}"
            type="text/javascript"></script>
    <script src="{{ URL::asset('beagle-assets/lib/datetimepicker/js/bootstrap-datetimepicker.min.js') }}"
            type="text/javascript"></script>
    <!-- wysiwyg -->
    <script src="{{ URL::asset('beagle-assets/lib/summernote/summernote-bs4.min.js') }}"
            type="text/javascript"></script>
    <script src="{{ URL::asset('beagle-assets/lib/summernote/summernote-ext-beagle.js') }}"
            type="text/javascript"></script>
    <script src="{{ URL::asset('beagle-assets/js/app-form-wysiwyg.js') }}" type="text/javascript"></script>
    <!-- validation -->
    <script src="{{ URL::asset('beagle-assets/lib/parsley/parsley.min.js') }}"
            type="text/javascript"></script><!-- frontend validation -->
    <script>
      $(document).ready(function() {
        //initialize the javascript
        App.init();
        App.formElements();
        $('#evaluation-form').parsley();
      });

      // instansiate summernote(wysiwyg) editor
      App.textEditors('#education', '#experience', '#eligibility', '#training');

      // place data into summernote(wysiwyg) editor
      $('#education').summernote('code', `{!! $applicant->job->education !!}`);
      $('#experience').summernote('code', `{!! $applicant->job->experience !!}`);
      $('#eligibility').summernote('code', `{!! $applicant->job->eligibility !!}`);
      $('#training').summernote('code', `{!! $applicant->job->training !!}`);

      // readonly summernote(wysiwyg) editor
      $('#education').summernote('disable');
      $('#experience').summernote('disable');
      $('#eligibility').summernote('disable');
      $('#training').summernote('disable');

      // remove tools on summernote(wysiwyg) editor
      $('.note-toolbar').remove();
    </script>
@endsection
