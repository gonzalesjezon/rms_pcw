@section('css')
    <link rel="stylesheet" type="text/css"
        href="{{ URL::asset('beagle-assets/lib/datetimepicker/css/bootstrap-datetimepicker.min.css') }}">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.min.css"
          rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('beagle-assets/lib/summernote/summernote-bs4.css') }}" />
@endsection


{!! Form::open(['action' => $action, 'method' => $method, 'id' => 'evaluation-form']) !!}


<div class="form-group row">
        {{ Form::label('', 'Applicant Name', [
          'class'=>'col-12 col-sm-3 col-form-label text-sm-right'
       ])}}
      <div class="col-12 col-sm-8 col-lg-3">
        <select name="applicant_id" class="form-control form-control-xs" id="applicant_id">
            <option value="0">Select applicant</option>
            @foreach($applicants as $applicant)
            <option data-email="{{ Crypt::decrypt($applicant->email_address) }}" value="{{$applicant->id}}" {{ ($applicant->id == @$boarding->applicant_id) ? 'selected' : '' }}>{!! $applicant->last_name !!}, {!! $applicant->first_name !!} {!! $applicant->middle_name !!} </option>
            @endforeach
        </select>
    </div>
</div>

<div class="form-group row">
    {{ Form::label('', 'Date Start:', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
    <div class="col-3">
        <div data-min-view="2" data-date-format="yyyy-mm-dd" class="input-group date datetimepicker">
            <input size="16" type="text" value="{{@$boarding->start_date}}" name="start_date"
                   class="form-control form-control-sm"
                   required="true"
            >
            <div class="input-group-append">
                <button class="btn btn-primary"><i class="icon-th mdi mdi-calendar"></i></button>
            </div>
        </div>
    </div>
</div>


<div class="form-group row">
    {{ Form::label('', 'Time:', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
    <div class="col-3">
        {{ Form::text('start_time', @$boarding->start_time, [
                'class' => 'form-control form-control-sm',
                'required' => 'true'
            ])
        }}
    </div>
</div>

<div class="form-group row">
    {{ Form::label('', 'Status:', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
    <div class="col-3">
        {{ Form::select('board_status', config('params.boarding_status'), @$boarding->board_status,[
                'class' => 'form-control form-control-xs',
                'required' => 'true',
                'placeholder' => 'Select status'
            ])
        }}
    </div>
</div>

<div class="row">
    <div class="col-6 text-right">
        <a class="btn btn-success mt-2" style="color: #fff;height: 30px;" id="send_mail">
            <i class="mdi mdi-mail-send"></i>
            Notify
        </a>
    </div>
</div>


<input type="hidden" name="id" value="{{@$boarding->id}}" class="form-control">
<input type="hidden" name="email" id="email" value="{{ (@$boarding->applicant) ?  Crypt::decrypt(@$boarding->applicant->email_address) : '' }}">


<div class="form-group row text-right">
    <div class="col col-sm-10 col-lg-12">
        {{ Form::submit('Submit', ['id' => 'job-submit', 'class'=>'btn btn-primary btn-space']) }}
        <!-- {{ Form::reset('Clear Form', ['id'=>'clear-form', 'class'=>'btn btn-space btn-danger']) }} -->
    </div>
</div>

{!! Form::close() !!}

@section('scripts')
    <!-- JS Libraries -->
    <script src="{{ URL::asset('beagle-assets/lib/moment.js/min/moment.min.js')}}"></script>
    <script src="{{ URL::asset('beagle-assets/lib/datetimepicker/js/bootstrap-datetimepicker.min.js') }}"
            type="text/javascript"></script>
    <script src="{{ URL::asset('beagle-assets/lib/summernote/summernote-bs4.min.js') }}"
            type="text/javascript"></script>
    <script src="{{ URL::asset('beagle-assets/lib/summernote/summernote-ext-beagle.js') }}"
            type="text/javascript"></script>
    <script src="{{ URL::asset('beagle-assets/js/app-form-wysiwyg.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('beagle-assets/lib/select2/js/select2.min.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('beagle-assets/lib/select2/js/select2.full.min.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('beagle-assets/lib/bootstrap-slider/bootstrap-slider.min.js') }}"
            type="text/javascript"></script>
    <script src="{{ URL::asset('beagle-assets/js/app-form-elements.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('beagle-assets/lib/parsley/parsley.min.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('js/sweetalert.min.js') }}" type="text/javascript"></script>
    <script>
      $(document).ready(function() {
        //initialize the javascript
        App.init();
        App.formElements();
        $('#evaluation-form').parsley(); // frontend validation

        $('#applicant_id').on('change',function(){
            email = $(this).find(':selected').data('email');
            $('#email').val(email);
        })

        $('#send_mail').click(function(){
            Swal.fire({
              title: 'Are you sure?',
              text: "You won't be able to revert this!",
              type: 'warning',
              showCancelButton: true,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              confirmButtonText: 'Yes'
            }).then((result) => {
              if (result.value) {

                var arr = {};
                $("[class*='form-control']").each(function () {
                    var obj_name = $(this).attr("name");
                    var value    = $(this).val();
                    arr[obj_name] = value;
                });

                $.ajax({
                    url:`{{ url('boarding_applicant/sendMail') }}`,
                    data:{
                        'data':arr,
                        '_token':`{{ csrf_token() }}`
                    },
                    type:'POST',
                    dataType:'JSON',
                    success:function(result){

                        if(result.status)
                        {
                            Swal.fire(
                              'Sent Successfully!',
                              'Your mail has been sent.',
                              'success'
                            )
                        }else{
                            Swal.fire({
                                title:'Send Failed',
                                type:'warning',
                                confirmButtonText:'OK'
                            })
                        }

                    }
                })
              }

        });

    });

      });
    </script>
@endsection
