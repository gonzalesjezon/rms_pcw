@extends('layouts.app')

@section('content')
    <div class="page-head">
        <h2 class="page-head-title">Edit Erasure and Alteration</h2>
    </div>

    <!-- Assumption Form -->
    <div class="row">
        <div class="col-md-12">
            <div class="card card-border-color card-border-color-primary">
                <div class="card-header card-header-divider">
                    <span class="card-subtitle">You can edit an erasure and alteration in the form below.</span></div>
                <div class="card-body">
                    @include('erasure_alterations._form', [
                        'action' => ['ErasureAlterationController@update', $erasures->id],
                        'method' => 'PATCH',
                        'erasures' => $erasures,
                    ])
                </div>
            </div>
        </div>
    </div>
@endsection
