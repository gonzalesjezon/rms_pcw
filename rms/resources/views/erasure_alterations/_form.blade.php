@section('css')
    <link rel="stylesheet" type="text/css"
        href="{{ URL::asset('beagle-assets/lib/datetimepicker/css/bootstrap-datetimepicker.min.css') }}">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.min.css"
          rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('beagle-assets/lib/summernote/summernote-bs4.css') }}" />
@endsection


{!! Form::open(['action' => $action, 'method' => $method, 'id' => 'evaluation-form']) !!}


<div class="form-group row">
   {{ Form::label('', 'Applicant Name', ['class'=>'col-12 col-sm-2 col-form-label text-sm-right']) }}
    <div class="col-3">
         @if(@$erasures)
            {!! Form::label('', @$erasures->applicant->getFullName(), ['class'=>'col-12 col-sm-3 col-form-label text-sm-right font-weight-bold']) !!}
            <input type="hidden" name="applicant_id" value="{{@$erasures->applicant_id}}">
        @else
            <select name="applicant_id" class="form-control form-control-xs" id="applicant_id">
                <option value="0">Select applicant</option>
                  @foreach($appointees as $appointee)
                  <option value="{{$appointee->applicant_id}}">{{ $appointee->applicant->getFullName()}}</option>
                  @endforeach
              </select>
        @endif
    </div>
</div>

<div class="form-group row">
   {{ Form::label('', 'ERASURE(S)/ALTERATION(S) MADE', ['class'=>'col-12 col-sm-4 col-form-label text-sm-right font-weight-bold']) }}
</div>

<div class="form-group row">
    {{ Form::label('', 'From Date:', ['class'=>'col-12 col-sm-2 col-form-label text-sm-right']) }}
    <div class="col-3">
        <div data-min-view="2" data-date-format="yyyy-mm-dd" class="input-group date datetimepicker">
            <input size="16" type="text" value="{{@$erasures->from_date}}" name="from_date"
                   class="form-control form-control-sm"
                   required="true"
            >
            <div class="input-group-append">
                <button class="btn btn-primary"><i class="icon-th mdi mdi-calendar"></i></button>
            </div>
        </div>
    </div>
</div>

<div class="form-group row">
    {{ Form::label('', 'From To:', ['class'=>'col-12 col-sm-2 col-form-label text-sm-right']) }}
    <div class="col-3">
        <div data-min-view="2" data-date-format="yyyy-mm-dd" class="input-group date datetimepicker">
            <input size="16" type="text" value="{{@$erasures->to_date}}" name="to_date"
                   class="form-control form-control-sm"
                   required="true"
            >
            <div class="input-group-append">
                <button class="btn btn-primary"><i class="icon-th mdi mdi-calendar"></i></button>
            </div>
        </div>
    </div>
</div>

<div class="row form-group">
    <div class="col-6">
        <div class=" row {{ $errors->has('particulars') ? 'has-error' : ''}}">
            {{ Form::label('particulars', 'Particular', ['class'=>'col-12 col-sm-4 col-form-label text-sm-right']) }}
            <div class="col-8">
                <div id="particulars" name="particulars"></div>
                <textarea name="particulars" class="d-none"></textarea>
                <input type="hidden" name="particulars" id="particulars-text">
                {!! $errors->first('compentency_1', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
            </div>
        </div>
    </div>
</div>

<div class="form-group row">
    {{ Form::label('', 'Appointing Officer:', ['class'=>'col-12 col-sm-2 col-form-label text-sm-right']) }}
    <div class="col-3">
        {{ Form::text('appointing_officer', @$erasures->appointing_officer, [
                'class' => 'form-control form-control-sm',
                'required' => 'true'
            ])
        }}
    </div>
</div>

<div class="form-group row">
    {{ Form::label('', 'Date Sign:', ['class'=>'col-12 col-sm-2 col-form-label text-sm-right']) }}
    <div class="col-3">
        <div data-min-view="2" data-date-format="yyyy-mm-dd" class="input-group date datetimepicker">
            <input size="16" type="text" value="{{@$erasures->sign_date}}" name="sign_date"
                   class="form-control form-control-sm"
                   required="true"
            >
            <div class="input-group-append">
                <button class="btn btn-primary"><i class="icon-th mdi mdi-calendar"></i></button>
            </div>
        </div>
    </div>
</div>

<input type="hidden" name="id" value="{{@$erasures->id}}">


<div class="form-group row text-right">
    <div class="col col-sm-10 col-lg-9 offset-sm-1 offset-lg-0">
        {{ Form::submit('Submit', ['id' => 'job-submit', 'class'=>'btn btn-primary btn-space']) }}
        {{ Form::reset('Clear Form', ['id'=>'clear-form', 'class'=>'btn btn-space btn-danger']) }}
    </div>
</div>

{!! Form::close() !!}

@section('scripts')
    <!-- JS Libraries -->
    <script src="{{ URL::asset('beagle-assets/lib/moment.js/min/moment.min.js')}}"></script>
    <script src="{{ URL::asset('beagle-assets/lib/datetimepicker/js/bootstrap-datetimepicker.min.js') }}"
            type="text/javascript"></script>
    <script src="{{ URL::asset('beagle-assets/lib/summernote/summernote-bs4.min.js') }}"
            type="text/javascript"></script>
    <script src="{{ URL::asset('beagle-assets/lib/summernote/summernote-ext-beagle.js') }}"
            type="text/javascript"></script>
    <script src="{{ URL::asset('beagle-assets/js/app-form-wysiwyg.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('beagle-assets/lib/select2/js/select2.min.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('beagle-assets/lib/select2/js/select2.full.min.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('beagle-assets/lib/bootstrap-slider/bootstrap-slider.min.js') }}"
            type="text/javascript"></script>
    <script src="{{ URL::asset('beagle-assets/js/app-form-elements.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('beagle-assets/lib/parsley/parsley.min.js') }}" type="text/javascript"></script>
    <script>
      $(document).ready(function() {
        //initialize the javascript
        App.init();
        App.formElements();
        $('#evaluation-form').parsley(); // frontend validation

        let data = {
          '#particulars': `{!! @$erasures->particulars !!}`,
        };


        // initialize editors for each data element
        App.textEditors(Object.keys(data));

        // when validation fails, get data from hidden input texts
        // and set as value for wysiwyg editor
        for (var key in data) {
          if (data.hasOwnProperty(key)) {
            if (data[key] === '') {
              data[key] = $(`${key}-text`).html();
            }
            setData(key, data[key]);
          }
        }

        // set data for wysiwyg editors
        function setData(selector = '', data = '') {
          $(selector).next('.note-editor').find('.note-editing-area > .note-editable').html(data);
        }

        $('.note-toolbar').remove();
        // on form submit, get data from wysiwyg editors
        // pass to hidden input elements
        $('#job-submit').click(function() {
          for (let key in data) {
            if (data.hasOwnProperty(key)) {
              let element = getData(key);
              $(`${key}-text`).val(element);
            }
          }
        });

        function getData(selector = '') {
          let data = $(selector).next('.note-editor').find('.note-editing-area > .note-editable');
          return data.html();
        }

        $('#job_id').change(function(){
            $('#get-form').submit();
        })
      });
    </script>
@endsection
