@section('css')
    <link rel="stylesheet" type="text/css"
        href="{{ URL::asset('beagle-assets/lib/datetimepicker/css/bootstrap-datetimepicker.min.css') }}">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.min.css"
          rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('beagle-assets/lib/summernote/summernote-bs4.css') }}" />

    <style type="text/css">
    	.table>thead>tr>th{
    		padding: 2px;
    	}
    </style>
@endsection


{!! Form::open(['action' => $action, 'method' => $method, 'id' => 'evaluation-form']) !!}
<input type="hidden" name="id" value="{{ @$bchecking->id }}">
<input type="hidden" name="applicant_id" value="{{ @$bchecking->applicant_id }}">

<div class="form-group row">
        {{ Form::label('', 'Applicant Name', [
          'class'=>'col-2 col-form-label text-sm-left'
       ])}}
      <div class="col-3">
      	@if(@$bchecking)
        <input type="text" class="form-control form-control-sm" value="{{ @$bchecking->applicant->getFullName() }}" readonly>
        @else
        <select name="applicant_id" id="applicant_id" class="form-control form-control-xs" required>
		      <option value="0">Select applcaint</option>
		      @foreach($applicants as $applicant)
		      <option value="{{$applicant->id }}" {{ ($applicant->id == @$bchecking->applicant_id) ? 'selected' : '' }}>{{ $applicant->getFullName() }}</option>
		      @endforeach
	    	</select>
        @endif
    	</div>
</div>

<hr>

<div class="row">
	<div class="col-3">
		<label class="h4 font-weight-bold text-right">Guidelines</label>
	</div>
</div>

<div class="row">
	<div class="col-3">
		<label class="h4 font-weight-bold text-right">A. Via Phone/Email </label>
	</div>
</div>

<!-- Introduction -->
<table class="table table-bordered">
	<thead class="bg-dark">
		<tr>
			<th style="color: #fff;" colspan="2">
				INTRODUCTION
			</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td style="width: 55%;" class="text-justify">
				My name is < HR staff name > and I’m calling to conduct a reference check for < name of applicant > who is being considered for a position in _______ at the Office of the Vice President. Your details have been provided to me by < applicant’s name > and I would first like to check if you are prepared to provide a reference?
			</td>
			<td>
				 <div class="row">
				 	<div class="col-md-8">
				 		<span>HR Staff Name</span>
				 		<div class="form-group">
				 			<input type="text" name="hr_staff_name" class="form-control form-control-sm" placeholder="HR Staff Name" value="{{ @$bchecking->hr_staff_name }}">
				 		</div>
				 	</div>
				 	<div class="col-md-4">
				 		<div class="form-group row">
		            <div class="col-12 offset-2 mt-2">
		                <div class="switch-button switch-button-success switch-button-yesno">
		                    <input type="checkbox" name="provided_reference" id="swt0" {{ @$bchecking->provided_reference ? 'checked' : '' }}><span>
		                        <label for="swt0"></label></span>
		                </div>
		            </div>
		        </div>
				 	</div>
				 </div>
			</td>
		</tr>
		<tr>
			<td class="text-justify">
				The reference check will take approximately fifteen (15) minutes to complete. Is this a good time for you? If not, when is a convenient time for us to continue this conversation?
			</td>
			<td>
				<select class="form-control form-control-xs" name="reference_check">
					<option>Select</option>
					<option value="1" {{ (@$bchecking->reference_check) ? 'selected' : '' }}>Call Back</option>
					<option value="2" {{ (@$bchecking->reference_check) ? 'selected' : '' }}>Proceed</option>
				</select>
			</td>
		</tr>
		<tr>
			<td style="width: 55%;" class="text-justify">
				Please note that this reference will be used in the overall evaluation of the applicant and will affect whether they are selected for the job.  The information you provide may be given to the candidate if requested, Do I have your permission to proceed?
			</td>
			<td>
				 <div class="form-group row">
		            <div class="col-12 offset-4">
		                <div class="switch-button switch-button-success switch-button-yesno">
		                    <input type="checkbox" name="permission" id="swt1" {{ @$bchecking->permission ? 'checked' : '' }}><span>
		                        <label for="swt1"></label></span>
		                </div>
		            </div>
		        </div>
			</td>
		</tr>
	</tbody>
</table>

<!-- For Other Character References -->
<table class="table table-bordered">
	<thead class="bg-dark">
		<tr>
			<th style="color: #fff;" colspan="2">
				For Other Character References
			</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td style="width: 55%;" class="text-justify">
				What is the nature of your relationship with the applicant?
			</td>
			<td>
				 <input type="text" name="applicant_relationship" class="form-control form-control-sm" value="{{ @$bchecking->applicant_relationship }}">
			</td>
		</tr>
		<tr>
			<td style="width: 55%;" class="text-justify">
				Name of character reference
			</td>
			<td>
				 <input type="text" name="character_reference" class="form-control form-control-sm" value="{{ @$bchecking->character_reference }}">
			</td>
		</tr>
		<tr>
			<td style="width: 55%;" class="text-justify">
				In what capacity is/was the applicant employed in your office?
			</td>
			<td>
				 <input type="text" name="applicant_capacity" class="form-control form-control-sm" value="{{ @$bchecking->applicant_capacity }}">
			</td>
		</tr>
	</tbody>
</table>

<!-- For HR only -->
<table class="table table-bordered">
	<thead class="bg-dark">
		<tr>
			<th style="color: #fff;" colspan="3">
				For HR Only
			</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td style="width: 55%;" class="text-justify">
				What were the dates of his/her employment?
			</td>
			<td>
				 <div class="col-12">
	                <div data-min-view="2" data-date-format="yyyy-mm-dd" class="input-group date datetimepicker">
	                    <input size="16" type="text" value="{{ @$bchecking->date_from }}" name="date_from"
	                           class="form-control form-control-sm" placeholder="Date From">
	                    <div class="input-group-append">
	                        <button class="btn btn-primary"><i class="icon-th mdi mdi-calendar"></i></button>
	                    </div>
	                </div>
	            </div>
			</td>
			<td>
				 <div class="col-12">
	                <div data-min-view="2" data-date-format="yyyy-mm-dd" class="input-group date datetimepicker">
	                    <input size="16" type="text" value="{{ @$bchecking->date_to }}" name="date_to"
	                           class="form-control form-control-sm" placeholder="Date To">
	                    <div class="input-group-append">
	                        <button class="btn btn-primary"><i class="icon-th mdi mdi-calendar"></i></button>
	                    </div>
	                </div>
	            </div>
			</td>
		</tr>
		<tr>
			<td style="width: 55%;" class="text-justify">
				What duties and responsibilities does/did the applicant have?
			</td>
			<td colspan="2">
				 <input type="text" name="duties_and_responsibilitties" class="form-control form-control-sm" value="{{ @$bchecking->duties_and_responsibilitties }}">
			</td>
		</tr>
		<tr>
			<td style="width: 55%;" class="text-justify">
				Does he/she have any pending administrative/criminal case?
			</td>
			<td colspan="2">
				 <input type="text" name="have_pending_case" class="form-control form-control-sm" value="{{ @$bchecking->have_pending_case }}">
			</td>
		</tr>
		<tr>
			<td style="width: 55%;" class="text-justify">
				Does the applicant have satisfactory/unsatisfactory attendance? If unsatisfactory, explain further.
			</td>
			<td colspan="2">
				 <input type="text" name="applicant_satisfactory" class="form-control form-control-sm" value="{{ @$bchecking->applicant_satisfactory }}">
			</td>
		</tr>
		<tr>
			<td style="width: 55%;" class="text-justify">
				What is the applicant’s reason for leaving?
			</td>
			<td colspan="2">
				 <input type="text" name="reason_for_leaving" class="form-control form-control-sm" value="{{ @$bchecking->reason_for_leaving }}">
			</td>
		</tr>
	</tbody>
</table>

<!-- For Other Character References -->
<table class="table table-bordered">
	<thead class="bg-dark">
		<tr>
			<th style="color: #fff;" colspan="2">
				For Other Character References
			</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td style="width: 55%;" class="text-justify">
				Did he/she get along well with management and co-workers? <br>
				Did he/she prefer to work on a team or independently? If yes, how did he/she support co-workers? Can you describe this person's experience working as a member of a team?
			</td>
			<td >
				 <input type="text" name="applicant_adaptability" class="form-control form-control-sm" value="{{ @$bchecking->applicant_adaptability }}">
			</td>
		</tr>
		<tr>
			<td style="width: 55%;" class="text-justify">
				How would you describe the applicant’s overall work performance?
			</td>
			<td >
				 <input type="text" name="applicant_work_performance" class="form-control form-control-sm" value="{{ @$bchecking->applicant_work_performance }}">
			</td>
		</tr>
		<tr>
			<td style="width: 55%;" class="text-justify">
				What was his/her significant contribution while working for your company/office?
			</td>
			<td >
				 <input type="text" name="applicant_contribution" class="form-control form-control-sm" value="{{ @$bchecking->applicant_contribution }}">
			</td>
		</tr>
		<tr>
			<td style="width: 55%;" class="text-justify">
				Have you had any concern/s with his/her performance? <br>
				If yes, please explain when was it identified? When was it discussed with the individual? What has been done or what progress has been made?

			</td>
			<td >
				 <input type="text" name="performance_concerns" class="form-control form-control-sm" value="{{ @$bchecking->performance_concerns }}">
			</td>
		</tr>
		<tr>
			<td>
				Can you comment on the applicant’s.
			</td>
			<td>
				<input type="text" name="comments" class="form-control form-control-sm" value="{{ @$bchecking->comments }}">
			</td>
		</tr>
		<tr>
			<td>
				< In this section, you should prepare your list of questions based on the skills or attributes, or competencies required for the job, for example: <br>
				In this role, the applicant is required to be very well organized and be able to manage a very busy office. 
				Please describe < applicant’s name > ability to organize their workload?>

			</td>
			<td>
				<input type="text" name="job_requirements" class="form-control form-control-sm" value="{{ @$bchecking->job_requirements }}">
			</td>
		</tr>
	</tbody>
</table>

<!-- IN CLOSING -->
<table class="table table-bordered">
	<thead class="bg-dark">
		<tr>
			<th style="color: #fff;" colspan="2">
				IN CLOSING
			</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td style="width: 55%;" class="text-justify">
				Would you re-employ the applicant? Why/why not?
			</td>
			<td >
				 <input type="text" name="reemploy_the_applicant" class="form-control form-control-sm" value="{{ @$bchecking->reemploy_the_applicant }}">
			</td>
		</tr>
		<tr>
			<td style="width: 55%;" class="text-justify">
				Do you have any final comments?
			</td>
			<td >
				 <input type="text" name="final_comments" class="form-control form-control-sm" value="{{ @$bchecking->final_comments }}">
			</td>
		</tr>
		<tr>
			<td style="width: 55%;" class="text-justify">
				Thank you for taking the time to provide feedback.  If you wish to provide any further information, you can contact me on < your contact details >.
			</td>
			<td >
				 <input type="text" name="interview_contact" class="form-control form-control-sm" value="{{ @$bchecking->interview_contact }}">
			</td>
		</tr>
	</tbody>
</table>

<div class="row">
	<div class="col-3">
		<label class="h4 font-weight-bold text-right">B. Via Social Media </label>
	</div>
</div>

<!-- FACEBOOK/TWITTER/INSTAGRAM -->
<table class="table table-bordered">
	<thead class="bg-dark">
		<tr>
			<th style="color: #fff;" colspan="2">
				FACEBOOK/TWITTER/INSTAGRAM
			</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td style="width: 55%;" class="text-justify">
				Use of profane/bad language
			</td>
			<td >
				 <div class="form-group row">
		            <div class="col-12 offset-4">
		                <div class="switch-button switch-button-success switch-button-yesno">
		                    <input type="checkbox" name="use_of_profance_language" id="swt3" {{ @$bchecking->use_of_profance_language ? 'checked' : '' }}><span>
		                        <label for="swt3"></label></span>
		                </div>
		            </div>
		        </div>
			</td>
		</tr>
		<tr>
			<td style="width: 55%;" class="text-justify">
				Signs of drug use
			</td>
			<td >
				 <div class="form-group row">
		            <div class="col-12 offset-4">
		                <div class="switch-button switch-button-success switch-button-yesno">
		                    <input type="checkbox" name="drug_user" id="swt4" {{ @$bchecking->drug_user ? 'checked' : '' }}><span>
		                        <label for="swt4"></label></span>
		                </div>
		            </div>
		        </div>
			</td>
		</tr>
		<tr>
			<td style="width: 55%;" class="text-justify">
				Risqué photos/posts
			</td>
			<td >
				 <div class="form-group row">
		            <div class="col-12 offset-4">
		                <div class="switch-button switch-button-success switch-button-yesno">
		                    <input type="checkbox" name="risque_photos" id="swt5" {{ @$bchecking->risque_photos ? 'checked' : '' }}><span>
		                        <label for="swt5"></label></span>
		                </div>
		            </div>
		        </div>
			</td>
		</tr>
		<tr>
			<td style="width: 55%;" class="text-justify">
				Discriminating comments related to race, gender, religion, sexuality
			</td>
			<td >
				 <div class="form-group row">
		            <div class="col-12 offset-4">
		                <div class="switch-button switch-button-success switch-button-yesno">
		                    <input type="checkbox" name="discriminating_comments" id="swt6" {{ @$bchecking->discriminating_comments ? 'checked' : '' }}><span>
		                        <label for="swt6"></label></span>
		                </div>
		            </div>
		        </div>
			</td>
		</tr>
		<tr>
			<td style="width: 55%;" class="text-justify">
				Statements that bully others
			</td>
			<td >
				 <div class="form-group row">
		            <div class="col-12 offset-4">
		                <div class="switch-button switch-button-success switch-button-yesno">
		                    <input type="checkbox" name="bullying_statement" id="swt7" {{ @$bchecking->bullying_statement ? 'checked' : '' }}><span>
		                        <label for="swt7"></label></span>
		                </div>
		            </div>
		        </div>
			</td>
		</tr>
		<tr>
			<td style="width: 55%;" class="text-justify">
				Observed poor grammar and spelling
			</td>
			<td >
				 <div class="form-group row">
		            <div class="col-12 offset-4">
		                <div class="switch-button switch-button-success switch-button-yesno">
		                    <input type="checkbox" name="observe_grammar" id="swt8" {{ @$bchecking->observe_grammar ? 'checked' : '' }}><span>
		                        <label for="swt8"></label></span>
		                </div>
		            </div>
		        </div>
			</td>
		</tr>
		<tr>
			<td style="width: 55%;" class="text-justify">
				Sharing confidential information about previous employer/s
			</td>
			<td >
				 <div class="form-group row">
		            <div class="col-12 offset-4">
		                <div class="switch-button switch-button-success switch-button-yesno">
		                    <input type="checkbox" name="sharing_confidential_information" id="swt9" {{ @$bchecking->sharing_confidential_information ? 'checked' : '' }}><span>
		                        <label for="swt9"></label></span>
		                </div>
		            </div>
		        </div>
			</td>
		</tr>
	</tbody>
</table>

<div class="form-group row text-right">
    <div class="col col-12">
        {{ Form::submit('Submit', ['id' => 'job-submit', 'class'=>'btn btn-primary btn-space']) }}
        {{ Form::reset('Clear Form', ['id'=>'clear-form', 'class'=>'btn btn-space btn-danger']) }}
    </div>
</div>

{!! Form::close() !!}

@section('scripts')
    <!-- JS Libraries -->
    <script src="{{ URL::asset('beagle-assets/lib/moment.js/min/moment.min.js')}}"></script>
    <script src="{{ URL::asset('beagle-assets/lib/datetimepicker/js/bootstrap-datetimepicker.min.js') }}"
            type="text/javascript"></script>
    <script src="{{ URL::asset('beagle-assets/lib/summernote/summernote-bs4.min.js') }}"
            type="text/javascript"></script>
    <script src="{{ URL::asset('beagle-assets/lib/summernote/summernote-ext-beagle.js') }}"
            type="text/javascript"></script>
    <script src="{{ URL::asset('beagle-assets/js/app-form-wysiwyg.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('beagle-assets/lib/select2/js/select2.min.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('beagle-assets/lib/select2/js/select2.full.min.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('beagle-assets/lib/bootstrap-slider/bootstrap-slider.min.js') }}"
            type="text/javascript"></script>
    <script src="{{ URL::asset('beagle-assets/js/app-form-elements.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('beagle-assets/lib/parsley/parsley.min.js') }}" type="text/javascript"></script>
    <script>
      $(document).ready(function() {
        //initialize the javascript
        App.init();
        App.formElements();
        $('#evaluation-form').parsley(); // frontend validation

        $('#applicant_id').on('change',function(){
            email = $(this).find(':selected').data('email');
            $('#email').val(email);
        })

      });
    </script>
@endsection
