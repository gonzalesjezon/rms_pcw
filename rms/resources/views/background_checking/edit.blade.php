@extends('layouts.app')

@section('content')
    <div class="page-head">
        <h2 class="page-head-title">Background Checking</h2>
    </div>

    <!-- Assumption Form -->
    <div class="row">
        <div class="col-md-12">
            <div class="card card-border-color card-border-color-primary">
                <div class="card-header card-header-divider">
                    <span class="card-subtitle">You can create a background checking in the form below.</span></div>
                <div class="card-body">
                    @include('background_checking._form', [
                        'action' => ['BackgroundCheckingController@update', @$bchecking->id],
                        'method' => 'PATCH',
                        'bchecking' => @$bchecking
                    ])
                </div>
            </div>
        </div>
    </div>
@endsection
