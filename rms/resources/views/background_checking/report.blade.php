@extends('layouts.print')

@section('css')
<style type="text/css">
  @media print{
    @page{
      size: a4 landscape;
    }
    .table>thead>tr>th{
      padding: 3px !important;
      border: 1px solid #000;
    }
  }
  .table>thead>tr>th, .table>tbody>tr>td{
    padding: 3px !important;
    border: 1px solid #000;
  }
</style>

@endsection

@section('content')

<div class="row text-right d-print-none">
  <div class="col col-sm-10 col-lg-9 offset-sm-1 offset-lg-3">
    <button class="btn btn-primary btn-space" id="evaluation-report" type="submit"><i class="mdi mdi-print"></i> Print</button>
    <!-- {{ Form::reset('Cancel', ['class'=>'btn btn-space btn-danger']) }} -->
  </div>
</div>

<br>

<div id="reports" style="margin: auto;font-size: 12px;font-family: Arial, Helvetica, sans-serif;">

<div class="row mb-4">
  <div class="col-12 text-center">
  	<h4 class="p-0 m-0 font-weight-bold">Republic of the Philippines</h4>
      <div style="font-size: 21px;" class="font-weight-bold">Philippine Commission on Women</div>
      <div>1145 J.P Laurel St. San Miguel Manila</div>
  </div>
</div>

<div class="row form-group mb-2">
	<div class="col-12 text-center">
		<span class="font-weight-bold" style="font-size: 15pt;">HUMAN RESOURCES MANAGEMENT UNIT</span>
	</div>
</div>

<div class="row form-group mb-4">
	<div class="col-12 text-center">
		<u class="font-weight-bold" style="font-size: 15pt;">GUIDE FOR REFERENCE CHECK</u>
	</div>
</div>

<div class="row">
	<label class="col-2 font-weight-bold">Name of Interview :</label>
	<div class="col-3">
		{!! @$bchecking->applicant->getFullName() !!}
	</div>

	<label class="col-2 font-weight-bold">Office:</label>
	<div class="col-3">
		{!! @$bchecking->applicant->job->plantilla_item->office->Name !!}
	</div>
</div>

<div class="row">
	<label class="col-2 font-weight-bold">Position :</label>
	<div class="col-3">
		{!! @$bchecking->applicant->job->plantilla_item->position->Name !!}
	</div>

	<label class="col-2 font-weight-bold">Date Conducted:</label>
	<div class="col-3">

	</div>
</div>

<div class="border-bottom border-dark"></div>

<div class="row">
	<div class="col-3">
		<label class="h4 font-weight-bold text-right">Guidelines</label>
	</div>
</div>

<div class="row">
	<div class="col-3">
		<label class="h4 font-weight-bold text-right">A. Via Phone/Email </label>
	</div>
</div>

<!-- Introduction -->
<table class="table table-bordered">
	<thead class="bg-dark">
		<tr>
			<th style="color: #fff;" colspan="2">
				INTRODUCTION
			</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td style="width: 55%;" class="text-justify">
				My name is <b>{!! @$bchecking->hr_staff_name  !!}</b> and I’m calling to conduct a reference check for <b>{!! @$bchecking->applicant->getFullName() !!}</b> who is being considered for a position in <u class="font-weight-bold">{!! @$bchecking->applicant->job->plantilla_item->position->Name !!}</u> at the Philippine Commission on Women. Your details have been provided to me by <b>{!! @$bchecking->applicant->getFullName() !!}</b> and I would first like to check if you are prepared to provide a reference?
			</td>
			<td class="text-center">
				 {{ @$bchecking->provided_reference ? 'Yes' : 'No' }}
			</td>
		</tr>
		<tr>
			<td class="text-justify">
				The reference check will take approximately fifteen (15) minutes to complete. Is this a good time for you? If not, when is a convenient time for us to continue this conversation?
			</td>
			<td class="text-center">
				{{ (@$bchecking->reference_check == 1) ? 'Call Back' : 'Proceed' }}
			</td>
		</tr>
		<tr>
			<td style="width: 55%;" class="text-justify">
				Please note that this reference will be used in the overall evaluation of the applicant and will affect whether they are selected for the job.  The information you provide may be given to the candidate if requested, Do I have your permission to proceed?
			</td>
			<td class="text-center">
				 {{ @$bchecking->provided_reference ? 'Yes' : 'No' }}
			</td>
		</tr>
	</tbody>
</table>

<!-- For Other Character References -->
<table class="table table-bordered">
	<thead class="bg-dark">
		<tr>
			<th style="color: #fff;" colspan="2">
				For Other Character References
			</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td style="width: 55%;" class="text-justify">
				What is the nature of your relationship with the applicant?
			</td>
			<td>
				 {{ @$bchecking->applicant_relationship }}
			</td>
		</tr>
		<tr>
			<td style="width: 55%;" class="text-justify">
				Name of character reference
			</td>
			<td>
				{{ @$bchecking->hr_staff_name }}
			</td>
		</tr>
		<tr>
			<td style="width: 55%;" class="text-justify">
				In what capacity is/was the applicant employed in your office?
			</td>
			<td>
				 {{ @$bchecking->applicant_capacity }}
			</td>
		</tr>
	</tbody>
</table>

<!-- For HR only -->
<table class="table table-bordered">
	<thead class="bg-dark">
		<tr>
			<th style="color: #fff;" colspan="3">
				For HR Only
			</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td style="width: 55%;" class="text-justify">
				What were the dates of his/her employment?
			</td>
			<td class="text-center">
				 {{ date('F d, Y',strtotime(@$bchecking->date_from))  }}
			</td>
			<td class="text-center">
				 {{ date('F d, Y',strtotime(@$bchecking->date_to))  }}
			</td>
		</tr>
		<tr>
			<td style="width: 55%;" class="text-justify">
				What duties and responsibilities does/did the applicant have?
			</td>
			<td colspan="2">
				 {{ @$bchecking->duties_and_responsibilitties }}
			</td>
		</tr>
		<tr>
			<td style="width: 55%;" class="text-justify">
				Does he/she have any pending administrative/criminal case?
			</td>
			<td colspan="2">
				 {{ @$bchecking->have_pending_case }}
			</td>
		</tr>
		<tr>
			<td style="width: 55%;" class="text-justify">
				Does the applicant have satisfactory/unsatisfactory attendance? If unsatisfactory, explain further.
			</td>
			<td colspan="2">
				 {{ @$bchecking->applicant_satisfactory }}
			</td>
		</tr>
		<tr>
			<td style="width: 55%;" class="text-justify">
				What is the applicant’s reason for leaving?
			</td>
			<td colspan="2">
				 {{ @$bchecking->reason_for_leaving }}
			</td>
		</tr>
	</tbody>
</table>

<!-- For Other Character References -->
<table class="table table-bordered">
	<thead class="bg-dark">
		<tr>
			<th style="color: #fff;" colspan="2">
				For Other Character References
			</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td style="width: 55%;" class="text-justify">
				Did he/she get along well with management and co-workers? <br>
				Did he/she prefer to work on a team or independently? If yes, how did he/she support co-workers? Can you describe this person's experience working as a member of a team?
			</td>
			<td >
				 {{ @$bchecking->applicant_adaptability }}
			</td>
		</tr>
		<tr>
			<td style="width: 55%;" class="text-justify">
				How would you describe the applicant’s overall work performance?
			</td>
			<td >
				 <input type="text" name="applicant_work_performance" class="form-control form-control-sm" value="{{ @$bchecking->applicant_work_performance }}">
			</td>
		</tr>
		<tr>
			<td style="width: 55%;" class="text-justify">
				What was his/her significant contribution while working for your company/office?
			</td>
			<td >
				 {{ @$bchecking->applicant_contribution }}
			</td>
		</tr>
		<tr>
			<td style="width: 55%;" class="text-justify">
				Have you had any concern/s with his/her performance? <br>
				If yes, please explain when was it identified? When was it discussed with the individual? What has been done or what progress has been made?

			</td>
			<td >
				 {{ @$bchecking->performance_concerns }}
			</td>
		</tr>
		<tr>
			<td>
				Can you comment on the applicant’s:
			</td>
			<td>
				{{ @$bchecking->comments }}
			</td>
		</tr>
		<tr>
			<td>
				< In this section, you should prepare your list of questions based on the skills or attributes, or competencies required for the job, for example: <br>
				In this role, the applicant is required to be very well organized and be able to manage a very busy office.
				Please describe < applicant’s name > ability to organize their workload?>

			</td>
			<td>
				{{ @$bchecking->job_requirements }}
			</td>
		</tr>
	</tbody>
</table>

<!-- IN CLOSING -->
<table class="table table-bordered">
	<thead class="bg-dark">
		<tr>
			<th style="color: #fff;" colspan="2">
				IN CLOSING
			</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td style="width: 55%;" class="text-justify">
				Would you re-employ the applicant? Why/why not?
			</td>
			<td >
				 {{ @$bchecking->reemploy_the_applicant }}
			</td>
		</tr>
		<tr>
			<td style="width: 55%;" class="text-justify">
				Do you have any final comments?
			</td>
			<td >
				 {{ @$bchecking->final_comments }}
			</td>
		</tr>
		<tr>
			<td style="width: 55%;" class="text-justify">
				Thank you for taking the time to provide feedback.  If you wish to provide any further information, you can contact me on < your contact details >.
			</td>
			<td >
				 {{ @$bchecking->interview_contact }}
			</td>
		</tr>
	</tbody>
</table>

<div class="row">
	<div class="col-3">
		<label class="h4 font-weight-bold text-right">B. Via Social Media </label>
	</div>
</div>

<!-- FACEBOOK/TWITTER/INSTAGRAM -->
<table class="table table-bordered">
	<thead class="bg-dark">
		<tr>
			<th style="color: #fff;" colspan="2">
				FACEBOOK/TWITTER/INSTAGRAM
			</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td style="width: 55%;" class="text-justify">
				Use of profane/bad language
			</td>
			<td class="text-center">
				 {{ @$bchecking->use_of_profance_language ? 'Yes' : 'No' }}
			</td>
		</tr>
		<tr>
			<td style="width: 55%;" class="text-justify">
				Signs of drug use
			</td>
			<td class="text-center">
				{{ @$bchecking->drug_user ? 'Yes' : 'No' }}
			</td>
		</tr>
		<tr>
			<td style="width: 55%;" class="text-justify">
				Risqué photos/posts
			</td>
			<td class="text-center">
				 {{ @$bchecking->risque_photos ? 'Yes' : 'No' }}
			</td>
		</tr>
		<tr>
			<td style="width: 55%;" class="text-justify">
				Discriminating comments related to race, gender, religion, sexuality
			</td>
			<td class="text-center">
				{{ @$bchecking->discriminating_comments ? 'Yes' : 'No' }}
			</td>
		</tr>
		<tr>
			<td style="width: 55%;" class="text-justify">
				Statements that bully others
			</td>
			<td class="text-center">
				{{ @$bchecking->bullying_statement ? 'Yes' : 'No' }}
			</td>
		</tr>
		<tr>
			<td style="width: 55%;" class="text-justify">
				Observed poor grammar and spelling
			</td>
			<td class="text-center">
				{{ @$bchecking->observe_grammar ? 'Yes' : 'No' }}
			</td>
		</tr>
		<tr>
			<td style="width: 55%;" class="text-justify">
				Sharing confidential information about previous employer/s
			</td>
			<td class="text-center">
				{{ @$bchecking->sharing_confidential_information ? 'Yes' : 'No' }}
			</td>
		</tr>
	</tbody>
</table>


</div>

@endsection

@section('scripts')
<script type="text/javascript">
  $(document).ready(function() {
    $('#evaluation-report').click(function() {
      window.print();
    });
  });
</script>
@endsection