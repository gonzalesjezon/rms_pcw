@section('css')
      <link rel="stylesheet" type="text/css"
        href="{{ URL::asset('beagle-assets/lib/datetimepicker/css/bootstrap-datetimepicker.min.css') }}">
  <link rel="stylesheet" type="text/css" href="{{ URL::asset('beagle-assets/lib/select2/css/select2.min.css') }}">
  <link rel="stylesheet" type="text/css"
        href="{{ URL::asset('beagle-assets/lib/bootstrap-slider/css/bootstrap-slider.min.css') }}">
@endsection

{!! Form::open(['action' => $action, 'method' => $method, 'id' => 'store-appointment-form']) !!}

@if(@$form)

  <div class="row">
    <div class="col-6">
      <div class="form-group row">
        {{ Form::label('', 'Applicant', ['class' => 'col-12 col-sm-4 col-form-label text-sm-right font-weight-bold']) }}
        <div class="col-12 col-sm-8 col-lg-6">
          <input type="text" value="{!! @$form->applicant->getFullName() !!}" class="form-control-sm form-control" readonly>
        </div>
      </div>
    </div>
  </div>

  <input type="hidden" name="applicant_id" value="{{@$form->applicant_id}}">

@else

  <div class="row">
    <div class="col-6">
      <div class="form-group row">
        {{ Form::label('', 'Applicant', ['class' => 'col-12 col-sm-4 col-form-label text-sm-right font-weight-bold']) }}
        <div class="col-12 col-sm-8 col-lg-6">
          <select name="applicant_id" class="form-control form-control-xs" id="applicant_id">
            <option value="0">Select applicant</option>
              @foreach($appointees as $appointee)
              <option value="{{@$appointee->applicant_id}}" data-position="{{ @$appointee->applicant->job->plantilla_item->position->Name }}" data-office="{{ @$appointee->applicant->job->plantilla_item->office->Name }}" data-item_number="{{ @$appointee->applicant->job->plantilla_item->item_number }}" data-salary_grade="{{ @$appointee->applicant->job->plantilla_item->salary_grade->Name }}" data-basic_salary="{{ number_format(@$appointee->applicant->job->plantilla_item->basic_salary,2) }}" data-publication="{{ config('params.publication.'.@$appointee->applicant->job->publication) }}" >{{ $appointee->applicant->getFullName()}}</option>
              @endforeach
          </select>
        </div>
      </div>
    </div>
  </div>

@endif

<div class="form-group row">
    {{ Form::label('position', 'Position', ['class'=>'col-12 col-sm-2 col-form-label text-sm-right']) }}
    <div class="col-3">
        {{ Form::text('', @$form->applicant->job->plantilla_item->position->Name, [
                'class' => 'form-control form-control-sm',
                'disabled' => true,
                'id' => 'position',
            ])
        }}
    </div>

    <label class="col-12 col-sm-3 col-form-label text-sm-right">Publication Date From </label>
    <div class="col-12 col-sm-7 col-md-5 col-lg-4 col-xl-3">
        <div data-min-view="2" data-date-format="yyyy-mm-dd" class="input-group date datetimepicker">
            <input size="16" type="text" name="publication_date_from"
                   class="form-control form-control-sm"
                   placeholder="Select date"
                   value="{{ @$form->publication_date_from}}">
            <div class="input-group-append">
                <button class="btn btn-primary"><i class="icon-th mdi mdi-calendar"></i></button>
            </div>
        </div>
    </div>
</div>

<div class="form-group row">

    {{ Form::label('office', 'Office/Department Unit', ['class'=>'col-12 col-sm-2 col-form-label text-sm-right']) }}
    <div class="col-3">
        {{ Form::text('', @$form->applicant->job->plantilla_item->office->Name, [
                'class' => 'form-control form-control-sm',
                'disabled' => true,
                'id' => 'office'
            ])
        }}
    </div>

    <label class="col-12 col-sm-3 col-form-label text-sm-right">To </label>
    <div class="col-12 col-sm-7 col-md-5 col-lg-4 col-xl-3">
        <div data-min-view="2" data-date-format="yyyy-mm-dd" class="input-group date datetimepicker">
            <input size="16" type="text" name="publication_date_to"
                   class="form-control form-control-sm"
                   placeholder="Select date"
                   value="{{ @$form->publication_date_to}}">
            <div class="input-group-append">
                <button class="btn btn-primary"><i class="icon-th mdi mdi-calendar"></i></button>
            </div>
        </div>
    </div>
</div>

<!-- <div class="form-group row">
    <div class="col-5 text-center">
        {{ Form::label('','CERTIFICATION', ['class' => 'col-12 col-form-label font-weight-bold'])}}
    </div>
</div> -->

<div class="form-group row">

    {{ Form::label('item_no', 'Item Number', ['class'=>'col-12 col-sm-2 col-form-label text-sm-right']) }}
    <div class="col-3">
        {{ Form::text('',  @$form->applicant->job->plantilla_item->item_number, [
                'class' => 'form-control form-control-sm',
                'disabled' => true,
                'id' => 'item_number'
            ])
        }}
    </div>
    

    {{ Form::label('', 'Publish At', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
    <div class="col-12 col-sm-7 col-md-5 col-lg-4 col-xl-3">
        {{ Form::text('',  config('params.publication.'.@$form->applicant->publication), [
                'class' => 'form-control form-control-sm',
                'disabled' => true,
                'id' => 'publication'
            ])
        }}
    </div>

</div>

<div class="form-group row">

    {{ Form::label('salary_grade', 'Salary Grade', ['class'=>'col-12 col-sm-2 col-form-label text-sm-right']) }}
    <div class="col-3">
        {{ Form::text('', @$form->applicant->job->plantilla_item->salary_grade->Name, [
                'class' => 'form-control form-control-sm',
                'disabled' => true,
                'id' => 'salary_grade'
            ])
        }}
    </div>
   

    {{ Form::label('', 'Posted In', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
    <div class="col-12 col-sm-7 col-md-5 col-lg-4 col-xl-3">
        {{ Form::text('posted_in',@$form->posted_in , [
                'class' => 'form-control form-control-sm',
            ])
        }}
    </div>

</div>

<div class="form-group row">

    {{ Form::label('', 'Monthly Rate', ['class'=>'col-12 col-sm-2 col-form-label text-sm-right']) }}
    <div class="col-3">
        {{ Form::text('', number_format(@$form->applicant->job->plantilla_item->basic_salary,2), [
                'class' => 'form-control form-control-sm',
                'disabled' => true,
                'id' => 'basic_salary'
            ])
        }}
    </div>

    {{ Form::label('', 'Signatory HRMO', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
    <div class="col-12 col-sm-7 col-md-5 col-lg-4 col-xl-3">
        {{ Form::text('hrmo', @$form->hrmo, [
                'class' => 'form-control form-control-sm',
            ])
        }}
    </div>

</div>

<div class="form-group row">

    <label class="col-12 col-sm-2 col-form-label text-sm-right">Date Issued</label>
    <div class="col-12 col-sm-7 col-md-5 col-lg-4 col-xl-3">
        <div data-min-view="2" data-date-format="yyyy-mm-dd" class="input-group date datetimepicker">
            <input size="16" type="text" name="date_issued"
                   class="form-control form-control-sm"
                   placeholder="Select date"
                   value="{{@$form->date_issued}}">
            <div class="input-group-append">
                <button class="btn btn-primary"><i class="icon-th mdi mdi-calendar"></i></button>
            </div>
        </div>
    </div>

    <label class="col-12 col-sm-3 col-form-label text-sm-right">Deliberation Date</label>
    <div class="col-12 col-sm-7 col-md-5 col-lg-4 col-xl-3">
        <div data-min-view="2" data-date-format="yyyy-mm-dd" class="input-group date datetimepicker">
            <input size="16" type="text" name="chairperson_deliberation_date"
                   class="form-control form-control-sm"
                   placeholder="Select date"
                   value="{{@$form->chairperson_deliberation_date}}">
            <div class="input-group-append">
                <button class="btn btn-primary"><i class="icon-th mdi mdi-calendar"></i></button>
            </div>
        </div>
    </div>

</div>

<div class="row form-group">

    <label class="col-12 col-sm-2 col-form-label text-sm-right">Assessment Date</label>
    <div class="col-12 col-sm-7 col-md-5 col-lg-4 col-xl-3">
        <div data-min-view="2" data-date-format="yyyy-mm-dd" class="input-group date datetimepicker">
            <input size="16" type="text" name="assessment_date"
                   class="form-control form-control-sm"
                   placeholder="Select date"
                   value="{{@$form->assessment_date}}">
            <div class="input-group-append">
                <button class="btn btn-primary"><i class="icon-th mdi mdi-calendar"></i></button>
            </div>
        </div>
    </div>

    {{ Form::label('', 'Chairperson, HRMPSB/ Placement', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
    <div class="col-12 col-sm-7 col-md-5 col-lg-4 col-xl-3">
        {{ Form::text('chairperson', @$form->chairperson, [
                'class' => 'form-control form-control-sm',
            ])
        }}
    </div>
</div>


<div class="row form-group">

    {{ Form::label('', 'Nature of Appointment', ['class'=>'col-12 col-sm-2 col-form-label text-sm-right']) }}
    <div class="col-3">
        {{ Form::select('nature_of_appointment', config('params.nature_of_appointment'), @$form->nature_of_appointment,[
                'class' => 'form-control form-control-xs',
                'placeholder' => 'Select nature of appointment'
            ])
        }}
    </div>

    {{ Form::label('', 'Appointing Officer', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
    <div class="col-12 col-sm-7 col-md-5 col-lg-4 col-xl-3">
        {{ Form::text('appointing_officer', @$form->appointing_officer, [
                'class' => 'form-control form-control-sm',
            ])
        }}
    </div>
</div>

<div class="row form-group">

    <label class="col-12 col-sm-2 col-form-label text-sm-right">Date Sign</label>
    <div class="col-12 col-sm-7 col-md-5 col-lg-4 col-xl-3">
        <div data-min-view="2" data-date-format="yyyy-mm-dd" class="input-group date datetimepicker">
            <input size="16" type="text" name="date_sign"
                   class="form-control form-control-sm"
                   placeholder="Select date"
                   value="{{@$form->date_sign}}">
            <div class="input-group-append">
                <button class="btn btn-primary"><i class="icon-th mdi mdi-calendar"></i></button>
            </div>
        </div>
    </div>

    {{ Form::label('', 'Status', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
    <div class="col-3">

        <select class="form-control form-control-xs" name="form_status">
            <option>Select status</option>
            @foreach(config('params.status') as $key => $value)
            <option value="{{$key}}" {!! ($key == @$form->form_status) ? 'selected' : '' !!}>{!! $value !!}</option>
            @endforeach
        </select>
    </div>

</div>

<div class="row form-group">
    {!! Form::label('', 'Vice', ['class' => 'col-12 col-sm-2 col-form-label text-sm-right'])!!}
    <div class="col-3">
        {!! Form::text('vice', @$form->vice, [
            'class' => 'form-control form-control-sm'
        ]) !!}
    </div>
</div>

<div class="row form-group">
    {!! Form::label('', 'Who', ['class' => 'col-12 col-sm-2 col-form-label text-sm-right'])!!}
    <div class="col-3">
        {!! Form::text('who', @$form->who, [
            'class' => 'form-control form-control-sm'
        ]) !!}
    </div>
</div>

<div class="form-group row text-right">
  <div class="col col-sm-12 ">
    <input type="hidden" name="id" value="{{@$form->id}}">
    {{ Form::submit('Save', ['id' => 'appointment-submit', 'class'=>'btn btn-success btn-space']) }}
    {{ Form::reset('Cancel', ['id'=>'clear-form', 'class'=>'btn btn-space btn-danger']) }}
  </div>
</div>
{!! Form::close() !!}

@section('scripts')
    <!-- JS Libraries -->
    <script src="{{ URL::asset('beagle-assets/lib/jquery-ui/jquery-ui.min.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('beagle-assets/lib/moment.js/min/moment.min.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('beagle-assets/js/app-form-elements.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('beagle-assets/lib/select2/js/select2.min.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('beagle-assets/lib/bootstrap-slider/bootstrap-slider.min.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('beagle-assets/lib/datetimepicker/js/bootstrap-datetimepicker.min.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('beagle-assets/js/app-form-elements.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('beagle-assets/lib/parsley/parsley.min.js') }}" type="text/javascript"></script>
    <script>
      $(document).ready(function() {
        //initialize the javascript
        App.init();
        App.formElements();
        $('#store-appointment-form').parsley(); //frontend validation

        $('#applicant_id').change(function(){
            position     = $(this).find(':selected').data('position')
            office       = $(this).find(':selected').data('office')
            item_number  = $(this).find(':selected').data('item_number')
            salary_grade = $(this).find(':selected').data('salary_grade')
            publication  = $(this).find(':selected').data('publication')
            basic_salary  = $(this).find(':selected').data('basic_salary')

            $('#position').val(position)
            $('#office').val(office)
            $('#item_number').val(item_number)
            $('#salary_grade').val(salary_grade)
            $('#publication').val(publication)
            $('#basic_salary').val(basic_salary)
        });

        $('#applicant_id').trigger('change');

      });
    </script>
@endsection
