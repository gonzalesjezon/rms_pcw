<?php

namespace App\Http\Controllers;

use App\ErasureAlteration;
use App\AppointmentForm;
use App\Applicant;
use App\Job;
use Auth;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;

class ErasureAlterationController extends Controller
{

    public function __construct()
    {
        View::share('title', 'Erasures and Alteration');
        $this->middleware('auth');
        $this->module = 'erasure_alterations';
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $perPage = 100;
        $erasures = ErasureAlteration::latest()
        ->paginate($perPage);

        return view('erasure_alterations.index', [
            'erasures' => $erasures,
            'module' => $this->module
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {

        $appointees = AppointmentForm::where('form_status',1)
        ->get();

        return view('erasure_alterations.create', [
            'appointees' => $appointees,
            'module' => $this->module
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $erasures = new ErasureAlteration();
        $erasures->fill($request->all());
        $erasures->created_by = Auth::id();

        $erasures->save();

       return redirect()
           ->route('erasure_alterations.edit',[
            'erasures' => $erasures
           ])->with('success', 'Erasure and alteration was successfully created.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ErasureAlteration  $erasureAlteration
     * @return \Illuminate\Http\Response
     */
    public function show(ErasureAlteration $erasureAlteration)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ErasureAlteration  $erasureAlteration
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $erasures = new ErasureAlteration();
        if ($id) {
            $erasures = ErasureAlteration::find($id)->first();
        }

        $jobs = Job::leftJoin('positionitem as p','p.RefId','=','jobs.plantilla_item_id')
            ->where('publish',1)->getModels();

        $applicants = Applicant::where('qualified',1)->getModels();

        return view('erasure_alterations.edit')->with([
            'erasures' => $erasures,
            'jobs' => $jobs,
            'applicants' => $applicants,
            'module' => $this->module
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ErasureAlteration  $erasureAlteration
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {

        $erasures = ErasureAlteration::find($request->id);
        $erasures->fill($request->all());
        $erasures->updated_by = Auth::id();

        $erasures->save();

        return redirect()
           ->route('erasure_alterations.edit',[
            'erasures' => $erasures
           ])->with('success', 'Erasure and alteration was successfully updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ErasureAlteration  $erasureAlteration
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $erasure = ErasureAlteration::find($id);
        $erasure->delete();
        return redirect('erasure_alterations')->with('success', 'Erasure and alteration record deleted!');
    }


    public function report(Request $request){

        $erasures = ErasureAlteration::find($request->id)->first();

        return view('erasure_alterations.report',[
            'erasures' => $erasures
        ]);
    }
}
