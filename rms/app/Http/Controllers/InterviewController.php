<?php

namespace App\Http\Controllers;

use App\Examination;
use App\Interview;
use App\Job;
use App\SelectionLineup;
use App\Applicant;
use App\Appointment;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;

use Illuminate\Support\Facades\Mail;
use App\Mail\SendMailable;
use Carbon\Carbon;

class InterviewController extends Controller
{

    public function __construct()
    {
        View::share('title', 'Schedule of Interview');
        $this->middleware('auth');
        $this->module = 'interviews';
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $perPage = 100;
        $interview = Interview::latest()
        ->paginate($perPage);

        return view('interviews.index',[
            'interviews' => $interview,
            'module' => $this->module
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $jobs = Job::leftJoin('positionitem as p','p.RefId','=','jobs.plantilla_item_id')
            ->where('status',$request->status)
            ->where('publish',1)->getModels();

            return view('interviews.create',[
                'jobs' => $jobs,
                'method' => 'POST',
                'status' => $request->status,
                'module' => $this->module
            ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $interview =  new Interview();
        $interview->fill($request->all());
        $interview->notify = ($request->notify) ? 1 : 0;
        $interview->created_by = Auth::id();

        if($request->interview_status == 3){
            $this->updateApplicant($request,'add');
        }

        if($interview->notify == 1){
            $message['status'] = $request->interview_status;
            $message['data'] = $interview;
            $message['type'] = 'interview';

            $this->mail($request->email, 'Subject', $message);
        }

        if($request->interview_status == 6)
        {
            $interview->date_approved = Carbon::now();
        }

        $interview->save();

        return redirect()
            ->route('interviews.edit',[
                'interview_id' => $interview->id,
                'status' => $request->status,
            ])->with('success', 'The interview status was successfully created.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Interview  $interview
     * @return \Illuminate\Http\Response
     */
    public function show(Interview $interview)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Interview  $interview
     * @return \Illuminate\Http\Response
     */
    public function edit($id, Request $request)
    {
        $interview = Interview::find($id);
        $applicant = Applicant::find($interview->applicant_id);
        $currentJob = 0;
        $selected_applicant = [];
        if(isset($applicant->job_id)){
            $currentJob = Job::find($applicant->job_id);
            $applicants = Applicant::where('job_id',$applicant->job_id)
            ->pluck('id')->toArray();

            $selected_applicant = Examination::whereIn('applicant_id',$applicants)
            ->where('exam_status',6)->getModels();
        }

        $jobs = Job::leftJoin('positionitem as p','p.RefId','=','jobs.plantilla_item_id')
            ->where('status',$currentJob->status)
            ->where('publish',1)->getModels();

        return view('interviews.edit',[
            'jobs' => $jobs,
            'currentJob' => $currentJob,
            'selected' => $selected_applicant,
            'interview' => $interview,
            'status' => $currentJob->status,
            'module' => $this->module
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Interview  $interview
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {

        $interview = Interview::find($request->interview_id);
        $interview->fill($request->all());
        $interview->notify                   = ($request->notify) ? 1 : 0;
        $interview->confirmed                = ($request->confirmed) ? 1 : 0;
        $interview->noftiy_resched_interview = ($request->noftiy_resched_interview) ? 1 : 0;
        $interview->cost_for_hire            = ($request->cost_for_hire) ? str_replace(',', '', $request->cost_for_hire) : 0;
        $interview->updated_by               = Auth::id();
        $interview->save();

        if($request->interview_status == 3){
            $this->updateApplicant($request,'add');
        }

        if($interview->notify == 1){
            $message['status'] = $request->interview_status;
            $message['data'] = $interview;
            $message['type'] = 'interview';

            $this->mail($request->email, 'Subject', $message);
        }

        if($request->interview_status == 6)
        {
            $interview->date_approved = Carbon::now();
        }
        $interview->save();

        return redirect()
            ->route('interviews.edit', [
                'interview_id' => $interview->id,
            ])
            ->with('success', 'The interview status was successfully updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Interview  $interview
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, Request $request)
    {
        $interview = Interview::find($id);
        $interview->delete();
        $this->updateApplicant($request,'delete');
        return redirect('interviews')->with('success', 'Interview schedule deleted!');
    }

    public function getApplicant(Request $request){

        $applicants = new Applicant();

        $currentJob = 0;
        if(isset($request->job_id)){
            $currentJob = Job::find($request->job_id);
            $applicants = $applicants
            ->where('job_id',$request->job_id)
            ->pluck('id')->toArray();
        }

        $selected_applicant = Examination::whereIn('applicant_id',$applicants)
            ->where('exam_status',6)->getModels();

        $jobs = Job::leftJoin('positionitem as p','p.RefId','=','jobs.plantilla_item_id')
            ->where('status',$request->status)
            ->where('publish',1)->getModels();

        return view('interviews.create',[
            'selected' => $selected_applicant,
            'jobs' => $jobs,
            'status' => $request->status,
            'currentJob' => $currentJob,
            'module' => $this->module
        ]);

    }

    public function mail($to, $subject, $message){

        Mail::to($to)->send(new SendMailable($message));

    }

    public function updateApplicant($request,$status){

        Applicant::where('id',$request->applicant_id)
        ->update([
            'interview_status' => ($status == 'add') ? 1 : 0
        ]);
    }

    public function sendMail(Request $request)
    {
        $subject = config('params.interview_status.'.$request->data['interview_status']);
        $interview = new Interview;
        $interview->job_id                     = $request->data['job_id'];
        $interview->applicant_id               = $request->data['applicant_id'];
        $interview->interview_date             = $request->data['interview_date'];
        $interview->interview_time             = $request->data['interview_time'];
        $interview->interview_location         = $request->data['interview_location'];
        $interview->interview_location         = $request->data['interview_location'];
        $interview->email                      = $request->data['email'];
        $interview->resched_interview_date     = $request->data['resched_interview_date'];
        $interview->resched_interview_time     = $request->data['resched_interview_time'];

        $message['status'] = $request->data['interview_status'];
        $message['data']   = $interview;
        $message['type']   = 'interview';

        $this->mail($request->data['email'], $subject, $message);

        return json_encode([
            'status' => true,
        ]);
    }
}
