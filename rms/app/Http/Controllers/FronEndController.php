<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;
use App\Job;
use App\Countries;
use App\Applicant;
use App\WorkExperience;
use App\Eligibility;
use App\Training;
use App\Education;

use Crypt;
class FronEndController extends Controller
{
    /**
     * Define your validation rules in a property in
     * the controller to reuse the rules.
     */
    protected $validationRules = [
        'first_name' => 'required|regex:/(^[A-Za-z0-9 ]+$)+/',
        'last_name' => 'required|regex:/(^[A-Za-z0-9 ]+$)+/',
        'email_address' => 'required|email'
    ];

    /**
     * Modify the globally used view variable here
     * initialization found on app\Providers\AppServiceProvider
     */
    public function __construct()
    {
        View::share('title', 'Philippine Commission on Women');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $perPage = 100;
        $jobs = Job::where('publish',1)
        ->latest()->paginate($perPage);
        return view('frontend.index')
        ->with([
            'jobs' => $jobs
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $id = Crypt::decrypt($request->id);
        $countries = Countries::all('code', 'name')
            ->pluck('name', 'code')->toArray();

        $jobs = Job::where('id',$id)->first();

        return view('frontend.create')->with([
            'action' => 'FronEndController@store',
            'civilStatus' => Applicant::getCivilStatus(),
            'gender' => Applicant::getGender(),
            'countries' => $countries,
            'jobs' => $jobs,

        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, $this->validationRules);

        $applicant = new Applicant;
        $applicant->fill($request->all());
        $applicant->first_name      =  Crypt::encrypt($request->first_name);
        $applicant->last_name       =  Crypt::encrypt($request->last_name);
        $applicant->middle_name     =  Crypt::encrypt($request->middle_name);
        $applicant->extension_name  =  Crypt::encrypt($request->extension_name);
        $applicant->email_address   =  Crypt::encrypt($request->email_address);
        $applicant->birthday        =  Crypt::encrypt($request->birthday);

        # Address
        $applicant->house_number    =  Crypt::encrypt($request->house_number);
        $applicant->street          =  Crypt::encrypt($request->street);
        $applicant->subdivision     =  Crypt::encrypt($request->subdivision);
        $applicant->barangay        =  Crypt::encrypt($request->barangay);
        $applicant->city            =  Crypt::encrypt($request->city);
        $applicant->province        =  Crypt::encrypt($request->province);
        $applicant->zip_code        =  Crypt::encrypt($request->zip_code);

        # Permanent Address
        $applicant->permanent_house_number    =  Crypt::encrypt($request->permanent_house_number);
        $applicant->permanent_street          =  Crypt::encrypt($request->permanent_street);
        $applicant->permanent_subdivision     =  Crypt::encrypt($request->permanent_subdivision);
        $applicant->permanent_barangay        =  Crypt::encrypt($request->permanent_barangay);
        $applicant->permanent_city            =  Crypt::encrypt($request->permanent_city);
        $applicant->permanent_province        =  Crypt::encrypt($request->permanent_province);
        $applicant->permanent_zip_code        =  Crypt::encrypt($request->permanent_zip_code);


        $applicant->reference_no = uniqid();
        $applicant->saveImageFileNames($request);
        $applicant->saveDocumentFileNames($request);
        $applicant->setIsFilipino($request->filipino);
        $applicant->setIsNaturalized($request->naturalized);
        $applicant->created_by = (\Auth::id()) ? \Auth::id() : 88888888;


        if ($applicant->save()) {
            $applicant->uploadImageFiles($request);
            $applicant->uploadDocumentFiles($request);
            $primary = $this->savePrimaryEduc($request->primary,$applicant->id);
            $secondary = $this->saveSecondaryEduc($request->secondary,$applicant->id);
            $vocational = $this->saveVocational($request->vocational,$applicant->id);
            $college = $this->saveCollege($request->college,$applicant->id);
            $graduate = $this->saveGraduate($request->graduate,$applicant->id);
            $eligibility = $this->saveEligibility($request->eligibility,$applicant->id);
            $workexperience = $this->saveWorkExperience($request->work_experience,$applicant->id);
            $training = $this->saveTraining($request->training,$applicant->id);

        }

        return redirect('/frontend')->with('success', 'Successfully applied to vacant position.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $jobID = Crypt::decrypt($id);
        $jobs = Job::where('id',$jobID)->first();

        $split = explode('.', round(@$jobs->professional_fees,2));
        $numberInWord = $this->convert_number_to_words(@$jobs->professional_fees);
        $decimalInWord = $this->convert_number_to_words(@$split[1]);

        return view('frontend.show')->with([
            'jobs' => $jobs,
            'number_in_word' => $numberInWord,
            'decimal_in_word' => $decimalInWord

        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function saveEligibility($data,$applicant_id){

        foreach ($data as $key => $value) {

            if(isset($value['eligibility_ref'])){
                $eligibility = Eligibility::find(@$value['id']);

                if(empty($eligibility)){
                    $eligibility = new Eligibility;
                }
                $eligibility->applicant_id         = $applicant_id;
                $eligibility->eligibility_ref      = $value['eligibility_ref'];
                $eligibility->rating               = $value['rating'];
                $eligibility->exam_place           = $value['exam_place'];
                $eligibility->license_number       = $value['license_number'];
                $eligibility->license_validity     = $value['license_validity'];
                $eligibility->exam_date            = $value['exam_date'];
                $eligibility->save();
            }

        }
    }

    public function saveWorkExperience($data,$applicant_id){

        foreach ($data as $key => $value) {

            if(isset($value['position_title'])){
                $workexperience = WorkExperience::find(@$value['id']);

                if(empty($workexperience)){
                    $workexperience = new WorkExperience;
                }
                $workexperience->applicant_id           = $applicant_id;
                $workexperience->inclusive_date_from    = $value['inclusive_date_from'];
                $workexperience->inclusive_date_to      = $value['inclusive_date_to'];
                $workexperience->present_work           = (@$value['present_work']) ? 1 : 0;
                $workexperience->position_title         = $value['position_title'];
                $workexperience->department             = $value['department'];
                $workexperience->salary_grade           = $value['salary_grade'];
                $workexperience->status_of_appointment  = $value['status_of_appointment'];
                $workexperience->govt_service           = (@$value['govt_service']) ? 1 : 0;
                $workexperience->save();
            }

        }
    }

    public function saveTraining($data,$applicant_id){

        foreach ($data as $key => $value) {

            if(isset($value['title_learning_programs'])){
                $training = Training::find(@$value['id']);

                if(empty($training)){
                    $training = new Training;
                }
                $training->applicant_id             = $applicant_id;
                $training->title_learning_programs  = $value['title_learning_programs'];
                $training->inclusive_date_from      = $value['inclusive_date_from'];
                $training->inclusive_date_to        = $value['inclusive_date_to'];
                $training->number_hours             = $value['number_hours'];
                $training->ld_type                  = $value['ld_type'];
                $training->sponsored_by             = $value['sponsored_by'];
                $training->save();
            }

        }
    }

    public function savePrimaryEduc($primary,$applicant_id){


        $education = Education::find(@$primary[0]['id']);
        if(empty($education)){
            $education = new Education;
        }

        if(isset($primary[0]['school_name'])){
            $education->applicant_id        = $applicant_id;
            $education->school_name         = $primary[0]['school_name'];
            $education->course              = $primary[0]['course'];
            $education->attendance_from     = $primary[0]['attendance_from'];
            $education->attendance_to       = $primary[0]['attendance_to'];
            $education->level               = $primary[0]['level'];
            $education->graduated           = $primary[0]['graduated'];
            $education->awards              = $primary[0]['awards'];
            $education->educ_level          = 1;
            $education->save();
        }

    }

    public function saveSecondaryEduc($secondary,$applicant_id){

        foreach ($secondary as $key => $value) {

            $education = Education::find(@$value['id']);
            if(empty($education)){
                $education = new Education;
            }
            if(isset($value['school_name'])){
                $education->applicant_id        = $applicant_id;
                $education->school_name         = $value['school_name'];
                $education->course              = $value['course'];
                $education->attendance_from     = $value['attendance_from'];
                $education->attendance_to       = $value['attendance_to'];
                $education->level               = $value['level'];
                $education->graduated           = $value['graduated'];
                $education->awards              = $value['awards'];
                $education->educ_level          = 2;
                $education->save();
            }
        }

    }

    public function saveVocational($vocational,$applicant_id){

        if($vocational){
            foreach ($vocational as $key => $value) {
                $education = Education::find(@$value['id']);
                if(empty($education)){
                    $education = new Education;
                }
                if(isset($value['school_name'])){
                    $education->applicant_id        = $applicant_id;
                    $education->school_name         = $value['school_name'];
                    $education->course              = $value['course'];
                    $education->attendance_from     = $value['attendance_from'];
                    $education->attendance_to       = $value['attendance_to'];
                    $education->level               = $value['level'];
                    $education->graduated           = $value['graduated'];
                    $education->awards              = $value['awards'];
                    $education->educ_level          = 3;
                    $education->save();
                }
            }
        }


    }

    public function saveCollege($college,$applicant_id){

        foreach ($college as $key => $value) {
            $education = Education::find(@$value['id']);
            if(empty($education)){
                $education = new Education;
            }

            if(isset($value['school_name'])){
                $education->applicant_id        = $applicant_id;
                $education->school_name         = $value['school_name'];
                $education->course              = $value['course'];
                $education->attendance_from     = $value['attendance_from'];
                $education->attendance_to       = $value['attendance_to'];
                $education->level               = $value['level'];
                $education->graduated           = $value['graduated'];
                $education->awards              = $value['awards'];
                $education->educ_level          = 4;
                $education->save();
            }
        }

    }

    public function saveGraduate($graduate,$applicant_id){

        if(isset($graduate)){
            foreach ($graduate as $key => $value) {
                $education = Education::find(@$value['id']);
                if(empty($education)){
                    $education = new Education;
                }
                if(isset($value['school_name'])){
                    $education->applicant_id        = $applicant_id;
                    $education->school_name         = $value['school_name'];
                    $education->course              = $value['course'];
                    $education->attendance_from     = $value['attendance_from'];
                    $education->attendance_to       = $value['attendance_to'];
                    $education->level               = $value['level'];
                    $education->graduated           = $value['graduated'];
                    $education->awards              = $value['awards'];
                    $education->ongoing             = (@$value['ongoing']) ? 1 :0;
                    $education->educ_level          = 5;
                    $education->save();
                }
            }
        }


    }

    public function postJob(Request $request)
    {
        return redirect()
            ->route('frontend.create',['id' => Crypt::encrypt($request->id)]);
    }
}
