<?php

namespace App\Http\Controllers;

use App\InterviewGuide;
use App\Interview;
use App\Examination;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;

use Auth;

class InterviewGuideController extends Controller
{
    public function __construct()
    {
        View::share('title', 'Interview Form');
        $this->middleware('auth');
        $this->module = 'interview_guide';
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $perPage = 100;
        $guides = InterviewGuide::latest()
        ->paginate($perPage);

        return view('interview_guide.index',[
            'guides' => $guides,
            'module' => $this->module
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $interviewees = Examination::where('exam_status',6)->getModels();

        return view('interview_guide.create',[
            'interviewees' => $interviewees,
            'module' => $this->module
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'applicant_id' => 'required'
        ]);

        $guide = new InterviewGuide;
        $guide->fill($request->all());
        $guide->created_by = Auth::id();
        $guide->save();


        return redirect()
            ->route('interview_guide.edit',[
                'guide' => $guide,

            ])->with('success', 'The interview guide was successfully created.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\InterviewGuide  $interviewGuide
     * @return \Illuminate\Http\Response
     */
    public function show(InterviewGuide $interviewGuide)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\InterviewGuide  $interviewGuide
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $guide = InterviewGuide::find($id)->first();

        return view('interview_guide.edit',[
            'interviewees' => Interview::where('confirmed',1)->getModels(),
            'guide' => $guide,
            'module' => $this->module
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\InterviewGuide  $interviewGuide
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $guide = InterviewGuide::find($request->id);
        $guide->fill($request->all());
        $guide->updated_by = Auth::id();
        $guide->save();

        return redirect()
            ->route('interview_guide.edit',[
                'guide' => $guide,

            ])->with('success', 'The interview guide was successfully updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\InterviewGuide  $interviewGuide
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $interview = InterviewGuide::find($id);
        $interview->delete();
        return redirect('interview_guide')->with('success', 'Interview guide deleted!');
    }

    public function report(Request $request)
    {
        $guide = InterviewGuide::find($request->id)->first();

        return view('interview_guide.report',[
            'guide' => $guide
        ]);
    }
}
