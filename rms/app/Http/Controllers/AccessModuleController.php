<?php

namespace App\Http\Controllers;

use App\AccessModule;
use App\AccessRight;
use App\Module;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Auth;

class AccessModuleController extends Controller
{

    public function __construct()
    {
        View::share('title', 'Access Type');
        $this->middleware('auth');
        $this->module = 'access_modules';
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->check($this->module);

        $perPage = 100;
        $access_module = AccessModule::latest()
        ->paginate($perPage);

        return view('access_modules.index',[
            'access_modules' => $access_module,
            'module' => $this->module
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->check($this->module);

        $modules = Module::orderBy('description','asc')->getModels();

        return view('access_modules.create',[
            'modules' => $modules,
            'module' => $this->module
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'access_name' => 'required'
        ]);

        $access_module = new AccessModule;
        $access_module->fill($request->all());
        $access_module->created_by = Auth::id();

        if($access_module->save()){
            $access_right = $this->storeAccessRight($request,$access_module->id);
        }

        return redirect('access_modules/')
            ->with('success','Access module successfully created');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\AccessModule  $accessModule
     * @return \Illuminate\Http\Response
     */
    public function show(AccessModule $accessModule)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\AccessModule  $accessModule
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->check($this->module);
        $access_module = AccessModule::find($id);
        $access_right = [];
        if(count($access_module->access_rights) > 1){
            foreach ($access_module->access_rights as $key => $value) {
                $access_right[$value->module_id] = $value->id;
            }
        }


        return view('access_modules.edit',[
            'access_module' => $access_module,
            'access_right' => $access_right,
            'modules' => Module::orderBy('description','asc')->getModels(),
            'module' => $this->module
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\AccessModule  $accessModule
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {

        $this->validate($request,[
            'access_name' => 'required'
        ]);

        $access_module = AccessModule::find($request->id);

        if(empty($access_module)){
            $access_module = new AccessModule;
        }

        $access_module->fill($request->all());
        $access_module->updated_by = Auth::id();

        if($access_module->save()){
            $access_right = $this->storeAccessRight($request,$access_module->id);
        }

        return redirect('access_modules/')
            ->with('success','Access module successfully updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\AccessModule  $accessModule
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $module = AccessModule::find($id);
        $module->delete();
        AccessRight::where('access_module_id',$id)->delete();
        return redirect('access_modules')->with('success', 'Access module was successfully deleted!');
    }

    public static function storeAccessRight($request,$access_module_id){

        foreach ($request->access_right as $key => $value) {


            $access_right = AccessRight::find(@$request->access_id[$key]);

            if(empty($access_right)){
                $access_right = new AccessRight;
            }

            $access_right->access_module_id = $access_module_id;
            $access_right->module_id = $key;
            $access_right->to_view = (isset($value)) ? 1 :0;

            if($access_right->exists()){
                $access_right->updated_by = Auth::id();
            }else{
                $access_right->created_by = Auth::id();
            }
            $access_right->save();
        }
    }

}
