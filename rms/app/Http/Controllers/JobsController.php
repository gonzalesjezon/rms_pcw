<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Http\Requests,
    App\Job,
    App\Office,
    App\Division,
    App\PSIPOP,
    App\PlantillaItem,
    App\EmployeeInformation,
    App\EmployeeStatus,
    App\Employee,
    Illuminate\Http\Request,
    Illuminate\Support\Facades\View;
use http\Env\Response;
use Illuminate\Http\JsonResponse;

use Carbon\Carbon;

class JobsController extends Controller
{
    /**
     * Define your validation rules in a property in
     * the controller to reuse the rules.
     */
    protected $validationRules = [
        'appointer_id' => 'required',
    ];

    /**
     * Call behavior handling for authentication
     * authentication section via middleware
     * =====================================
     * Modify the globally used view variable here
     * initialization found on app\Providers\AppServiceProvider
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['index', 'careers']]);
        View::share('title', 'Job Posting');
        $this->module = 'jobs';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $perPage = 100;
        $jobs = Job::latest()
            ->paginate($perPage);

        return view('jobs.index', [
            'jobs' => $jobs,
            'module' => $this->module
        ]);
    }

    /**
     * Display a listing of the resource [non-plantilla].
     *
     * @return \Illuminate\View\View
     */
    public function nonPlantilla(Request $request)
    {
        $perPage = 100;
        $jobs = Job::latest()
            ->where('status', '=', 'non-plantilla')
            ->paginate($perPage);

        return view('jobs.non-plantilla', [
            'jobs' => $jobs,
            'division' => config('params.division')
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param $request \Illuminate\Http\Request $request
     *
     * @return \Illuminate\View\View
     */
    public function create(Request $request)
    {
        $job = new Job();
        if (empty($job->status)) {
            $job->status = $request->type;
        }

        $category = ($request->status == 'plantilla') ? 1 : 0;

        $arrStatus = EmployeeStatus::where('category',$category)
        ->pluck('RefId')
        ->toArray();

        $items = PSIPOP::where('status',0)
        ->whereIn('employee_status_id',$arrStatus)
        ->orderBy('item_number','asc')
        ->get();

        $employees = Employee::orderBy('FirstName','asc')->getModels();

        return view('jobs.create')->with([
            'job'            => $job,
            'status'         => $request->status,
            'items'          => $items,
            'employees'      => $employees,
            'module'         => $this->module
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {


        $this->validate($request, $this->validationRules);
        $job = new Job;
        $job->fill($request->all());
        if (!empty($job->publish)) {
            $job->publish = 1;
            $job->publish_date = Carbon::now();
        }
        $job->station              = ($request->station2) ? $request->station2 : $request->station;
        $job->reporting_line       = ($request->reporting_line2) ? $request->reporting_line2 : $request->reporting_line;
        $job->daily_salary         = ($request->daily_salary) ? str_replace(',', '', $request->daily_salary) : 0;
        $job->pera_amount          = ($request->pera_amount) ? str_replace(',', '', $request->pera_amount) : 0;
        $job->clothing_amount      = ($request->clothing_amount) ? str_replace(',', '', $request->clothing_amount) : 0;
        $job->midyear_amount       = ($request->midyear_amount) ? str_replace(',', '', $request->midyear_amount) : 0;
        $job->yearend_amount       = ($request->yearend_amount) ? str_replace(',', '', $request->yearend_amount) : 0;
        $job->cashgift_amount      = ($request->cashgift_amount) ? str_replace(',', '', $request->cashgift_amount) : 0;
        $job->publication_1 = ($request->publication_1) ? 1 : 0;
        $job->publication_2 = ($request->publication_2) ? 1 : 0;
        $job->publication_3 = ($request->publication_3) ? 1 : 0;
        $job->publication_4 = ($request->publication_4) ? 1 : 0;
        $job->created_by = \Auth::id();
        $job->save();

        return redirect()
            ->route('jobs.edit', [
                'id' => $job->id,
                'status' => $job->status
            ])
            ->with('success', 'The job post was successfully created.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $job = Job::findOrFail($id);

        return view('jobs.show',[
            'job' => $job,
            'module' => $this->module
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id, Request $request)
    {

        $job = Job::findOrFail($id);
        $currentItem = new PSIPOP();
        if(isset($job->plantilla_item_id)){
            $currentItem = $currentItem->find($job->plantilla_item_id);
        }

        $items = PSIPOP::where('status',0)
        ->orderBy('item_number','asc')
        ->get();

        $employees = Employee::orderBy('FirstName','asc')
        ->getModels();

        return view('jobs.edit', [
            'job'           => $job,
            'status'        => $request->status,
            'items'         => $items,
            'currentItem'   => $currentItem,
            'employees'  => $employees,
            'module' => $this->module
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {

        $this->validate($request, $this->validationRules);

        $job = Job::findOrFail($id);
        $job->fill($request->all());
        $job->station              = ($request->station2) ? $request->station2 : $request->station;
        $job->reporting_line       = ($request->reporting_line2) ? $request->reporting_line2 : $request->reporting_line;
        $job->monthly_basic_salary = ($request->monthly_basic_salary) ? str_replace(',', '', $request->monthly_basic_salary) : 0;
        $job->daily_salary         = ($request->daily_salary) ? str_replace(',', '', $request->daily_salary) : 0;
        $job->pera_amount          = ($request->pera_amount) ? str_replace(',', '', $request->pera_amount) : 0;
        $job->clothing_amount      = ($request->clothing_amount) ? str_replace(',', '', $request->clothing_amount) : 0;
        $job->midyear_amount       = ($request->midyear_amount) ? str_replace(',', '', $request->midyear_amount) : 0;
        $job->yearend_amount       = ($request->yearend_amount) ? str_replace(',', '', $request->yearend_amount) : 0;
        $job->cashgift_amount      = ($request->cashgift_amount) ? str_replace(',', '', $request->cashgift_amount) : 0;
        $job->publish = (!empty($request->publish)) ? 1 : 0;
        $job->publish_date = (!empty($request->publish)) ? Carbon::now() : '';
        $job->publication_1 = ($request->publication_1) ? 1 : 0;
        $job->publication_2 = ($request->publication_2) ? 1 : 0;
        $job->publication_3 = ($request->publication_3) ? 1 : 0;
        $job->publication_4 = ($request->publication_4) ? 1 : 0;

        $job->update();

        return redirect()
            ->route('jobs.edit', [
                'id' => $job->id,
                'status' => $job->status
            ])
            ->with('success', 'The job post was successfully updated.');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param int $id job id value
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function publish(Request $request)
    {
        $job = Job::findOrFail($request->id);
        $job->publish = (empty($job->publish)) ? 1 : 0;
        $job->update();

        return \response()->json([
                'status' => 'success',
                'data' => [
                    'id' => $job->id
                ],
            ]
            , 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Job::destroy($id);

        return redirect('jobs')->with('success', 'Job post deleted!');
    }

    // public function getPsipop(Request $request){
    //     $currentItem = new PSIPOP();
    //     $job = Job::where('plantilla_item_id',$request->plantilla_item_id)->first();
    //     $page = 'edit';
    //     if(empty($job)){
    //         $page = 'create';
    //     }

    //     if(isset($request->plantilla_item_id)){
    //         $currentItem = $currentItem->find($request->plantilla_item_id);
    //     }

    //     $plantilla_item = PSIPOP::where('status',0)
    //     ->orderBy('item_number','asc')
    //     ->pluck('item_number','id')
    //     ->toArray();

    //     $employeeinfo = EmployeeInformation::getModels();

    //     return view('jobs.'.$page, [
    //         'plantilla_item' => $plantilla_item,
    //         'currentItem' => $currentItem,
    //         'status' => $request->request_status,
    //         'job' => $job,
    //         'employeeinfo' => $employeeinfo
    //     ]);
    // }

    public function typePersonnel(Request $request){

        return redirect()->route('jobs.create',[
            'status' => $request->type_personnel
        ]);

    }

    public function report(Request $request)
    {
        $jobs = Job::where('id',$request->id)->first();

        return view('jobs.report',[
            'jobs' => $jobs
        ]);   
    }
}
