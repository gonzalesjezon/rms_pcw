<?php

namespace App\Http\Controllers;

use App\BackgroundChecking;
use App\Applicant;
use App\Interview;
use App\JobOffer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Auth;

class BackgroundCheckingController extends Controller
{
    public function __construct()
    {
        View::share('title', 'Background Checking');
        $this->middleware('auth');
        $this->module = 'background_checking';
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $perPage = 100;

        $bcheckings = BackgroundChecking::latest()
        ->paginate($perPage);

        return view('background_checking.index',[
            'bcheckings' => $bcheckings,
            'module' => $this->module,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $applicants = Applicant::where('qualified',1)->getModels();

        return view('background_checking.create',[
            'applicants' => $applicants,
            'module' => $this->module
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request,[
            'applicant_id' => 'required'
        ]);

        $bchecking = new BackgroundChecking;
        $bchecking->fill($request->all());
        $bchecking->provided_reference                  = ($request->provided_reference) ? 1 : 0;
        $bchecking->permission                          = ($request->permission) ? 1 : 0;
        $bchecking->use_of_profance_language            = ($request->use_of_profance_language) ? 1 : 0;
        $bchecking->drug_user                           = ($request->drug_user) ? 1 : 0;
        $bchecking->risque_photos                       = ($request->risque_photos) ? 1 : 0;
        $bchecking->discriminating_comments             = ($request->discriminating_comments) ? 1 : 0;
        $bchecking->bullying_statement                  = ($request->bullying_statement) ? 1 : 0;
        $bchecking->observe_grammar                     = ($request->observe_grammar) ? 1 : 0;
        $bchecking->sharing_confidential_information    = ($request->sharing_confidential_information) ? 1 : 0;
        $bchecking->reference_check                     = ($request->reference_check) ? 1 : 0;
        $bchecking->created_by                          = Auth::id();
        $bchecking->save();

        return redirect()
                ->route('background_checking.edit',[
                    'bchecking' => $bchecking
                ])->with('success','The background checking was successfully created.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\BackgroundChecking  $backgroundChecking
     * @return \Illuminate\Http\Response
     */
    public function show(BackgroundChecking $backgroundChecking)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\BackgroundChecking  $backgroundChecking
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $bchecking = BackgroundChecking::find($id);

        return view('background_checking.edit',[
            'bchecking' => $bchecking,
            'module' => $this->module
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\BackgroundChecking  $backgroundChecking
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {

        $bchecking = BackgroundChecking::find($request->id);
        $bchecking->fill($request->all());
        $bchecking->provided_reference                  = ($request->provided_reference) ? 1 : 0;
        $bchecking->permission                          = ($request->permission) ? 1 : 0;
        $bchecking->use_of_profance_language            = ($request->use_of_profance_language) ? 1 : 0;
        $bchecking->drug_user                           = ($request->drug_user) ? 1 : 0;
        $bchecking->risque_photos                       = ($request->risque_photos) ? 1 : 0;
        $bchecking->discriminating_comments             = ($request->discriminating_comments) ? 1 : 0;
        $bchecking->bullying_statement                  = ($request->bullying_statement) ? 1 : 0;
        $bchecking->observe_grammar                     = ($request->observe_grammar) ? 1 : 0;
        $bchecking->sharing_confidential_information    = ($request->sharing_confidential_information) ? 1 : 0;
        $bchecking->reference_check                     = ($request->reference_check) ? 1 : 0;
        $bchecking->updated_by = Auth::id();
        $bchecking->save();

        return redirect()
                ->route('background_checking.edit',[
                    'bchecking' => $bchecking
                ])->with('success','The background checking was successfully updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\BackgroundChecking  $backgroundChecking
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $bchecking = BackgroundChecking::find($id);
        $bchecking->delete();
        return redirect('background_checking/')->with('success','Deleted successfully.');
    }

    public function storeJobOffer($request){

        $joboffer = JobOffer::where('applicant_id',$request->applicant_id)->first();
        if (empty($joboffer)) {
            $joboffer = new JobOffer;
        }
        if($joboffer->exists()){
            $joboffer->updated_by = Auth::id();
        }else{
            $joboffer->created_by = Auth::id();
        }
        $joboffer->applicant_id = $request->applicant_id;
        $joboffer->save();
    }

    public function report(Request $request){
        $bchecking = BackgroundChecking::find($request->id)->first();
        return view('background_checking.report',[
            'bchecking' => $bchecking
        ]);
    }
}
