<?php

namespace App\Http\Controllers;

use App\Applicant;
use App\Evaluation;
use App\Http\Requests\StoreMatrixQualification;
use App\Job;
use App\Recommendation;
use App\MatrixQualification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;

class EvaluationController extends Controller
{
    /**
     * Define your validation rules in a property in
     * the controller to reuse the rules.
     */
    protected $validationRules = [

    ];

    /**
     * Modify the globally used view variable here
     * initialization found on app\Providers\AppServiceProvider
     */
    public function __construct()
    {
        View::share('title', 'Evaluations');
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $perPage = 100;
        $matrixQualifications = MatrixQualification::latest()
            ->paginate($perPage);

        return view('evaluation.index', [
            'matrixQualifications' => $matrixQualifications
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $evaluation = new Evaluation();

        if ($request->reference) {
            $applicant = Applicant::where('reference_no', $request->reference)
                ->first();
        }

        if (empty($applicant)) {
            return redirect('/evaluation')->with('error', 'Applicant reference number not given.');
        }


        return view('evaluation.create')->with([
            'evaluation' => $evaluation,
            'applicant' => $applicant,
            'action' => 'EvaluationController@store',
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @throws
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request, $this->validationRules);

        // get applicant data
        if ($request->reference) {
            $applicant = Applicant::where('reference_no', $request->reference)
                ->first();
        } else {
            return redirect('/evaluation')->with('error', 'Applicant reference number not given.');
        }

        // redirect if not found applicant
        if (empty($applicant->id)) {
            return redirect('/evaluation')->with('error', 'Applicant data not found.');
        }

        $evaluation = Evaluation::where('applicant_id', $applicant->id)
            ->first();
        if (empty($evaluation->id)) {
            $evaluation = new Evaluation();
        }

        $evaluation->fill($request->all());
        $evaluation->applicant_id = $applicant->id;
        $evaluation->job_id = $applicant->job_id;
        $evaluation->created_by = \Auth::id();
        $evaluation->save();

        return redirect('/evaluation')->with('success', 'The evaluation was successfully created.');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Evaluation $evaluation
     * @return \Illuminate\Http\Response
     */
    public function rating(Request $request)
    {
        $applicant = $this->applicantData($request);
        if (!$applicant) {
            return redirect('/evaluation')->with('error', 'Applicant data not found.');
        }

        $evaluation = $this->evaluationData($request);
        if (!$evaluation) {
            $evaluation = new Evaluation();
        }

        return view('evaluation.rating')->with([
            'evaluation' => $evaluation,
            'applicant' => $applicant,
            'action' => 'EvaluationController@store',
        ]);
    }

    /**
     * Retrieves the applicant model data
     *
     * @param \Illuminate\Http\Request $request
     * @return mixed
     */
    public function applicantData($request)
    {
        if ($request->reference) {
            $applicant = Applicant::where('reference_no', $request->reference)
                ->first();
        }

        // redirect if not found applicant
        if (!empty($applicant->id)) {
            return $applicant;
        }

        return false;
    }

    /**
     * Retrieves the applicant model data with matrix
     *
     * @param \Illuminate\Http\Request $request
     * @return mixed
     */
    public function withMatrix($request)
    {
        if ($request->currentJob) {
            $applicant = Applicant::where('job_id', $request->currentJob)
                ->getModels();
        }

        // redirect if not found applicant
        if (count($applicant) > 0) {
            return $applicant;
        }

        return false;
    }

    /**
     * Retrieves the evaluation model data
     *
     * @param \Illuminate\Http\Request $request
     * @return Evaluation|\Illuminate\Http\RedirectResponse|mixed
     */
    public function evaluationData($request)
    {
        if ($request->id) {
            $evaluation = Evaluation::find($request->id);
        }

        if (!empty($evaluation->id)) {
            return $evaluation;
        }

        return false;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Evaluation $evaluation
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        MatrixQualification::destroy($id);
        return redirect('/evaluation')->with('success', 'Evaluation data deleted!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Evaluation $evaluation
     * @return \Illuminate\Http\Response
     */
    public function matrixQualification(Request $request)
    {

        $withMatrix = new MatrixQualification;
        $withMatrix = $withMatrix->select('applicant_id')->get()->toArray();

        $currentJob = new Job();
        $applicant  = new Applicant;
        $applicants = [];
        if (!empty($request->all())) {
            $currentJob = Job::find($request->position_consideration);
            // $applicants = $currentJob->applicants()->getModels();
            $applicants = $applicant
                ->where('job_id',$request->position_consideration)
                ->whereNotIn('id',@$withMatrix)->getModels();
        }

        $jobs = Job::where('status', '=', 'plantilla')
            ->pluck('title', 'id')
            ->toArray();

        return view('evaluation.create-matrix')->with([
            'action' => 'EvaluationController@storeMatrixQualification',
            'actionPosition' => 'EvaluationController@matrixQualification',
            'applicants' => $applicants,
            'currentJob' => $currentJob,
            'jobs' => $jobs,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Evaluation $evaluation
     * @throws
     * @return \Illuminate\Http\Response
     */
    public function storeMatrixQualification(Request $request)
    {

        foreach ($request->matrix as $matrixData) {

            if(isset($matrixData['checked'])){

                $this->validate(new Request($matrixData), (new StoreMatrixQualification)->rules());

                $matrix = MatrixQualification::where('applicant_id', $matrixData['applicant_id'])->first();
                if (empty($matrix->id)) {
                    $matrix = new MatrixQualification();
                }
                $matrix->fill($matrixData);
                $matrix->isc_chairperson = $request->isc_chairperson;
                $matrix->isc_member_one = $request->isc_member_one;
                $matrix->isc_member_two = $request->isc_member_two;
                $matrix->ea_representative = $request->ea_representative;

                if ($matrix->exists()) {
                    $matrix->updated_by = Auth::id();
                } else {
                    $matrix->created_by = Auth::id();
                }

                $matrix->save();
            }

        }

        return redirect('/evaluation')->with('success', 'The evaluation was successfully created.');
    }

    public function recommendEmployee(Request $request){

        dd($request->all());
        return redirect('/evaluation')->with('success', 'The evaluation was successfully created.');
    }

    /**
     * Displays Evaluation Report
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function evaluationReport(Request $request)
    {
        $applicant = $this->applicantData($request);
        $evaluation = $this->evaluationData($request);

        return view('evaluation.report')->with([
            'evaluation' => $evaluation,
            'applicant' => $applicant,
        ]);
    }

    /**
     * Displays Matrix Qualification Report
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function matrixQualificationReport(Request $request)
    {
        $job = new Job;
        $applicants = [];
        $currentJob = [];

        if(isset($request->currentJob)){
            $applicants = $this->withMatrix($request);
            $currentJob = $job->find($request->currentJob);
        }

        return view('evaluation.matrix-report')->with([
            'applicants' => $applicants,
            'jobs' => $currentJob
        ]);
    }

    /**
     * Displays Matrix Qualification Report
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function comparativeReport(Request $request)
    {

        $evaluations = [];
        $currentJob = new Job();

        if(isset($request->currentJob)){
            $currentJob = Job::find($request->currentJob);
            $evaluations = $currentJob->evaluations()->getModels();

        }

        return view('evaluation.comparative-report')->with([
            'evaluations' => $evaluations,
            'jobs' => $currentJob
        ]);
    }


    /**
     * Display the specified resource.
     *
     * @param  \App\Evaluation $evaluation
     * @return \Illuminate\Http\Response
     */
    public function comparativeRanking(Request $request)
    {
        $currentJob = new Job();
        $evaluations = [];
        if (!empty($request->position_consideration)) {
            $currentJob = Job::find($request->position_consideration);
            $evaluations = $currentJob->evaluations()->getModels();
        }
        $jobs = Job::where('status', '=', 'plantilla')
            ->pluck('title', 'id')
            ->toArray();

        return view('evaluation.create-comparative')->with([
            'action' => 'EvaluationController@storeComparativeRanking',
            'actionPosition' => 'EvaluationController@comparativeRanking',
            'evaluations' => $evaluations,
            'currentJob' => $currentJob,
            'jobs' => $jobs,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Evaluation $evaluation
     * @throws
     * @return \Illuminate\Http\Response
     */
    public function storeComparativeRanking(Request $request)
    {

        switch ($request->status) {
            case 'recommend':
                $ctr = 0;
                foreach ($request->matrix as $recommendData) {
                    if(isset($recommendData['recommend'])){
                        $recommend = Recommendation::where('applicant_id',$recommendData['applicant_id'])->first();
                        if(empty($recommend->id)){
                            $recommend = new Recommendation();
                        }
                        if ($recommend->exists()) {
                            $recommend->updated_by = Auth::id();
                        } else {
                            $recommend->created_by = Auth::id();

                        }
                        $recommend->applicant_id = $recommendData['applicant_id'];
                        $recommend->save();

                        $matrix = Evaluation::where('applicant_id', $recommendData['applicant_id'])->first();
                        $matrix->recommended = 1;
                        $matrix->updated_by = Auth::id();
                        $matrix->save();

                        $ctr++;
                    }
                }

                if($ctr > 0){
                    $Response = redirect('/evaluation')->with('success', 'Applicant was successfully recommended.');
                }else{
                    $Response = redirect('/evaluation')->with('error', 'Applicant was failed recommended.');
                }

                break;

            default:

                foreach ($request->matrix as $matrixData) {
                    $this->validate(new Request($matrixData), (new StoreMatrixQualification)->rules());

                    $matrix = MatrixQualification::where('applicant_id', $matrixData['applicant_id'])->first();
                    if (empty($matrix->id)) {
                        $matrix = new MatrixQualification();
                    }
                    $matrix->fill($matrixData);
                    $matrix->isc_chairperson = $request->isc_chairperson;
                    $matrix->isc_member_one = $request->isc_member_one;
                    $matrix->isc_member_two = $request->isc_member_two;
                    $matrix->ea_representative = $request->ea_representative;

                    if ($matrix->exists()) {
                        $matrix->updated_by = Auth::id();
                    } else {
                        $matrix->created_by = Auth::id();
                    }

                    $matrix->save();

                    $Response = redirect('/evaluation')->with('success', 'The evaluation was successfully created.');
                }

                break;
        }


        return $Response;
    }
}
