<?php

namespace App\Http\Controllers;

use App\Applicant;
use App\Appointment;
use App\AppointmentForm;
use App\AppointmentRequirement;
use App\Job;
use App\JobOffer;
use App\Assumption;
use App\OathOffice;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Auth;

class AppointmentFormController extends Controller
{
    /**
     * Modify the globally used view variable here
     * initialization found on app\Providers\AppServiceProvider
     */
    public function __construct()
    {
        View::share('title', 'Form');
        $this->middleware('auth');
        $this->module = 'appointment-form';
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $perPage = 100;
        $appointments = AppointmentForm::latest()
            ->paginate($perPage);

        return view('appointment-form.index', [
            'appointments' => $appointments,
            'module' => $this->module
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $jobId = Job::where('publish',1)
        ->where('status','plantilla')
        ->pluck('id')
        ->toArray();

        $appId = Applicant::whereIn('job_id',$jobId)
        ->pluck('id')
        ->toArray();

        $appointees = AppointmentRequirement::whereIn('applicant_id',$appId)
        ->where('complied',1)
        ->get();

        return view('appointment-form.create')->with([
            'appointees' => $appointees,
            'module' => $this->module
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        
        $appointmentform = new AppointmentForm;
        $appointmentform->fill($request->all());
        $appointmentform->created_by = Auth::id();
        
        $appointmentform->form_status = ($request->form_status) ? 1 : 0;
        if($appointmentform->save()){
            $assumption = $this->saveAssumption($request);
            $oathoffice = $this->saveOath($request);
            $requirement = $this->saveRequirement($request);
        }
        return redirect()->route('appointment-form.edit',[
            'form' => $appointmentform
        ])->with('success', 'The Appointment was successfully created.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $appointee = AppointmentForm::find($id)->first();

        return view('appointment-form.edit',[
            'form' => $appointee,
            'module' => $this->module
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {

        $appointmentform = AppointmentForm::find($request->id);
        $appointmentform->fill($request->all());
        $appointmentform->updated_by = Auth::id();
        
        $appointmentform->form_status = ($request->form_status) ? 1 : 0;
        if($appointmentform->save()){
            $assumption = $this->saveAssumption($request);
            $oathoffice = $this->saveOath($request);
            $requirement = $this->saveRequirement($request);
        }
        return redirect()->route('appointment-form.edit',[
            'form' => $appointmentform
        ])->with('success', 'The Appointment was successfully updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $form = AppointmentForm::find($id);
        $form->delete();
        return redirect('/appointment-form')->with('success', 'Appointment data deleted!');
    }

    public function report(Request $request){

        $applicant = AppointmentForm::find($request->id)->first();
        $numberInWord = $this->convert_number_to_words(@$applicant->applicant->job->plantilla_item->basic_salary);

        return view('appointment-form.report',[
            'applicant' => $applicant,
            'number_in_word' => $numberInWord
        ]);
    }

    public function saveAssumption($request){
        $assumption = Assumption::where('applicant_id',$request->applicant_id)->first();
        if(empty($assumption)){
            $assumption = new Assumption();
        }
        $assumption->fill($request->all());

        if($assumption->exists()){
            $assumption->updated_by = Auth::id();
        }else{
            $assumption->created_by = Auth::id();
        }
        $assumption->save();
    }

    public function saveOath($request){
        $oathoffice = OathOffice::where('applicant_id',$request->applicant_id)->first();
        if(empty($oathoffice)){
            $oathoffice = new OathOffice();
        }
        $oathoffice->fill($request->all());

        if($oathoffice->exists()){
            $oathoffice->updated_by = Auth::id();
        }else{
            $oathoffice->created_by = Auth::id();
        }
        $oathoffice->save();
    }

    public function saveRequirement($request){
        $requirement = AppointmentRequirement::where('applicant_id',$request->applicant_id)->first();
        if(empty($requirement)){
            $requirement = new AppointmentRequirement();
        }
        $requirement->fill($request->all());

        if($requirement->exists()){
            $requirement->updated_by = Auth::id();
        }else{
            $requirement->created_by = Auth::id();
        }
        $requirement->save();
    }

}
