<?php

namespace App\Http\Controllers;

use App\OathOffice;
use App\AppointmentForm;
use App\Applicant;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;

class OathOfficeController extends Controller
{

    public function __construct()
    {
        View::share('title', 'Oath of Office');
        $this->middleware('auth');
        $this->module = 'oath-office';
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $perPage = 100;
        $oathoffice = OathOffice::latest()
        ->paginate($perPage);

        return view('oath-office.index', [
            'oathoffices' => $oathoffice,
            'module' => $this->module
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $appointees = AppointmentForm::where('form_status',1)
        ->get();

        return view('oath-office.create',[
            'appointees' => $appointees,
            'module' => $this->module
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $oathoffice = new OathOffice;
        $oathoffice->fill($request->all());
        $oathoffice->created_by = Auth::id();

        $oathoffice->save();

       return redirect()
            ->route('oath-office.edit',[
                'oathoffice' => $oathoffice
            ])->with('success', 'Oath of Office was successfully created.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\OathOffice  $oathOffice
     * @return \Illuminate\Http\Response
     */
    public function show(OathOffice $oathOffice)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\OathOffice  $oathOffice
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $oathoffice = new OathOffice();
        $applicant = new Applicant();

        if ($id) {
            $oathoffice = OathOffice::find($id)->first();
        }

        return view('oath-office.edit')->with([
            'oathoffice' => $oathoffice,
            'module' => $this->module
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\OathOffice  $oathOffice
     * @return \Illuminate\Http\Response
     */
    public function update($id,Request $request)
    {
        $oathoffice = OathOffice::find($id);
        $oathoffice->fill($request->all());
        $oathoffice->updated_by = Auth::id();

        $oathoffice->save();

       return redirect()
            ->route('oath-office.edit',[
                'oathoffice' => $oathoffice
            ])->with('success', 'Oath of Office was successfully updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\OathOffice  $oathOffice
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $oath = OathOffice::find($id);
        $oath->delete();
        return redirect('/oath-office')->with('success', 'Oath of Office was successfully deleted.');
    }

    public function oathOfficeReport(Request $request){

        $oathoffice = OathOffice::find($request->id);

        return view('oath-office.report',[
            'oathoffice' => $oathoffice,
        ]);
    }
}
