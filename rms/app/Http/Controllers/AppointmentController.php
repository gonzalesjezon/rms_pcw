<?php

namespace App\Http\Controllers;

use App\Applicant;
use App\Appointment;
use App\AppointmentForm;
use App\Job;
use App\JobOffer;
use App\Assumption;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Auth;

class AppointmentController extends Controller
{
    /**
     * Define your validation rules in a property in
     * the controller to reuse the rules.
     */
    protected $validationRules = [

    ];

    /**
     * Modify the globally used view variable here
     * initialization found on app\Providers\AppServiceProvider
     */
    public function __construct()
    {
        View::share('title', 'Appointment');
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $perPage = 100;
        $appointments = Appointment::latest()
            ->paginate($perPage);

        return view('appointment.index', [
            'appointments' => $appointments
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $appointment = new Appointment();
        $applicant = new Applicant();

        if ($request->applicant_id) {
            $applicant = Applicant::where('id', $request->applicant_id)
                ->first();
            $appointment = Appointment::where('applicant_id', $request->applicant_id)
                ->first();
        }

        return view('appointment.create')->with([
            'appointment' => $appointment,
            'applicant' => $applicant,
            'action' => 'AppointmentController@store',
        ]);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        if(isset($request->appointment_id)){
            $appointment = Appointment::find($request->appointment_id);
            $appointment->fill($request->all());
            $appointment->form33_hrmo = (isset($request->form33_hrmo)) ? 1 : 0;
            $appointment->form34b_hrmo = (isset($request->form34b_hrmo)) ? 1 : 0;
            $appointment->form212_hrmo = (isset($request->form212_hrmo)) ? 1 : 0;
            $appointment->eligibility_hrmo = (isset($request->eligibility_hrmo)) ? 1 : 0;
            $appointment->form1_hrmo = (isset($request->form1_hrmo)) ? 1 : 0;
            $appointment->form32_hrmo = (isset($request->form32_hrmo)) ? 1 : 0;
            $appointment->form4_hrmo = (isset($request->form4_hrmo)) ? 1 : 0;
            $appointment->save();
        }

        return redirect('/appointment')->with('success', 'The Appointment was successfully created.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Appointment  $appointment
     * @return \Illuminate\Http\Response
     */
    public function show(Appointment $appointment)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Appointment  $appointment
     * @return \Illuminate\Http\Response
     */
    public function edit(Appointment $appointment)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Appointment  $appointment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Appointment $appointment)
    {

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Appointment  $appointment
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $appointment = Appointment::destroy($id);
        $appointmentform = AppointmentForm::where('applicant_id',$appointment->applicant_id)->delete();
        return redirect('/appointment')->with('success', 'Appointment data deleted!');
    }

    public function report(Request $request){

        $appointment = new Appointment();
        $applicant = new Applicant();

        if ($request->applicant_id) {
            $applicant = Applicant::where('id', $request->applicant_id)
                ->first();
            $appointment = Appointment::where('applicant_id', $request->applicant_id)
                ->first();
        }

        return view('appointment.report')->with([
            'appointment' => $appointment,
            'applicant' => $applicant,
        ]);
    }


}
