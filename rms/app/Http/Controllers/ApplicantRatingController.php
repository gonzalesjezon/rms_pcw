<?php

namespace App\Http\Controllers;

use App\ApplicantRating;
use App\Applicant;
use App\Examination;
use Auth;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;

class ApplicantRatingController extends Controller
{

    public function __construct()
    {   
        View::share('title', 'Applicant Rating');
        $this->middleware('auth', [
            'except' => ['create', 'store']
        ]);
        $this->module = 'applicant_ratings';
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $perPage = 100;
        $ratings = ApplicantRating::latest()
        ->paginate($perPage);

        return view('applicant_ratings.index',[
            'ratings' => $ratings,
            'module' => $this->module,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $applicants = Applicant::where('qualified',1)
        ->where('appointment_status','plantilla')
        ->getModels();

        return view('applicant_ratings.create',[
            'applicants' => $applicants,
            'module' => $this->module
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $this->validate($request,[
            'applicant_id' => 'required'
        ]);

        $rating = new ApplicantRating;
        $rating->fill($request->all());

        switch ($request->position_level) {
            case '1st Level':
                $rating->education_points = $request->education_points_1;
                $rating->experience_points = $request->experience_points_1;
                $rating->training_points = $request->training_points_1;
                $rating->experience_remarks = $request->experience_remarks_1;
                $rating->training_remarks = $request->training_remarks_1;
                break;
            
            case '2nd Level':
                $rating->education_points = $request->education_points_2;
                $rating->experience_points = $request->experience_points_2;
                $rating->training_points = $request->training_points_2;
                $rating->experience_remarks = $request->experience_remarks_2;
                $rating->training_remarks = $request->training_remarks_2;
                break;

            case '3rd Level':
                $rating->education_points = $request->education_points_3;
                $rating->experience_points = $request->experience_points_3;
                $rating->training_points = $request->training_points_3;
                $rating->experience_remarks = $request->experience_remarks_3;
                $rating->training_remarks = $request->training_remarks_3;
                break;
        }

        $rating->created_by = Auth::id();
        $rating->save();

        return redirect()
                ->route('applicant_ratings.edit',[
                    'rating' => $rating
                ])->with('success','The applicant rating was successfully created.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ApplicantRating  $applicantRating
     * @return \Illuminate\Http\Response
     */
    public function show(ApplicantRating $applicantRating)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ApplicantRating  $applicantRating
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $rating = ApplicantRating::find($id)->first();

        return view('applicant_ratings.edit',[
            'applicants' => Applicant::where('qualified',1)->getModels(),
            'rating'    => $rating,
            'module' => $this->module
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ApplicantRating  $applicantRating
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $rating = ApplicantRating::find($request->id);
        $rating->fill($request->all());

        switch ($request->position_level) {
            case '1st Level':
                $rating->education_points = $request->education_points_1;
                $rating->experience_points = $request->experience_points_1;
                $rating->training_points = $request->training_points_1;
                $rating->experience_remarks = $request->experience_remarks_1;
                $rating->training_remarks = $request->training_remarks_1;
                break;
            
            case '2nd Level':
                $rating->education_points = $request->education_points_2;
                $rating->experience_points = $request->experience_points_2;
                $rating->training_points = $request->training_points_2;
                $rating->experience_remarks = $request->experience_remarks_2;
                $rating->training_remarks = $request->training_remarks_2;
                break;

            case '3rd Level':
                $rating->education_points = $request->education_points_3;
                $rating->experience_points = $request->experience_points_3;
                $rating->training_points = $request->training_points_3;
                $rating->experience_remarks = $request->experience_remarks_3;
                $rating->training_remarks = $request->training_remarks_3;
                break;
        }

        $rating->updated_by = Auth::id();
        $rating->save();

        return redirect()
                ->route('applicant_ratings.edit',[
                    'rating' => $rating
                ])->with('success','The applicant rating was successfully created.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ApplicantRating  $applicantRating
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $rating = ApplicantRating::find($id);
        $rating->delete();
        return redirect('applicant_ratings')->with('success', 'Applicant rating successfully deleted!');
    }

    public function report(Request $request)
    {
        $rating = ApplicantRating::find($request->id)->first();

        return view('applicant_ratings.report',[
            'rating' => $rating
        ]);
    }
}
