<?php

namespace App\Http\Controllers;

use App\Job;
use App\PSIPOP;
use App\PreliminaryEvaluation;
use App\Applicant;
use App\ApplicantRating;
use App\Recommendation;
use App\Appointment;
use App\AppointmentForm;
use App\AppointmentCasual;
use App\SelectionLineup;
use App\ErasureAlteration;
use App\AppointmentIssued;
use App\AcceptanceResignation;
use App\AppointmentRequirement;
use App\EmployeeInformation;
use App\EmployeeStatus;
use App\Employee;
use App\PlantillaItem;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;
use DateTime;
use Input;

class ReportController extends Controller
{
    /**
     * @var array list of available report documents
     */
    protected $reports = [
        // 'preliminary_evaluation' => 'Preliminary Evaluation',
        // 'selection_lineup' => 'Selection Line Up',
        // 'checklist' => 'Checklist',
        'appointment-transmital' => 'CS Form No. 1 Appointment Transmittal and Action Form',
        // 'absence_qualified_eligible' => 'CS Form No. 5 Certification of the  Absence of a Qualified Eligible',
        'appointments_issued' => 'CS Form No. 2 Report on Appointment Issued (RAI)',
        'absence_qualified_eligible' => 'CS Form No. 5 Certification that there is no applicant who meets all the qualifications requirements',
        'publication_vacant_position' => 'CS Form No. 9 Request for Publication of Vacant Positions',
        // 'appointment_form_regulated' => 'CS Form No. 33-A Revised 2018 Appointment Form',
        'appointments_casual' => 'CS Form No. 34-A Plantilla of Casual Appointment Regulated',
        'medical_certificate' => 'CS Form No. 211 Medical Certificate',
        'pipeline-report' => 'Pipeline Report',
        'status_of_vacancy' => 'Status of Vacancy and Turn-around Time ',
        'work_force_complement' => 'Workforce Complement ',
        'cos_agreement' => 'Contract of Service Agreement',
        'comparative_matrix' => 'Comparative Data Matrix',
        'shortlisted' => 'Report on Shortlisted Applicants'
        // 'accession_form' => 'Accession Form',
        // 'separation_form' => 'Separation Form',
        // 'dibar' => 'CS Form No. 8 Report on DIBAR',
        // 'oath_office' => 'CS Form No. 32 Oath of Office',

        // 'position_description' => 'DBM-CSC Form No. 1 Position Description Forms',
    ];

    /**
     * Define your validation rules in a property in
     * the controller to reuse the rules.
     */
    protected $validationRules = [

    ];

    /**
     * Modify the globally used view variable here
     * initialization found on app\Providers\AppServiceProvider
     */
    public function __construct()
    {
        View::share('title', 'Reports');
        $this->middleware('auth');

        $this->status = 'plantilla';
        $this->module = 'report';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $jobs = Job::leftJoin('positionitem as p','p.RefId','=','jobs.plantilla_item_id')
            ->where('publish',1)->getModels();

        $applicants = Applicant::where('qualified',1)
            ->where('appointment_status','plantilla')
            ->orderBy('first_name','asc')
            ->get();


        $hasId = AppointmentRequirement::get()->pluck('applicant_id')->toArray();

        $cos = Applicant::where('qualified',1)
            ->where('appointment_status','nonplantilla')
            ->whereIn('id',$hasId)
            ->orderBy('first_name','asc')
            ->get();

        $employees = Employee::orderBy('FirstName','asc')->getModels();

        return view('report.index', [
            'reports' => $this->reports,
            'jobs' => $jobs,
            'applicants' => $applicants,
            'cos' => $cos,
            'months' => config('params.months'),
            'employees' => $employees,
            'module' => $this->module
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('report.sched-exam');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function appointmentTransmital(Request $request){

        $job = Job::pluck('id')->toArray();

        $applicant = Applicant::whereIn('job_id',$job)->pluck('id')->toArray();

        $date = json_decode($request->sign);

        $appointment = AppointmentIssued::where('created_at','>=',$date->from)
        ->where('created_at','<=',$date->to)
        ->whereIn('applicant_id',$applicant)
        ->getModels();

        return view('report.appointment-transmital',[
            'appointment' => $appointment,
            'signatory'  => json_decode($request->sign),
        ]);
    }

    public function appointmentIssued(Request $request){
        $date = new DateTime($request->date);
        $print_date = $date->format('F Y');

        $job = Job::where('status','plantilla')->pluck('id')->toArray();
        $applicant = Applicant::whereIn('job_id',$job)->pluck('id')->toArray();

        $date = json_decode($request->sign);

        $appointment = AppointmentIssued::where('created_at','>=',$date->from)
        ->where('created_at','<=',$date->to)
        ->whereIn('applicant_id',$applicant)->getModels();

        return view('report.appointments_issued')->with([
            'print_date' => $print_date,
            'appointments' => $appointment,
            'signatory'  => json_decode($request->sign),
        ]);
    }

    public function appointmentCasual(Request $request){
        $job = Job::where('status','plantilla')->pluck('id')->toArray();
        $applicant = Applicant::whereIn('job_id',$job)->pluck('id')->toArray();
        $casual = AppointmentCasual::whereIn('applicant_id',$applicant)->getModels();
        return view('report.appointments_casual',[
            'casuals' => $casual
        ]);
    }

    public function absenceQualifiedEligible(Request $request){
        return view('report.absence_qualified_eligible');
    }

    public function dibarReport(Request $request){
        return view('report.dibar');
    }

    public function sepearationForm(Request $request){
        return view('report.separation_form');
    }

    public function accessionForm(Request $request){
        return view('report.accession_form');
    }

    public function pipelineReport(Request $request){
        $month = $request->month;

        $jobs = Job::with([
            'plantilla_item',
            'no_of_applicants' => function($qry) use($month){
                $qry = $qry->whereMonth('created_at',$month);
            },
            'not_qualified_applicant' => function($qry) use($month){
                $qry = $qry->whereMonth('created_at',$month);
            },

            'qualified_applicant' => function($qry) use($month){
                $qry = $qry->whereMonth('created_at',$month);
            },

            'applicant_for_reference' => function($qry) use($month){
                $qry = $qry->whereMonth('created_at',$month);
            },

            'applicant_widthrawn' => function($qry) use($month){
                $qry = $qry->whereMonth('created_at',$month);
            },

            'pass_exam' => function($qry) use($month){
                $qry = $qry->whereMonth('created_at',$month);
            },

            'pass_interview' => function($qry) use($month){
                $qry = $qry->whereMonth('created_at',$month);
            },

        ])
        ->where('status',$request->status)
        ->get();
        
        return view('report.pipeline-report',[
            'report' => 'pipeline-report',
            'jobs' => $jobs
        ]);
    }

    public function statusVacancyReport(Request $request){

        $jobs = Job::whereMonth('created_at',$request->month)
        ->getModels();

        return view('report.status_of_vacancy',[
            'report' => 'status_of_vacancy',
            'jobs'   => $jobs
        ]);
    }

    public function workForceComplement(Request $request){

        $perma  = EmployeeStatus::where('code','P')->first();
        $cos    = EmployeeStatus::where('code','COS')->first();
        $cas    = EmployeeStatus::where('code','CAS')->first();
        $cont   = EmployeeStatus::where('code','CONT')->first();
        $jo     = EmployeeStatus::where('code','JO')->first();

        $countPF = PSIPOP::with('job')
                    ->where('employee_status_id',$perma->RefId)
                    ->where('status',1)
                    ->count();

        $countPUF =  PSIPOP::where('employee_status_id',$perma->RefId)
                    ->where('status',0)
                    ->count();

        $countCOSF = PSIPOP::where('employee_status_id',$cos->RefId)
                    ->where('status',1)
                    ->count();

        $countCOSUF =  PSIPOP::where('employee_status_id',$cos->RefId)
                        ->where('status',0)
                        ->count();

        $countCASF = PSIPOP::where('employee_status_id',$cas->RefId)
                    ->where('status',1)
                    ->count();

        $countCASUF =  PSIPOP::where('employee_status_id',$cas->RefId)
                        ->where('status',0)
                        ->count();

        $countCONTF = PSIPOP::where('employee_status_id',$cont->RefId)
                    ->where('status',1)
                    ->count();

        $countCONTUF =  PSIPOP::where('employee_status_id',$cont->RefId)
                        ->where('status',0)
                        ->count();

        $countJOF = PSIPOP::where('employee_status_id',@$jo->RefId)
                    ->where('status',1)
                    ->count();

        $countJOUF =  PSIPOP::where('employee_status_id',@$jo->RefId)
                        ->where('status',0)
                        ->count();


        $response = array(

            'perma_f'   => $countPF,
            'perma_uf'  => $countPUF,
            'cos_f'     => $countCOSF,
            'cos_uf'    => $countCOSUF,
            'cas_f'     => $countCASF,
            'cas_uf'    => $countCASUF,
            'cont_f'    => $countCONTF,
            'cont_uf'   => $countCONTUF,
            'jo_f'      => $countJOF,
            'jo_uf'     => $countJOUF,

        );
        return view('report.work_force_complement',[
            'report' => 'work_force_complement',
            'response' => $response
        ]);
    }

    public function vacantPosition(Request $request){

        $date = json_decode($request->sign);

        $jobs = Job::where('publish',1)
        ->where('created_at','>=',$date->from)
        ->where('created_at','<=',$date->to)
        ->getModels();

        $employeeinfo = EmployeeInformation::where('RefId',$request->signatory_id)->first();

        return view('report.publication_vacant_position',[
            'jobs' => $jobs,
            'employeeinfo' => $employeeinfo
        ]);
    }

    public function oathOffice(Request $request){

        $applicant = Applicant::find($request->id);

        return view('report.oath_office',[
            'applicant' => $applicant,
        ]);
    }

    public function appointmentFormRegulated(Request $request){
        $applicant = AppointmentForm::where('applicant_id',$request->id)->first();
        $numberInWord = $this->convert_number_to_words(@$applicant->applicant->job->plantilla_item->basic_salary);

        return view('report.appointment_form_regulated',[
            'applicant' => $applicant,
            'number_in_word' => $numberInWord
        ]);
    }

    public function medicalCertificate(Request $request){
        $applicant = Applicant::find($request->id)->first();
        return view('report.medical_certificate',[
            'applicant' => $applicant
        ]);
    }

    public function preliminaryEvaluation(Request $request){

        $applicant = Applicant::where('job_id',$request->id)->pluck('id')->toArray();
        $preliminary = PreliminaryEvaluation::whereIn('applicant_id',$applicant)->getModels();
        $jobs = Job::where('id',$request->id)
        ->where('status',$this->status)->first();

        return view('report.preliminary_evaluation')
        ->with([
            'preliminary' => $preliminary,
            'jobs' => $jobs,

        ]);
    }

    public function selectionLineup(Request $request){

        $applicants = Applicant::where('job_id',$request->id)->pluck('id')->toArray();
        $job = Job::find($request->id);
        $recommend = SelectionLineup::whereIn('applicant_id',$applicants)->getModels();

        return view('report.selection_lineup')
        ->with([
            'recommend' => $recommend,
            'jobs' => $job
        ]);
    }

    public function checklistReport(Request $request){

        $appointment = new Appointment();
        $applicant = new Applicant();

        if ($request->id) {
            $applicant = Applicant::where('id', $request->id)
                ->first();
            $appointment = Appointment::where('applicant_id', $request->id)
                ->first();
        }

        return view('report.checklist')->with([
            'appointment' => $appointment,
            'applicant' => $applicant,
        ]);
    }

    public function cosAgreement(Request $request){
        $applicant = Applicant::where('id',$request->id)->first();

        $split = explode('.', round(@$applicant->job->plantilla_item->basic_salary,2));
        $numberInWord = $this->convert_number_to_words(@$applicant->job->plantilla_item->basic_salary);
        $decimalInWord = $this->convert_number_to_words(@$split[1]);

        return view('report.cos_agreement',[
            'applicant' => $applicant,
            'whole'     => $numberInWord,
            'decimal'   => $decimalInWord
        ]);
    }

    public function comparativeMatrix(Request $request)
    {
        $arrId = ApplicantRating::where('position_level',$request->level)
        ->whereMonth('created_at',$request->month)
        ->pluck('applicant_id')
        ->toArray();

        $applicants = Applicant::where('job_id',$request->id)
        ->whereIn('id',$arrId)
        ->getModels();

        return view('report.comparative_matrix',[
            'applicants' => $applicants,
            'level'      => $request->level,
            'signatory'  => json_decode($request->sign),
            'job'        => Job::find($request->id)
        ]);
    }

    public function shortListed(Request $request)
    {

        $applicants = Applicant::whereMonth('created_at',$request->month)
        ->where('qualified',1)
        ->getModels();

        return view('report.shortlisted',[
            'applicants' => $applicants,
            'report' => 'shortlisted',
        ]);
    }


}

