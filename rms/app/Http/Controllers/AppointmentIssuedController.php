<?php

namespace App\Http\Controllers;

use App\AppointmentIssued;
use App\AppointmentProcessing;
use App\AppointmentRequirement;
use App\Appointment;
use App\Applicant;
use App\Job;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;

class AppointmentIssuedController extends Controller
{

    /**
     * Modify the globally used view variable here
     * initialization found on app\Providers\AppServiceProvider
     */
    public function __construct()
    {
        View::share('title', 'Transmital');
        $this->middleware('auth');
        $this->module = 'appointment-issued';
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $perPage = 100;
        $issues = AppointmentIssued::latest()
            ->paginate($perPage);

        return view('appointment-issued.index', [
            'issues' => $issues,
            'module' => $this->module
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $jobId = Job::where('publish',1)
        ->where('status','plantilla')
        ->pluck('id')
        ->toArray();

        $appId = Applicant::whereIn('job_id',$jobId)
        ->pluck('id')
        ->toArray();

        $appointees = AppointmentRequirement::whereIn('applicant_id',$appId)
        ->where('complied',1)
        ->get();

        return view('appointment-issued.create')->with([
            'appointees' => $appointees,
            'action' => 'AppointmentIssuedController@store',
            'module' => $this->module
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $issued = new AppointmentIssued;
        $issued->fill($request->all());
        $issued->created_by = Auth::id();
        if($issued->save()){
            // $assumption = $this->saveAssumption($request);
            // $oathoffice = $this->saveOath($request);
            // $processing = $this->storeAppointee($issued->applicant_id);
        }
        return redirect()
            ->route('appointment-issued.edit',[
                'form' => $issued
            ])->with('success', 'The Transmital was successfully created.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\AppointmentIssued  $appointmentIssued
     * @return \Illuminate\Http\Response
     */
    public function show(AppointmentIssued $appointmentIssued)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\AppointmentIssued  $appointmentIssued
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $issued = AppointmentIssued::find($id)->first();

        return view('appointment-issued.edit',[
            'form' => $issued,
            'module' => $this->module
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\AppointmentIssued  $appointmentIssued
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $issued = AppointmentIssued::find($request->id);
        $issued->fill($request->all());
        $issued->updated_by = Auth::id();
        if($issued->save()){
            // $assumption = $this->saveAssumption($request);
            // $oathoffice = $this->saveOath($request);
            // $processing = $this->storeAppointee($issued->applicant_id);
        }
        return redirect()
            ->route('appointment-issued.edit',[
                'form' => $issued
            ])->with('success', 'The Transmital was successfully updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\AppointmentIssued  $appointmentIssued
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $form = AppointmentIssued::find($id);
        $form->delete();
        return redirect('/appointment-issued')->with('success', 'Transmital data deleted!');
    }

    public function storeAppointee($applicant_id){

        $appointment = AppointmentProcessing::where('applicant_id',$applicant_id)->first();
        if (empty($appointment)) {
            $appointment = new AppointmentProcessing;
        }
        if($appointment->exists()){
            $appointment->updated_by = Auth::id();
            $response = redirect('/interviews')->with('success', 'Applicant appointed was updated successfully.');
        }else{
            $appointment->created_by = Auth::id();

            $response = redirect('/interviews')->with('success', 'Applicant appointed was created successfully.');
        }
        $appointment->applicant_id = $applicant_id;
        $appointment->save();
    }
}
