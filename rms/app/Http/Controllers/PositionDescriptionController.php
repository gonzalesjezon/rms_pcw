<?php

namespace App\Http\Controllers;

use App\Appointment;
use App\Applicant;
use App\PositionDescription;
use App\AppointmentForm;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;

class PositionDescriptionController extends Controller
{

    /**
     * Modify the globally used view variable here
     * initialization found on app\Providers\AppServiceProvider
     */
    public function __construct()
    {
        View::share('title', 'Position Description');
        $this->middleware('auth');
        $this->module = 'position-descriptions';
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $perPage = 100;
        $appointments = PositionDescription::latest()
            ->paginate($perPage);

        return view('position-descriptions.index', [
            'appointments' => $appointments,
            'module' => $this->module
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $appointees = AppointmentForm::where('form_status',1)->get();

        return view('position-descriptions.create')->with([
            'appointees' => $appointees,
            'option' => config('params.option_1'),
            'module' => $this->module
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $position_desc = new PositionDescription;
        $position_desc->fill($request->all());
        $position_desc->created_by = Auth::id();
        $position_desc->save();
        return redirect()->route('position-descriptions.edit',[
            'form' => $position_desc
        ])->with('success', 'The Postion Description was successfully created.');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\PositionDescription  $positionDescription
     * @return \Illuminate\Http\Response
     */
    public function show(PositionDescription $positionDescription)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\PositionDescription  $positionDescription
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $form = PositionDescription::find($id)->first();

        return view('position-descriptions.edit',[
            'form'   => $form,
            'option' => config('params.option_1'),
            'module' => $this->module
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PositionDescription  $positionDescription
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $position_desc = PositionDescription::find($request->id);
        $position_desc->fill($request->all());
        $position_desc->updated_by = Auth::id();
        $position_desc->save();
        return redirect()
        ->route('position-descriptions.edit',[
            'form' => $position_desc
        ])->with('success', 'The Postion Description was successfully updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PositionDescription  $positionDescription
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $desc = PositionDescription::find($id);
        $desc->delete();
        return redirect('/position-descriptions')->with('success', 'Position description data deleted!');
    }

    public function report(Request $request)
    {
        $pos_desc = PositionDescription::find($request->id)->first();
        return view('position-descriptions.report',[
            'post_desc' => $pos_desc
        ]);
    }
}
