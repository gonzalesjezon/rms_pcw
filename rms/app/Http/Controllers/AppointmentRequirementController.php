<?php

namespace App\Http\Controllers;

use App\AppointmentRequirement;
use App\Interview;
use App\Applicant;
use App\Appointment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Auth;

class AppointmentRequirementController extends Controller
{

    /**
     * Modify the globally used view variable here
     * initialization found on app\Providers\AppServiceProvider
     */
    public function __construct()
    {
        View::share('title', 'Pre Employment Requirements');
        $this->middleware('auth');
        $this->module = 'appointment-requirements';
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $perPage = 100;
        $requirments = AppointmentRequirement::latest()
            ->paginate($perPage);

        return view('appointment-requirements.index', [
            'requirments' => $requirments,
            'module' => $this->module
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {

        $interviewees = Interview::where('interview_status',6)
        ->get();

        return view('appointment-requirements.create')->with([
            'interviewees' => $interviewees,
            'module' => $this->module
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $requirement = new AppointmentRequirement;
        $requirement->fill($request->all());
        $requirement->complied = $request->complied;
        $requirement->saveDocumentFileNames($request);
        $requirement->created_by = Auth::id();

        if($requirement->save()){
            $applicant = Applicant::find($request->applicant_id)->first();

            if($applicant->appointment_status == 'plantilla'){
                if($request->complied == 1){
                    // $appointment = $this->storeAppointee($request->applicant_id);
                }
            }

            $requirement->uploadDocumentFiles($request);
        }
        return redirect()->route('appointment-requirements.edit',[
            'requirement' => $requirement
        ])->with('success', 'The Appointment - Requirements was successfully created.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\AppointmentRequirement  $appointmentRequirement
     * @return \Illuminate\Http\Response
     */
    public function show($id, Request $request)
    {

        $requirement = AppointmentRequirement::find($id)->first();
        return view('appointment-requirements.show', [
            'requirement' => $requirement,
            'documentView' => $request->document,
            'module' => $this->module
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\AppointmentRequirement  $appointmentRequirement
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $requirement = AppointmentRequirement::find($id)->first();

        return view('appointment-requirements.edit',[
            'requirement' => $requirement,
            'module' => $this->module
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\AppointmentRequirement  $appointmentRequirement
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $requirement = AppointmentRequirement::find($request->id);
        $requirement->fill($request->all());
        $requirement->complied = $request->complied;
        $requirement->saveDocumentFileNames($request);
        $requirement->updated_by = Auth::id();

        if($requirement->save()){
            $applicant = Applicant::find($requirement->applicant_id)->first();
            if($applicant->appointment_status == 'plantilla'){
                if($request->complied == 1){
                    // $appointment = $this->storeAppointee($request->applicant_id);
                }
            }

            $requirement->uploadDocumentFiles($request);
        }
        return redirect()->route('appointment-requirements.edit',[
            'requirement' => $requirement
        ])->with('success', 'The Appointment - Requirements was successfully updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\AppointmentRequirement  $appointmentRequirement
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        AppointmentRequirement::where('applicant_id',$id)->delete();
        return redirect('/appointment-requirements')->with('success', 'Appointment - Requirements data deleted!');
    }

    public function storeAppointee($applicant_id){

        $appointment = Appointment::where('applicant_id',$applicant_id)->first();
        if (empty($appointment)) {
            $appointment = new Appointment;
        }
        if($appointment->exists()){
            $appointment->updated_by = Auth::id();
            $response = redirect('/appointment-requirements')->with('success', 'Applicant appointed was updated successfully.');
        }else{
            $appointment->created_by = Auth::id();

            $response = redirect('/appointment-requirements')->with('success', 'Applicant appointed was created successfully.');
        }
        $appointment->applicant_id = $applicant_id;
        $appointment->save();
    }
}
