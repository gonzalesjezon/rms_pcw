<?php

namespace App\Http\Controllers;

use App\Applicant;
use App\AppointmentProcessing;
use App\AppointmentRequirement;
use App\AppointmentForm;
use App\Appointment;
use App\Job;
use App\JobOffer;
use App\Assumption;
use App\AppointmentChecklist;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Auth;

class AppointmentProcessingController extends Controller
{
    /**
     * Define your validation rules in a property in
     * the controller to reuse the rules.
     */
    protected $validationRules = [

    ];

    /**
     * Modify the globally used view variable here
     * initialization found on app\Providers\AppServiceProvider
     */
    public function __construct()
    {
        View::share('title', 'Processing Checklist');
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $perPage = 100;
        $appointments = AppointmentProcessing::latest()
            ->paginate($perPage);

        return view('appointment-processing.index', [
            'appointments' => $appointments
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $appointees = AppointmentRequirement::where('complied',1)
        ->get();

        return view('appointment-processing.create')->with([
            'appointees' => $appointees,
        ]);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
 
        $appointment = new AppointmentProcessing;
        $appointment->fill($request->all());
        $appointment->educ_check = ($request->educ_check) ? 1 : 0;
        $appointment->exp_check = ($request->exp_check) ? 1 : 0;
        $appointment->training_check = ($request->training_check) ? 1 : 0;
        $appointment->eligibility_check = ($request->eligibility_check) ? 1 : 0;
        $appointment->other_check = ($request->other_check) ? 1 : 0;
            $appointment->created_by = Auth::id();

        $appointment->save();

        return redirect()
            ->route('appointment-processing.edit',[
                'form' => $appointment
            ])->with('success', 'Save Successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Appointment  $appointment
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Appointment  $appointment
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $form = AppointmentProcessing::find($id)->first();

        return view('appointment-processing.edit',[
            'form' => $form
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Appointment  $appointment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $appointment = AppointmentProcessing::find($request->id);
        $appointment->fill($request->all());
        $appointment->educ_check = ($request->educ_check) ? 1 : 0;
        $appointment->exp_check = ($request->exp_check) ? 1 : 0;
        $appointment->training_check = ($request->training_check) ? 1 : 0;
        $appointment->eligibility_check = ($request->eligibility_check) ? 1 : 0;
        $appointment->other_check = ($request->other_check) ? 1 : 0;
        $appointment->updated_by = Auth::id();

        $appointment->save();

        return redirect()
            ->route('appointment-processing.edit',[
                'form' => $appointment
            ])->with('success', 'Update Successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Appointment  $appointment
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        AppointmentProcessing::destroy($id);
        return redirect('/appointment-processing')->with('success', 'Appointment data deleted!');
    }

    public function report(Request $request){

        $appointment = AppointmentProcessing::find($request->id)
            ->first();

        return view('appointment-processing.report')->with([
            'appointment' => $appointment,
        ]);
    }

    public function storeAppointee($applicant_id){

        $appointment = AppointmentChecklist::where('applicant_id',$applicant_id)->first();
        if (empty($appointment)) {
            $appointment = new AppointmentChecklist;
        }
        if($appointment->exists()){
            $appointment->updated_by = Auth::id();
        }else{
            $appointment->created_by = Auth::id();
        }
        $appointment->applicant_id = $applicant_id;
        $appointment->save();
    }


}
