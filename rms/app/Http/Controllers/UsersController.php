<?php

namespace App\Http\Controllers;

use App\User;
use App\AccessModule;
use Auth;
use Illuminate\Support\Facades\View;
use Illuminate\Http\Request;

class UsersController extends Controller
{

    /**
     * Modify the globally used view variable here
     * initialization found on app\Providers\AppServiceProvider
     */
    public function __construct()
    {
        View::share('title', 'Users');
        $this->middleware('auth');
        $this->module = 'users';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->check($this->module);
        $perPage = 100;
        $users = User::latest()
        ->paginate($perPage);

        return view('users.index',[
            'users' => $users,
            'module' => $this->module
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $this->check($this->module);
        $access_module = AccessModule::orderBy('access_name')->getModels();

        return view('users.create',[
            'access_module' => $access_module,
            'action' => 'UsersController@store',
            'module' => $this->module
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request,[
            'name' => 'required|min:3|max:50',
            'username' => 'required',
            'password' => 'required|min:6|same:password_confirmation'
        ]);

        $user = new User();
        $user->fill($request->all());
        $user->password = bcrypt($request->password);
        $user->created_by = Auth::id();
        $user->save();

        return redirect()
            ->route('users.edit',[
                'user' => $user
            ])
            ->with('success','The user has successfully created');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->check($this->module);
        $user = User::find($id);
        $access_module = AccessModule::orderBy('access_name')->getModels();

        return view('users.edit',[
            'user' => $user,
            'access_module' => $access_module,
            'module' => $this->module
        ]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'name' => 'required|min:3|max:50',
            'username' => 'required',
            'password' => 'required|min:6|same:password_confirmation'
        ]);

        $user = User::find($id);
        $user->fill($request->all());
        $user->password = bcrypt($request->password);
        $user->updated_by = Auth::id();
        $user->save();

        return redirect()
            ->route('users.edit',[
                'user' => $user
            ])
            ->with('success','The user has successfully updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);
        $user->delete();

        return redirect('users/')->with('success','User successfully deleted');
    }
}
