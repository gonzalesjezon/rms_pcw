<?php

namespace App\Http\Controllers;

use App\PSIPOP;
use App\Department;
use App\Division;
use App\Office;
use App\Section;
use App\Position;
use App\SalaryGrade;
use App\StepIncrement;
use App\EmployeeStatus;
use Auth;

use Illuminate\Support\Facades\View;
use Illuminate\Http\Request;

class PSIPOPController extends Controller
{
    /**
     * Call behavior handling for authentication
     * authentication section via middleware
     * =====================================
     * Modify the globally used view variable here
     * initialization found on app\Providers\AppServiceProvider
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['index', 'careers']]);
        View::share('title', 'Itemization & Plantilla of Personnel');
        $this->module = 'psipop';
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $perPage = 100;
        $psipop = PSIPOP::orderBy('item_number','asc')
        ->paginate($perPage);

        return view('psipop.index', [
            'psipop' => $psipop,
            'module' => $this->module
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $offices         = Office::select('Name','RefId')->orderBy('Name','asc')->get();
        $divisions       = Division::select('Name','RefId')->orderBy('Name','asc')->get();
        $positions       = Position::select('Name','RefId')->orderBy('Name','asc')->get();
        $salaryGrades    = SalaryGrade::select('Name','RefId')->orderBy('Name','asc')->get();
        $steps           = StepIncrement::select('Name','RefId')->orderBy('Name','asc')->get();
        $departments     = Department::select('Name','RefId')->orderBy('Name','asc')->get();
        $employeeStatus  = EmployeeStatus::select('Name','RefId')->orderBy('Name','asc')->get();

        return view('psipop.create',[
            'offices'       => $offices,
            'divisions'     => $divisions,
            'positions'     => $positions,
            'salary_grades' => $salaryGrades,
            'steps'         => $steps,
            'departments'   => $departments,
            'employee_status'   => $employeeStatus,
            'module' => $this->module
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $psipop =  new PSIPOP();
        $psipop->fill($request->all());
        $psipop->basic_salary = ($request->basic_salary) ? str_replace(',', '', $request->basic_salary) : 0;
        $psipop->created_by = Auth::id();
        $psipop->save();

        return redirect()
            ->route('psipop.edit', [
                'id' => $psipop->id,
            ])
            ->with('success', 'The itemization & plantilla of personnel was successfully created.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\PSIPOP  $pSIPOP
     * @return \Illuminate\Http\Response
     */
    public function show(PSIPOP $pSIPOP)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\PSIPOP  $pSIPOP
     * @return \Illuminate\Http\Response
     */
    public function edit($id, Request $request)
    {
        $psipop  = PSIPOP::findOrFail($id);

        $offices      = Office::select('Name','RefId')->orderBy('Name','asc')->get();
        $divisions    = Division::select('Name','RefId')->orderBy('Name','asc')->get();
        $positions    = Position::select('Name','RefId')->orderBy('Name','asc')->get();
        $salaryGrades = SalaryGrade::select('Name','RefId')->orderBy('Name','asc')->get();
        $steps        = StepIncrement::select('Name','RefId')->orderBy('Name','asc')->get();
        $departments  = Department::select('Name','RefId')->orderBy('Name','asc')->get();
        $employeeStatus  = EmployeeStatus::select('Name','RefId')->orderBy('Name','asc')->get();

        return view('psipop.edit',[
            'psipop'        => $psipop,
            'offices'       => $offices,
            'divisions'     => $divisions,
            'positions'     => $positions,
            'salary_grades' => $salaryGrades,
            'steps'         => $steps,
            'departments'   => $departments,
            'employee_status'   => $employeeStatus,
            'module' => $this->module
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PSIPOP  $pSIPOP
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $psipop = PSIPOP::findOrFail($id);
        $psipop->fill($request->all());
        $psipop->basic_salary = ($request->basic_salary) ? str_replace(',', '', $request->basic_salary) : 0;
        $psipop->updated_by = Auth::id();
        $psipop->save();

        return redirect()
            ->route('psipop.edit', [
                'id' => $psipop->id,
            ])
            ->with('success', 'The itemization & plantilla of personnel was successfully updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PSIPOP  $pSIPOP
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        PSIPOP::destroy($id);
        return redirect('psipop')->with('success', 'PSIPOP successfully deleted!');
    }
}
