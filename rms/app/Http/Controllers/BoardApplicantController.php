<?php

namespace App\Http\Controllers;

use App\BoardApplicant;
use App\Applicant;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendMailable;

use Auth;
class BoardApplicantController extends Controller
{
    public function __construct()
    {
        View::share('title', 'Applicant Onboarding');
        $this->middleware('auth');
        $this->module = 'boarding_applicant';
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $perPage = 100;
        $boardings = BoardApplicant::latest()
        ->paginate($perPage);

        return view('boarding_applicant.index', [
            'boardings' => $boardings,
            'module' => $this->module
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $applicants = Applicant::where('active',0)
        ->orderBy('last_name','asc')
        ->getModels();

         return view('boarding_applicant.create',[
            'applicants' => $applicants,
            'module' => $this->module
         ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'applicant_id' => 'required',
            'start_date' => 'required',
            'start_time' => 'required',
            'board_status' => 'required'
        ]);

        $boarding = new BoardApplicant();
        $boarding->fill($request->all());
        $boarding->created_by = Auth::id();
        $boarding->save();

        // if($boarding->board_status == 1){
        //     $message['status'] = $boarding->board_status;
        //     $message['data'] = $boarding;
        //     $message['type'] = 'onboard';


        //     $this->mail($request->email, 'Subject', $message);
        // }

        $applicants = Applicant::where('active',0)
        ->orderBy('last_name','asc')
        ->getModels();

        return redirect()
            ->route('boarding_applicant.edit',[
                'boarding' => $boarding,
            ])
            ->with('success', 'The applicant on board was successfully created.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\BoardApplicant  $boardApplicant
     * @return \Illuminate\Http\Response
     */
    public function show(BoardApplicant $boardApplicant)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\BoardApplicant  $boardApplicant
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $boarding = BoardApplicant::find($id);

        $applicants = Applicant::where('active',0)
        ->orderBy('last_name','asc')
        ->getModels();

        return view('boarding_applicant.edit',[
            'boarding' => $boarding,
            'applicants' => $applicants,
            'module' => $this->module
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\BoardApplicant  $boardApplicant
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $boarding = BoardApplicant::find($request->id);
        $boarding->fill($request->all());
        $boarding->updated_by = Auth::id();
        $boarding->save();

        // if($boarding->board_status == 1){
        //     $message['status'] = $boarding->board_status;
        //     $message['data'] = $boarding;
        //     $message['type'] = 'onboard';

        //     $this->mail($request->email, 'Subject', $message);
        // }

        return redirect()
            ->route('boarding_applicant.edit',[
                'boarding' => $boarding,
            ])
            ->with('success', 'The applicant on board was successfully updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\BoardApplicant  $boardApplicant
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $board = BoardApplicant::find($id);
        $board->delete();
        return redirect('boarding_applicant')->with('success', 'Applicant on boarding record deleted!');
    }

    public function mail($to, $subject, $message){

        Mail::to($to)->send(new SendMailable($message));

    }

    public function sendMail(Request $request)
    {   
        $status = false;
        if($request->data['board_status'] == 1)
        {   
            $subject = config('params.boarding_status.'.$request->data['board_status']);
            $applicant = Applicant::find($request->data['applicant_id']);
            $applicant->start_date = $request->data['start_date'];
            $applicant->start_time = $request->data['start_time'];
            $message['status'] = $request->data['board_status'];
            $message['data']   = $applicant;
            $message['type']   = 'onboard';

            $this->mail($applicant->email_address, 'Subject', $message);

            $status = true;
        }


        return json_encode([
            'status' => $status,
        ]);
    }
}
