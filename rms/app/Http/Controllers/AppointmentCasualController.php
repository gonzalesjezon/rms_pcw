<?php

namespace App\Http\Controllers;

use App\AppointmentCasual;
use App\AppointmentForm;
use App\Applicant;
use App\Appointment;
use Illuminate\Support\Facades\View;
use Illuminate\Http\Request;
use Auth;

class AppointmentCasualController extends Controller
{
    /**
     * Modify the globally used view variable here
     * initialization found on app\Providers\AppServiceProvider
     */
    public function __construct()
    {
        View::share('title', 'Plantilla of Casual Appointment');
        $this->middleware('auth');
        $this->module = 'appointment-casual';
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $perPage = 100;
        $appointments = AppointmentCasual::latest()
            ->paginate($perPage);

        return view('appointment-casual.index', [
            'appointments' => $appointments,
            'module' => $this->module
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {

        $appointees = AppointmentForm::where('form_status',1)
        ->get();

        return view('appointment-casual.create')->with([
            'appointees' => $appointees,
            'module' => $this->module
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $appointmentcasual = new AppointmentCasual;
        $appointmentcasual->fill($request->all());
        $appointmentcasual->created_by = Auth::id();
        $appointmentcasual->save();

        return redirect()
                ->route('appointment-casual.edit',[
                    'form' => $appointmentcasual
                ])
                ->with('success', 'The Appointment was successfully created.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $form = AppointmentCasual::find($id)->first();
        return view('appointment-casual.edit',[
            'form' => $form,
            'module' => $this->module
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $appointmentcasual = AppointmentCasual::find($request->id);
        $appointmentcasual->fill($request->all());
        $appointmentcasual->updated_by = Auth::id();
        $appointmentcasual->save();

        return redirect()
                ->route('appointment-casual.edit',[
                    'form' => $appointmentcasual
                ])
                ->with('success', 'The Appointment was successfully updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $casual = AppointmentCasual::find($id);
        $casual->delete();
        return redirect('/appointment-casual')->with('success', 'Appointment data deleted!');
    }
}
