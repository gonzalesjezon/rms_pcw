<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Examination extends Model
{
		use SoftDeletes;
    protected $primaryKey = 'id';

    protected $table = 'examinations';

    protected $fillable = [

		'applicant_id',
		'exam_date',
		'exam_time',
		'exam_location',
		'resched_exam_date',
		'resched_exam_time',
		'exam_status',
		'notifiy_resched_exam',
		'notify_exam_status',
		'notify',
		'average_raw_score',
		'sub_total_rating'

    ];

    public function applicant(){
    	return $this->belongsTo('App\Applicant');
    }
}
