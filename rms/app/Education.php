<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Education extends Model
{
    use SoftDeletes;
    protected $primaryKey = 'id';

    protected $table = 'educations';

    protected $fillable = [
    	'applicant_id',
        'school_name',
        'course',
        'attendance_from',
        'attendance_to',
        'level',
        'graduated',
        'awards',
        'educ_level',
        'ongoing'
    ];

    public function applicants(){
    	return $this->belongsTo('App\Applicant');
    }
}
