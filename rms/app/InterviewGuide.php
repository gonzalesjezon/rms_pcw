<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class InterviewGuide extends Model
{
    use SoftDeletes;
    protected $primaryKey = 'id';
    protected $table = 'interview_guides';
    protected $fillable = [
    	'applicant_id',
    	'interviewer',
    	'date_created',
    	'question_point_1',
    	'question_point_2',
    	'question_point_3',
    	'question_point_4',
    	'question_point_5',
    	'question_point_6',
    	'question_point_7',
        'comments',
    	'total_score',
    	'created_by',
    	'updated_by'
    ];

    public function applicant()
    {
    	return $this->belongsTo('App\Applicant','applicant_id');
    }
}
