<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Training extends Model
{	
		use SoftDeletes;
    protected $primaryKey = 'id';
    protected $table = 'trainings';
    protected $fillable = [

		'applicant_id',
		'title_learning_programs',
		'inclusive_date_from',
		'inclusive_date_to',
		'number_hours',
		'ld_type',
		'sponsored_by'

    ];

    public function applicant(){
    	return $this->belongsTo('App\Applicant');
    }
}
