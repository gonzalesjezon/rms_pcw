<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SalaryGrade extends Model
{
    protected $primaryKey = 'RefId';

    protected $table = 'salarygrade';

    protected $fillable = [

		'Code',
		'Name',
		'Step1',
		'EffectivityDateS1',
		'Step2',
		'EffectivityDateS2',
		'Step3',
		'EffectivityDateS3',
		'Step4',
		'EffectivityDateS4',
		'Step5',
		'EffectivityDateS5',
		'Step6',
		'EffectivityDateS6',
		'Step7',
		'EffectivityDateS7',
		'Step8',
		'EffectivityDateS8'



    ];
}
