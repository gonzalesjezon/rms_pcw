<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class PositionDescription extends Model
{
		use SoftDeletes;
    protected $primaryKey = 'id';

    protected $table = 'position_descriptions';

    protected $fillable = [
		'applicant_id',
		'immediate_supervisor',
		'higher_supervisor',
		'item_number',
		'position_title',
		'used_tools',
		'managerial',
		'supervisor',
		'non_supervisor',
		'staff',
		'general_public',
		'other_agency',
		'other_contacts',
		'office_work',
		'field_work',
		'other_condition',
		'description_function_unit',
		'description_function_position',
		'compentency_1',
		'compentency_2',
		'percentage_work_time',
		'responsibilities',
		'supervisor_name'
    ];

    public function applicant(){
    	return $this->belongsTo('App\Applicant','applicant_id');
    }
}
