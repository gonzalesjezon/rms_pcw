<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use phpDocumentor\Reflection\Types\Integer;
use Illuminate\Database\Eloquent\SoftDeletes;
use Crypt;
class Applicant extends Model
{
    use SoftDeletes;
    /**
     * The upload storage path for images
     * @var string
     *
     */
    public $imageStorage = 'images/';
    /**
     * The upload storage path for documents
     * @var string
     *
     */
    public $documentStorage = 'documents/';
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'applicants';
    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';
    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        'job_id',
        'first_name',
        'middle_name',
        'last_name',
        'extension_name',
        'nickname',
        'mobile_number',
        'contact_number',
        'telephone_number',
        'publication',
        'birthday',
        'birth_place',
        'email_address',
        'gender',
        'civil_status',
        'citizenship',
        'filipino',
        'naturalized',
        'height',
        'weight',
        'blood_type',
        'pagibig',
        'gsis',
        'philhealth',
        'tin',
        'sss',
        'govt_issued_id',
        'govt_id_issued_number',
        'govt_id_issued_place',
        'govt_id_date_issued',
        'govt_id_valid_until',
        'house_number',
        'street',
        'subdivision',
        'barangay',
        'city',
        'province',
        'country',
        'permanent_house_number',
        'permanent_street',
        'permanent_subdivision',
        'permanent_barangay',
        'permanent_city',
        'permanent_province',
        'permanent_country',
        'permanent_telephone_number',
        'image_path',
        'application_letter_path',
        'pds_path',
        'employment_certificate_path',
        'tor_path',
        'coe_path',
        'training_certificate_path',
        'info_sheet_path',
        'curriculum_vitae_path',
        'reference_no',
        'remarks',
        'active',
        'qualified',
        'gwa',
        'zip_code',
        'permanent_zip_code',
        'appointment_status',
        'interview_status',
        'exam_status',
        'indigenous_member',
        'person_id_no',
        'solo_id_no',
        'date_qualified'
    ];
    /**
     * List of applicant images for upload
     *
     * @var array
     */
    protected $images = [
        'image_path',
    ];
    /**
     * List of applicant documents for upload
     *
     * @var array
     */
    protected $documents = [
        'application_letter_path',
        'pds_path',
        'employment_certificate_path',
        'tor_path',
        'coe_path',
        'training_certificate_path'
    ];

    /**
     * @var string $fullname concatenated first and last name
     */
    protected $fullname = '';

    /**
     * Attribute values for civil status
     *
     * @return array civil status options
     */
    public static function getCivilStatus(): array
    {
        return [
            'married' => 'Married',
            'single' => 'Single',
            'divorced' => 'Divorced',
            'widowed' => 'Widowed '
        ];
    }

    /**
     * Attribute values for civil status
     *
     * @return array civil status options
     */
    public static function getGender(): array
    {
        return [
            'male' => 'Male',
            'female' => 'Female'
        ];
    }

    /**
     * Retrieve Evaluation Relation model
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function evaluation()
    {
        return $this->hasOne('App\Evaluation');
    }

    /**
     * Relation: one is to one
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function job()
    {
        return $this->belongsTo('App\Job');
    }

    public function appointmentform()
    {
        return $this->belongsTo('App\AppointmentForm','applicant_id','id');
    }

    /**
     * Relation: one is to one
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function citizenOf()
    {
        return $this->belongsTo('App\Countries', 'citizenship', 'code');
    }

    /**
     * Relation: one is to one
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function countryOf()
    {
        return $this->belongsTo('App\Countries', 'country', 'code');
    }

    /**
     * Sets is filipino property to true
     */
    public function setIsFilipino($isFilipino): Applicant
    {
        $this->filipino = false;
        if ($isFilipino) {
            $this->filipino = true;
        }

        return $this;
    }

    /**
     * Sets is naturalized property to true
     */
    public function setIsNaturalized($isNaturalized): Applicant
    {
        $this->naturalized = false;
        if ($isNaturalized) {
            $this->naturalized = true;
        }

        return $this;
    }

    /**
     * Saves the image file name to model object
     * deletes existing media file
     *
     * @param object $request Illuminate\Http\Request object
     *
     * @return object App\Applicant model
     *
     */
    public function saveImageFileNames($request)
    {
        foreach ($this->images as $image) {
            if ($request->hasFile($image)) {
                $file = $request->file($image);
                $md5Name = md5_file($file->getRealPath());
                $this->$image = $md5Name . '.'
                    . $file->getClientOriginalExtension();
            }
        }

        return $this;
    }

    /**
     * Delete applicant media files
     *
     * @param object $request Illuminate\Http\Request
     * @param array $oldAttributes media object attribute names
     *
     * @return int number of deleted files
     */
    public function deleteMediaFiles($request, $oldAttributes = []): int
    {
        $count = 0;
        // delete images
        foreach ($this->images as $image) {
            if ($request->hasFile($image)) {
                if ($this->deleteMediaFile($this->imageStorage . $oldAttributes[$image])) {
                    $count++;
                }

            }
        }

        // delete documents
        foreach ($this->documents as $document) {
            if ($request->hasFile($document)) {
                if ($this->deleteMediaFile($this->documentStorage . $oldAttributes[$document])) {
                    $count++;
                }
            }
        }

        return $count;
    }

    /**
     * Uploads the document file to the storage container
     *
     * @param string $fileName path and name of media file
     *
     * @return bool
     */
    public function deleteMediaFile($fileName)
    {
        if (Storage::delete($fileName)) {
            return true;
        }

        return false;
    }

    /**
     * Uploads the document file to the storage container
     *
     * @param array $oldAttributes media object attribute names
     *
     * @return int number of deleted files
     */
    public function deleteAllMediaFiles($oldAttributes = []): int
    {
        $count = 0;
        // delete images
        foreach ($this->images as $image) {
            if ($this->deleteMediaFile($this->imageStorage . $oldAttributes[$image])) {
                $count++;
            }
        }

        // delete documents
        foreach ($this->documents as $document) {
            if ($this->deleteMediaFile($this->documentStorage . $oldAttributes[$document])) {
                $count++;
            }
        }

        return $count;
    }

    /**
     * Saves the document file name to model object
     * deletes existing media file
     *
     * @param object $request Illuminate\Http\Request object
     *
     * @return object App\Applicant model
     */
    public function saveDocumentFileNames($request)
    {
        foreach ($this->documents as $document) {
            if ($request->hasFile($document)) {
                $file = $request->file($document);
                $md5Name = md5_file($file->getRealPath());
                $this->$document = $md5Name . '.'
                    . $file->getClientOriginalExtension();
            }
        }

        return $this;
    }

    /**
     * Uploads the image file to the storage container
     *
     * @param object $request Illuminate\Http\Request object
     *
     */
    public function uploadImageFiles($request)
    {
        foreach ($this->images as $image) {
            if ($request->hasFile($image)) {
                $request->file($image)->storeAs(
                    $this->imageStorage,
                    $this->$image
                );
            }
        }
    }

    /**
     * Uploads the document file to the storage container
     *
     * @param object $request Illuminate\Http\Request object
     *
     */
    public function uploadDocumentFiles($request)
    {
        foreach ($this->documents as $document) {
            if ($request->hasFile($document)) {
                $request->file($document)->storeAs(
                    $this->documentStorage,
                    $this->$document
                );
            }
        }
    }

    /**
     * Generates fullname of applicant
     *
     * @return string $fullname
     */
    public function getFullName()
    {
        return $this->fullname = Crypt::decrypt($this->first_name)
            . ' ' . Crypt::decrypt($this->middle_name)
            . ' ' . Crypt::decrypt($this->last_name);
    }

    protected $address = '';

    public function getAddress()
    {
        return $this->address = Crypt::decrypt($this->house_number)
        . ' ' . Crypt::decrypt($this->street)
        . ' ' . Crypt::decrypt($this->barangay)
        . ' ' . Crypt::decrypt($this->city);
    }

    public function education(){
        return $this->hasMany('App\Education');
    }

    public function eligibility(){
        return $this->hasMany('App\Eligibility');
    }

    public function workexperience(){
        return $this->hasMany('App\WorkExperience');
    }

    public function training(){
        return $this->hasMany('App\Training');
    }

    public function erasure(){
        return $this->belongsTo('App\ErasureAlteration');
    }

    public function examination()
    {
        return $this->hasOne('App\Examination','applicant_id');   
    }

    public function interview()
    {
        return $this->hasOne('App\Interview','applicant_id');   
    }

    public function applicant_rating()
    {
        return $this->hasOne('App\ApplicantRating','applicant_id');   
    }

    public function latest_eligibility(){
        return $this->hasOne('App\Eligibility')->orderBy('created_at','desc');
    }

    public function highestEducation()
    {
        return $this->hasOne('App\Education')->orderBy('educ_level','desc');
    }

    public function latestTraining()
    {
        return $this->hasOne('App\Training')->orderBy('created_at','desc');
    }

    public function latestExperience()
    {
        return $this->hasOne('App\WorkExperience')->orderBy('created_at','desc');
    }

    public function firstInterview()
    {
        return $this->hasOne('App\Interview','applicant_id')->orderBy('interview_date','asc');  
    }

    public function firstExam()
    {
        return $this->hasOne('App\Examination','applicant_id')->orderBy('exam_date','asc');  
    }

    public function firstBChecking()
    {
        return $this->hasOne('App\BackgroundChecking','applicant_id')->orderBy('created_at','asc');
    }

    public function firstInterviewApproved()
    {
        return $this->hasOne('App\Interview','applicant_id')->orderBy('date_approved','asc');  
    }

    public function dateAppointmentSigned()
    {
        return $this->hasOne('App\AppointmentForm','applicant_id')->orderBy('date_sign','asc');
    }

    public function dateAssumption()
    {
        return $this->hasOne('App\Assumption','applicant_id')->orderBy('assumption_date','asc');   
    }


}
