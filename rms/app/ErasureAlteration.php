<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class ErasureAlteration extends Model
{
    use SoftDeletes;
    protected $primaryKey = 'id';
    protected $table = 'erasure_alterations';
    protected $fillable = [

		'applicant_id',
		'from_date',
		'to_date',
		'particulars',
		'appointing_officer',
		'sign_date'
    ];

    public function applicant(){
    	return $this->belongsTo('App\Applicant');
    }
}
