<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class AccessModule extends Model
{
		use SoftDeletes;
    protected $primaryKey = 'id';

    protected $table = 'access_modules';

    protected $fillable = [
		'access_name',
    ];

    public function access_rights(){
    	return $this->hasMany('App\AccessRight');
    }

}
