<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class AppointmentForm extends Model
{		
		use SoftDeletes;
		protected $primaryKey = 'id';
    protected $table = 'appointment_forms';
    protected $fillable = [
		'applicant_id',
		'employee_status',
		'nature_of_appointment',
		'appointing_officer',
		'hrmo',
		'chairperson',
		'date_sign',
		'hrmo_assessment_date',
		'chairperson_deliberation_date',
		'form_status',
		'period_emp_from',
		'period_emp_to',
		'publication_date_from',
		'publication_date_to',
		'date_issued',
		'assessment_date',
		'vice',
		'who',
		'created_by',
		'updated_by',
		'posted_in'
    ];

    public function applicant(){
    	return $this->belongsTo('App\Applicant');
    }
}
