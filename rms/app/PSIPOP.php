<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PSIPOP extends Model
{
    protected $primaryKey = 'id';

    protected $table = 'psipop';

    protected $fillable = [
		'item_number',
        'incumbent_name',
        'mode_of_separation',
        'date_vacated',
        'position_id',
		'office_id',
		'division_id',
        'department_id',
		'employee_status_id',
		'salary_grade_id',
        'step_id',
        'basic_salary',
		// 'code',
		// 'level',
		// 'type',
        'status',
        // 'ppa_attribution'

    ];

    public function position()
    {
        return $this->belongsTo('App\Position','position_id');
    }

    public function office(){
    	return $this->belongsTo('App\Office','office_id');
    }

    public function division(){
    	return $this->belongsTo('App\Division','division_id');
    }

    public function salary_grade()
    {
        return $this->belongsTo('App\SalaryGrade','salary_grade_id');
    }

    public function step()
    {
        return $this->belongsTo('App\StepIncrement','step_id');   
    }

    public function employee_status()
    {
        return $this->belongsTo('App\EmployeeStatus','employee_status_id');
    }

    public function job()
    {
        return $this->hasMany('App\Job');
    }


}
