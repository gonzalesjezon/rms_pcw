<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class AcceptanceResignation extends Model
{
    use SoftDeletes;
    protected $primaryKey = 'id';
    protected $table = 'resignation_acceptance';
    protected $fillable = [

		'applicant_id',
		'letter_date',
		'resignation_date',
		'appointing_officer',
		'sign_date'
    ];

    public function applicant(){
    	return $this->belongsTo('App\Applicant');
    }
}
