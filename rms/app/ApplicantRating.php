<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class ApplicantRating extends Model
{
    use SoftDeletes;
    protected $primaryKey = 'id';
    protected $table = 'applicant_ratings';
    protected $fillable = [
    	'applicant_id',
    	'education_points',
    	'training_points',
    	'experience_points',
    	'total_points',
    	'position_level',
        'training_remarks',
        'experience_remarks',
    	'create_by',
    	'updated_by',
    ];

    public function applicant()
    {
    	return $this->belongsTo('App\Applicant','applicant_id');
    }
    
}
