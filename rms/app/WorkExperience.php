<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class WorkExperience extends Model
{
		use SoftDeletes;
    protected $primaryKey = 'id';
    protected $table = 'workexperiences';
    protected $fillable = [

		'applicant_id',
		'inclusive_date_from',
		'inclusive_date_to',
		'present_work',
		'position_title',
		'department',
		'monthly_salary',
		'salary_grade',
		'status_of_appointment',
		'govt_service'

    ];

    public function applicant(){
    	return $this->belongsTo('App\Applicant');
    }
}
