<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BackgroundChecking extends Model
{
	use SoftDeletes;
	protected $primaryKey = 'id';

	protected $table = 'background_checkings';

	protected $fillable = [

		'applicant_id',
		'provided_reference',
		'reference_check',
		'permission',
		'applicant_relationship',
		'applicant_capacity',
		'date_from',
		'date_to',
		'duties_and_responsibilitties',
		'have_pending_case',
		'applicant_satisfactory',
		'reason_for_leaving',
		'applicant_adaptability',
		'applicant_work_performance',
		'applicant_contribution',
		'performance_concerns',
		'comments',
		'job_requirements',
		'reemploy_the_applicant',
		'final_comments',
		'interview_contact',
		'use_of_profance_language',
		'drug_user',
		'risque_photos',
		'discriminating_comments',
		'bullying_statement',
		'observe_grammar',
		'sharing_confidential_information',
		'condacted_by',
		'character_reference',
		'hr_staff_name'

	];

	public function applicant(){
		return $this->belongsTo('App\Applicant','applicant_id');
	}


}
