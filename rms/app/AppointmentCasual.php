<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class AppointmentCasual extends Model
{
		use SoftDeletes;
    protected $primaryKey = 'id';
    protected $table = 'appointment_casual';
    protected $fillable = [
		'applicant_id',
		'employee_status',
		'nature_of_appointment',
		'appointing_officer',
		'hrmo',
		'date_sign',
		'hrmo_date_sign',
		'period_emp_from',
		'period_emp_to',
		'daily_wage',
    ];

    public function applicant(){
    	return $this->belongsTo('App\Applicant');
    }
}
