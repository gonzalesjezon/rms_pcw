<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Eligibility extends Model
{
		use SoftDeletes;
    protected $primaryKey = 'id';
    protected $table = 'eligibilities';
    protected $fillable = [
		'applicant_id',
		'eligibility_ref',
		'other_specify',
		'rating',
		'exam_place',
		'license_number',
		'license_validity',
		'exam_date'

    ];

    public function applicants(){
    	return $this->belongsTo('App\Applicant');
    }
}
