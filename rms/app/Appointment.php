<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Appointment extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'appointments';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [

        'applicant_id',
        'form33_hrmo',
        'form33_cscfo',
        'form34b_hrmo',
        'form34b_cscfo',
        'form212_hrmo',
        'form212_cscfo',
        'eligibility_hrmo',
        'eligibility_cscfo',
        'form1_hrmo',
        'form1_cscfo',
        'form32_hrmo',
        'form32_cscfo',
        'form4_hrmo',
        'form4_cscfo',
    ];
    /**
     * Relation: one is to one
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function applicant()
    {
        return $this->belongsTo('App\Applicant');
    }

    /**
     * Relation: one is to one
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function author()
    {
        return $this->belongsTo('App\User', 'created_by', 'id');
    }

    /**
     * Relation: one is to one
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function authorUpdate()
    {
        return $this->belongsTo('App\User', 'updated_by', 'id');
    }

    public function appointment_form()
    {
        return $this->belongsTo('App\AppointmentForm','applicant_id','applicant_id');
    }

    public function appointment_processing()
    {
        return $this->belongsTo('App\AppointmentProcessing','applicant_id','applicant_id');
    }

    public function position_description(){
      return $this->belongsTo('App\PositionDescription','applicant_id','applicant_id');   
    }
}
