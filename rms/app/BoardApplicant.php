<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class BoardApplicant extends Model
{
    use SoftDeletes;
    protected $primaryKey = 'id';
    protected $table = 'boarding_applicants';
    protected $fillable = [

		'applicant_id',
		'start_date',
		'start_time',
		'board_status',
    ];

    public function applicant(){
    	return $this->belongsTo('App\Applicant');
    }
}
