<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Job extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'jobs';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        'plantilla_item_id',
        'description',
        'education',
        'experience',
        'training',
        'eligibility',
        'duties_responsibilities',
        'key_competencies',
        'monthly_basic_salary',
        'daily_salary',
        'pera_amount',
        'clothing_amount',
        'midyear_amount',
        'yearend_amount',
        'cashgift_amount',
        'status',
        'requirements',
        'compentency_1',
        'compentency_2',
        'compentency_3',
        'compentency_4',
        'compentency_5',
        'csc_education',
        'csc_work_experience',
        'csc_eligibility',
        'csc_training',
        'other_qualification',
        'expires',
        'deadline_date',
        'publish_date',
        'publish',
        'station',
        'reporting_line',
        'publication',
        'professional_fees',
        'duration_from',
        'duration_to',
        'cos_position_title',
        'appointer_id',
        'publication_1',
        'publication_2',
        'publication_3',
        'publication_4',
        'approved_date',
        'other_specify'

    ];

    /**
     * Relation: Job has many applicants
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function applicants()
    {
        return $this->hasMany('App\Applicant');
    }

    public function plantilla_item(){
        return $this->belongsTo('App\PSIPOP','plantilla_item_id');
    }

    public function evaluations()
    {
        return $this->hasMany('App\Evaluation');
    }

    public function office(){
        return $this->belongsTo('App\Office');
    }

    public function division(){
        return $this->belongsTo('App\Division');
    }

    public function no_of_applicants()
    {
        return $this->hasMany('App\Applicant');
    }

    public function not_qualified_applicant()
    {
        return $this->hasMany('App\Applicant')->where('qualified',2);
    }

    public function qualified_applicant()
    {
        return $this->hasMany('App\Applicant')->where('qualified',1);
    }

    public function applicant_for_reference()
    {
        return $this->hasMany('App\Applicant')->where('qualified',4);
    }

    public function applicant_widthrawn()
    {
        return $this->hasMany('App\Applicant')->where('qualified',3);
    }

    public function pass_exam(){
        return $this->hasMany('App\Applicant')->where('exam_status',1);
    }

    public function pass_interview(){
        return $this->hasMany('App\Applicant')->where('interview_status',1);
    }

    public function appointer(){
        return $this->belongsTo('App\Employee','appointer_id');
    }

    public function dateFirstQualified()
    {
        return $this->hasOne('App\Applicant')->orderBy('date_qualified','asc');
    }

    public function getDate()
    {
        return $this->hasOne('App\Applicant')->with('firstExam','firstInterview','firstBChecking','firstInterviewApproved','dateAppointmentSigned','dateAssumption');
    }




}
