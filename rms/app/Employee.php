<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    protected $primaryKey = 'RefId';

    protected $table = 'employees';

    protected $fillable = [

		'LastName',
		'FirstName',
		'MiddleName',
		'EmpStatusRefId',
		'Inactive',

    ];

    /**
     * @var string $fullname concatenated first and last name
     */
    protected $fullname = '';

    public function getFullName(){
    	return $this->fullname = @$this->FirstName.' '.mb_substr(@$this->MiddleName,0,1).'. '.@$this->LastName;
    }

    public function employeeinfo()
    {
        return $this->hasOne('App\EmployeeInformation','EmployeesRefId','RefId');
    }


}
