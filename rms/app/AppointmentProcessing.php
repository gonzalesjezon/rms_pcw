<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AppointmentProcessing extends Model
{
     /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'appointment_processing';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [


            'applicant_id',
            'educ_qualification',
            'educ_remarks',
            'educ_check',
            'exp_qualification',
            'exp_remarks',
            'exp_check',
            'training_qualification',
            'training_remarks',
            'training_check',
            'eligibility_qualification',
            'eligibility_remarks',
            'eligibility_check',
            'other_qualification',
            'other_remarks',
            'other_check',
            'ra_form_33',
            'ra_employee_status',
            'ra_nature_appointment',
            'ra_appointing_authority',
            'ra_date_sign',
            'ra_date_publication',
            'ra_certification',
            'ra_pds',
            'ra_eligibility',
            'ra_position_description',
            'ar_01',
            'ar_02',
            'ar_03',
            'ar_04',
            'ar_05',
            'created_by',
            'updated_by'

    ];
    /**
     * Relation: one is to one
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function applicant()
    {
        return $this->belongsTo('App\Applicant');
    }

    /**
     * Relation: one is to one
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function author()
    {
        return $this->belongsTo('App\User', 'created_by', 'id');
    }

    /**
     * Relation: one is to one
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function authorUpdate()
    {
        return $this->belongsTo('App\User', 'updated_by', 'id');
    }
}
