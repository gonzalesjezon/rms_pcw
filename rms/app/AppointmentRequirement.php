<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class AppointmentRequirement extends Model
{
    use SoftDeletes;
	/**
     * The upload storage path for documents
     * @var string
     *
     */
    public $documentStorage = 'documents/';

    protected $primaryKey = 'id';
    protected $table = 'appointment_requirements';
    protected $fillable = [

		'applicant_id',
		'pds_path',
		'saln_path',
		'eligibility_path',
		'training_path',
		'psa_path',
		'tor_path',
		'diploma_path',
		'philhealth_path',
		'nbi_path',
		'medical_path',
		'bir_path',
		'bir2_path',
		'coe_path',
		'tin_number',
		'pagibig_number',
        'complied',

    ];

    /**
     * List of applicant documents for upload
     *
     * @var array
     */
    protected $documents = [
        'pds_path',
		'saln_path',
		'eligibility_path',
		'training_path',
		'psa_path',
		'tor_path',
		'diploma_path',
		'philhealth_path',
		'nbi_path',
		'medical_path',
		'bir_path',
		'bir2_path',
		'coe_path',
    ];

    /**
     * Saves the document file name to model object
     * deletes existing media file
     *
     * @param object $request Illuminate\Http\Request object
     *
     * @return object App\Applicant model
     */
    public function saveDocumentFileNames($request)
    {
        foreach ($this->documents as $document) {
            if ($request->hasFile($document)) {
                $file = $request->file($document);
                $md5Name = md5_file($file->getRealPath());
                $this->$document = $md5Name . '.'
                    . $file->getClientOriginalExtension();
            }
        }

        return $this;
    }

    /**
     * Uploads the document file to the storage container
     *
     * @param object $request Illuminate\Http\Request object
     *
     */
    public function uploadDocumentFiles($request)
    {
        foreach ($this->documents as $document) {
            if ($request->hasFile($document)) {
                $request->file($document)->storeAs(
                    $this->documentStorage,
                    $this->$document
                );
            }
        }
    }

    /**
     * Uploads the document file to the storage container
     *
     * @param string $fileName path and name of media file
     *
     * @return bool
     */
    public function deleteMediaFile($fileName)
    {
        if (Storage::delete($fileName)) {
            return true;
        }

        return false;
    }

    /**
     * Uploads the document file to the storage container
     *
     * @param array $oldAttributes media object attribute names
     *
     * @return int number of deleted files
     */
    public function deleteAllMediaFiles($oldAttributes = []): int
    {
        $count = 0;
        // delete images
        foreach ($this->images as $image) {
            if ($this->deleteMediaFile($this->imageStorage . $oldAttributes[$image])) {
                $count++;
            }
        }

        // delete documents
        foreach ($this->documents as $document) {
            if ($this->deleteMediaFile($this->documentStorage . $oldAttributes[$document])) {
                $count++;
            }
        }

        return $count;
    }

    public function applicant(){
    	return $this->belongsTo('App\Applicant');
    }
}
